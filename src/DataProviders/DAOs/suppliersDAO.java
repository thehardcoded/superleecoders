package DataProviders.DAOs;

import SharedClasses.DTOs.SupplierDTO;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by AmirPC on 25/04/2017.
 */
public class suppliersDAO {

    Connection con;

    public suppliersDAO(Connection con) {
        this.con = con;
        createTableIfNotExist();
    }

    private void createTableIfNotExist()
    {
        Statement createSt;
        try
        {
            createSt = con.createStatement();
            String CreationSqlSt = "CREATE TABLE IF NOT EXISTS suppliers("+
                    "Address                TEXT            PRIMARY KEY,"+
                    "Region                 INTEGER         NOT NULL,"+
                    "ContactName            TEXT            NOT NULL,"+
                    "PhoneNum               TEXT            NOT NULL"+
                    ");";
            createSt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the suppliers table");
            e.printStackTrace();
        }
    }

    public boolean insert(SupplierDTO ToAdd)
    {
        try {
            PreparedStatement pStatment = con.prepareStatement("INSERT INTO suppliers VALUES (?, ?, ?, ?)");
            pStatment.setString(1, ToAdd.getAddress());
            pStatment.setInt(2, ToAdd.getRegion());
            pStatment.setString(3, ToAdd.getContactName());
            pStatment.setString(4, ToAdd.getPhoneNum());
            pStatment.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean delete(String Address)
    {
        try {
            PreparedStatement pStatment = con.prepareStatement("DELETE FROM suppliers WHERE Address = ?");
            pStatment.setString(1, Address);
            pStatment.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean exist(String Address){
        try {
            PreparedStatement pStatment = con.prepareStatement("SELECT * FROM suppliers WHERE Address = ?");
            pStatment.setString(1, Address);
            ResultSet selectedSupplier = pStatment.executeQuery();
            if(selectedSupplier.next())
                return true;
        } catch (SQLException e) {}
        return false;
    }

    public boolean existByRegion(String Address, int Region){
        try {
            PreparedStatement pStatment = con.prepareStatement("SELECT * FROM suppliers WHERE (Address = ? AND Region = ?)");
            pStatment.setString(1, Address);
            pStatment.setInt(2, Region);
            ResultSet selectedSupplier = pStatment.executeQuery();
            if(selectedSupplier.next())
                return true;
        } catch (SQLException e) {}
        return false;
    }
/*
    public List<String> getRemovableSuppliers()
    {
        List<String> suppliersList = new LinkedList<>();
        try {
            PreparedStatement pStatment = con.prepareStatement("SELECT * FROM suppliers");
            ResultSet suppliers = pStatment.executeQuery();
            while (suppliers.next()){
                String Address = suppliers.getString(1);
                suppliersList.add(Address);
            }
        } catch (SQLException e) {}

        return suppliersList;
    }
    */

    public List<String> getByRegion(int r)
    {
        List<String> suppliersList = new LinkedList<>();
        try {
            PreparedStatement pStatment = con.prepareStatement("SELECT * FROM suppliers WHERE Region = ?");
            pStatment.setInt(1, r);
            ResultSet suppliers = pStatment.executeQuery();
            while (suppliers.next()){
                String Address = suppliers.getString(1);
                suppliersList.add(Address);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return suppliersList;
    }

}
