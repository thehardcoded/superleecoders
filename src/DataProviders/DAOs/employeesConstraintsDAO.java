package DataProviders.DAOs;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import SharedClasses.DTOs.EmployeeDTO;
import SharedClasses.DTOs.EmployeeConstraintDTO;
import SharedClasses.DTOs.ShiftInDateDTO;


/**
 * Created by Dvir Alkobi on 3/29/2017.
 */
public class employeesConstraintsDAO {
    /**
     * fields
     */
    Connection con;

    /**
     * Constructor
     * @param con
     */
    public employeesConstraintsDAO(Connection con)
    {
        this.con=con;
        createTableIfNotExist();
    }

    /**
     *  Create EmployeeConstraints Table if it haven't been created already
     */
    private void createTableIfNotExist()
    {
        Statement createSt;
        try
        {
            createSt = con.createStatement();
            String CreationSqlSt="CREATE TABLE IF NOT EXISTS EmployeesConstraints("+
                    "ID                         TEXT           ," +
                    "ShiftDate                  TEXT            NOT NULL,"+
                    "ShiftType                  INTEGER         NOT NULL," +
                    "StoreAddress               TEXT            NOT NULL,"+
                    "ConstraintDescription      TEXT            ," +
                    "PRIMARY KEY (ShiftDate,ShiftType, StoreAddress,ID),"+
                    "FOREIGN KEY(ShiftDate,ShiftType,StoreAddress) REFERENCES ShiftsInDates(ShiftDate,ShiftType,StoreAddress)," +
                    "FOREIGN KEY (ID) REFERENCES Employees(EmployeeID)"+
                    ");";
            createSt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the EmployeesConstraints table");
            e.printStackTrace();
        }
    }

    /**
     *
     * @param emp
     * @return specific employee constraint's - this main use is for presenting constraint's for employee.
     */
    public List<EmployeeConstraintDTO> getEmployeeConstraints(EmployeeDTO emp)
    {
        List<EmployeeConstraintDTO> ans = new LinkedList<>();
        try {
            PreparedStatement pStatment=con.prepareStatement("SELECT * FROM EmployeesConstraints WHERE ID=? AND ShiftDate>=?");
            pStatment.setString(1,emp.getID());
            pStatment.setString(2,new SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date()));
            ResultSet selectedRoleData=pStatment.executeQuery();
            while (selectedRoleData.next()) {
                EmployeeConstraintDTO next = new EmployeeConstraintDTO(selectedRoleData.getString("ID"),selectedRoleData.getInt("ShiftType"),selectedRoleData.getString("ShiftDate"),selectedRoleData.getString("ConstraintDescription"),selectedRoleData.getString("StoreAddress")) ;
                ans.add(next);
            }
        } catch (SQLException e) {
            ans=null;
        }
        return ans;
    }


    public boolean checkIfEmployeeConstraint(ShiftInDateDTO dto)
    {
        try {
            PreparedStatement pStatment=con.prepareStatement("SELECT * FROM ShiftsInDates WHERE ShiftType=? AND ShiftDate=? and StoreAddress=?");
            pStatment.setInt(1,dto.getShiftType());
            pStatment.setString(2,dto.getShiftDate());
            pStatment.setString(3,dto.getStore());
            ResultSet selectedRoleData=pStatment.executeQuery();
            if (selectedRoleData.next()) {
                return true;
            }
        } catch (SQLException e) {
            return false;
        }
        return false;
    }


    /**
     *
     * @param ToAdd
     * @return true if constraint of employee added to database successfully ,false otherwize.
     */
    public Boolean addEmployeeConstraint(EmployeeConstraintDTO ToAdd)
    {
        try {
            PreparedStatement pStatment=con.prepareStatement("INSERT INTO EmployeesConstraints VALUES (?,?,?,?,?)");
            pStatment.setString(1,ToAdd.getID());
            pStatment.setString(2, EmployeeDTO.shiftDateFormat(ToAdd.getDate()));
            pStatment.setInt(3, ToAdd.getShiftType());
            pStatment.setString(4,ToAdd.getStore());
            pStatment.setString(5,ToAdd.getConstraint());
            pStatment.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    /**
     * @param shiftType
     * @param shiftedDate
     * @param ID
     * @return true if employee's constraint deleted successfully,false otherwise.
     */
    public Boolean delEmployeeConstraint(int shiftType,String shiftedDate,String ID)
    {
        try {
            PreparedStatement pStatment=con.prepareStatement("DELETE FROM EmployeesConstraints WHERE ShiftType= ? AND ShiftDate=? AND ID=?");
            pStatment.setInt(1,shiftType);
            pStatment.setString(2,shiftedDate);
            pStatment.setString(3,ID);
            pStatment.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    /**
     *
     * @param EmployeeConstraint
     * @return true if employee constraint updated successfully ,false otherwise.
     */
    public Boolean updateEmployeeConstraint (String EmployeeConstraint)//can edit only Constraint for now!!!!!!
    {
        try {
            PreparedStatement pStatment=con.prepareStatement("UPDATE EmployeesConstraints Set ConstraintDescription=? WHERE ID= ? and ShiftType=? and ShiftDate=?");
            pStatment.setString(1,EmployeeConstraint);
            pStatment.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }
}
