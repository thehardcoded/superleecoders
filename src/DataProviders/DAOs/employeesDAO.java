package DataProviders.DAOs;

import SharedClasses.DTOs.EmployeeDTO;

import java.sql.*;


/**
 * Created by SHLOMI PAKADO on 3/15/2017.
 */
public class employeesDAO {
    /**
     * Fields
     */
    Connection con;

    /**
     * Constructor
     * @param con
     */
    public employeesDAO(Connection con)
    {
        this.con=con;
        createTableIfNotExist();
    }

    public void close() {
        try {
            con.close();
        } catch (SQLException ignored) {
        }
    }

    /**
     * Create EmployeeCs Table if it haven't been created already
     */
    private void createTableIfNotExist() {
        Statement createSt;
        try {
            createSt = con.createStatement();
            String CreationSqlSt = "CREATE TABLE IF NOT EXISTS Employees (" +
                    "EmployeeID                     TEXT                PRIMARY KEY," +
                    "FirstName                      TEXT                NOT NULL," +
                    "LastName                       TEXT                NOT NULL," +
                    "Role                           TEXT                NOT NULL," +
                    "Salary                         INTEGER             CHECK(salary >= 0)," +
                    "DateOfTransaction              TEXT                NOT NULL," +
                    "EmploymentTerminationDate      TEXT                DEFAULT NULL," +
                    "TermsOfTheDeal                 TEXT                DEFAULT NULL," +
                    "StoreAddress                   TEXT                DEFAULT NULL," +
                    "BankNumber                     INTEGER             NOT NULL," +
                    "BranchNumber                   INTEGER             NOT NULL," +
                    "BankAccount                    INTEGER             NOT NULL," +
                    "FOREIGN KEY (StoreAddress) REFERENCES stores    (Address)," +
                    "FOREIGN KEY (Role) REFERENCES Roles    (Role)" +
                    ")";
            createSt.executeUpdate(CreationSqlSt);
            insertAdminTupleIfNotExist();
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the Employees table");
            e.printStackTrace();
        }
    }

    /**
     * Initializing necessary Employee users for the system . (default users-admin,etc...)
     */
    private void insertAdminTupleIfNotExist()
    {
        Statement createSt = null;
        try {
            createSt = con.createStatement();
            String CreationSqlSt;
            if(getEmployeeById("111111111")==null){
                CreationSqlSt ="INSERT INTO  Employees " +
                        "SELECT '111111111','peopleMan','ofPeople','ManPower Manager',1000,'2017-1-1',NULL ,'http://www.workrights.co.il/%D7%AA%D7%A0%D7%90%D7%99_%D7%94%D7%A2%D7%A1%D7%A7%D7%94','General',1,1,1 " +
                        "WHERE NOT EXISTS (SELECT * FROM Employees WHERE EmployeeID = '111111111');";
                createSt.executeUpdate(CreationSqlSt);
            }
            if(getEmployeeById("222222222")==null){
                CreationSqlSt ="INSERT INTO  Employees " +
                        "SELECT '222222222','truckMan','ofTrucks','Logistic Manager',1000,'2017-1-1',NULL ,'http://www.workrights.co.il/%D7%AA%D7%A0%D7%90%D7%99_%D7%94%D7%A2%D7%A1%D7%A7%D7%94','General',2,2,2 " +
                        "WHERE NOT EXISTS (SELECT * FROM Employees WHERE EmployeeID = '222222222');";
                createSt.executeUpdate(CreationSqlSt);
            }
            if(getEmployeeById("333333333")==null){
                CreationSqlSt ="INSERT INTO  Employees " +
                        "SELECT '333333333','storeMan','ofStores','Administrative Manager',1000,'2017-1-1',NULL ,'http://www.workrights.co.il/%D7%AA%D7%A0%D7%90%D7%99_%D7%94%D7%A2%D7%A1%D7%A7%D7%94','General',3,3,3 " +
                        "WHERE NOT EXISTS (SELECT * FROM Employees WHERE EmployeeID = '333333333');";
                createSt.executeUpdate(CreationSqlSt);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param ID
     * @return Employee DTO object from DB of given ID .
     */
    public EmployeeDTO getEmployeeById(String ID) {
        EmployeeDTO ans = null;
        try {
            PreparedStatement pStatment = con.prepareStatement("SELECT * FROM Employees WHERE EmployeeID= ?");
            pStatment.setString(1, ID);
            ResultSet selectedEmployeeData = pStatment.executeQuery();
            if (selectedEmployeeData.next())
                ans = new EmployeeDTO(selectedEmployeeData.getString(1), selectedEmployeeData.getString(2), selectedEmployeeData.getString(3),
                        selectedEmployeeData.getString(4), selectedEmployeeData.getInt(5), selectedEmployeeData.getString(6),
                        selectedEmployeeData.getString(7), selectedEmployeeData.getString(8), selectedEmployeeData.getString(9), selectedEmployeeData.getInt(10)
                        , selectedEmployeeData.getInt(11), selectedEmployeeData.getInt(12));
        } catch (SQLException e) {
            ans = null;
        }
        return ans;
    }


//    public List<String> getIdOfSevenYearsRetired() {
//        List<String> ans = new LinkedList();
//        try {
//            PreparedStatement pStatment = con.prepareStatement("SELECT * FROM Employees WHERE (((julianday('now')) - (julianday(EmploymentTerminationDate))))<0");
//           // pStatment.setString(1,new SimpleDateFormat("YYYY-MM-DD").format(new java.util.Date()));
//            ResultSet selectedRoleData = pStatment.executeQuery();
//            while (selectedRoleData.next()) {
//                ans.add(selectedRoleData.getString("EmployeeID"));
//            }
//        } catch (SQLException e) {
//            ans = null;
//        }
//        return ans;
//    }


    /**
     * @param ToAdd
     * @return true if Employee was Added successfully ,false otherwise.
     */
    public Boolean AddEmployee(EmployeeDTO ToAdd) {
        try {
            String terms="http://www.workrights.co.il/%D7%AA%D7%A0%D7%90%D7%99_%D7%94%D7%A2%D7%A1%D7%A7%D7%94";
            PreparedStatement pStatment = con.prepareStatement("INSERT INTO Employees VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
            pStatment.setString(1, ToAdd.getID());
            pStatment.setString(2, ToAdd.getFirstName());
            pStatment.setString(3, ToAdd.getLastName());
            pStatment.setString(4, ToAdd.getRole());
            pStatment.setInt(5, ToAdd.getSalary());
            pStatment.setString(6, ToAdd.getDateOfTransaction());
            pStatment.setString(7, ToAdd.getEmploymentTerminationDate());
           // pStatment.setString(8, ToAdd.getTermsOfTheDeal());
            pStatment.setString(8,terms);
            pStatment.setString(9,ToAdd.getStore());
            pStatment.setInt(10, ToAdd.getBankNumber());
            pStatment.setInt(11, ToAdd.getBranchNumber());
            pStatment.setInt(12, ToAdd.getBankAccount());
            pStatment.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    /**
     * @param ID
     * @return true if Employee deleted successfully,false otherwise.
     */
    public Boolean DeleteEmployee(String ID) {
        try {
            PreparedStatement pStatment = con.prepareStatement("DELETE FROM Employees WHERE EmployeeID= ?");
            pStatment.setString(1, ID);
            pStatment.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    /**
     * @param employeesOldId
     * @param emp
     * @return true if employee details was update successfully ,false otherwise.
     */
    public Boolean updateEmployee(String employeesOldId, EmployeeDTO emp) {
        try {
            PreparedStatement pStatment = con.prepareStatement("UPDATE Employees Set EmployeeID=?,FirstName=?,LastName=?,Role=?,Salary=?,DateOfTransaction=?,EmploymentTerminationDate=?,TermsOfTheDeal=?,StoreAddress=?,BankNumber=?,BranchNumber=?,BankAccount=?  WHERE EmployeeID= ?");
            pStatment.setString(1, emp.getID());
            pStatment.setString(2, emp.getFirstName());
            pStatment.setString(3, emp.getLastName());
            pStatment.setString(4, emp.getRole());
            pStatment.setInt(5, emp.getSalary());
            pStatment.setString(6, emp.getDateOfTransaction());
            pStatment.setString(7, emp.getEmploymentTerminationDate());
            pStatment.setString(8, emp.getTermsOfTheDeal());
            pStatment.setString(9, emp.getStore());
            pStatment.setInt(10, emp.getBankNumber());
            pStatment.setInt(11, emp.getBranchNumber());
            pStatment.setInt(12, emp.getBankAccount());
            pStatment.setString(13,employeesOldId);
            pStatment.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
}
