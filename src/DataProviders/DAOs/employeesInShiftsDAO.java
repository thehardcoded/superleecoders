package DataProviders.DAOs;

import SharedClasses.DTOs.EmployeeDTO;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by AmirPC on 21/04/2017.
 */
public class employeesInShiftsDAO {
    /**
     * Fields
     */
    Connection con;

    /**
     * Constructors
     * @param con
     */
    public employeesInShiftsDAO(Connection con) {
        this.con = con;
        createTableIfNotExist();
    }
    /**
     * Create EmployeesInShifts Table if it haven't been created already
     */
    private void createTableIfNotExist()
    {
        Statement createSt;
        try
        {
            createSt = con.createStatement();
            String CreationSqlSt =  "CREATE TABLE IF NOT EXISTS EmployeesInShifts ("+
                    "ShiftDate              TEXT            NOT NULL,"+
                    "ShiftType              INTEGER         NOT NULL,"+
                    "StoreAddress           TEXT            NOT NULL,"+
                    "ID                     TEXT            NOT NULL,"+
                    "RoleInShift            TEXT            NOT NULL,"+
                    "PRIMARY KEY (ShiftDate,ShiftType, StoreAddress,ID),"+
                    "FOREIGN KEY(ShiftDate,ShiftType,StoreAddress) REFERENCES ShiftsInDates(ShiftDate,ShiftType,StoreAddress)," +
                    "FOREIGN KEY (ID) REFERENCES Employees(EmployeeID),"+
                    "FOREIGN KEY (RoleInShift) REFERENCES Roles(Role)"+
                    ");";
            createSt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the EmployeesInShifts table");
            e.printStackTrace();
        }
    }

    /**
     * @param shiftType
     * @param shiftedDate
     * @param IdPostions
     * @param store
     * @return true if EmployeeInShift added successfully ,false otherwise.
     */
    public boolean insertToEmployeesInShift(int shiftType,String shiftedDate,ArrayList<String []> IdPostions,String store) {
        try {
            PreparedStatement pStatment=con.prepareStatement("INSERT INTO EmployeesInShifts (ShiftDate,ShiftType,StoreAddress,ID, RoleInShift) VALUES (?,?,?,?,?);");
            for(String [] Details : IdPostions){
                pStatment.setInt(2,shiftType);
                pStatment.setString(1,shiftedDate);
                pStatment.setString(3,store);
                pStatment.setString(4,Details[0]);
                pStatment.setString(5,Details[1]);
                pStatment.addBatch();
            }
            pStatment.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * @param date
     * @param shiftType
     * @param store
     * @return return true if shift in given date of certain store is scheduled ,false otherwise.
     */
    public boolean shiftWasScheduled(String date, int shiftType,String store) {
        try {
            PreparedStatement pStatment = con.prepareStatement("SELECT * FROM EmployeesInShifts WHERE EmployeesInShifts.ShiftDate=? AND EmployeesInShifts.ShiftType=? AND EmployeesInShifts.StoreAddress=?;");
            pStatment.setString(1,date);
            pStatment.setInt(2,shiftType);
            pStatment.setString(3,store);
            ResultSet selectedEmployeeData = pStatment.executeQuery();
            if (selectedEmployeeData.next())
                return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * @param DateOfTransDocToRemove
     * @param driverIDToRemove
     * @param shiftType
     * @return return true if a shift with this parameters was deleted.
     */
    public boolean deleteEmployeeShiftByDriver(String DateOfTransDocToRemove, String driverIDToRemove, int shiftType) {
        try {
            PreparedStatement pStatment = con.prepareStatement("DELETE FROM EmployeesInShifts WHERE ShiftDate = ? AND ID = ? AND ShiftType = ?;");
            pStatment.setString(1,DateOfTransDocToRemove);
            pStatment.setString(2,driverIDToRemove);
            pStatment.setInt(3,shiftType);
            pStatment.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

	public boolean ValidateStorekeeperWorksOnShift(String date, int shiftType, String store)
    {
		try {
			PreparedStatement pStatment = con.prepareStatement("SELECT * FROM EmployeesInShifts WHERE EmployeesInShifts.ShiftDate=? AND EmployeesInShifts.ShiftType=? AND EmployeesInShifts.StoreAddress=? AND RoleInShift=?;");
			pStatment.setString(1,date);
			pStatment.setInt(2,shiftType);
			pStatment.setString(3,store);
            pStatment.setString(4,"Storekeeper");
			ResultSet selectedEmployeeData = pStatment.executeQuery();
			if (selectedEmployeeData.next())
				return true;
		} catch (SQLException e) {
            return false;
		}
		return false;
	}

/*    *//**

    public void DeleteScheduledShift(String s, int type,String store) {
        try {
            PreparedStatement pStatment = con.prepareStatement("DELETE FROM EmployeesInShifts WHERE ShiftDate= ? AND ShiftType=? AND StoreAddress=?");
            pStatment.setInt(2, type);
            pStatment.setString(1, s);
            pStatment.setString(3, store);
            pStatment.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }//////////////////////////////////////////////////////////////////////////////////////////*/

}
