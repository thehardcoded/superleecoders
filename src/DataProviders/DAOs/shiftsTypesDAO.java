package DataProviders.DAOs;

import java.sql.*;

/**
 * Created by AmirPC on 21/04/2017.
 */
public class shiftsTypesDAO {
    /**
     * Fields
     */
    Connection con;

    /**
     * Constructors
     * @param con
     */
    public shiftsTypesDAO(Connection con)
    {
        this.con=con;
        createTableIfNotExist();
        InsertShiftsType();
    }
    /**
     * Create ShiftType Table if it haven't been created already
     */
    private void createTableIfNotExist()
    {
        Statement createSt;
        try
        {
            createSt = con.createStatement();
            String CreationSqlSt = "CREATE TABLE IF NOT EXISTS ShiftsTypes ("+
                    "ShiftType              INTEGER         PRIMARY KEY"+
                    ");";
            createSt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the ShiftsTypes table");
            e.printStackTrace();
        }
    }

    /**
     * Inserting shift type to the DB.
     */
    private void InsertShiftsType()
    {
        Statement createSt;
        try {
            createSt = con.createStatement();
            String CreationSqlSt ="INSERT INTO  ShiftsTypes \n" +
                    "SELECT 1 " +
                    "WHERE NOT EXISTS (SELECT * FROM ShiftsTypes WHERE ShiftType = 1);"
                    +"INSERT INTO  ShiftsTypes \n" +
                    "SELECT 2 " +
                    "WHERE NOT EXISTS (SELECT * FROM ShiftsTypes WHERE ShiftType = 2);";
            createSt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
        }
    }


}
