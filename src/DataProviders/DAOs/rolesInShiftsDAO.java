package DataProviders.DAOs;

import java.sql.*;
import java.util.ArrayList;
import SharedClasses.DTOs.RoleInShiftDTO;


/**
 * Created by Dvir Alkobi on 3/29/2017.
 */
public class rolesInShiftsDAO {
    /**
     * Fields
     */
    Connection con;

    /**
     * Constructors
     * @param con
     */
    public rolesInShiftsDAO(Connection con)
    {
        this.con=con;
        createTableIfNotExist();
    }

    /**
     * Create RolesInShifts Table if it haven't been created already
     */
    private void createTableIfNotExist()
    {
        Statement createSt;
        try
        {
            createSt = con.createStatement();
            String CreationSqlSt = "CREATE TABLE IF NOT EXISTS RolesInShifts (" +
                    "Role                           TEXT                    NOT NULL," +
                    "ShiftType                      INTEGER                 NOT NULL," +
                    "Amount                         INTEGER                 DEFAULT 1," +
                    "StoreAddress                   TEXT                    NOT NULL, " +
                    "PRIMARY KEY (ShiftType, Role,StoreAddress)," +
                    "FOREIGN KEY(StoreAddress) REFERENCES stores(address)," +
                    "FOREIGN KEY(ShiftType) REFERENCES ShiftsTypes(ShiftType)," +
                    "FOREIGN KEY(Role) REFERENCES Roles(Role)" +
                    ");";
            createSt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the RolesInShifts table");
            e.printStackTrace();
        }
    }

    /**
     * @param shift
     * @param StoreAddress
     * @return Arraylist of RoleInShiftDTOs from DB.
     */
    public ArrayList<RoleInShiftDTO> getRolesInShift(int shift,String StoreAddress) //ToDo:MabyChange String to DTO
    {
        ArrayList<RoleInShiftDTO> ans =null;
        try {
            PreparedStatement pStatment=con.prepareStatement("SELECT Role,Amount FROM RolesInShifts WHERE ShiftType= ? AND StoreAddress=?");
            pStatment.setInt(1,shift);
            pStatment.setString(2,StoreAddress);
            ResultSet selectedRoleData=pStatment.executeQuery();
            ans=new ArrayList<>();
            while (selectedRoleData.next()) { //check if valid
                String column1 = selectedRoleData.getString("Role");
                int column2 = selectedRoleData.getInt("Amount");
                ans.add(new RoleInShiftDTO( column1, shift,column2,StoreAddress));
            }
        } catch (SQLException e) {
            ans=null;
        }
        return ans;
    }

    /************************************************************************
     *
     * Delete Erelevant code here.
     *
     ***********************************************************************/
//    public Boolean addRoleInShift(RoleInShiftDTO ToAdd)
//    {
//        try {
//            PreparedStatement pStatment=con.prepareStatement("INSERT INTO RolesInShifts VALUES (?,?,?)");
//            pStatment.setString(1,ToAdd.getRole());
//            pStatment.setInt(2,ToAdd.getShiftType());
//            pStatment.setInt(3,ToAdd.getAmount());
//            pStatment.executeUpdate();
//            return true;
//        } catch (SQLException e) {
//            return false;
//        }
//    }
//
//    public Boolean delRoleInShift(int shiftType,String Role)
//    {
//        try {
//            PreparedStatement pStatment=con.prepareStatement("DELETE FROM RolesInShifts WHERE Role= ? AND shiftType=?");
//            pStatment.setString(1,Role);
//            pStatment.setInt(2,shiftType);

    public Boolean addRoleInShift(RoleInShiftDTO ToAdd)
    {
        try {
            PreparedStatement pStatment=con.prepareStatement("INSERT INTO RolesInShifts VALUES (?,?,?,?)");
            pStatment.setString(1,ToAdd.getRole());
            pStatment.setInt(2,ToAdd.getShiftType());
            pStatment.setInt(3,ToAdd.getAmount());
            pStatment.setString(4,ToAdd.getStoreAddress());
            pStatment.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    /**
     * @param roles
     * @param shiftType
     * @return true if shift standard was setting in DB successfully ,false otherwise.
     */
    public boolean setShiftStandardList(ArrayList<RoleInShiftDTO> roles, int shiftType) {
        try {
            PreparedStatement pStatment=con.prepareStatement("DELETE FROM RolesInShifts WHERE ShiftType=? AND StoreAddress=?");
            pStatment.setInt(1,shiftType);
            pStatment.setString(2,roles.get(0).getStoreAddress());
            pStatment.executeUpdate();
            pStatment=con.prepareStatement("INSERT INTO RolesInShifts (Role, ShiftType, Amount,StoreAddress) VALUES (?,?,?,?);");
            for(RoleInShiftDTO role : roles){
                pStatment.setString(1,role.getRole());
                pStatment.setInt(2, role.getShiftType());
                pStatment.setInt(3, role.getAmount());
                pStatment.setString(4,role.getStoreAddress());
                pStatment.addBatch();
            }
            pStatment.executeBatch();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
}
