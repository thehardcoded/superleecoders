package DataProviders.DAOs;

import java.sql.*;
import SharedClasses.DTOs.TruckDTO;

/**
 * Created by AmirPC on 28/03/2017.
 */
public class trucksDAO {

    Connection con;

    public trucksDAO(Connection con) {
        this.con = con;
        createTableIfNotExist();
    }

    private void createTableIfNotExist()
    {
        Statement createSt;
        try
        {
            createSt = con.createStatement();
            String CreationSqlSt = "CREATE TABLE IF NOT EXISTS trucks ("+
                    "ID                     TEXT            PRIMARY KEY,"+
                    "Model                  INTEGER         NOT NULL        CHECK (Model>=1 AND Model<=5),"+
                    "FOREIGN KEY (Model)    REFERENCES truckModels(Model)"+
                    ");";
            createSt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the trucks table");
            e.printStackTrace();
        }
    }

    public boolean insert(TruckDTO ToAdd)
    {
        try {
            PreparedStatement pStatment = con.prepareStatement("INSERT INTO trucks VALUES (?, ?)");
            pStatment.setString(1,ToAdd.getID());
            pStatment.setInt(2,ToAdd.getModel());
            pStatment.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean delete(String ID)
    {
        try {
            PreparedStatement pStatment = con.prepareStatement("DELETE FROM trucks WHERE ID = ?");
            pStatment.setString(1, ID);
            pStatment.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean exist(String ID){
        try {
            PreparedStatement pStatment = con.prepareStatement("SELECT * FROM trucks WHERE ID = ?");
            pStatment.setString(1, ID);
            ResultSet selectedTruck = pStatment.executeQuery();
            if(selectedTruck.next())
                return true;
        } catch (SQLException e) {}
        return false;
    }

    public int getModel(String newTruckID) {
        try {
            PreparedStatement pStatement = con.prepareStatement("SELECT Model FROM trucks WHERE ID = ?");
            pStatement.setString(1, newTruckID);
            ResultSet model = pStatement.executeQuery();
            if(model.next())
                return model.getInt(1);
        } catch (SQLException e) {}
        return 0;
    }
}
