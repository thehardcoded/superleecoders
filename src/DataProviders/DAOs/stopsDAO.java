package DataProviders.DAOs;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import SharedClasses.DTOs.StopDTO;


/**
 * Created by AmirPC on 29/03/2017.
 */
public class stopsDAO
{
	Connection con;

	public stopsDAO(Connection con) {
		this.con = con;
		createTableIfNotExist();
	}

	private void createTableIfNotExist()
	{
		Statement createSt;
		try
		{
			createSt = con.createStatement();
			String CreationSqlSt = "CREATE TABLE IF NOT EXISTS stops("+
					"ProductsDocNo          TEXT            NOT NULL,"+
					"TransportationDocNo    TEXT            NOT NULL,"+
					"destination            TEXT            NOT NULL,"+
					"PRIMARY KEY (ProductsDocNo, TransportationDocNo, destination),"+
					"FOREIGN KEY (destination)              REFERENCES stores(address),"+
					"FOREIGN KEY (TransportationDocNo)      REFERENCES transportationDocs(ID)"+
					");";
			createSt.executeUpdate(CreationSqlSt);
		} catch (SQLException e) {
			System.out.println("there was a problem in creating the stops table");
			e.printStackTrace();
		}
	}

	public boolean insert(StopDTO ToAdd)
	{
		try {
			PreparedStatement pStatment = con.prepareStatement("INSERT INTO stops VALUES (?, ?, ?)");
			pStatment.setInt(2, ToAdd.getTransportationDocNo());
			pStatment.setString(3, ToAdd.getDestination());
			pStatment.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean removeSpecificStop(int transportationDocNo, String destination)
	{
		try {
			PreparedStatement pStatment = con.prepareStatement("DELETE FROM stops WHERE (transportationDocNo = ? AND destination = ?)");
			pStatment.setInt(1, transportationDocNo);
			pStatment.setString(2, destination);
			pStatment.executeUpdate();
			return true;
		} catch (SQLException e) {
			return false;
		}
	}


	public boolean deleteByTransportationDocNo(int transportationDocNo)
	{
		try {
			PreparedStatement pStatment = con.prepareStatement("DELETE FROM stops WHERE transportationDocNo = ?");
			pStatment.setInt(1, transportationDocNo);
			pStatment.executeUpdate();
			return true;
		} catch (SQLException e){
			return false;
		}
	}//

	public boolean exist(String productsDocNo){
		try {
			PreparedStatement pStatment = con.prepareStatement("SELECT * FROM stops WHERE ProductsDocNo = ?");
			pStatment.setString(1, productsDocNo);
			ResultSet selectedTD = pStatment.executeQuery();
			if(selectedTD.next())
				return true;
		} catch (SQLException e) {}
		return false;
	}

	public boolean transDestExist(String transDocID, String dest){
		try {
			PreparedStatement pStatment = con.prepareStatement("SELECT * FROM stops WHERE TransportationDocNo = ? AND destination = ?");
			pStatment.setString(1, transDocID);
			pStatment.setString(2, dest);
			ResultSet selectedTD = pStatment.executeQuery();
			if(selectedTD.next())
				return true;
		} catch (SQLException e) {}
		return false;
	}

	public List<String> getStops(int transDocId)
	{
		ArrayList<String> ans=new ArrayList<String>();
		try {
			PreparedStatement pStatment = con.prepareStatement("SELECT destination FROM stops WHERE transportationDocNo = ?");
			pStatment.setInt(1,transDocId);
			ResultSet selectedStops = pStatment.executeQuery();
			while (selectedStops.next()) {
				String address = selectedStops.getString(1);
				ans.add(address);
			}
		} catch (SQLException e) {
			ans=null;
		}
		return ans;

	}

}
