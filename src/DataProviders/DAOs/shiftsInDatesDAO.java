package DataProviders.DAOs;

import SharedClasses.DTOs.EmployeeDTO;
import java.sql.*;

/**
 * Created by AmirPC on 21/04/2017.
 */
public class shiftsInDatesDAO {
    /**
     * Fields
     */
    Connection con;

    /**
     * Constructors
     * @param con
     */
    public shiftsInDatesDAO(Connection con)
    {
        this.con=con;
        createTableIfNotExist();
    }

    public void close() {
        try {
            con.close();
        } catch (SQLException ignored) {
        }
    }

    /**
     * Create ShiftsInDates Table if it haven't been created already
     */
    private void createTableIfNotExist()
    {
        Statement createSt;
        try
        {
            createSt = con.createStatement();
            String CreationSqlSt = "CREATE TABLE IF NOT EXISTS ShiftsInDates ("+
                    "ShiftDate              TEXT            NOT NULL,"+
                    "ShiftType              INTEGER         NOT NULL," +
                    "StoreAddress           TEXT            NOT NULL,"+
                    "PRIMARY KEY (ShiftType, ShiftDate,StoreAddress),"+
                    "FOREIGN KEY(StoreAddress) REFERENCES stores(address)," +
                    "FOREIGN KEY (ShiftType) REFERENCES ShiftsTypes(ShiftType)"+
                    ");";
            createSt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the ShiftsInDates table");
            e.printStackTrace();
        }
    }

    /**
     * @param shiftDate
     * @param shiftType
     * @param store
     * @DO- adding new shift represented by date ,type and the store the shift was scheduled in.
     */
    public void AddShiftDate(String shiftDate, int shiftType,String store) {
        try {
            PreparedStatement pStatment = con.prepareStatement("INSERT INTO ShiftsInDates VALUES (?,?,?)");
            pStatment.setString(1,shiftDate);
            pStatment.setInt(2,shiftType);
            pStatment.setString(3,store);
            pStatment.executeUpdate();
        } catch (SQLException e) {

        }
    }

    public void deleteShiftDate(String shiftDate, int shiftType,String store) {
        try {
            PreparedStatement pStatment = con.prepareStatement("DELETE FROM ShiftsInDates WHERE " +
                            "ShiftDate='" + shiftDate + "' AND ShiftType=" + shiftType + " AND ShiftsInDates.StoreAddress='" + store + "'");
            pStatment.executeUpdate();
        } catch (SQLException e) {

        }
    }

    /**
     *
     * @param date
     * @param shiftType
     * @param store
     * @return true if shift was scheduled in the given store on the given date,false otherwise.
     */
    public boolean checkIfShiftDated(String date,int shiftType,String store) //checks if the shift was given date in the dateBase
    {
        EmployeeDTO ans = null;
        try {
            PreparedStatement pStatment = con.prepareStatement("SELECT * From ShiftsInDates WHERE ShiftDate=? AND ShiftType=? AND StoreAddress=?");
            pStatment.setString(1,date);
            pStatment.setInt(2,shiftType);
            pStatment.setString(3,store);
            ResultSet selectedEmployeeData = pStatment.executeQuery();
            if (!selectedEmployeeData.next())
                return  false;
            return  true;
        } catch (SQLException e) {
            ans = null;
        }
        return true;
    }

}
