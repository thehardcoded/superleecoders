package DataProviders.DAOs;
import SharedClasses.DTOs.EmployeeDTO;

import java.sql.*;


/**
 * Created by AmirPC on 29/03/2017.
 */
public class truckModelsDAO {

    Connection con;

    public truckModelsDAO(Connection con) {
        this.con = con;
        createTableIfNotExist();
    }

    private void createTableIfNotExist()
    {
        Statement createSt;
        try
        {
            createSt = con.createStatement();
            String CreationSqlSt = "CREATE TABLE IF NOT EXISTS truckModels ("+
                    "Model                  INTEGER         PRIMARY KEY,"+
                    "TruckWeight            INTEGER         NOT NULL        CHECK (TruckWeight>0),"+
                    "MaxWeight              INTEGER         NOT NULL        CHECK (MaxWeight>0) "+
                    ");";
            createSt.executeUpdate(CreationSqlSt);
            insertWightsIfNotExist();
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the truckModels table");
            e.printStackTrace();
        }
    }

    public boolean getEmployeeById(int model) {
        EmployeeDTO ans = null;
        try {
            PreparedStatement pStatment = con.prepareStatement("SELECT * FROM truckModels WHERE Model= ?");
            pStatment.setInt(1, model);
            ResultSet selectedEmployeeData = pStatment.executeQuery();
            if (selectedEmployeeData.next())
               return true;
        } catch (SQLException e) {
            ans = null;
        }
        return false;
    }

    private void insertWightsIfNotExist()
    {
        Statement createSt = null;
        try {
            createSt = con.createStatement();
            String CreationSqlSt;
            if(!getEmployeeById(1))
            {
            CreationSqlSt ="INSERT INTO  truckModels " +
                        "SELECT 1,'10000','15000'" +
                        "WHERE NOT EXISTS (SELECT * FROM truckModels WHERE Model = 't1');";
                createSt.executeUpdate(CreationSqlSt);///need to check if this is necessary
            }
            if(!getEmployeeById(2)) {
                CreationSqlSt = "INSERT INTO  truckModels " +
                        "SELECT 2,'15000','25000'" +
                        "WHERE NOT EXISTS (SELECT * FROM truckModels WHERE Model = 't2');";
                createSt.executeUpdate(CreationSqlSt);///need to check if this is necessary
            }
            if(!getEmployeeById(3)) {
                CreationSqlSt = "INSERT INTO  truckModels " +
                        "SELECT 3,'20000','35000'" +
                        "WHERE NOT EXISTS (SELECT * FROM truckModels WHERE Model = 't3');";
                createSt.executeUpdate(CreationSqlSt);///need to check if this is necessary
            }
            if(!getEmployeeById(4)) {
                CreationSqlSt = "INSERT INTO  truckModels " +
                        "SELECT 4,'25000','45000'" +
                        "WHERE NOT EXISTS (SELECT * FROM truckModels WHERE Model = 't4');";
                createSt.executeUpdate(CreationSqlSt);///need to check if this is necessary
            }
            if(!getEmployeeById(5)){
                CreationSqlSt = "INSERT INTO  truckModels " +
                        "SELECT 5,'30000','60000'" +
                        "WHERE NOT EXISTS (SELECT * FROM truckModels WHERE Model = 't5');";
                createSt.executeUpdate(CreationSqlSt);///need to check if this is necessary
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
