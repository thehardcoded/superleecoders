package DataProviders.DAOs;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;


/**
 * Created by AmirPC on 21/04/2017.
 */
public class rolesDAO {
    /**
     * Field
     */
    Connection con;

    /**
     * Constructores
     * @param con
     */
    public rolesDAO(Connection con)
    {
        this.con=con;
        createTableIfNotExist();
        AddRoleIfNotExist("ManPower Manager", "Shift Manager", "Logistic Manager" ,"Driver", "Chashier", "Storekeeper", "Administrative Manager","Store Manager");
    }

    /**
     * Create Roles Table if it haven't been created already.
     */
    private void createTableIfNotExist()
    {
        Statement createSt;
        try
        {
            createSt = con.createStatement();
            String CreationSqlSt = "CREATE TABLE IF NOT EXISTS Roles ("+
                    "Role                   TEXT            PRIMARY KEY"+
                    ");";
            createSt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the Roles table");
            e.printStackTrace();
        }
    }

    /**
     * Add list of roles to DB if it's not exist already.
     * @param roles
     */
    private void AddRoleIfNotExist(String ...roles)
    {
        Statement createSt = null;
        for (String role : roles) {
            try {
                createSt = con.createStatement();
                String CreationSqlSt ="INSERT INTO  Roles \n" +
                        "SELECT '"+role+"' " +
                        "WHERE NOT EXISTS (SELECT * FROM Roles WHERE Role = '"+role+"');";
                createSt.executeUpdate(CreationSqlSt);
            } catch (SQLException e) {

            }
        }
    }

    /**
     * @return returning list of all roles in the System of LEE-SUPER.
     */
    public List<String> getValidRoles()
    {
        List<String> ans = new LinkedList<String>();
        try {
            PreparedStatement pStatment=con.prepareStatement("SELECT * FROM Roles");
            ResultSet selectedRoleData=pStatment.executeQuery();
            while (selectedRoleData.next()) {
                String column1 = selectedRoleData.getString("Role");
                ans.add(column1);
            }
        } catch (SQLException e) {
            ans=null;
        }
        return ans;
    }

    /**
     * @param roleType
     * @return true if role was added successfully to the DB,False otherwise.
     */
    public boolean insertNewRole(String roleType) {
        try {
            PreparedStatement pStatment=con.prepareStatement("INSERT INTO Roles VALUES (?)");
            pStatment.setString(1,roleType);
            pStatment.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

}
