package DataProviders.DAOs;

import java.sql.*;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import SharedClasses.DTOs.EmployeeDTO;
import SharedClasses.DTOs.ShiftInDateDTO;
import SharedClasses.DTOs.TransportationDocDTO;

/**
 * Created by AmirPC on 28/03/2017.
 */
public class transportationDocsDAO {

    Connection con;

    public transportationDocsDAO(Connection con) {
        this.con = con;
        createTableIfNotExist();
    }

    private void createTableIfNotExist()
    {
        Statement createSt;
        try
        {
            createSt = con.createStatement();
            String CreationSqlSt = "CREATE TABLE IF NOT EXISTS transportationDocs("+
                    "ID                     TEXT            PRIMARY KEY,"+
                    "Date                   TEXT            NOT NULL,"+
                    "Shift                  TEXT            NOT NULL,"+
                    "TruckID                TEXT            NOT NULL,"+
                    "DriverID               TEXT            NOT NULL,"+
                    "Supplier               TEXT            NOT NULL,"+
                    "Origin                 TEXT            NOT NULL,"+
                    "WeightAtLeave          INTEGER         DEFAULT NULL,"+
                    "Region                 INTEGER         NOT NULL,"+
                    "FOREIGN KEY (TruckID)  REFERENCES trucks(ID),"+
                    "FOREIGN KEY (DriverID) REFERENCES drivers(ID),"+
                    "FOREIGN KEY (supplier) REFERENCES suppliers(Address),"+
                    "FOREIGN KEY (Shift)    REFERENCES ShiftsTypes(ShiftType)"+
                    ");";
            createSt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the transportationDocs table");
            e.printStackTrace();
        }
    }

    public boolean insert(TransportationDocDTO ToAdd)
    {
        try {
            PreparedStatement pStatment = con.prepareStatement("INSERT INTO transportationDocs VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
            pStatment.setInt(1,ToAdd.getID());
            pStatment.setString(2,ToAdd.getDate());
            pStatment.setInt(3,ToAdd.getShift());
            pStatment.setString(4,ToAdd.getTruckID());
            pStatment.setString(5,ToAdd.getDriverID());
            pStatment.setString(6,ToAdd.getSupplier());
            pStatment.setInt(7,ToAdd.getWeightAtLeave());
            pStatment.setInt(8,ToAdd.getRegion());
            pStatment.setInt(9,0);
            pStatment.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean exist(int ID){
        try {
            PreparedStatement pStatment = con.prepareStatement("SELECT * FROM transportationDocs WHERE ID = ?");
            pStatment.setInt(1, ID);
            ResultSet selectedTD = pStatment.executeQuery();
            if(selectedTD.next())
                return true;
        } catch (SQLException e) {}
        return false;
    }
    public boolean existByShiftInDate(ShiftInDateDTO sid){
        try {
            PreparedStatement pStatment = con.prepareStatement("SELECT * FROM transportationDocs WHERE transportationDocs.shift=? AND transportationDocs.date= ?");
            pStatment.setInt(1, sid.getShiftType());
            pStatment.setString(2, EmployeeDTO.shiftDateFormat(sid.getShiftDate()));
            ResultSet selectedTD = pStatment.executeQuery();
            if(selectedTD.next())
                return true;
        } catch (SQLException e) {}
        return false;
    }

    public TransportationDocDTO getTransportDoc(int docNum)
    {
        TransportationDocDTO ans = null;
        try {
            PreparedStatement pStatement = con.prepareStatement("SELECT * FROM transportationDocs WHERE ID= ?");
            pStatement.setInt(1,docNum);
            ResultSet selectedDocument = pStatement.executeQuery();
            if(selectedDocument.next())
                ans = new TransportationDocDTO(selectedDocument.getInt(1),selectedDocument.getString(2),selectedDocument.getInt(3),selectedDocument.getString(4),selectedDocument.getString(5),selectedDocument.getString(6),selectedDocument.getInt(7), selectedDocument.getInt(8) - 1,0);
        } catch (SQLException e) {
            ans = null;
        }
        return ans;

    }

    public boolean updateWeight(TransportationDocDTO transDoc, int weight) {
        boolean ans = true;
        try {
            PreparedStatement pStatement = con.prepareStatement("UPDATE transportationDocs SET WeightAtLeave = ? WHERE ID = ? ");
            pStatement.setInt(1, weight);
            pStatement.setInt(2,transDoc.getID());
            pStatement.executeUpdate();
        }
        catch (SQLException e){
            ans = false;
        }
        return ans;
    }

    public boolean updateTruckID(TransportationDocDTO transDoc, String newTruckID) {
        boolean ans = true;
        try {
            PreparedStatement pStatement = con.prepareStatement("UPDATE transportationDocs SET TruckID = ? WHERE ID = ?");
            pStatement.setString(1, newTruckID);
            pStatement.setInt(2,transDoc.getID());
            pStatement.executeUpdate();
        }
        catch (SQLException e){
            ans = false;
        }
        return ans;
    }

    public List<Integer> getUnconfirmedIDList()
    {
        List<Integer> unconfirmedIDList = new LinkedList<>();
        try {
            PreparedStatement pStatment = con.prepareStatement("SELECT * FROM transportationDocs WHERE Confirmed = 0");
            ResultSet unconfirmedID = pStatment.executeQuery();
            while (unconfirmedID.next()){
                int ID = unconfirmedID.getInt(1);
                unconfirmedIDList.add(ID);
            }
        } catch (SQLException e) {}

        return unconfirmedIDList;
    }


    public List<Integer> getConfirmedID()
    {
        List<Integer> NotFinishedIDList = new LinkedList<>();
        try {
            PreparedStatement pStatment = con.prepareStatement("SELECT * FROM transportationDocs WHERE Confirmed = 1");
            ResultSet notFinishedID = pStatment.executeQuery();
            while (notFinishedID.next()){
                int ID = notFinishedID.getInt(1);
                NotFinishedIDList.add(ID);
            }
        } catch (SQLException e) {}

        return NotFinishedIDList;
    }

    public boolean delete(int ID)
    {
        try {
            PreparedStatement pStatment = con.prepareStatement("DELETE FROM transportationDocs WHERE ID = ?");
            pStatment.setInt(1, ID);
            pStatment.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean confirmTransDoc(int ID)
    {
        try {
            PreparedStatement pStatment = con.prepareStatement("UPDATE transportationDocs SET Confirmed = 1 WHERE ID = ?");
            pStatment.setInt(1, ID);
            pStatment.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

	public List<Integer> getPastDocList() {
		List<Integer> NotFinishedIDList = new LinkedList<>();
		String today= LocalDate.now().toString();
		try {
			PreparedStatement pStatment = con.prepareStatement("SELECT * FROM transportationDocs WHERE Date < ?");
			pStatment.setString(1, today);
			ResultSet notFinishedID = pStatment.executeQuery();
			while (notFinishedID.next()){
				int ID = notFinishedID.getInt(1);
				NotFinishedIDList.add(ID);
			}
		} catch (SQLException e) {}

		return NotFinishedIDList;

	}
}
