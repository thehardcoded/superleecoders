package DataProviders.DAOs;

import SharedClasses.DTOs.StoreDTO;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by AmirPC on 25/04/2017.
 */
public class storesDAO {
    Connection con;

    public storesDAO(Connection con) {
        this.con = con;
        createTableIfNotExist();
    }

    public void close() {
        try {
            con.close();
        } catch (SQLException ignored) {
        }
    }

    private void createTableIfNotExist()
    {
        Statement createSt;
        try
        {
            createSt = con.createStatement();
            String CreationSqlSt = "CREATE TABLE IF NOT EXISTS stores("+
                    "Address                TEXT            PRIMARY KEY,"+
                    "Region                 INTEGER         NOT NULL,"+
                    "ContactName            TEXT            NOT NULL,"+
                    "PhoneNum               TEXT            NOT NULL"+
                    ");";
            createSt.executeUpdate(CreationSqlSt);
            if(!exist("General"))
                insertGeneralRow();
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the stores table");
            e.printStackTrace();
        }
    }

	private void insertGeneralRow() //add store general for general workers
	{
		Statement statement;
		try {
			statement = con.createStatement();
			String query = "INSERT INTO stores VALUES('General',6,'ADMIN','0500000000')";
			statement.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

    public boolean insert(StoreDTO ToAdd)
    {
        try {
            PreparedStatement pStatment = con.prepareStatement("INSERT INTO stores VALUES (?, ?, ?, ?)");
            pStatment.setString(1, ToAdd.getAddress());
            pStatment.setInt(2, ToAdd.getRegion());
            pStatment.setString(3, ToAdd.getContactName());
            pStatment.setString(4, ToAdd.getPhoneNum());
            pStatment.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean delete(String Address)
    {
        try {
            PreparedStatement pStatment = con.prepareStatement("DELETE FROM stores WHERE Address = ?");
            pStatment.setString(1, Address);
            pStatment.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean exist(String Address){
        try {
            PreparedStatement pStatment = con.prepareStatement("SELECT * FROM stores WHERE Address = ?");
            pStatment.setString(1, Address);
            ResultSet selectedStore = pStatment.executeQuery();
            if(selectedStore.next())
                return true;
        } catch (SQLException e) {}
        return false;
    }

    public boolean existByRegion(String Address, int Region){
        try {
            PreparedStatement pStatment = con.prepareStatement("SELECT * FROM stores WHERE (Address = ? AND Region = ?)");
            pStatment.setString(1, Address);
            pStatment.setInt(2, Region);
            ResultSet selectedStore = pStatment.executeQuery();
            if(selectedStore.next())
                return true;
        } catch (SQLException e) {}
        return false;
    }

    public List<String> getAll()
    {
        List<String> storesList = new LinkedList<>();
        try {
            PreparedStatement pStatment = con.prepareStatement("SELECT * FROM stores ");
            ResultSet stores = pStatment.executeQuery();
            while (stores.next()){
                String Address = stores.getString(1);
                storesList.add(Address);
            }
        } catch (SQLException e) {}

        return storesList;
    }

    public List<String> getByRegion(int r)
    {
        List<String> storesList = new LinkedList<>();
        try {
            PreparedStatement pStatment = con.prepareStatement("SELECT * FROM stores WHERE Region = ?");
            pStatment.setInt(1, r);
            ResultSet stores = pStatment.executeQuery();
            while (stores.next()){
                String Address = stores.getString(1);
                storesList.add(Address);
            }
        } catch (SQLException e) {}

        return storesList;
    }
}
