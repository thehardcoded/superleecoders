package DataProviders.DAOs;

import SharedClasses.DTOs.DriverDTO;
import SharedClasses.Enums;

import java.sql.*;


/**
 * Created by AmirPC on 28/03/2017.
 */
public class driversDAO {

    Connection con;

    public driversDAO(Connection con) {
        this.con = con;
        createTableIfNotExist();
    }

    public void close() {
        try {
            con.close();
        } catch (SQLException ignored) {
        }
    }

    private void createTableIfNotExist()
    {
        Statement createSt;
        try
        {
            createSt = con.createStatement();
            String CreationSqlSt = "CREATE TABLE IF NOT EXISTS drivers("+
                    "ID                     TEXT            PRIMARY KEY    NOT NULL,"+
                    "License                INTEGER         NOT NULL," +
                    "Region                 INTEGER         NOT NULL ,"+
                    "FOREIGN KEY (ID) REFERENCES Employees(EmployeeID)"+
                    ");";
            createSt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the drivers table");
            e.printStackTrace();
        }
    }

    public boolean insert(DriverDTO ToAdd)
    {
        try {
            PreparedStatement pStatment = con.prepareStatement("INSERT INTO drivers VALUES (?,?,?)");
            pStatment.setString(1, ToAdd.getID());
            pStatment.setInt(2, ToAdd.getLicense());
            pStatment.setInt(3, ToAdd.getRegion());
            pStatment.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean delete(String ID)
    {
        try {
            PreparedStatement pStatment = con.prepareStatement("DELETE FROM drivers WHERE ID = ?");
            pStatment.setString(1, ID);
            pStatment.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean exist(String ID){
        try {
            PreparedStatement pStatment = con.prepareStatement("SELECT * FROM drivers WHERE ID = ?");
            pStatment.setString(1, ID);
            ResultSet selectedDriver = pStatment.executeQuery();
            if(selectedDriver.next())
                return true;
        } catch (SQLException e) {}
        return false;
    }

    public int getLicenseType(String ID)
    {
        int ans = 0;
        try {
            PreparedStatement pStatment = con.prepareStatement("SELECT License FROM drivers WHERE ID = ?");
            pStatment.setString(1, ID);
            ResultSet selectedDriver = pStatment.executeQuery();
            if(selectedDriver.next())
                ans = selectedDriver.getInt(1);
        } catch (SQLException e) {}
        return ans;
    }

    public DriverDTO getDriver(String ID)
    {
        DriverDTO ans = null;
        try {
            PreparedStatement pStatment = con.prepareStatement("SELECT * FROM drivers WHERE ID = ?");
            pStatment.setString(1, ID);
            ResultSet selectedDriver = pStatment.executeQuery();
            if(selectedDriver.next())
                    ans = new DriverDTO(selectedDriver.getString("ID"),Enums.License.values()[selectedDriver.getInt(2)-1],Enums.Region.values()[selectedDriver.getInt(3)-1]);
        } catch (SQLException e) {}
        return ans;
    }
}
