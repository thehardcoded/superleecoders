package DataProviders;

import DataProviders.DAOs.*;
import SharedClasses.DTOs.*;
import SharedClasses.Enums;
import javafx.util.Pair;
import org.sqlite.SQLiteConfig;

import java.time.LocalDate;
import java.util.*;
import java.sql.*;

/**
 * Created by SHLOMI PAKADO on 3/15/2017.
 */
public class Repository {

    private Connection con;
    private storesDAO stores;
    private shiftsTypesDAO shiftsTypes;
    private employeesDAO employeesDB;
    private rolesInShiftsDAO rolesInShiftsDP;
    private employeesConstraintsDAO employeesConstraintsDP;
    private employeesInShiftsDAO employeesInShifts;
    private shiftsInDatesDAO shiftsInDates;
    private rolesDAO roles;
    private driversDAO drivers;
    private suppliersDAO suppliers;
    private stopsDAO stops;
    private transportationDocsDAO transportationDocs;
    private truckModelsDAO truckModels;
    private trucksDAO trucks;

    public Repository()
    {
        this.con = null;
        try {
            Class.forName("org.sqlite.JDBC");
            SQLiteConfig config = new SQLiteConfig();
            con = DriverManager.getConnection("jdbc:sqlite:DB/Shop.db", config.toProperties());
            //------------------------------ DAOs initiations ------------------------------
            stores = new storesDAO(con);
            shiftsTypes = new shiftsTypesDAO(con);
            roles = new rolesDAO(con);
            employeesDB = new employeesDAO(con);
            shiftsInDates = new shiftsInDatesDAO(con);
            employeesInShifts = new employeesInShiftsDAO(con);
            rolesInShiftsDP = new rolesInShiftsDAO(con);
            employeesConstraintsDP = new employeesConstraintsDAO(con);
            stops = new stopsDAO(con);
            drivers = new driversDAO(con);
            suppliers = new suppliersDAO(con);
            transportationDocs = new transportationDocsDAO(con);
            truckModels = new truckModelsDAO(con);
            trucks = new trucksDAO(con);
            stops = new stopsDAO(con);
            updateNotification();
            CheckShiftManagerPerStore();
        }
        catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
    }


    public Connection getConn() {
        return con;
    }

    //--------------------------------------- JOIN queries -------------------------------------------------------

    public void updateNotification(){
        try {
            PreparedStatement pStatment=con.prepareStatement("UPDATE transportationDocs Set Confirmed=? WHERE Date <= ?");
            pStatment.setInt(1,1);
            pStatment.setString(2, LocalDate.now().toString());
            pStatment.executeUpdate();
        } catch (SQLException e) {
        }
    }

    public int getMaxWeightByID(String truckID) {
        try {
            PreparedStatement pStatement = con.prepareStatement("SELECT truckModels.maxWeight FROM truckModels JOIN trucks ON truckModels.model = trucks.model WHERE ID = ?");
            pStatement.setString(1, truckID);
            ResultSet weight = pStatement.executeQuery();
            if(weight.next())
                return weight.getInt(1);
        } catch (SQLException e) {}
        return 0;
    }

    public ArrayList<String[]> getListEmployeesByRole(String Date, int shiftType ,String Role, String store)
    {
        ArrayList<String[]> ans=null;
        try {
            PreparedStatement pStatment=con.prepareStatement("SELECT EmployeeID,FirstName,LastName FROM Employees as E WHERE e.StoreAddress=? AND e.Role=?" +
                    " And e.EmployeeID NOT IN (SELECT EmployeeID FROM EmployeesConstraints AS EC WHERE EC.ShiftDate=? AND  EC.ShiftType=?);");
            pStatment.setString(1,store);
            pStatment.setString(2,Role);
            pStatment.setString(3,Date);
            pStatment.setInt(4,shiftType);
            ResultSet selectedEmployeeData=pStatment.executeQuery();
            ans=getEmployeeDetailsArrayList(selectedEmployeeData);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        return  ans;
    }

    public ArrayList<String[]> getEmployeesInShiftByDate(String shiftedDate,int shiftType,String store)
    {
        ArrayList<String[]> ans=null;
        try {
            PreparedStatement pStatment=con.prepareStatement("SELECT EmployeeID,FirstName,LastName, RoleInShift  FROM Employees AS E JOIN EmployeesInShifts AS EIS  ON E.EmployeeID=EIS.ID " +
                    "WHERE EIS.ShiftDate = ? AND EIS.ShiftType=? AND E.StoreAddress=?");
            pStatment.setString(1,shiftedDate);
            pStatment.setInt(2,shiftType);
            pStatment.setString(3,store);
            ResultSet selectedEmployeeData=pStatment.executeQuery();
            ans=new ArrayList<String[]>();
            try
            {
                while (selectedEmployeeData.next())
                {
                    String [] temp=new String[4];
                    temp[0] = selectedEmployeeData.getString("EmployeeID");
                    temp[1] = selectedEmployeeData.getString("FirstName");
                    temp[2] = selectedEmployeeData.getString("LastName");
                    temp[3] = selectedEmployeeData.getString("RoleInShift");
                    ans.add(temp);
                }
            }catch (SQLException e) {
                e.printStackTrace();
            }
            return ans;
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        return ans;
    }

    public ArrayList<String> getRolesNotInShiftArray(int shiftType, String store){
        ArrayList<String> ans=new ArrayList<String>();
        try {

            PreparedStatement pStatment=con.prepareStatement("SELECT Role FROM Roles where  " +
                    " Role not in (SELECT Role FROM RolesInShifts WHERE ShiftType=? AND StoreAddress=?) and Role<> 'ManPower Manager' and Role<> 'Logistic Manager' and Role<> 'Administrative Manager'and Role<> 'Driver'");
            pStatment.setInt(1,shiftType);
            pStatment.setString(2,store);
            ResultSet selectedRoleData=pStatment.executeQuery();
            while (selectedRoleData.next()) {
                ans.add(selectedRoleData.getString("Role"));
            }
        } catch (SQLException e) {
            ans=null;
        }
        return ans;
    }

    public List<String> getAvailableDriversId(String truckID, int region, String date, int shift)
    {
        int truckModel = this.trucks.getModel(truckID);
        ArrayList<String> ans=new ArrayList<String>();
        try {
            PreparedStatement pStatment=con.prepareStatement("SELECT ID FROM drivers " +
                    "WHERE license >= ? AND region=? "+
                    "AND ID not in (SELECT ID FROM employeesConstraints WHERE ShiftType=? AND ShiftDate=?)" +
                    "AND ID not in (SELECT ID FROM EmployeesInShifts WHERE ShiftType=? AND ShiftDate=?)ORDER BY license ASC");
            pStatment.setInt(1,truckModel);
            pStatment.setInt(2,region);
            pStatment.setInt(3,shift);
            pStatment.setString(4,date);
            pStatment.setInt(5,shift);
            pStatment.setString(6,date);
            ResultSet selectedIds=pStatment.executeQuery();
            while (selectedIds.next()) {
                ans.add(selectedIds.getString("ID"));
            }
        } catch (SQLException e) {
            ans=null;
        }
        return ans;
    }

    public List<String> getStoresByRegion(int region,String Date,int shiftType, int transDocNO)
    {
        List<String> storesList = new LinkedList<>();
        try {
            PreparedStatement pStatment = con.prepareStatement("SELECT * FROM stores where region=? AND address " +
                    "IN(SELECT StoreAddress FROM EmployeesInShifts WHERE ShiftDate =? and ShiftType=? and RoleInShift='Storekeeper') " +
                    "AND stores.address NOT IN(SELECT destination FROM stops WHERE transportationDocNo=?)");
            pStatment.setInt(1, region);
            pStatment.setString(2, Date);
            pStatment.setInt(3, shiftType);
            pStatment.setInt(4, transDocNO);
            ResultSet stores = pStatment.executeQuery();
            while (stores.next()){
                String address = stores.getString(1);
                storesList.add(address);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return storesList;
    }


    public List<Pair<String,String>> getNotificationsByRole(String Role)
    {
        List<Pair<String,String>> notifications = new LinkedList<>();
        try {
            PreparedStatement pStatment = con.prepareStatement("SELECT * FROM Notifications where Role=?");
            pStatment.setString(1, Role);
            ResultSet stores = pStatment.executeQuery();
            while (stores.next()){
                String Date = stores.getString("Date");
                String OrderNo = stores.getString("OrderNo");
                notifications.add(new Pair(Date,OrderNo));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return notifications;
    }

    public boolean deleteNotification(String Role,String orderNo)
    {
        try {
            PreparedStatement pStatment=con.prepareStatement("DELETE FROM Notifications WHERE Role=? AND OrderNo=?");
            pStatment.setString(1,Role);
            pStatment.setString(2,orderNo);
            pStatment.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }























    public boolean DeleteScheduledShift(ShiftInDateDTO sid) {
        try {
            if (!this.transportationDocs.existByShiftInDate(sid)) {
                PreparedStatement pStatment = con.prepareStatement("DELETE FROM EmployeesInShifts WHERE ShiftDate= ? AND ShiftType=? AND StoreAddress=?");
                pStatment.setString(1, EmployeeDTO.shiftDateFormat(sid.getShiftDate()));
                pStatment.setInt(2, sid.getShiftType());
                pStatment.setString(3, sid.getStore());
                pStatment.executeUpdate();
                return true;
            }

        } catch (SQLException e) {
            return false;
        }
        return false;
    }

    /**
     * @param data
     * @return Arraylist of employee details (ID, Firstname, Lastname)
     */
    private ArrayList<String[]> getEmployeeDetailsArrayList(ResultSet data)
    {
        ArrayList<String[]> ans = new ArrayList();
        try
        {
            while (data.next())
            {
                String [] temp=new String[3];
                temp[0] = data.getString("EmployeeID");
                temp[1] = data.getString("FirstName");
                temp[2] = data.getString("LastName");
                ans.add(temp);
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return ans;
    }

	public List<String> getRemovableTrucks()
	{
		ArrayList<String> ans=new ArrayList();
		try {
			PreparedStatement pStatment=con.prepareStatement("SELECT ID FROM trucks " +
					"WHERE trucks.ID NOT IN(SELECT truckID FROM transportationDocs WHERE WeightAtLeave = 0)");
			ResultSet selectedTrucks = pStatment.executeQuery();
			while (selectedTrucks.next()) {
				String truck = selectedTrucks.getString(1);
				ans.add(truck);
			}
		} catch (SQLException e) {
			ans=null;
		}
		return ans;

	}

	public List<String> getRemovableSuppliers()
	{
		ArrayList<String> ans=new ArrayList();
		try {
			PreparedStatement pStatment=con.prepareStatement("SELECT address FROM suppliers " +
					"WHERE suppliers.address NOT IN(SELECT Supplier FROM transportationDocs WHERE WeightAtLeave = 0)");
			ResultSet selectedSuppliers = pStatment.executeQuery();
			while (selectedSuppliers.next()) {
				String suppliers = selectedSuppliers.getString(1);
				ans.add(suppliers);
			}
		} catch (SQLException e) {
			ans=null;
		}
		return ans;
	}


    public ArrayList<Integer> getConfirmedTransDocsIDByStore(String Address)
    {
        ArrayList<Integer> ans = null;
        try {
            PreparedStatement pStatment = con.prepareStatement("SELECT ID FROM transportationDocs AS T JOIN stops AS S ON T.ID = S.TransportationDocNo " +
                    "WHERE S.destination = ?");
            pStatment.setString(1, Address);
            ResultSet confirmedDocs = pStatment.executeQuery();
            ans = new ArrayList();
            try
            {
                while (confirmedDocs.next())
                {
                    int temp = confirmedDocs.getInt(1);
                    ans.add(temp);
                }
            }catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        return ans;
    }

    //----------------------------------- pipe functions queries -------------------------------------------------

    public boolean confirmTransDoc(int docID) {
		return this.transportationDocs.confirmTransDoc(docID);
	}

	public TruckDTO getMinFreeTruck(String currentDate,int shiftType,int weight){
		List<TruckDTO> availableTrucks = getAvailableTrucks (currentDate, shiftType);
		List<TruckDTO> matchingTrucks = new LinkedList<TruckDTO>();
		for(TruckDTO truck: availableTrucks)
		{
			if(truck.getTruckMaxWeightByModel(truck.getModel()) >= weight)
			{
				matchingTrucks.add(truck);
			}
		}
		if(matchingTrucks.isEmpty())
			return null;
		return matchingTrucks.remove(0);
	}

	public List<TruckDTO> getAvailableTrucks(String date, int shift)
	{
		ArrayList<TruckDTO> ans=new ArrayList<TruckDTO>();
		try {
			PreparedStatement pStatment=con.prepareStatement("SELECT* FROM trucks " +
					"WHERE trucks.ID not in (SELECT truckID FROM transportationDocs WHERE shift=? AND date=?) ORDER BY Model ASC");
			pStatment.setInt(1,shift);
			pStatment.setString(2,date);
			ResultSet selectedTrucks = pStatment.executeQuery();
			while (selectedTrucks.next()) {
				TruckDTO t = new TruckDTO(selectedTrucks.getString("ID"), Enums.TruckModel.values()[selectedTrucks.getInt("model")-1]);
				ans.add(t);
			}
		} catch (SQLException e) {
			ans=null;
		}
		return ans;
	}

	public void addNotification (String role, String date, int OrderNo){
		try {
			PreparedStatement pStatment=con.prepareStatement("INSERT INTO Notifications VALUES (?,?,?)");
			pStatment.setString(1, date);
			pStatment.setString(2, role);
            pStatment.setInt(3, OrderNo);
			pStatment.executeUpdate();
		} catch (SQLException e) {

		}
	}

	public void addStopsToTransDoc(int id, LinkedList<String> stores)
	{
		try {
			for(String store: stores)
			{
				PreparedStatement pStatment = con.prepareStatement("INSERT INTO Stops VALUES (?,?)");
				pStatment.setInt(1, id);
				pStatment.setString(2, store);
				pStatment.executeUpdate();
			}
			} catch (SQLException e) {

		}
	}


    //----------------------------------- pipe functions queries -------------------------------------------------

    public boolean storekeeperWorksOnShift(String date,int shiftType,String store)
    {
        return employeesInShifts.ValidateStorekeeperWorksOnShift(date, shiftType, store);
    }

    public boolean freeDriverShiftByDate(String DateOfTransDocToRemove, String driverIDToRemove, int shiftType)
    {
        return this.employeesInShifts.deleteEmployeeShiftByDriver(DateOfTransDocToRemove, driverIDToRemove, shiftType);
    }

    public boolean deleteConstraint(int shiftType, String shiftedDate, String ID)
    {
        return this.employeesConstraintsDP.delEmployeeConstraint(shiftType,shiftedDate,ID);
    }

    public EmployeeDTO getEmployeeById(String ID)
    {
        return this.employeesDB.getEmployeeById(ID);
    }

    public Boolean AddEmployee(EmployeeDTO ToAdd)
    {
        return this.employeesDB.AddEmployee(ToAdd);
    }

    public Boolean DeleteEmployee(String ID)
    {
        return this.employeesDB.DeleteEmployee(ID);
    }

    public Boolean AddTransportationDocs(TransportationDocDTO toAdd)
    {
        return this.transportationDocs.insert(toAdd);
    }

    public Boolean editEmployee (String employeesOldId, EmployeeDTO emp )
    {
        return this.employeesDB.updateEmployee(employeesOldId, emp);
    }

    public ArrayList<RoleInShiftDTO> getRolesInShiftsList(int shift,String store) {
        return this.rolesInShiftsDP.getRolesInShift(shift,store);
    }

    public boolean setShiftStandardDB(ArrayList<RoleInShiftDTO> roles, int shiftType) {
        return this.rolesInShiftsDP.setShiftStandardList(roles,shiftType);
    }

    public boolean addEmployeeConstraint(EmployeeConstraintDTO toAdd)
    {
        return this.employeesConstraintsDP.addEmployeeConstraint(toAdd);
    }

    public List<EmployeeConstraintDTO> getEmployeeConstraints (EmployeeDTO toAdd)
    {
        return this.employeesConstraintsDP.getEmployeeConstraints(toAdd);
    }

    public boolean addDriver(DriverDTO ToAdd)
    {
        return this.drivers.insert(ToAdd);
    }

    public DriverDTO getDriver (String ID)
    {
        return this.drivers.getDriver(ID);
    }

    public boolean addStore(StoreDTO toAdd)
    {
        return this.stores.insert(toAdd);
    }

    public boolean addSupplier(SupplierDTO toAdd) {return this.suppliers.insert(toAdd);}

    public boolean addStop(StopDTO toAdd) {return this.stops.insert(toAdd);}

    public boolean checkIfShiftDated(EmployeeConstraintDTO constraint)
    {
        String date = EmployeeDTO.shiftDateFormat(constraint.getDate());
        int shiftType = constraint.getShiftType();
        String store = constraint.getStore();
	    return this.shiftsInDates.checkIfShiftDated(date, shiftType, store);
	}

    public boolean addRole(String roleType) {return this.roles.insertNewRole(roleType);}

    public boolean checkStopTransDestExist(String transDocID, String dest){
        return this.stops.transDestExist(transDocID, dest);
    }

    public boolean isValidStore(String address, int region){
        return (this.stores.existByRegion(address, region));
    }

    public List<String> getAllStores(){
        return this.stores.getAll();
    }


    public List<Integer> getUnconfirmedIDList(){
        return this.transportationDocs.getUnconfirmedIDList();
    }



    public TransportationDocDTO getTransportDoc(int docNum) {
        return (this.transportationDocs.getTransportDoc(docNum));
    }

    public boolean updateTruckID(TransportationDocDTO transDoc, String newTruckID) {
        return this.transportationDocs.updateTruckID(transDoc , newTruckID);
    }

    public void AddShiftDate(String shiftDate, int shiftType, String store){
        this.shiftsInDates.AddShiftDate(shiftDate, shiftType, store);
    }

    public boolean AddToEmployeesInShift(int shiftType,String shiftDate, ArrayList<String[]> employeesToSet ,String store){
        return this.employeesInShifts.insertToEmployeesInShift(shiftType, shiftDate, employeesToSet,store);
    }

    public boolean addTruck(TruckDTO toAdd){return this.trucks.insert(toAdd);}

    public void addShiftDate(EmployeeConstraintDTO constraint)
    {
        String date = EmployeeDTO.shiftDateFormat(constraint.getDate());
        int shiftType = constraint.getShiftType();
        String store = constraint.getStore();
        this.shiftsInDates.AddShiftDate(date, shiftType, store);
    }

    public boolean deleteSupplier(String address)
    {
        return this.suppliers.delete(address);
    }

    public boolean deleteStore(String address)
    {
        return this.stores.delete(address);
    }

    public boolean deleteDriver(String ID)
    {
        return this.drivers.delete(ID);
    }

    public boolean deleteTruck(String ID){
        return this.trucks.delete(ID);
    }

    public boolean removeStopByTransportationDocNo(int transportationDocNo){
        return this.stops.deleteByTransportationDocNo(transportationDocNo);
    }

    public boolean removeTransportationDoc(int ID){
        return this.transportationDocs.delete(ID);
    }

    public boolean removeSpecificStop(int transportationDocNo, String destination)
    {
        return this.stops.removeSpecificStop(transportationDocNo, destination);
    }

    public boolean checkTransDocIDExist(int ID)
    {
        return this.transportationDocs.exist(ID);
    }

    public boolean removeStopByProductDocNo(String productsDocNo, String transportationDocNo) {
        return false;
    }

    public boolean checkSupplierExist(String address){
        return this.suppliers.exist(address);
    }

    public boolean checkDatedShiftExist(String address){
        return this.stores.exist(address);
    }

    public boolean checkDriverIDExist(String ID){
        return this.drivers.exist(ID);
    }

    public boolean checkTruckIDExist(String ID){
        return this.trucks.exist(ID);
    }

    public boolean checkStopProductsDocNoExist(String productsDocNo){
        return this.stops.exist(productsDocNo);
    }

    public List<String> getStops(int transDocId){return this.stops.getStops(transDocId);}

	public List<Integer> getPastDocList() {
    	return this.transportationDocs.getPastDocList();
	}

    public List<Integer> getConfirmedID() {
		return this.transportationDocs.getConfirmedID();
	}


    public boolean checkScheduledShiftExist(String Date, int shiftType,String store) {
        return (this.employeesInShifts.shiftWasScheduled(Date,shiftType,store));
    }

    public int getLicenseType(String driverId){
        return (this.drivers.getLicenseType(driverId));
    }

    public int getModel(String newTruckID){
        return (this.trucks.getModel(newTruckID));
    }

    public List<String> getValidRoles(){
        return (this.roles.getValidRoles());
    }

    public List<String> getSuppliersAddressesByRegion(int region){
        return (this.suppliers.getByRegion(region));
    }

    public boolean updateWeight(TransportationDocDTO transDoc, int weight) {
        return (this.transportationDocs.updateWeight(transDoc , weight));
    }

    public boolean checkDatedShiftExist(String shiftDate, int shiftType, String store){
        String sd = EmployeeDTO.shiftDateFormat(shiftDate);
        return this.shiftsInDates.checkIfShiftDated(sd, shiftType, store);
    }

    /*public void deleteScheduledShift(String shiftDate, int type,String store){
         this.employeesInShifts.DeleteScheduledShift(EmployeeDTO.shiftDateFormat(shiftDate), type,store);
    }*/

    //public List<String> getIdOfSevenYearsRetired() {return this.employeesDB.getIdOfSevenYearsRetired(); }

    //-------------------------------------------- else -----------------------------------------------------------

    /**
     * getting all roles by stores and check if there is shift manager by default in loop
     */
    private void CheckShiftManagerPerStore() {
        Statement createSt;
        List<String> stores=this.stores.getAll();
        try
        {
            for (String Store: stores)
            {
                createSt = con.createStatement();
                String CreationSqlSt ="INSERT INTO  RolesInShifts (ShiftType,Role,StoreAddress) \n" +
                        "SELECT 1,'Shift Manager', " +"'"+Store+"'"+
                        "WHERE NOT EXISTS (SELECT * FROM RolesInShifts WHERE ShiftType = 1 and Role='Shift Manager' AND StoreAddress='"+Store +"');"+
                        "INSERT INTO  RolesInShifts (ShiftType,Role,StoreAddress) \n" +
                        "SELECT 2,'Shift Manager' ," +"'"+Store+"'"+
                        "WHERE NOT EXISTS (SELECT * FROM RolesInShifts WHERE ShiftType = 2 and Role='Shift Manager' AND StoreAddress='"+Store+"');";
                createSt.executeUpdate(CreationSqlSt);
            }
        } catch (SQLException e)
        {
            System.out.println("there was a problem in the insertion to the RolesInShifts table");
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            this.con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
