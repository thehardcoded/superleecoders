package DataProviders;

import SharedClasses.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.*;


public class DAL {
    private Connection c = null;
    private Statement stmt = null;


    //Constructor
    public DAL(Connection conn) {
        try {
            Class.forName( "org.sqlite.JDBC" );
            c = conn;
        } catch (Exception e) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.out.println( "Cannot connect to DB. Exiting..." );
            System.exit( 0 );
        }
    }

    public void close() {
        try {
            c.close();
        } catch (SQLException ignored) {
        }
    }

    //INSERTS!
    public boolean insertToSuppliers(Supplier supplier) {
        try {
            stmt = c.createStatement();
            String sql = "INSERT INTO Suppliers (id, name, address, bank_account, payment, supply_terms, supply_days, region) VALUES ('" +
                    supplier.getId() + "', '" + supplier.getName() + "', '" + supplier.getAddress() + "', '" + supplier.getBankAccount()
                    + "', '" + supplier.getTermsOfPayment() + "', '" + supplier.getTerms()
                    + "', '" + supplier.getSupply_days() + "', " + supplier.getRegion() + ");";
            stmt.executeUpdate( sql );
            for (Contact c : supplier.getContacts()) {
                insertToContacts( supplier.getId(), c, contactExistsInContacts( c.getPhone() ) );
            }
            for (Item item : supplier.getItems()) {
                insertToItemsInSupplier( supplier.getId(), item );
            }
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean insertToContacts(String id, Contact contact, boolean shouldEnterToContacts) {
        try {
            stmt = c.createStatement();
            String sql = "INSERT INTO Contacts_Of_Suppliers (supplier_id, phone_number) VALUES ('" +
                    id + "', '" + contact.getPhone() + "');";
            stmt.executeUpdate( sql );
            if (!shouldEnterToContacts) {
                sql = "INSERT INTO Contacts (name, phone_number) VALUES ('" +
                        contact.getName() + "', '" + contact.getPhone() + "');";
                stmt.executeUpdate( sql );
            } else {
                sql = "UPDATE Contacts SET name = '" + contact.getName() + "' WHERE phone_number = '" + contact.getPhone() + "';";
                stmt.executeUpdate( sql );
            }
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    //TODO changed
    public boolean insertToItemsInSupplier(String supplier_id, Item item) {
        try {
            String sql;
            stmt = c.createStatement();
            if(!itemExists(supplier_id, item.getCatalogNumber())) {
                sql = "INSERT INTO Items_In_Contract (supplier_id, catalog_number, serialNumber, price) VALUES ('" +
                        supplier_id + "', '" + item.getCatalogNumber() + "', '" + item.getSerialNumber() + "', " + item.getPrice() + ");";
                stmt.executeUpdate(sql);
            }
            insertToStoresOfItemsInContract(supplier_id, item);

            if (item.getDiscounts() != null) {
                for (Discount discount : item.getDiscounts()) {
                    insertToDiscounts( supplier_id, item.getCatalogNumber(), discount );
                }
            }
            stmt.close();

        } catch (SQLException e) {
            System.out.println( e.getMessage() );
            return false;
        }
        return true;
    }

    //TODO changed
    public boolean insertToStoresOfItemsInContract(String supplier_id, Item item) {
        try {
            stmt = c.createStatement();
            String sql = "";
            Supplier supplier = this.getSupplier( supplier_id );
            if (supplier.getTerms() == SupplyTerms.SupplyOnSetDays) {
                for (Map.Entry entry : item.getStoreQuantity().entrySet()) {

                    sql = "INSERT INTO StoresOfItemsInContract (supplier_id, catalog_number, store, quantity) VALUES (" + supplier_id + ", '" + item.getCatalogNumber() + "', '" + (String) entry.getKey() + "', '" + (Integer) entry.getValue() + "' );";
                    stmt.executeUpdate( sql );
                    stmt.close();
                }
            }
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean insertToDiscounts(String supplierId, String catalogNum, Discount discount) {
        try {
            stmt = c.createStatement();
            String sql = "INSERT INTO Discounts (supplier_id, catalog_number, quantity, discount) VALUES ('" +
                    supplierId + "', '" + catalogNum + "', " + discount.getQuantity() + ", " + discount.getDiscount() + ");";
            stmt.executeUpdate( sql );
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean insertSale(Sale sale) {
        try {
            stmt = c.createStatement();
            stmt.execute( "INSERT INTO Sales " +
                    "VALUES(NULL, '" + sale.getStartDate() + "', '" + sale.getEndDate() + "', " + sale.getPercentOff() + ")" );
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean insertProduct(Product p) {
        try {
            stmt = c.createStatement();
            if (p.getCategory() == null) {
                stmt.execute( "INSERT INTO Products VALUES('" + p.getSerialNumber() + "', '" + p.getName() + "', '" + p.getManufacturer() + "', " + "NULL" + ", " + p.getWeight() + ", " + p.getCost() + ", " + p.getSellingPrice() + ")" );

            } else {
                stmt.execute( "INSERT INTO Products VALUES('" + p.getSerialNumber() + "', '" + p.getName() + "', '" + p.getManufacturer() + "', " + p.getCategory().getId() + ", " + p.getWeight() + ", " + p.getCost() + ", " + p.getSellingPrice() + ")" );
            }
            //stmt.execute("INSERT INTO Inventory VALUES('" + p.getSerialNumber() + "', " + stock.getMinimalQuantity()+ ", '" +stock.getShelfLocation() + "', '" + stock.getStorageLocation() + "', " + stock.getShelfQuantity() + ", " + stock.getStorageQuantity() + ", 0, 0" + ")");
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean insertInventory(Stock stock) {
        try {
            stmt = c.createStatement();
            stmt.execute( "INSERT INTO Inventory VALUES('" + stock.getSerialNumber() + "', " + stock.getMinimalQuantity() + ", '" + stock.getShelfLocation() + "', '" + stock.getStorageLocation() + "', " + stock.getShelfQuantity() + ", " + stock.getStorageQuantity() + ", 0, 0 , '" + stock.getStore() + "')" );
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean insertCategory(Category cat) {
        try {
            stmt = c.createStatement();
            stmt.execute( "INSERT INTO Categories " +
                    "VALUES(NULL, '" + cat.getName() + "', " + cat.getFatherCategory() + ")" );
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean addItemsToOrder(int order, LinkedList<Item> items) {
        try {
            int truckMaxSize = 8000000;
            int currentOrderWeight = getOrderWeight(order);
            int totalweight = currentOrderWeight;
            LinkedList<Order> orderByNumber = getOrderByOrderNumber(order);
            Supplier supplier = orderByNumber.getFirst().getSupplier();
            boolean isOnOrder = supplier.getTerms() == SupplyTerms.OnOrder;
            LinkedList<Item> couldntBeInserted = new LinkedList<>();
            int sum = 0;
            for (Item item : items) {
                boolean shouldEnter = true;
                int totalOrder = 0;
                HashMap<String, Integer> quantity = item.getStoreQuantity();
                String serNum = item.getSerialNumber();
                int weightOfItems = totalWeightOfHashMap(quantity, serNum);
                if (isOnOrder && truckMaxSize < totalweight + weightOfItems) {
                    couldntBeInserted.add(item);
                    shouldEnter = false;
                }
                if (shouldEnter) {
                    for (Map.Entry entry : item.getStoreQuantity().entrySet()) {
                        String serNum2 = item.getSerialNumber();
                        sum += (int) entry.getValue() * getProductDetails(serNum2).getWeight();
                        totalweight = sum + currentOrderWeight;
                        double discount = 0;
                        for (Discount disc : item.getDiscounts()) {
                            int discQuan = disc.getQuantity();
                            double discDiscount = disc.getDiscount();
                            if (totalOrder > discQuan && discDiscount > discount) {
                                discount = discDiscount;
                            }
                        }
                        stmt = c.createStatement();
                        String sql = "INSERT INTO Order_Items VALUES(" + order + ", '" + item.getSerialNumber() + "', '" + item.getCatalogNumber() + "', " + (Integer) entry.getValue() + ", " + item.getPrice() + ", " + discount + ", '" + entry.getKey() + "'," + 0 + ");";
                        stmt.executeUpdate( sql );
                        stmt.close();
                    }
                }
            }
            int newsum = sumWeight(order, sum);
            if(newsum == 0){
                removeOrder(order);
            }
            //if sum == 0, delete order
            if (couldntBeInserted.size() > 0) {
                LinkedList<Item> toIter = new LinkedList<>(couldntBeInserted);
                for(Item item : toIter){
                    if( 8000000 <totalWeightOfHashMap(item.getStoreQuantity(), item.getSerialNumber())){
                        //split to two items.
                        HashMap<String, Integer> hashOne = new HashMap<>();
                        HashMap<String, Integer> hashTwo = new HashMap<>();
                        for(Map.Entry entry : item.getStoreQuantity().entrySet()){
                            String store = (String)entry.getKey();
                            int quantity = (Integer)entry.getValue();
                            int partOne = 0;
                            int partTwo = 0;
                            if(quantity%2 == 0){
                                partOne = quantity/2;
                                partTwo = quantity/2;
                            }
                            else {
                                partOne = quantity/2;
                                partTwo = quantity/2 + 1;
                            }
                            hashOne.put(store, partOne);
                            hashTwo.put(store, partTwo);
                        }
                        Item one = new Item(item.getCatalogNumber(), item.getPrice(), item.getSerialNumber(), item.getDiscounts(), hashOne);
                        Item two = new Item(item.getCatalogNumber(), item.getPrice(), item.getSerialNumber(),  item.getDiscounts(), hashTwo);
                        couldntBeInserted.remove(item);
                        couldntBeInserted.add(one);
                        couldntBeInserted.add(two);
                    }
                }
                //make new order with these items:
                String supID = supplier.getId();
                String dateNow = LocalDate.now().toString();
                int newOrder = addNewOrder(supID, dateNow, "1" );
                addItemsToOrder(newOrder, couldntBeInserted);
            }
        } catch (SQLException ignored) {
            return false;
        }
        return true;
    }

    public int totalWeightOfHashMap(HashMap<String, Integer> map, String serialNum) {
        int totalsum = 0;
        for (Map.Entry entry : map.entrySet()) {
            totalsum += (Integer) entry.getValue();
        }
        Product p = getProductDetails(serialNum);
        totalsum = totalsum * p.getWeight();
        return totalsum;
    }

    //TODO changed
    public int sumWeight(int order, int sum) {
        int ans = 0;
        try {
            int currsum = getOrderWeight( order );
            int newsum = currsum + sum;
            ans = newsum;
            stmt = c.createStatement();
            stmt.executeUpdate( "UPDATE Orders " +
                    "SET weight=" + newsum +
                    " WHERE order_number=" + order + ";" );
            stmt.close();

        } catch (SQLException ignored) {
            return 0;
        }
        return ans;
    }

    public int getOrderWeight(int order) {
        try {


            stmt = c.createStatement();
            String sql = "SELECT weight FROM Orders WHERE order_number = " + order + ";";
            ResultSet rs = stmt.executeQuery( sql );
            rs.next();
            int currsum = rs.getInt( 1 );
            stmt.close();
            return currsum;
        } catch (SQLException e) {
            System.out.println( e.getMessage() );
        }
        return 0;
    }

    //TODO changed
    public int addNewOrder(String supplier, String date, String executed) {
        int ans = 0;
        try {
            stmt = c.createStatement();
            stmt.executeUpdate( "INSERT INTO Orders(supplier_id, date, executed, weight) VALUES('" + supplier + "', '" + date + "', '" + executed + "'," + 0 + " );" );
            ResultSet rs = stmt.getGeneratedKeys();
            rs.next();
            ans = rs.getInt( 1 );
            stmt.close();
        } catch (SQLException e) {
            return -1;
        }
        return ans;
    }

    /**
     * Add new category to Categories table
     *
     * @param category the category's name
     * @return Its new ID, 0 if failed
     */
    public int addNewCategory(String category) {
        int newId;
        try {
            stmt = c.createStatement();
            stmt.executeUpdate( "INSERT INTO Categories(CategoryName, FatherCategory) VALUES ('" + category + "', " + "NULL);" );
            newId = findCategory( category );
            stmt.close();
        } catch (SQLException e) {
            return 0;
        }
        return newId;
    }

    /**
     * puts item by given serial number on an existing sale
     *
     * @param serialNumber product's serial number
     * @param saleId       the sale id
     * @return true if succeeded, else false
     */
    public boolean addItemToSaleByProduct(String serialNumber, int saleId) {
        try {
            stmt = c.createStatement();
            stmt.executeUpdate( "INSERT INTO ItemsOnSale " +
                    "VALUES (" + saleId + ", '" + serialNumber + "')" );
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }


    //DELETES
    public boolean deleteItemOfSupplier(String supplier_id, String item) {
        try {
            stmt = c.createStatement();
            String sql = "DELETE FROM Items_In_Contract WHERE supplier_id = " + supplier_id + " AND catalog_number = " + item + ";";
            stmt.executeUpdate( sql );
            sql = "DELETE FROM Discounts WHERE supplier_id = '" + supplier_id + "'AND catalog_number = " + item + ";";
            stmt.executeUpdate( sql );
            sql = "DELETE FROM StoresOfItemsInContract WHERE supplier_id = '" + supplier_id + "' AND catalog_number = " + item + ";";
            stmt.executeUpdate( sql );
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean deleteContact(String supplier_id, String phoneNum, boolean deleteAll) {
        try {
            stmt = c.createStatement();
            if (this.getContacts( supplier_id ).size() != 1 || deleteAll) {
                String sql = "DELETE FROM Contacts_Of_Suppliers WHERE supplier_id = '" + supplier_id + "' AND phone_number = '" + phoneNum + "';";
                stmt.executeUpdate( sql );
                sql = "SELECT COUNT (*) > 0 FROM Contacts_Of_Suppliers WHERE phone_number = '" + phoneNum + "' ;";
                ResultSet rs = stmt.executeQuery( sql );
                if (rs.next()) {
                    boolean ans = rs.getBoolean( 1 );
                    if (!ans) {
                        sql = "DELETE FROM Contacts WHERE phone_number = '" + phoneNum + "';";
                        stmt.executeUpdate( sql );
                    }
                }
                stmt.close();
            } else {
                return false;
            }
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    //todo
    public boolean deleteSupplier(String id) {
        try {
            stmt = c.createStatement();
            String sql = "SELECT phone_number FROM Contacts_Of_Suppliers WHERE supplier_id = '" + id + "';";
            ResultSet rs = stmt.executeQuery( sql );
            while (rs.next()) {
                String phoneNum = rs.getString( 1 );
                this.deleteContact( id, phoneNum, true );
            }
            sql = "DELETE FROM Discounts WHERE supplier_id = '" + id + "';";
            stmt.executeUpdate( sql );
            sql = "DELETE FROM Items_In_Contract WHERE supplier_id = '" + id + "';";
            stmt.executeUpdate( sql );
            sql = "DELETE FROM Suppliers WHERE id = '" + id + "';";
            stmt.executeUpdate( sql );
            sql = "DELETE FROM StoresOfItemsInContract WHERE supplier_id = '" + id + "';";
            stmt.executeUpdate( sql );
            LinkedList<Order> orders = getOrderBySupplier( id );
            for (Order order : orders) {
                removeAllItemsFromOrder( order.getOrder_number() );
                removeOrder( order.getOrder_number() );
            }
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean deleteDiscount(String supplierId, String catalog_number, int quantity) {
        try {
            stmt = c.createStatement();
            String sql = "DELETE FROM Discounts WHERE supplier_id = " + supplierId + " AND quantity = " + quantity + " AND catalog_number = " + catalog_number + ";";
            stmt.executeUpdate( sql );
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean removeCategory(int catID) {
        try {
            stmt = c.createStatement();
            stmt.executeUpdate( "UPDATE Products SET Category = NULL WHERE Category = " + catID );
            stmt.executeUpdate( "DELETE FROM Categories " +
                    "WHERE ID=" + catID );
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean removeProduct(String serialNum) {
        try {
            stmt = c.createStatement();
            stmt.executeUpdate( "DELETE FROM Products WHERE SerialNumber='" + serialNum + "'" );
            stmt.executeUpdate( "DELETE FROM Inventory WHERE SerialNumber='" + serialNum + "'" );
            stmt.executeUpdate( "DELETE FROM ItemsOnSale WHERE ProductId ='" + serialNum + "'" );

            removeItemsInContract( serialNum );
            removeDeletedItemFromAllOrders( serialNum );
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    private void removeDeletedItemFromAllOrders(String serialNum) {
        try {
            LinkedList<Integer> orderNums = getOrderNumberThatHaveItem( serialNum );
            stmt = c.createStatement();
            stmt.executeUpdate( "DELETE FROM Order_Items WHERE serialNumber = '" + serialNum + "';" );
            for (Integer i : orderNums) {
                if (getNumberOfItemsInOrder( i ) == 0) {
                    removeOrder( i );
                }
            }
            stmt.close();
        } catch (SQLException ignored) {
        }
        return;
    }

    public boolean removeOrder(int orderNumber) {
        try {
            stmt = c.createStatement();
            String sql = "DELETE FROM Order_Items WHERE order_number = " + orderNumber + ";";
            stmt.executeUpdate( sql );
            sql = "DELETE FROM Orders WHERE order_number = " + orderNumber + ";";
            stmt.executeUpdate(sql);
            sql = "DELETE FROM Notifications WHERE OrderNo = " + orderNumber +";";
            stmt.executeUpdate(sql);

            stmt.close();
        } catch (SQLException e) {
            System.out.println( e.getMessage() );
            return false;
        }
        return true;
    }

    private LinkedList<Integer> getOrderNumberThatHaveItem(String serialNum) {
        LinkedList<Integer> ans = new LinkedList<Integer>();
        try {
            stmt = c.createStatement();
            String sql = "SELECT order_number FROM Order_Items WHERE serial_number = '" + serialNum + "';";
            ResultSet rs = stmt.executeQuery( sql );
            while (rs.next()) {
                ans.add( rs.getInt( 1 ) );
            }
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return ans;
    }

    //todo
    private boolean removeItemsInContract(String serialNum) {
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT catalog_number FROM Items_In_Contract WHERE serialNumber = '" + serialNum + "';" );
            stmt.executeUpdate( "DELETE FROM Items_In_Contract WHERE serialNumber = '" + serialNum + "';" );
            stmt.close();
            while (rs.next()) {
                String catNum = rs.getString( 1 );
                stmt.executeUpdate( "DELETE FROM StoresOfItemsInContract WHERE catalog_number = '" + catNum + "';" );
            }
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean removeSale(int saleId) {
        try {
            stmt = c.createStatement();
            stmt.executeUpdate( "DELETE FROM ItemsOnSale " +
                    "WHERE SaleId=" + saleId );
            stmt.executeUpdate( "DELETE FROM Sales " +
                    "WHERE Id=" + saleId );
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    /**
     * Remove product from an existing sale
     *
     * @param saleId       the sale
     * @param serialNumber the product's serial number
     */
    public boolean removeItemFromSale(int saleId, String serialNumber) {
        try {
            stmt = c.createStatement();
            stmt.executeUpdate( "DELETE FROM ItemsOnSale " +
                    "WHERE SaleId=" + saleId + " AND ProductId='" + serialNumber + "'" );
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    //todo
    public boolean removeItemFromOrderThatHasNotBeenSentYet(int orderNum, String serialNum, String store) {
        if (getExecutedState( orderNum ) == 1) {
            try {
                stmt = c.createStatement();
                String sql = "DELETE FROM Order_Items WHERE order_number= " + orderNum + " AND serial_number = " + serialNum + " AND store = '" + store + "';";
                stmt.executeUpdate( sql );
                if (getNumberOfItemsInOrder( orderNum ) == 0) {
                    removeOrderThatHasNotBeenSentYet( orderNum );
                }
                stmt.close();
            } catch (SQLException e) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    private int getNumberOfItemsInOrder(int orderNum) {
        int ans = -1;
        try {
            stmt = c.createStatement();
            String sql = "SELECT COUNT(*) FROM Order_Items WHERE order_number = " + orderNum + ";";
            ResultSet rs = stmt.executeQuery( sql );
            if (rs.next()) {
                ans = rs.getInt( 1 );
                return ans;
            }
            stmt.close();
        } catch (SQLException e) {
            return ans;
        }
        return ans;
    }

    public boolean removeOrderThatHasNotBeenSentYet(int orderNum) {
        if (getExecutedState( orderNum ) == 1) {
            try {
                stmt = c.createStatement();
                String sql = "DELETE FROM Orders WHERE order_number = " + orderNum + ";";
                stmt.executeUpdate(sql);
                removeAllItemsFromOrder(orderNum);
                sql = "DELETE FROM Notifications WHERE OrderNo = " + orderNum +";";
                stmt.executeUpdate(sql);
                removeAllItemsFromOrder( orderNum );
                stmt.executeUpdate( sql );
                stmt.close();
            } catch (SQLException e) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    }


    public boolean removeAllItemsFromOrder(int orderNum) {
        try {
            stmt = c.createStatement();
            String sql = "DELETE FROM Order_Items WHERE order_number= " + orderNum + ";";
            stmt.executeUpdate( sql );
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }


    //UPDATES
    public boolean updateSaleField(String field, String fieldValue, int saleId) {
        SaleFields parsedField = SaleFields.parse( field );
        if (parsedField == null) return false;

        try {
            stmt = c.createStatement();
            stmt.executeUpdate( "UPDATE Sales SET " + parsedField + " = '" + fieldValue + "' WHERE Id = " + saleId + ";" );
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean updateProductField(String field, String fieldValue, String serialNumber) {
        ProductFields parsedField = ProductFields.parse( field );
        if (parsedField == null) return false;
        try {
            stmt = c.createStatement();
            stmt.executeUpdate( "UPDATE Products SET " + parsedField + " = '" + fieldValue + "' " +
                    "WHERE SerialNumber = " + serialNumber + ";" );
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean updateCategoryField(String field, String fieldValue, int id) {
        CategoryFields parsedField = CategoryFields.parse( field );
        if (parsedField == null) return false;

        try {
            stmt = c.createStatement();
            stmt.executeUpdate( "UPDATE Categories SET " + parsedField + " = '" + fieldValue + "' WHERE ID=" + id + ";" );
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean updateItemPrice(String id, String catalogNum, double price) {
        try {
            stmt = c.createStatement();
            stmt.executeUpdate( "UPDATE Items_In_Contract SET price = " + price + " WHERE supplier_id = " + id + " AND catalog_number = " + catalogNum + ";" );
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    //todo
    public boolean UpdateQuantityItemInPereodicOrder(String id, String catalogNum, int quantity, String store) {
        try {
            stmt = c.createStatement();
            stmt.executeUpdate( "UPDATE StoresOfItemsInContract SET quantity = " + quantity + " WHERE supplier_id = '" + id + "' AND catalog_number = '" + catalogNum + "' AND store = '" + store + "';" );
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean updateExecutedInOrders(int order_number, String executed) {
        try {
            stmt = c.createStatement();
            stmt.executeUpdate( "UPDATE Orders SET executed='" + executed + "' WHERE order_number=" + order_number );
            stmt.close();
        } catch (SQLException e) {
            System.out.println( e.getMessage() );
            return false;
        }
        return true;
    }

    /**
     * Sets product's category by its serial number
     *
     * @param serialNumber
     * @param categoryId
     * @return true if succeeded, else false
     */
    public boolean setProductsCategoryBySerialNumber(String serialNumber, int categoryId) {
        try {
            stmt = c.createStatement();
            stmt.executeUpdate( "UPDATE Products " +
                    "SET Category=" + categoryId + " WHERE SerialNumber='" + serialNumber + "'" );
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    /**
     * Sets product's category by its name
     *
     * @param name
     * @param categoryId
     * @return true if succeeded, else false
     */
    public boolean setProductsCategoryByName(String name, int categoryId) {
        try {
            stmt = c.createStatement();
            stmt.executeUpdate( "UPDATE Products " +
                    "SET Category=" + categoryId + " WHERE ProductName='" + name + "'" );
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    //todo
    public boolean orderArrived(int order_number, String store) {
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT serial_number,quantity FROM Order_Items " +
                    "WHERE Order_number = " + order_number + " AND store = '" + store + "';" );
            while (rs.next()) {
                stmt.executeUpdate( "UPDATE Inventory SET storageQuantity = storageQuantity + " + rs.getInt( 2 ) + " WHERE SerialNumber ='" + rs.getString( 1 ) + "' AND store = '" + store + "';" );
            }
            //TODO update statusof order items
            //TODO! check if all items arrived, only than change.
            updateExecutedInOrders( order_number, "3" );
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean updateSupplierAddress(String supId, String address) {
        try {
            stmt = c.createStatement();
            stmt.executeUpdate( "UPDATE Suppliers SET address = '" + address + "' WHERE id = " + supId + ";" );
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean updateInventoryShelfLocation(String productSerialNumber, String location, String store) {
        try {
            stmt = c.createStatement();
            stmt.executeUpdate( "UPDATE Inventory " +
                    "SET shelfLocation='" + location + "'" +
                    " WHERE SerialNumber='" + productSerialNumber + "' AND store='" + store + "'" );
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean updateInventoryStorageLocation(String productSerialNumber, String location, String store) {
        try {
            stmt = c.createStatement();
            stmt.executeUpdate( "UPDATE Inventory " +
                    "SET storageLocation='" + location + "'" +
                    " WHERE SerialNumber='" + productSerialNumber + "' AND store='" + store + "'" );
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean updateInventoryMinimalQuantity(String productSerialNumber, String store, int minimum) {
        try {
            stmt = c.createStatement();
            stmt.executeUpdate( "UPDATE Inventory " +
                    "SET MinimalQuantity=" + minimum +
                    " WHERE SerialNumber='" + productSerialNumber + "' AND store='" + store + "'" );
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean updateInventoryQuantity(String productSerialNumber, String store, int loc, int quantity) {
        boolean ans;
        if (loc == 1) {
            ans = updateInventoryStorageQuantity( productSerialNumber, store, quantity );
        } else {
            ans = updateInventoryShelfQuantity( productSerialNumber, store, quantity );
        }
        return ans;
    }

    public boolean updateInventoryShelfQuantity(String productSerialNumber, String store, int quantity) {
        try {
            stmt = c.createStatement();
            stmt.executeUpdate( "UPDATE Inventory " +
                    "SET shelfQuantity=" + quantity +
                    " WHERE SerialNumber='" + productSerialNumber + "' AND store='" + store + "'" );
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean updateInventoryStorageQuantity(String productSerialNumber, String store, int quantity) {
        try {
            stmt = c.createStatement();
            stmt.executeUpdate( "UPDATE Inventory " +
                    "SET storageQuantity=" + quantity +
                    " WHERE SerialNumber='" + productSerialNumber + "' AND store='" + store + "'" );
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean updateInventoryShelfDefQuantity(String productSerialNumber, String store, int defQuantity) {
        try {
            stmt = c.createStatement();
            stmt.executeUpdate( "UPDATE Inventory " +
                    "SET shelfDefectives=" + defQuantity +
                    " WHERE SerialNumber='" + productSerialNumber + "' AND store='" + store + "'" );
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean updateInventoryStorageDefQuantity(String productSerialNumber, String store, int defQuantity) {
        try {
            stmt = c.createStatement();
            stmt.executeUpdate( "UPDATE Inventory " +
                    "SET storageDefectives=" + defQuantity +
                    " WHERE SerialNumber='" + productSerialNumber + "' AND store='" + store + "'" );
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    //todo
    public boolean updateItemQuantityInLacksOfInventory(int order_number, String serialNumber, int quantity, String store) {
        removeItemFromOrderThatHasNotBeenSentYet( order_number, serialNumber, store );
        return orderWhenLackOfStock( serialNumber, quantity, store );
    }


    //GETS
    //todo
    public LinkedList<Item> getSupplierItems(String supplier_id) {
        LinkedList<Item> ans = new LinkedList<Item>();
        try {
            stmt = c.createStatement();
            String sql = "SELECT * FROM Items_In_Contract WHERE supplier_id = " + supplier_id + ";";
            ResultSet rs = stmt.executeQuery( sql );
            while (rs.next()) {
                Item newItem = new Item( rs.getString( 2 ), rs.getDouble( 4 ), rs.getString( 3 ), getDiscounts( supplier_id, rs.getString( 2 ) ), null );
                ans.add( newItem );
                newItem.setStoreQuantity( getStoresAndQuantity( supplier_id, newItem.getCatalogNumber() ) );
            }
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return ans;
    }

    //todo
    private HashMap<String, Integer> getStoresAndQuantity(String supplier_id, String catNum) {
        HashMap<String, Integer> ans = new HashMap<>();
        try {
            stmt = c.createStatement();
            String sql = "SELECT * FROM StoresOfItemsInContract WHERE supplier_id =  '" + supplier_id + "' AND catalog_number = '" + catNum + "';";
            ResultSet rs = stmt.executeQuery( sql );
            while (rs.next()) {
                ans.put( rs.getString( 3 ), rs.getInt( 4 ) );
            }
            stmt.close();
        } catch (SQLException e) {
            return new HashMap<String, Integer>();
        }
        return ans;
    }

    //todo
    private LinkedList<OrderItem> getOrderItems(int order_number, String supplierID) {
        LinkedList<OrderItem> ans = new LinkedList<OrderItem>();
        try {
            stmt = c.createStatement();
            String sql = "SELECT * FROM Order_Items WHERE order_number = " + order_number + ";";
            ResultSet rs = stmt.executeQuery( sql );
            while (rs.next()) {
                String name = getProductName( rs.getString( 2 ) );
                ans.add( new OrderItem( supplierID, rs.getString( 2 ), rs.getString( 3 ), rs.getInt( 4 ), rs.getDouble( 5 ), rs.getDouble( 6 ), name, rs.getString( 7 ), rs.getInt( 8 ) ) );
            }
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return ans;
    }

    //todo
    private LinkedList<OrderItem> getOrderItemsForStore(int order_number, String supplierID, String store) {
        LinkedList<OrderItem> ans = new LinkedList<OrderItem>();
        try {
            stmt = c.createStatement();
            String sql = "SELECT * FROM Order_Items WHERE order_number = " + order_number + " AND store = '" + store + "';";
            ResultSet rs = stmt.executeQuery( sql );
            while (rs.next()) {
                String name = getProductName( rs.getString( 2 ) );
                ans.add( new OrderItem( supplierID, rs.getString( 2 ), rs.getString( 3 ), rs.getInt( 4 ), rs.getDouble( 5 ), rs.getDouble( 6 ), name, rs.getString( 7 ), rs.getInt( 8 ) ) );
            }
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return ans;
    }

    public LinkedList<Order> getAllOrders() {
        LinkedList<Order> ans = new LinkedList<Order>();
        try {
            stmt = c.createStatement();
            String sql = "SELECT * FROM Orders;";
            ResultSet rs = stmt.executeQuery( sql );
            while (rs.next()) {
                ans.add( buildOrder( rs ) );
            }
            stmt.close();
        } catch (SQLException e) {
            return new LinkedList<Order>();
        }
        return ans;
    }

    public LinkedList<Order> getOrderBySupplier(String supplierID) {
        LinkedList<Order> ans = new LinkedList<Order>();
        try {
            stmt = c.createStatement();
            String sql = "SELECT * FROM Orders WHERE supplier_id =" + supplierID + ";";
            ResultSet rs = stmt.executeQuery( sql );
            while (rs.next()) {
                ans.add( buildOrder( rs ) );
            }
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return ans;
    }

    public LinkedList<Order> getOrderByOrderNumber(int orderNumber) {
        LinkedList<Order> ans = new LinkedList<Order>();
        try {
            stmt = c.createStatement();
            String sql = "SELECT * FROM Orders WHERE order_number =" + orderNumber + ";";
            ResultSet rs = stmt.executeQuery( sql );
            while (rs.next()) {
                ans.add( buildOrder( rs ) );
            }
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return ans;
    }

    private String getCategoryName(int fatherCategory) {
        String ans;

        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM Categories WHERE ID=" + fatherCategory );
            ans = rs.getString( 2 );
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return ans;
    }

    /**
     * Fetch products report from Products table.
     *
     * @return List of all the products
     */
    public ArrayList<Product> getAllProducts() {
        ArrayList<Product> ans = new ArrayList<Product>();

        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT SerialNumber, Products.ProductName, Manufacturer, Category, " +
                    "Categories.CategoryName, FatherCategory, Products.Size, Cost, SellingPrice FROM Products JOIN Categories ON Category=ID" );
            while (rs.next()) {
                Category cat = new Category( rs.getInt( 4 ), rs.getString( 5 ), rs.getInt( 6 ) );
                ans.add( new Product( rs.getString( 1 ), rs.getString( 2 ), rs.getString( 3 ),
                        cat, rs.getInt( 7 ), rs.getInt( 8 ), rs.getInt( 9 ) ) );
            }
            rs = stmt.executeQuery( "SELECT SerialNumber, ProductName, Manufacturer, " +
                    "Size, Cost, SellingPrice FROM Products WHERE Category IS NULL" );
            while (rs.next()) {
                ans.add( new Product( rs.getString( 1 ), rs.getString( 2 ), rs.getString( 3 ),
                        null, rs.getInt( 4 ), rs.getInt( 5 ), rs.getInt( 6 ) ) );
            }
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return ans;
    }

    /**
     * Fetch inventory report from Inventory table.
     *
     * @return List of all the products
     */
    public ArrayList<Stock> getAllInventory() {
        ArrayList<Stock> ans = new ArrayList<>();

        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT I.SerialNumber, ProductName, MinimalQuantity, shelfLocation, " +
                    "storageLocation, shelfQuantity, storageQuantity, shelfDefectives, storageDefectives, store " +
                    "FROM Inventory AS I JOIN Products AS P ON I.SerialNumber=P.SerialNumber" );
            while (rs.next()) {
                Stock stock = new Stock( rs.getString( 1 ), rs.getInt( 3 ),
                        rs.getString( 4 ), rs.getString( 5 ), rs.getInt( 6 ),
                        rs.getInt( 7 ), rs.getInt( 8 ), rs.getInt( 9 ), rs.getString( 10 ) );
                stock.setProductName( rs.getString( 2 ) );
                ans.add( stock );
            }
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return ans;
    }

    /**
     * Fetch inventory report from Inventory table.
     *
     * @return List of all the products
     */
    public ArrayList<Stock> getAllInventory(String store) {
        ArrayList<Stock> ans = new ArrayList<>();

        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT I.SerialNumber, ProductName, MinimalQuantity, shelfLocation, " +
                    "storageLocation, shelfQuantity, storageQuantity, shelfDefectives, storageDefectives, store " +
                    "FROM Inventory AS I JOIN Products AS P ON I.SerialNumber=P.SerialNumber WHERE store='" + store + "'" );
            while (rs.next()) {
                Stock stock = new Stock( rs.getString( 1 ), rs.getInt( 3 ),
                        rs.getString( 4 ), rs.getString( 5 ), rs.getInt( 6 ),
                        rs.getInt( 7 ), rs.getInt( 8 ), rs.getInt( 9 ), rs.getString( 10 ) );
                stock.setProductName( rs.getString( 2 ) );
                ans.add( stock );
            }
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return ans;
    }

    /**
     * Fetch products report from Products table of a given category by name.
     *
     * @return List of all the products of the given category
     */
    public ArrayList<Product> getAllProducts(String catName) {
        ArrayList<Product> ans = new ArrayList<Product>();

        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT SerialNumber, Products.ProductName, Manufacturer, Category, " +
                    "Categories.CategoryName, FatherCategory, Products.Size, Cost, SellingPrice FROM Products JOIN Categories ON Category=ID " +
                    "WHERE Categories.CategoryName='" + catName + "'" );
            while (rs.next()) {
                Category cat = new Category( rs.getInt( 4 ), rs.getString( 5 ), rs.getInt( 6 ) );
                ans.add( new Product( rs.getString( 1 ), rs.getString( 2 ), rs.getString( 3 ),
                        cat, rs.getInt( 7 ), rs.getInt( 8 ), rs.getInt( 9 ) ) );
            }
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return ans;
    }

    /**
     * Fetch products report from Products table of a given category by name.
     *
     * @return List of all the products of the given category
     */
    public ArrayList<Product> getAllProducts(int catId) {
        ArrayList<Product> ans = new ArrayList<Product>();

        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT SerialNumber, Products.ProductName, Manufacturer, Category, " +
                    "Categories.CategoryName, FatherCategory, Products.Size, Cost, SellingPrice FROM Products JOIN Categories ON Category=ID " +
                    "WHERE Categories.ID=" + catId );
            while (rs.next()) {
                Category cat = new Category( rs.getInt( 4 ), rs.getString( 5 ), rs.getInt( 6 ) );
                ans.add( new Product( rs.getString( 1 ), rs.getString( 2 ), rs.getString( 3 ),
                        cat, rs.getInt( 7 ), rs.getInt( 8 ), rs.getInt( 9 ) ) );
            }
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return ans;
    }

    /**
     * Fetch defectives products report
     *
     * @return List of all defectives products
     */
    public ArrayList<Stock> getAllDefectives() {
        ArrayList<Stock> ans = new ArrayList<Stock>();

        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM Inventory WHERE " +
                    "shelfDefectives>0 OR storageDefectives>0" );
            while (rs.next()) {
                ans.add( new Stock( rs.getString( 1 ), rs.getInt( 2 ),
                        rs.getString( 3 ), rs.getString( 4 ), rs.getInt( 5 ),
                        rs.getInt( 6 ), rs.getInt( 7 ), rs.getInt( 8 ), rs.getString( 9 ) ) );
            }
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return ans;
    }

    /**
     * Fetch defectives products report
     *
     * @return List of all defectives products
     */
    public ArrayList<Stock> getAllDefectives(String store) {
        ArrayList<Stock> ans = new ArrayList<Stock>();

        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM Inventory WHERE store= '" + store + "' AND " +
                    "(shelfDefectives>0 OR storageDefectives>0)" );
            while (rs.next()) {
                ans.add( new Stock( rs.getString( 1 ), rs.getInt( 2 ),
                        rs.getString( 3 ), rs.getString( 4 ), rs.getInt( 5 ),
                        rs.getInt( 6 ), rs.getInt( 7 ), rs.getInt( 8 ), rs.getString( 9 ) ) );
            }
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return ans;
    }

    /**
     * Find category by its name
     *
     * @param category
     * @return The category ID. if not found, return 0
     */
    public int findCategory(String category) {
        int newId;
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT ID FROM Categories WHERE CategoryName = '" + category + "';" );
            if (rs.next()) {
                newId = rs.getInt( 1 );
            } else {
                newId = 0;
            }
            stmt.close();
        } catch (SQLException e) {
            return 0;
        }
        return newId;
    }

    /**
     * Get all the sales
     *
     * @return List of all sales in the DB
     */
    public ArrayList<Sale> getAllSales() {
        ArrayList<Sale> ans = new ArrayList<Sale>();
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM Sales" );
            while (rs.next()) {
                Sale sale = new Sale( rs.getInt( 1 ), rs.getString( 2 ), rs.getString( 3 ),
                        rs.getInt( 4 ) );
                ans.add( sale );
            }
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return ans;
    }

    /**
     * Get all current shortages
     *
     * @return List of all shortages
     */
    public ArrayList<Stock> getLowQuantity() {
        ArrayList<Stock> ans = new ArrayList<Stock>();
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM Inventory WHERE " +
                    "(shelfQuantity+storageQuantity-shelfDefectives-storageDefectives)<MinimalQuantity" );
            while (rs.next()) {
                ans.add( new Stock( rs.getString( 1 ), rs.getInt( 2 ),
                        rs.getString( 3 ), rs.getString( 4 ), rs.getInt( 5 ),
                        rs.getInt( 6 ), rs.getInt( 7 ), rs.getInt( 8 ), rs.getString( 9 ) ) );
            }
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return ans;
    }

    /**
     * Get all current shortages
     *
     * @return List of all shortages
     */
    public ArrayList<Stock> getLowQuantity(String store) {
        ArrayList<Stock> ans = new ArrayList<Stock>();
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM Inventory WHERE store='" + store + "' AND " +
                    "(shelfQuantity+storageQuantity-shelfDefectives-storageDefectives)<MinimalQuantity" );
            while (rs.next()) {
                ans.add( new Stock( rs.getString( 1 ), rs.getInt( 2 ),
                        rs.getString( 3 ), rs.getString( 4 ), rs.getInt( 5 ),
                        rs.getInt( 6 ), rs.getInt( 7 ), rs.getInt( 8 ), rs.getString( 9 ) ) );
            }
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return ans;
    }

    /**
     * Get all categories
     *
     * @return List of all categories
     */
    public ArrayList<Category> getAllCategories() {
        ArrayList<Category> ans = new ArrayList<Category>();
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM Categories" );
            while (rs.next()) {
                Category cat = new Category( rs.getInt( 1 ), rs.getString( 2 ),
                        rs.getInt( 3 ) );
                if (cat.getFatherCategory() != 0)
                    cat.setFatherCategoryName( getCategoryName( cat.getFatherCategory() ) );
                ans.add( cat );
            }
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return ans;
    }

    /**
     * fetch from db all items on a given sale
     *
     * @param saleId the sale's id
     * @return list of all the products on the sale
     */
    public ArrayList<Product> getAllItemsOnSale(int saleId) {
        ArrayList<Product> ans = new ArrayList<Product>();
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT ProductId FROM ItemsOnSale WHERE SaleId= " + saleId );
            while (rs.next()) {
                String serialNumber = rs.getString( 1 );
                Product p = getProductDetails( serialNumber );
                if (p != null)
                    ans.add( p );
            }
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return ans;
    }

    private LinkedList<Contact> getContacts(String supplierId) {
        LinkedList<Contact> ans = new LinkedList<>();

        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT Contacts.name, Contacts.phone_number FROM Contacts_Of_Suppliers JOIN Contacts ON " +
                    "Contacts_Of_Suppliers.phone_number=Contacts.phone_number WHERE supplier_id='" + supplierId + "'" );
            while (rs.next()) {
                Contact contact = new Contact( rs.getString( 1 ), rs.getString( 2 ) );
                ans.add( contact );
            }
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return ans;
    }

    public Product getProductDetails(String serialNum) {
        Product ans = null;

        try {
            stmt = c.createStatement();
            if (isCategoryNull( serialNum )) {
                ResultSet rs = stmt.executeQuery( "SELECT SerialNumber, ProductName, Manufacturer, " +
                        "Size, Cost, SellingPrice FROM Products WHERE SerialNumber='" + serialNum + "'" );
                while (rs.next()) {
                    ans = new Product( rs.getString( 1 ), rs.getString( 2 ), rs.getString( 3 ),
                            null, rs.getInt( 4 ), rs.getInt( 5 ), rs.getInt( 6 ) );
                }
            } else {
                ResultSet rs = stmt.executeQuery( "SELECT SerialNumber, Products.ProductName, Manufacturer, Category, " +
                        "Categories.CategoryName, FatherCategory, Products.Size, Cost, SellingPrice FROM Products JOIN Categories ON Category=ID" +
                        " WHERE SerialNumber='" + serialNum + "'" );
                while (rs.next()) {
                    Category cat = new Category( rs.getInt( 4 ), rs.getString( 5 ), rs.getInt( 6 ) );
                    ans = new Product( rs.getString( 1 ), rs.getString( 2 ), rs.getString( 3 ),
                            cat, rs.getInt( 7 ), rs.getInt( 8 ), rs.getInt( 9 ) );
                }
            }
            stmt.close();
        } catch (SQLException e) {
            return ans;
        }
        return ans;
    }

    public boolean isCategoryNull(String serialNum) {
        boolean ans = false;
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT Category FROM Products WHERE SerialNumber='" + serialNum + "'" );
            while (rs.next()) {
                return ( rs.getString( 1 ) == null );
            }
        } catch (SQLException e) {
            return ans;
        }
        return ans;
    }

    private LinkedList<String> getAllSuppliersOnDay(String day) {
        LinkedList<String> suppliers = new LinkedList<String>();
        try {
            stmt = c.createStatement();
            String sql = "SELECT * FROM Suppliers WHERE supply_terms = 'SupplyOnSetDays';";
            ResultSet rs = stmt.executeQuery( sql );
            while (rs.next()) {
                String days = rs.getString( 7 );
                String[] daysArr = days.split( "," );
                List valid = Arrays.asList( daysArr );
                if (valid.contains( day )) {
                    suppliers.add( rs.getString( 1 ) );
                }
            }

            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return suppliers;
    }

    //todo
    public OrderItem orderFrom(String serialNumber, int quantity, String store) {
        //todo get all suppliers that are in the store region
        LinkedList<Supplier> suppliers = getAllSuppliers( store );
        LinkedList<Supplier> toRemove = new LinkedList<>();

        for (Supplier sup : suppliers) {
            //Remove ones that don't have the item
            LinkedList<Item> items = sup.getItems();
            boolean doesHeHaveIt = false;
            for (Item item : items) {
                if (item.getSerialNumber().equals( serialNumber )) {
                    doesHeHaveIt = true;
                }
            }
            if (!doesHeHaveIt || sup.getTerms() == SupplyTerms.SupplyOnSetDays) {
                toRemove.add( sup );
            }
        }
        for (Supplier sup : toRemove) {
            suppliers.remove( sup );
        }
        //Now we have only suppliers that have the item
        //We need to find the lowest price for the quantity
        if (suppliers.size() == 0) {
            return null;
        }
        double bestPrice = Integer.MAX_VALUE;
        String bestPriceSupplierID = "";
        double bestRegPrice = 0;
        String catalogNum = "";
        double discount = 0;
        String name = getProductName( serialNumber );
        for (Supplier sup : suppliers) {
            double supplierBestPrice = 0;
            double itemRegPrice = 0;
            double supplierBestDiscount = 0;
            //calculate the best price he can give
            LinkedList<Item> items = sup.getItems();
            String supplierCatalogNum = "";
            for (Item item : items) {
                if (item.getSerialNumber().equals( serialNumber )) {
                    supplierBestPrice = item.getPrice();
                    itemRegPrice = item.getPrice();
                    supplierCatalogNum = item.getCatalogNumber();
                }
            }
            LinkedList<Discount> discounts = this.getDiscounts( sup.getId(), supplierCatalogNum );
            //if there are discounts for the item
            for (Discount supplierDiscount : discounts) {
                if (supplierDiscount.getQuantity() <= quantity) {
                    double discountedPrice = itemRegPrice * ( 1 - supplierDiscount.getDiscount() );
                    if (discountedPrice < supplierBestPrice) {
                        supplierBestPrice = discountedPrice;
                        supplierBestDiscount = supplierDiscount.getDiscount();
                    }
                }
            }
            if (supplierBestPrice < bestPrice) {
                bestPrice = supplierBestPrice;
                bestPriceSupplierID = sup.getId();
                bestRegPrice = itemRegPrice;
                discount = supplierBestDiscount;
            }
        }
        catalogNum = getCatalogNumberFromSerialNumber( bestPriceSupplierID, serialNumber );
        return new OrderItem( bestPriceSupplierID, serialNumber, catalogNum, quantity, bestRegPrice, discount, name, store, 0 );
    }

    private String getCatalogNumberFromSerialNumber(String supplierId, String serialNum) {
        String ans;
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT catalog_number FROM Items_In_Contract WHERE supplier_id = " + supplierId + " AND serialNumber = " + serialNum + ";" );
            ans = rs.getString( 1 );
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return ans;
    }

    public LinkedList<Supplier> getAllSuppliers() {
        LinkedList<Supplier> ans = new LinkedList<Supplier>();
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM Suppliers;" );
            while (rs.next()) {
                String id = rs.getString( 1 );
                String name = rs.getString( 2 );
                String address = rs.getString( 3 );
                String bankacc = rs.getString( 4 );
                String payment = rs.getString( 5 );
                String supplyTerms = rs.getString( 6 );
                String days = rs.getString( 7 );
                LinkedList<Contact> contacts = this.getContacts( id );
                int region = getRegionOfStore( id );
                ans.add( new Supplier( id, name, address, bankacc, payment, contacts, stringToSupplyTerms( supplyTerms ), days, getSupplierItems( id ), region ) );
            }
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return ans;
    }

    private int getRegionOfSupplier(String id) {
        int ans;
        try {
            stmt = c.createStatement();
            String sql = "SELECT region FROM Suppliers WHERE id = " + id + ";";
            ResultSet rs = stmt.executeQuery( sql );
            rs.next();
            ans = rs.getInt( 1 );
            stmt.close();
        } catch (SQLException e) {
            return 0;
        }
        return ans;
    }

    //todo
    public LinkedList<Supplier> getAllSuppliers(String store) {
        LinkedList<Supplier> ans = new LinkedList<Supplier>();
        int region = getRegionOfStore( store );
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM Suppliers WHERE region = " + region + ";" );
            while (rs.next()) {
                String id = rs.getString( 1 );
                String name = rs.getString( 2 );
                String address = rs.getString( 3 );
                String bankacc = rs.getString( 4 );
                String payment = rs.getString( 5 );
                String supplyTerms = rs.getString( 6 );
                String days = rs.getString( 7 );
                LinkedList<Contact> contacts = this.getContacts( id );
                ans.add( new Supplier( id, name, address, bankacc, payment, contacts, stringToSupplyTerms( supplyTerms ), days, getSupplierItems( id ), region ) );
            }
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return ans;
    }

    //todo
    public int getRegionOfStore(String store) {
        int ans;
        try {
            stmt = c.createStatement();
            String sql = "SELECT Region FROM stores WHERE Address = '" + store + "';";
            ResultSet rs = stmt.executeQuery( sql );
            rs.next();
            ans = rs.getInt( 1 );
            stmt.close();
        } catch (SQLException e) {
            return 0;
        }
        return ans;
    }


    public int getExecutedState(int orderNum) {
        int ans;
        try {
            stmt = c.createStatement();
            String sql = "SELECT * FROM Orders WHERE order_number = " + orderNum + ";";
            ResultSet rs = stmt.executeQuery( sql );
            rs.next();
            ans = rs.getInt( 4 );
            stmt.close();
        } catch (SQLException e) {
            return 0;
        }
        return ans;
    }

    /**
     * Gets a products name by its serial number
     *
     * @param serialNumber The products serial number
     * @return The products name
     */
    public String getProductName(String serialNumber) {
        String ans = "";

        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT ProductName FROM Products WHERE SerialNumber='" + serialNumber + "'" );
            ans = rs.getString( 1 );
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return ans;
    }

    /**
     * get a list of all the discounts supplier has on an item
     *
     * @param supplier_id   the supplier's id
     * @param catalogNumber the item's catalog number
     * @return linked list of discount objects
     */
    public LinkedList<Discount> getDiscounts(String supplier_id, String catalogNumber) {
        LinkedList<Discount> ans = new LinkedList<>();

        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT quantity, discount FROM Discounts" +
                    " WHERE supplier_id='" + supplier_id + "' AND catalog_number='" + catalogNumber + "';" );
            while (rs.next()) {
                Discount discount = new Discount( rs.getInt( 1 ), rs.getDouble( 2 ) );
                ans.add( discount );
            }
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return ans;
    }

    /**
     * get an Item
     *
     * @param supplierId the supplier's id
     * @param catalogNum the item's catalog number
     * @return Item object
     */
    //todo
    public Item getItem(String supplierId, String catalogNum) {
        Item ans = null;
        LinkedList<Discount> discounts = getDiscounts( supplierId, catalogNum );
        HashMap storesdiscount = getStoresAndQuantity( supplierId, catalogNum );

        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT serialNumber, price FROM Items_In_Contract" +
                    " WHERE supplier_id='" + supplierId + "' AND catalog_number='" + catalogNum + "'" );
            while (rs.next()) {
                ans = new Item( catalogNum, rs.getDouble( 2 ), rs.getString( 1 ), discounts, storesdiscount );
            }
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return ans;
    }

    /**
     * Returns the minimal quantity of a product
     *
     * @param serialNumber the product's serial number
     * @return the minimal quantity
     */
    public int getProductMinimalQuantity(String serialNumber, String store) {
        int ans = -1;

        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT MinimalQuantity FROM Inventory " +
                    "WHERE SerialNumber='" + serialNumber + "' AND store='" + store + "'" );
            while (rs.next()) {
                ans = rs.getInt( 1 );
            }
            stmt.close();
        } catch (SQLException e) {
            return -1;
        }
        return ans;
    }

    /**
     * get all supplier details
     *
     * @param supplierId the supplier's id
     * @return a Supplier object
     */
    public Supplier getSupplier(String supplierId) {
        Supplier ans = null;

        LinkedList<Contact> contacts = getContacts( supplierId );
        LinkedList<Item> items = getSupplierItems( supplierId );
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM Suppliers" +
                    " WHERE id='" + supplierId + "'" );
            while (rs.next()) {
                int region = getRegionOfSupplier(supplierId);
                ans = new Supplier( rs.getString( 1 ), rs.getString( 2 ), rs.getString( 3 ),
                        rs.getString( 4 ), rs.getString( 5 ), contacts,
                        stringToSupplyTerms( rs.getString( 6 ) ), rs.getString( 7 ), items, region );
            }
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return ans;
    }

    public ArrayList<Category> getAllSubCategories(ArrayList<String> categories) {
        ArrayList<Category> ans = new ArrayList<>();
        for (String cat : categories) {
            int id = getCategoryId( cat );
            try {
                stmt = c.createStatement();
                ResultSet rs = stmt.executeQuery( "SELECT * FROM Categories WHERE FatherCategory=" + id );
                while (rs.next()) {
                    Category subCat = new Category( rs.getInt( 1 ), rs.getString( 2 ), rs.getInt( 3 ) );
                    ans.add( subCat );
                }
                stmt.close();
            } catch (SQLException e) {
                return null;
            }
        }
        return ans;
    }

    public ArrayList<Category> getAllSubCategories(int catId) {
        ArrayList<Category> ans = new ArrayList<>();

        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM Categories WHERE FatherCategory=" + catId );
            while (rs.next()) {
                Category subCat = new Category( rs.getInt( 1 ), rs.getString( 2 ), rs.getInt( 3 ) );
                ans.add( subCat );
            }
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return ans;
    }

    public Stock getStock(String productSerialNumber, String store) {
        Stock ans = null;

        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM Inventory WHERE SerialNumber='" + productSerialNumber + "' AND store='" + store + "'" );
            while (rs.next()) {
                ans = new Stock( rs.getString( 1 ), rs.getInt( 2 ),
                        rs.getString( 3 ), rs.getString( 4 ), rs.getInt( 5 ),
                        rs.getInt( 6 ), rs.getInt( 7 ), rs.getInt( 8 ), rs.getString( 9 ) );
            }
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return ans;
    }

    public int getCategoryId(String catName) {
        int ans;
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT ID FROM Categories WHERE CategoryName='" + catName + "'" );
            if (rs.isClosed())
                return -1;
            rs.next();
            ans = rs.getInt( 1 );
            stmt.close();
        } catch (SQLException e) {
            return -1;
        }
        return ans;
    }

    //todo unused
    public LinkedList<Order> getOrdersThatHaveNotBeenSentYet() {
        LinkedList<Order> ans = new LinkedList<Order>();
        try {
            stmt = c.createStatement();
            String sql = "SELECT * FROM Orders WHERE executed = " + 1 + ";";
            ResultSet rs = stmt.executeQuery( sql );
            while (rs.next()) {
                Supplier sup = this.getSupplier( rs.getString( 2 ) );
                LinkedList<OrderItem> items = this.getOrderItems( rs.getInt( 1 ), rs.getString( 2 ) );
                ans.add( new Order( rs.getInt( 1 ), sup, rs.getString( 3 ), items, rs.getString( 4 ), rs.getInt( 5 ) ) );
            }
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return ans;
    }

    public LinkedList<Order> getOrdersWithExcecutedState(String excexuted) {
        LinkedList<Order> orders = new LinkedList<Order>();
        try {
            stmt = c.createStatement();
            String sql = "SELECT * FROM Orders WHERE executed = '" + excexuted + "';";
            ResultSet rs = stmt.executeQuery( sql );
            while (rs.next()) {
                Supplier sup = this.getSupplier(rs.getString(2));
                LinkedList<OrderItem> items = this.getOrderItems(rs.getInt(1), rs.getString(2));
                orders.add(new Order(rs.getInt(1), sup, rs.getString(3), items, rs.getString(4), rs.getInt(5)));
            }
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return orders;
    }


    //EXISTS
    public boolean supplierExists(String id) {
        try {
            stmt = c.createStatement();
            String sql = "SELECT * FROM Suppliers WHERE id = " + id + ";";
            ResultSet rs = stmt.executeQuery( sql );
            if (!rs.next()) {
                return false;
            }
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean itemExists(String supplierId, String catalog_num) {
        try {
            stmt = c.createStatement();
            String sql = "SELECT COUNT(*) > 0 FROM Items_In_Contract WHERE supplier_id = '" + supplierId + "' AND catalog_number = '" + catalog_num + "';";
            ResultSet rs = stmt.executeQuery( sql );
            if (rs.next()) {
                boolean ans = rs.getBoolean( 1 );
                return ans;
            }
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    //todo
    public boolean itemExistsInContractForSpecificStore(String supplierId, String catalog_num, String store) {
        try {
            stmt = c.createStatement();
            String sql = "SELECT COUNT(*) > 0 FROM StoresOfItemsInContract WHERE supplier_id = '" + supplierId + "' AND catalog_number = '" + catalog_num + "' AND store = " + store + ";";
            ResultSet rs = stmt.executeQuery( sql );
            if (rs.next()) {
                boolean ans = rs.getBoolean( 1 );
                return ans;
            }
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean contactExistsWithSupp(String supplierID, String phoneNum) {
        try {
            stmt = c.createStatement();
            String sql = "SELECT COUNT (*) > 0 FROM Contacts_Of_Suppliers WHERE supplier_id = '" + supplierID + "' AND phone_number = '" + phoneNum + "';";
            ResultSet rs = stmt.executeQuery( sql );
            if (rs.next()) {
                boolean ans = rs.getBoolean( 1 );
                return ans;
            }
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean contactExistsInContacts(String phoneNum) {
        try {
            stmt = c.createStatement();
            String sql = "SELECT COUNT (*) FROM Contacts WHERE phone_number = '" + phoneNum + "' ;";
            ResultSet rs = stmt.executeQuery( sql );
            if (rs.next()) {
                boolean ans = rs.getBoolean( 1 );
                return ans;
            }
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean orderExists(int order_number) {
        try {
            stmt = c.createStatement();
            String sql = "SELECT * FROM Orders WHERE order_number = " + order_number + ";";
            ResultSet rs = stmt.executeQuery( sql );
            if (!rs.next()) {
                return false;
            }
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    /**
     * checks if a given product exists in the database
     *
     * @param serialNumber the product's serial number
     * @return true if exists, else false
     */
    public boolean productExists(String serialNumber) {
        try {
            stmt = c.createStatement();
            String sql = "SELECT * FROM Products WHERE SerialNumber = " + serialNumber + ";";
            ResultSet rs = stmt.executeQuery( sql );
            if (!rs.next()) {
                return false;
            }
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean productExistsInStore(String serialNumber, String store) {
        try {
            stmt = c.createStatement();
            String sql = "SELECT * FROM Inventory WHERE SerialNumber = " + serialNumber + " AND store = '" + store + "';";
            ResultSet rs = stmt.executeQuery( sql );
            if (!rs.next()) {
                return false;
            }
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    private boolean isThereOrderSetToday(String supplierID) {
        try {
            stmt = c.createStatement();
            String sql = "SELECT * FROM Orders WHERE supplier_id = '" + supplierID + "' AND date = '" + LocalDate.now().toString() + "';";
            ResultSet rs = stmt.executeQuery( sql );
            if (!rs.next()) {
                return false;
            }
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public boolean categoryExists(String categoryId) {
        boolean ans = false;
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM Categories WHERE ID=" + categoryId );
            ans = rs.next();
            stmt.close();
        } catch (SQLException e) {
            return ans;
        }
        return ans;
    }

    public boolean saleExists(String saleId) {
        boolean ans = false;
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT Id FROM Sales WHERE Id=" + saleId + ";" );
            ans = rs.next();
            stmt.close();
        } catch (SQLException e) {
            return ans;
        }
        return ans;
    }


    //HELPERS
    private SupplyTerms stringToSupplyTerms(String text) {
        switch (text) {
            case "SupplyOnSetDays":
                return SupplyTerms.SupplyOnSetDays;
            case "OnOrder":
                return SupplyTerms.OnOrder;
            case "SelfTransport":
                return SupplyTerms.SelfTransport;
        }
        return null;
    }

    private String nextDay(String day) {
        String ans = "";
        switch (day) {
            case "Sunday":
                ans = "Monday";
                break;
            case "Monday":
                ans = "Tuesday";
                break;
            case "Tuesday":
                ans = "Wedensday";
                break;
            case "Wednesday":
                ans = "Thursday";
                break;
            case "Thursday":
                ans = "Friday";
                break;
            case "Friday":
                ans = "Saturday";
                break;
            case "Saturday":
                ans = "Sunday";
                break;
        }
        return ans;
    }

    private Order buildOrder(ResultSet rs) {
        try {
            LinkedList<OrderItem> items = getOrderItems( rs.getInt( 1 ), rs.getString( 2 ) );
            Supplier supplier = getSupplier( rs.getString( 2 ) );
            Order o = new Order( rs.getInt( 1 ), supplier, rs.getString( 3 ), items, rs.getString( 4 ), rs.getInt( 5 ) );
            return o;
        } catch (SQLException e) {
            return null;
        }
    }


    //TO SORT

    /**
     * The function chooses the best supplier to order from based on the lowest price for the quantity
     *
     * @param serialNumber - the product to order
     * @param quantity     - the amount to order
     * @return OrderItem - the item of supplier chosen to order
     */
    //todo
    public boolean orderWhenLackOfStock(String serialNumber, int quantity, String store) {
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM Order_Items JOIN Orders ON Order_Items.order_number=Orders.order_number WHERE (executed='1' OR executed = '2' OR executed = '3') AND store = '" + store +"';");
            while (rs.next()) {
                if (rs.getString( "serial_number" ).equals( serialNumber )) {
                    return false;
                }
            }
            //when the is not in any order
            OrderItem itemToOrder = orderFrom( serialNumber, quantity, store );
            if (itemToOrder == null) return false;
            rs = stmt.executeQuery( "SELECT * FROM Orders WHERE executed='1'" );
            while (rs.next()) {
                if (rs.getString( "supplier_id" ).equals( itemToOrder.getSupplier_id() )) {
                    int orderNum = rs.getInt( "order_number" );
                    stmt.execute(
                            "INSERT INTO Order_Items VALUES(" + rs.getInt( "order_number" ) + "," +
                                    serialNumber + "," + itemToOrder.getCatalog_number() + "," + quantity + "," +
                                    itemToOrder.getPrice() + "," + itemToOrder.getDiscount() + ", '" + store +
                                    "', " + 0 + ");" );

                    sumWeight( orderNum, itemToOrder.getQuantity() * getProductDetails( serialNumber ).getWeight() );
                    return true;
                }
            }
            int newOrder = addNewOrder( itemToOrder.getSupplier_id(), LocalDate.now().toString(), "1" );
            LinkedList<Item> item = new LinkedList<>();
            LinkedList<Discount> discounts = getDiscounts( itemToOrder.getSupplier_id(), itemToOrder.getCatalog_number() );
            HashMap<String, Integer> storeAndQuantity = new HashMap<>();
            storeAndQuantity.put( store, quantity );
            item.add( new Item( itemToOrder.getCatalog_number(), itemToOrder.getPrice(), itemToOrder.getSerial_number(), discounts, storeAndQuantity ) );
            addItemsToOrder( newOrder, item );
            stmt.close();
            return true;
        } catch (SQLException e) {
            System.out.println( e.getMessage() );
            return false;
        }
    }

    //todo
    public int orderPeriodic(String day) {
        int answer = -1;
        // get all suppliers that supply of the next day, makes the order and add all the order items
        String nextDay = nextDay(day);
        LinkedList<String> suppliers = getAllSuppliersOnDay(nextDay);
        if (suppliers != null) {
            for (String supplier : suppliers) {
                boolean isOrderSetToday = isThereOrderSetToday(supplier);
                if (!isOrderSetToday) {
                    LinkedList<Item> itemsOfContract = getSupplierItemsOfPeriodic( supplier );
                    int size = itemsOfContract.size();
                    if (size > 0) {
                        String date = LocalDate.now().toString();
                        int order = addNewOrder( supplier, date, "1" );
                        addItemsToOrder(order, itemsOfContract);
                        answer = order;
                    }
                }
            }
        }
        return answer;
    }

    private LinkedList<Item> getSupplierItemsOfPeriodic(String supplier) {
        LinkedList<Item> itemsOfContract = new LinkedList<Item>();
        try {
            stmt = c.createStatement();
            String sql = "SELECT * FROM Items_In_Contract WHERE supplier_id = " + supplier + ";";
            ResultSet rs = stmt.executeQuery( sql );
            while (rs.next()) {
                HashMap<String, Integer> storesquan = getStoresAndQuantity( supplier, rs.getString( 2 ) );
                if (storesquan.size() > 0) {
                    Item newItem = new Item( rs.getString( 2 ), rs.getDouble( 4 ), rs.getString( 3 ), getDiscounts( supplier, rs.getString( 2 ) ), storesquan );
                    itemsOfContract.add( newItem );
                }
            }
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return itemsOfContract;
    }


    public LocalDate getOrderTransportationDate(int orderNumber) {
        LocalDate result = null;

        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT Date FROM transportationDocs WHERE ID = '" + orderNumber + "'" );
            while (rs.next()) {
                String date = rs.getString( 1 );
                result = LocalDate.parse( date );
            }
        } catch (SQLException e) {
            System.out.println( e.getMessage() );
            return null;
        }
        return result;
    }

    public LinkedList<OrderItem> getOrderItemsInOrderByLocation(int order_number, String store) {
        LinkedList<OrderItem> orderItems = new LinkedList<>();
        String supplierId = getOrderByOrderNumber(order_number).getFirst().getSupplier().getId();

        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM Order_Items WHERE order_number = '" + order_number + "' AND store = '" + store + "';" );
            while (rs.next()) {
                orderItems.add(new OrderItem(supplierId, rs.getString(2), rs.getString(3), rs.getInt(4), rs.getDouble(5), rs.getDouble(6), getProductName(rs.getString(2)), rs.getString(7), rs.getInt(8)));

            }
            stmt.close();
        } catch (SQLException e) {
            return orderItems;
        }
        return orderItems;
    }

    public boolean approveOrder(int order_number, String store) {
        try {
            stmt = c.createStatement();
            stmt.executeUpdate( "UPDATE Order_Items SET status = '" + 1 + "' WHERE order_number = '" + order_number + "' AND store = '" + store + "';" );
            ResultSet rs = stmt.executeQuery( "SELECT COUNT(*) = 0 FROM Order_Items WHERE status = 0 AND order_number = '" + order_number + "';" );
            rs.next();
            boolean ans = rs.getBoolean( 1 );
            stmt.close();
            stmt = c.createStatement();
            if (ans) {
                stmt.executeUpdate( "UPDATE Orders SET executed = 4 WHERE order_number = " + order_number + ";" );
            }
            stmt.close();
        } catch (SQLException e) {

            return false;
        }
        return true;
    }

    public boolean deleteItemOfPeriodicSupplier(String supplier_id, String item, String store) {
        try {
            //delete the store and check if zero store, if so, delete from contract and discount
            stmt = c.createStatement();
            String sql = "DELETE FROM StoresOfItemsInContract WHERE supplier_id = '" + supplier_id + "' AND catalog_number = '" + item + "' AND store = '" + store + "';";
            stmt.executeUpdate(sql);
            ResultSet rs = stmt.executeQuery("SELECT COUNT(*) = 0  FROM StoresOfItemsInContract WHERE supplier_id = '" + supplier_id + "' AND catalog_number = '" + item + "';");
            rs.next();
            if(rs.getBoolean(1)){
                sql = "DELETE FROM Items_In_Contract WHERE supplier_id = " + supplier_id + " AND catalog_number = " + item + ";";
                stmt.executeUpdate(sql);
                sql = "DELETE FROM Discounts WHERE supplier_id = '" + supplier_id + "'AND catalog_number = " + item + ";";
                stmt.executeUpdate(sql);
            }
            stmt.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public LinkedList<Order> getOpenOrdersByStore(String store) {
        LinkedList<Order> ans = new LinkedList<Order>();
        try {
            stmt = c.createStatement();
            String sql = "SELECT * FROM Orders WHERE executed = " + 1 + ";";
            ResultSet rs = stmt.executeQuery( sql );
            while (rs.next()) {
                Supplier sup = this.getSupplier( rs.getString( 2 ) );
                LinkedList<OrderItem> items = this.getOrderItems( rs.getInt( 1 ), rs.getString( 2 ) );
                ans.add( new Order( rs.getInt( 1 ), sup, rs.getString( 3 ), items, rs.getString( 4 ), rs.getInt( 5 ) ) );
            }
            stmt.close();
        } catch (SQLException e) {
            return null;
        }
        return ans;
    }

    public int getStatus(int order_number, String store) {
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT status From Order_Items WHERE order_number = '" + order_number + "' AND store = '" + store + "';" );
            return rs.getInt( "status" );
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }
}


/*package*/ enum CategoryFields {
    //ID,
    CategoryName,
    FatherCategory;

    /*package*/
    static CategoryFields parse(String field) {
        field = field.toLowerCase();

        if (field.equals("category name") | (field.equals("categoryname")))
            return CategoryFields.CategoryName;
        if (field.equals("father category") | (field.equals("fathercategory")))
            return CategoryFields.FatherCategory;
        return null;
    }
}

/*package*/ enum ProductFields {
    //SerialNumber,
    Name,
    Manufacturer,
    Category,
    Size,
    Cost,
    SellingPrice;

    /*package*/
    static ProductFields parse(String field) {
        field = field.toLowerCase();
        if (field.equals("name"))
            return ProductFields.Name;
        if (field.equals("manufacturer"))
            return ProductFields.Manufacturer;
        if (field.equals("category"))
            return ProductFields.Category;
        if (field.equals("weight"))
            return ProductFields.Size;
        if (field.equals("cost"))
            return ProductFields.Cost;
        if (field.equals("selling price") | (field.equals("sellingprice")))
            return ProductFields.SellingPrice;
        return null;
    }
}

/*package*/ enum SaleFields {
    //Id,
    StartDate,
    EndDate,
    PercentOff;

    /*package*/
    static SaleFields parse(String field) {
        field = field.toLowerCase();
        if (field.equals( "start date" ) | ( field.equals( "startdate" ) ))
            return SaleFields.StartDate;

        if (field.equals( "end date" ) | ( field.equals( "enddate" ) ))
            return SaleFields.EndDate;

        if (field.equals( "percent off" ) | ( field.equals( "percentoff" ) ))
            return SaleFields.PercentOff;
        return null;
    }
}


