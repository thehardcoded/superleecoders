package LogicLayer;

import DataProviders.DAL;
import DataProviders.Repository;
import SharedClasses.*;
import SharedClasses.DTOs.*;
import javafx.util.Pair;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;

/**
 * Created by SHLOMI PAKADO on 3/15/2017.
 */
public class LogicManager {

    private Repository rep;
    public Validator validator;
    private DAL dal;


    public LogicManager() {
        this.rep = new Repository();
        this.validator = new Validator();
        dal = new DAL(rep.getConn());
    }

    public Repository getRep() {
        return rep;
    }

    public DAL getDal() {
        return dal;
    }

    public void closeDB() {
        this.rep.close();
    }

    //--------------------------------------- ADD pipe functions -------------------------------------------------------

    public void checkForArrivedOrders() {
        LocalDate today = LocalDate.now();
        LinkedList<Order> orders2 = dal.getOrdersWithExcecutedState("2");
        for (Order order : orders2) {
            int orderNumber = order.getOrder_number();
            LocalDate orderTranspDate = dal.getOrderTransportationDate(orderNumber);
            if (orderTranspDate != null) {
                if (today.compareTo(orderTranspDate) >= 0) {
                    dal.updateExecutedInOrders(orderNumber, "3");
                }
            }
        }
    }

    public boolean AddTransDoc(TransportationDocDTO toAdd) {
        return (rep).AddTransportationDocs(toAdd);
    }

    public boolean AddStore(StoreDTO toAdd) {
        return (rep).addStore(toAdd);
    }

    public boolean AddSupplier(SupplierDTO toAdd) {
        return (rep).addSupplier(toAdd);
    }

    public boolean AddDriver(DriverDTO toAdd) {
        return (rep).addDriver(toAdd);
    }

    public boolean AddTruck(TruckDTO toAdd) {
        return (rep).addTruck(toAdd);
    }

    public boolean AddStop(StopDTO toAdd) {
        return (rep).addStop(toAdd);
    }

    public boolean AddEmployee(EmployeeDTO toAdd) {
        return rep.AddEmployee(toAdd);
    }

    public boolean AddConstraint(EmployeeConstraintDTO constraint) {
        if (!(rep).checkIfShiftDated(constraint))
            (rep).addShiftDate(constraint);
        return this.rep.addEmployeeConstraint(constraint);
    }

    public boolean insetNewRole(String roleType) {
        return rep.addRole(roleType);
    }

    //public List<String> getIdOfSevenYearsRetired() {return (this.rep).getIdOfSevenYearsRetired();}

    //------------------------------------- REMOVE pipe functions ------------------------------------------------------

    public void deleteTransDocRequest(int docNum) {
        TransportationDocDTO transDoc = this.rep.getTransportDoc(docNum);
        this.rep.removeTransportationDoc(docNum);//deletes the transportation doc request
        String driver = transDoc.getDriverID();
        String date = transDoc.getDate();
        this.freeDriverShiftByDate(date, driver, transDoc.getShift());
        this.dal.updateExecutedInOrders(docNum, "1");
    }

    public boolean freeDriverShiftByDate(String DateOfTransDocToRemove, String driverIDToRemove, int shiftType) {
        return rep.freeDriverShiftByDate(DateOfTransDocToRemove, driverIDToRemove, shiftType);
    }

    public boolean removeSupplier(String address) {
        return (rep).deleteSupplier(address);
    }

    public boolean removeStore(String address) {
        return (rep).deleteStore(address);
    }

    public boolean removeDriver(String ID) {
        return (rep).deleteDriver(ID);
    }

    public boolean removeTruck(String ID) {
        return (rep).deleteTruck(ID);
    }

    public boolean removeSpecificStop(int transportationDocNo, String destination) {
        return (rep).removeSpecificStop(transportationDocNo, destination);
    }

    public boolean removeStopByTransportationDocNo(int transportationDocNo) {
        return rep.removeStopByTransportationDocNo(transportationDocNo);
    }

    public boolean removeTransportationDoc(int ID) {
        return rep.removeTransportationDoc(ID);
    }

    public boolean deleteScheduledShift(ShiftInDateDTO sid) {
        return rep.DeleteScheduledShift(sid);
    }

    public boolean deleteConstraint(int shiftType, String shiftedDate, String ID) {
        return (this.rep).deleteConstraint(shiftType, EmployeeDTO.shiftDateFormat(shiftedDate), ID);
    }

    /*public void deleteScheduledShift(String shiftDate, int type,String store) {
        (rep).deleteScheduledShift(EmployeeDTO.shiftDateFormat(shiftDate), type,store);
    }*/

    //---------------------------------------- EXIST functions ---------------------------------------------------------



    public boolean supplierExist(String address) {
        return (rep).checkSupplierExist(address);
    }

    public boolean storeExist(String address) {
        return (rep).checkDatedShiftExist(address);
    }

    public boolean driverIDExist(String ID) {
        return (rep).checkDriverIDExist(ID);
    }

    public boolean truckIDExist(String ID) {
        return (rep).checkTruckIDExist(ID);
    }

    public boolean stopProductsDocNoExist(String productsDocNo) {
        return (rep).checkStopProductsDocNoExist(productsDocNo);
    }

    public boolean stopTransDestExist(String transDocID, String dest) {
        return (rep).checkStopTransDestExist(transDocID, dest);
    }

    public boolean employeeIDExist(String ID) {
        return !(rep.getEmployeeById(ID) == null);
    }

    public boolean ScheduledShiftExist(String Date, int shiftType, String store) {
        String date = EmployeeDTO.shiftDateFormat(Date);
        return (rep).checkScheduledShiftExist(date, shiftType, store);
    }

    //------------------------------------- VALIDATION functions ------------------------------------------------------

    public boolean isValidStore(String address, int region) {
        return (rep).isValidStore(address, region);
    }

    public boolean isValidWeight(TransportationDocDTO transDoc, int weight) {
        String truckID = transDoc.getTruckID();
        int maxWeight = (rep).getMaxWeightByID(truckID);//the maximum weight this truck can carry
        return maxWeight >= weight;
    }

    public boolean driverCanDrive(String newTruckID, String driverId) {
        int license = (rep).getLicenseType(driverId);
        int truckType = (rep).getModel(newTruckID);
        return (license >= truckType);
    }

    public boolean isValidRole(String input, String store) {
        if (input.length() == 0 || (input.equals("Driver") && !store.equals("General")))
            return false;
        else
            return Validator.IsValidRole((this.rep).getValidRoles(), input);
    }

    //------------------------------------- GET RECORDS functions ------------------------------------------------------

    public List<Pair<String, String>> getNotificationsByRole(String role) {
        return this.rep.getNotificationsByRole(role);
    }


    public List<Integer> getConfirmedTransDocsIDByStore(String Address) {
        return rep.getConfirmedTransDocsIDByStore(Address);
    }

    public List<String> getAllStores() {
        return (rep).getAllStores();
    }

    public List<String> getRemovableSuppliers() {
        return (rep).getRemovableSuppliers();
    }


    public List<Integer> getUnconfirmedIDList() {
        return rep.getUnconfirmedIDList();
    }


    public List<String> getSuppliersAddressesByRegion(int region) {
        return rep.getSuppliersAddressesByRegion(region);
    }

    public List<String> getShopsByRegionWithStoreKeeper(int region, String Date, int shiftType, int transDoc) {
        return rep.getStoresByRegion(region, Date, shiftType, transDoc);
    }

    public TransportationDocDTO getTransportDoc(int docNum) {
        return rep.getTransportDoc(docNum);
    }

    public EmployeeDTO getEmployee(String id) {
        return rep.getEmployeeById(id);
    }

    public ArrayList<RoleInShiftDTO> getShiftRoles(int shiftType, String store) {
        return this.rep.getRolesInShiftsList(shiftType, store);
    }

    public ArrayList<String> getRelevantRoles(int shiftType, String store) {
        return this.rep.getRolesNotInShiftArray(shiftType, store);
    }

    public ArrayList<String[]> getEmployeesInShiftsBetweenDates(String date, int shiftType, String store) {
        return (this.rep).getEmployeesInShiftByDate(EmployeeDTO.shiftDateFormat(date), shiftType, store);
    }

    public List<EmployeeConstraintDTO> getEmployeeConstraint(EmployeeDTO emp) {
        return (rep).getEmployeeConstraints(emp);
    }

    public ArrayList<String[]> getNext14DaysShiftsStatus(String store) {
        ArrayList<String[]> ans = new ArrayList<>();//the ans will contain the date, the shift type and Done / Undone!
        LocalDate next = LocalDate.now();
        for (int i = 0; i < 14; i++) {
            next = next.plus(1, ChronoUnit.DAYS);
            for (int j = 1; j < 3; j++) {
                String[] temp = new String[3];
                temp[0] = EmployeeDTO.shiftDateFormat(next.toString().replace('-', '-'));
                temp[1] = "Morning";
                if (j == 2)
                    temp[1] = "Evening";
                temp[2] = "UnScheduled";
                String date = EmployeeDTO.shiftDateFormat(temp[0]);
                if ((rep).checkScheduledShiftExist(date, j, store)) {
                    temp[2] = "Scheduled";
                }
                ans.add(temp);
            }
        }
        return ans;
    }

    public ArrayList<String[]> getEmployeesToAssignByRole(String date, int shiftType, String Role, String store) {
        String d = EmployeeDTO.shiftDateFormat(date);
        return this.rep.getListEmployeesByRole(d, shiftType, Role, store);
    }

    public int getLicenseType(String driverID) {
        return this.rep.getLicenseType(driverID);
    }


    public List<Integer> getConfirmedID() {
        return this.rep.getConfirmedID();
    }


    public List<String> getAvailableDriversId(String truckID, int region, String date, int shift) {
        return this.rep.getAvailableDriversId(truckID, region, date, shift);
    }

    public List<TruckDTO> getAvailableTrucks(String date, int shift) {
        return this.rep.getAvailableTrucks(date, shift);
    }

    public List<Integer> getPastDocsList() {
        return this.rep.getPastDocList();
    }

    public List<String> getStops(int transDocId) {
        return this.rep.getStops(transDocId);
    }

    public List<String> getRemovableTrucks() {
        return this.rep.getRemovableTrucks();
    }

    public DriverDTO getDriver(String ID) {
        return (this.rep).getDriver(ID);
    }

//------------------------------------- UPDATE RECORDS functions ------------------------------------------------------

    public boolean confirmTransDoc(int docID) {
        return rep.confirmTransDoc(docID);
    }

    public boolean updateWeight(TransportationDocDTO transDoc, int weight) {
        return rep.updateWeight(transDoc, weight);
    }

    public boolean updateTruckID(TransportationDocDTO transDoc, String newTruckID) {
        return rep.updateTruckID(transDoc, newTruckID);
    }


    public boolean updateEmployee(String employeesOldId, EmployeeDTO emp) {
        return rep.editEmployee(employeesOldId, emp);
    }

    public boolean setShiftStandard(ArrayList<RoleInShiftDTO> roles, int shiftType) {
        return this.rep.setShiftStandardDB(roles, shiftType);
    }

    public boolean setEmployeesInShift(String shiftDate, int shiftType, ArrayList<String[]> employeesToSet, String store) {
        String shiftedDate = EmployeeDTO.shiftDateFormat(shiftDate);
        if (!(rep).checkDatedShiftExist(shiftedDate, shiftType, store))
            (rep).AddShiftDate(shiftedDate, shiftType, store);
        return (rep).AddToEmployeesInShift(shiftType, shiftedDate, employeesToSet, store);
    }

    public boolean addDriverToShift(String id, String date, int shift) {
        String[] stringArr = {id, "Driver"};
        ArrayList<String[]> driver = new ArrayList<String[]>();
        driver.add(stringArr);
        return setEmployeesInShift(EmployeeDTO.shiftDateFormat(date), shift, driver, "General");
    }


    public boolean createTransportationFromOrder(Order order) {
        //date, shift,truck,driver,
        LinkedList<String> stores = order.getStoresFromOrder();
        int weight = order.getWeight();
        int region = order.getSupplier().getRegion();
        int ID = order.getOrder_number();
        LocalDate today = LocalDate.now();
        LocalDate until = today.plusDays(8);//check if it is 7 days
        boolean notiToTransMan = false;
        boolean notiToManPowerMan = false;
        boolean shiftFound = false;

        for (LocalDate date = today.plusDays(1); date.isBefore(until) && !shiftFound; date = date.plusDays(1)) {
            notiToTransMan = false;
            notiToManPowerMan = false;
            String currDate = date.toString();
            for (int shiftType = 1; shiftType <= 2 && !shiftFound; shiftType++) {
                for (String store : stores) {
                    boolean ans = this.rep.storekeeperWorksOnShift(currDate, shiftType, store);
                    if (!ans) //if there is no storekeeper working on that shift
                    {
                        notiToManPowerMan = true;
                        break;
                    }
                    //get a free truck with the lowest level which can carry the weight
                    TruckDTO truck = this.rep.getMinFreeTruck(currDate, shiftType, weight);
                    if (truck == null)//if there is no free truck
                    {
                        notiToTransMan = true;
                        break;
                    }
                    String truckID = truck.getID();
                    List<String> drivers = this.rep.getAvailableDriversId(truckID, region, currDate, shiftType);
                    String driverID = null;
                    if (drivers.size() > 0)
                        driverID = drivers.get(0);
                    if (driverID == null)//if there is no matching driver
                    {
                        notiToTransMan = true;
                        break;
                    } else if (!notiToManPowerMan && !notiToTransMan) {
                        shiftFound = true;
                        //it's available to create a transportation
                        TransportationDocDTO toAdd = new TransportationDocDTO(ID, currDate, shiftType, truck.getID(), driverID, order.getSupplier().getId(), weight, region-1, 0);
                        this.rep.AddTransportationDocs(toAdd);//add the transdoc to the DB
                        //add stores to the transDoc stops
                        this.rep.addStopsToTransDoc(ID, stores);
                        //add the driver to the shift
                        this.addDriverToShift(driverID, currDate, shiftType);
                        return shiftFound;//true
                    }
                }
            }
        }
        //Not available to create a transportation for the order in the next 7 days send notifications
        String todayStr = today.toString();
        if (notiToManPowerMan)
            this.rep.addNotification("ManPower Manager", todayStr, ID);
        if (notiToTransMan)
            this.rep.addNotification("Logistic Manager", todayStr, ID);
        return shiftFound;//false
    }


//--------------------------------------- inventory and order -----------------------------------------------

    public void orderPeriodic() {
        Calendar calendar = Calendar.getInstance();
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        String day = intToDay(dayOfWeek);
        int order = dal.orderPeriodic(day);
        if (order != -1) {
            Order toTransport = getOrderByOrderNumber(order).getFirst();
            boolean didCreate = createTransportationFromOrder(toTransport);
            if (didCreate) {
                dal.updateExecutedInOrders(order, "2");
            }
        }
    }

    private String intToDay(int dayOfWeek) {
        String ans = "";
        switch (dayOfWeek) {
            case 1:
                ans = "Monday";
                break;
            case 2:
                ans = "Tuesday";
                break;
            case 3:
                ans = "Wedensday";
                break;
            case 4:
                ans = "Thursday";
                break;
            case 5:
                ans = "Friday";
                break;
            case 6:
                ans = "Saturday";
                break;
            case 7:
                ans = "Sunday";
                break;
        }
        return ans;
    }

    public boolean isNumber(String s) {
        boolean ans = true;
        for (int i = 0; i < s.length(); i++) {
            if (!Character.isDigit(s.charAt(i))) {
                ans = false;
            }
        }
        return ans;
    }

    public boolean legalName(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != ' ' && s.charAt(i) != '\n' && !Character.isLetter(s.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public boolean isPositiveDouble(String s) {
        boolean ans = false;
        try {
            double d = Double.parseDouble(s);
            if (d < 0) {
                ans = false;
            } else {
                ans = true;
            }
        } catch (NumberFormatException e) {
            ans = false;
        }
        return ans;
    }


    public boolean supplierExists(String id) {
        return dal.supplierExists(id);
    }

    public boolean insetToSuppliers(Supplier supplier) {
        return dal.insertToSuppliers(supplier);
    }

    public boolean updateSupplyTerms(String supplier_id, SupplyTerms terms) {
        //return dal.updateSupplyTerms(supplier_id, terms);
        return false;
    }

    public LinkedList<Item> getSupplierItems(String supplier_id) {
        return dal.getSupplierItems(supplier_id);
    }

    public boolean deleteContact(String supplier_id, String phoneNum) {
        return dal.deleteContact(supplier_id, phoneNum, false);
    }


    public boolean deleteNotification(String Role, String orderNo) {
        return this.rep.deleteNotification(Role, orderNo);
    }


    public boolean ItemExists(String supplierID, String catalogNum) {
        return dal.itemExists(supplierID, catalogNum);
    }

    public boolean deleteSupplier(String id) {
        return dal.deleteSupplier(id);
    }

    public boolean addItem(String id, Item item) {
        return dal.insertToItemsInSupplier(id, item);
    }

    //todo
    public boolean insertToStoresOfItemsInContract(String id, Item item) {
        return dal.insertToStoresOfItemsInContract(id, item);
    }

    public boolean deleteItem(String id, String catalogNum, String store) {
        Supplier sup = dal.getSupplier(id);
        if (sup.getTerms() == SupplyTerms.SupplyOnSetDays) {
            return dal.deleteItemOfPeriodicSupplier(id, catalogNum, store);
        } else {
            return dal.deleteItemOfSupplier(id, catalogNum);
        }
    }

    public boolean addContact(String id, Contact contact, boolean shouldInsertToContacts) {
        return dal.insertToContacts(id, contact, shouldInsertToContacts);
    }

    public boolean contactExistsWithSupp(String supplierID, String phoneNum) {
        return dal.contactExistsWithSupp(supplierID, phoneNum);
    }

    public boolean insertToDiscounts(String supplierID, String catalogNum, Discount discount) {
        return dal.insertToDiscounts(supplierID, catalogNum, discount);
    }

    public boolean deleteFromDiscounts(String supplierId, String catalog_number, int quantity) {
        return dal.deleteDiscount(supplierId, catalog_number, quantity);
    }

    public boolean contactExistInContacts(String phoneNum) {
        return dal.contactExistsInContacts(phoneNum);
    }

    public void exit() {
        dal.close();
    }

    public ArrayList<? extends Object> getLowQuantity() {
        ArrayList<Stock> ans = dal.getLowQuantity();
        return ans;
    }

    public ArrayList<? extends Object> getLowQuantity(String store) {
        ArrayList<Stock> ans = dal.getLowQuantity(store);
        for (Stock item : ans) {
            item.setProductName(dal.getProductName(item.getSerialNumber()));
        }
        return ans;
    }

    public ArrayList<? extends Object> getAllDeffectives(String store) {
        ArrayList<Stock> ans = dal.getAllDefectives(store);
        for (Stock item : ans) {
            item.setProductName(dal.getProductName(item.getSerialNumber()));
        }
        return ans;
    }

    /**
     * Prints products report
     */
    public ArrayList<? extends Object> getAllProducts() {
        ArrayList<Product> ans = dal.getAllProducts();
        return ans;
    }

    /**
     * Prints products report by a given list of categories
     *
     * @param categories
     */
    public ArrayList<? extends Object> getAllProducts(ArrayList<String> categories) {
        ArrayList<Product> ans = new ArrayList<>();
        ArrayList<Category> allCategories = dal.getAllSubCategories(categories);
        for (String category : categories) {
            ans.addAll(dal.getAllProducts(category));
        }
        for (Category c : allCategories) {
            ans.addAll(dal.getAllProducts(c.getName()));
        }
        return ans;
    }

    /**
     * Alerts the user of below minimal amount of an item
     *
     * @param stock The product with low quantity
     */
    public String alertLowQuanity(Stock stock) {
        //TODO need to add stock item in an order
        dal.orderWhenLackOfStock(stock.getSerialNumber(), stock.getMinimalQuantity() * 2, stock.getStore());
        String serialNumber = stock.getSerialNumber();
        int storageQuan = stock.getStorageQuantity();
        int shelfQuan = stock.getShelfQuantity();
        int total = storageQuan + shelfQuan;
        return "Warning! Product " + dal.getProductName(serialNumber) + "(" + serialNumber + ")" +
                " is running out of stock!\n" +
                "Minimal: " + stock.getMinimalQuantity() + ".\n Remaining:\n" +
                "Storage: " + storageQuan + "\n" +
                "Shelf: " + shelfQuan + "\n" +
                "Total: " + total + "\n";
    }

    public String newCategory(Category cat) {
        if (!dal.insertCategory(cat))
            return "Failed inserting new category";
        else
            return "Done successfully!";
    }

    public String newInventory(Stock s) {
        if (!dal.insertInventory(s))
            return "Failed inserting new Inventory to store";
        else
            return "Done successfully!";
    }

    public String newProduct(Product p) {
        if (!dal.insertProduct(p))
            return "Failed inserting new product";
        else
            return "Done successfully!";
    }

    public String newSale(Sale s) {
        if (!dal.insertSale(s))
            return "Failed inserting new sale";
        else
            return "Done successfully!";
    }

    public String deleteCategory(int catID) {
        if (!dal.removeCategory(catID))
            return "Category " + catID + " not found";
        else
            return "Done successfully!";
    }

    public String deleteProduct(String serialNum) {
        if (!dal.removeProduct(serialNum))
            return "Product " + serialNum + " not found";
        else
            return "Done successfully!";
    }

    public String deleteSale(int saleId) {
        if (!dal.removeSale(saleId))
            return "Sale number " + saleId + " not found";
        else
            return "Done successfully!";
    }

    /**
     * Sets product's category. if category doesn't exists, add it to the database
     *
     * @param choice   1 - data is serial number, 2 - data is products name
     * @param data     the products data
     * @param category the category name
     */
    public boolean setProductsCategory(String choice, String data, String category) {
        int categoryId = dal.findCategory(category);
        if (categoryId == 0) {
            categoryId = dal.addNewCategory(category);
        }
        boolean ans;
        if (choice.equals("1"))
            return dal.setProductsCategoryBySerialNumber(data, categoryId);
        else
            return dal.setProductsCategoryByName(data, categoryId);
    }

    /**
     * Prints report of all the inventory - amounts in all locations
     */
    public ArrayList<? extends Object> getAllInventory(String store) {
        ArrayList<Stock> ans = dal.getAllInventory(store);
        return ans;
    }

    /**
     * Prints report of all sales
     */
    public ArrayList<? extends Object> getAllSales() {
        ArrayList<Sale> ans = dal.getAllSales();
        return ans;
    }

    /**
     * Prints report of all the categories
     */
    public ArrayList<? extends Object> getAllCategories() {
        ArrayList<Category> ans = dal.getAllCategories();
        return ans;
    }

    private String checkForShortage(Stock stock) {
        int difference = getDiff(stock);
        if (difference < 0) {
            return alertLowQuanity(stock);
        }
        return "";
    }

    public int getDiff(Stock stock) {
        String serialNum = stock.getSerialNumber();
        int quantity = stock.getShelfQuantity() + stock.getStorageQuantity();
        int defectives = stock.getShelfDefectives() + stock.getStorageDefectives();
        int minimalQuantity = stock.getMinimalQuantity();
        int difference = quantity - defectives - minimalQuantity;
        return difference;
    }

    public boolean editSaleField(String field, String value, int saleId) {
        return dal.updateSaleField(field, value, saleId);
    }

    public boolean editCategoryField(String field, String value, int id) {
        return dal.updateCategoryField(field, value, id);
    }

    public boolean editProductField(String field, String value, String serialNumber) {
        return dal.updateProductField(field, value, serialNumber);
    }

    /**
     * puts items on an existing sale
     *
     * @param choice 1 - data is product's serial number, 2 - data is category's id
     * @param data   the data
     * @param saleId the sale id
     */
    public String addItemsToSale(String choice, String data, int saleId) {
        if (choice.equals("1")) {
            if (!dal.addItemToSaleByProduct(data, saleId))
                return "Failed";
            else
                return "Done successfully!";
        } else {
            boolean success = true;
            ArrayList<Product> ans = new ArrayList<>();
            int catId = Integer.parseInt(data);
            ArrayList<Category> categories = dal.getAllSubCategories(catId);
            ans.addAll(dal.getAllProducts(catId));
            for (Category cat : categories) {
                ans.addAll(dal.getAllProducts(cat.getId()));
            }
            for (Product p : ans) {
                if (!dal.addItemToSaleByProduct(p.getSerialNumber(), saleId))
                    success = false;
            }
            if (!success)
                return "Failed";
            else
                return "Done successfully!";
        }
    }

    /**
     * Prints all items on a given sale
     *
     * @param saleId the sale's id
     */
    public ArrayList<? extends Object> showAllItemsOnSale(int saleId) {
        ArrayList<Product> ans = dal.getAllItemsOnSale(saleId);
        ArrayList<String> str = new ArrayList<String>();
        if (ans == null) {
            str.add("Failed");
            return str;
        } else {
            if (ans.isEmpty()) {
                str.add("No items on this sale!");
                return str;
            } else
                return ans;
        }
    }

    /**
     * Remove product from an existing sale
     *
     * @param saleId       the sale
     * @param serialNumber the product's serial number
     */
    public String removeItemFromSale(int saleId, String serialNumber) {
        if (!dal.removeItemFromSale(saleId, serialNumber))
            return "Failed";
        else
            return "Done successfully!";
    }

    public boolean productExists(String serialNumber) {
        return dal.productExists(serialNumber);
    }

    public boolean productExistsInStore(String serialNumber, String store) {
        return dal.productExistsInStore(serialNumber, store);
    }

    public Product getProduct(String serialNumber) {
        return dal.getProductDetails(serialNumber);
    }

    public Item getItem(String id, String catalogNum) {
        return dal.getItem(id, catalogNum);
    }

    public Supplier getSupplier(String id) {
        return dal.getSupplier(id);
    }

    public double calculateFinalPrice(double price, double discount) {
        return (price - (price * discount));
    }

    public LinkedList<Order> getAllOrders() {
        return dal.getAllOrders();
    }

    public LinkedList<Order> getOrderByOrderNumber(int orderNumber) {
        return dal.getOrderByOrderNumber(orderNumber);
    }

    public LinkedList<Order> getOrderBySupplier(String supplierID) {
        return dal.getOrderBySupplier(supplierID);
    }

    public boolean orderExists(int order_number) {
        return dal.orderExists(order_number);
    }

    public boolean setArrival(int order_number, String store) {
        return dal.orderArrived(order_number, store);
    }

    public boolean setOrderSentToSupplier(Order order_number) {
        boolean ans = false;
        boolean didCreate = createTransportationFromOrder(getOrderByOrderNumber(order_number.getOrder_number()).getFirst());
        if (didCreate) {
            ans = dal.updateExecutedInOrders(order_number.getOrder_number(), "2");
        }
        return ans;
    }

    public boolean updateItemPrice(String id, String catalogNum, double price) {
        return dal.updateItemPrice(id, catalogNum, price);
    }

    public boolean UpdateQuantityItemInPereodicOrder(String id, String catalogNum, int quantity, String store) {
        return dal.UpdateQuantityItemInPereodicOrder(id, catalogNum, quantity, store);
    }

    public String updateInventoryShelfLocation(String productSerialNumber, String location, String store) {
        if (!dal.updateInventoryShelfLocation(productSerialNumber, location, store))
            return "Failed";
        else
            return "Done successfully!";
    }

    public String updateInventoryStorageLocation(String productSerialNumber, String location, String store) {
        if (!dal.updateInventoryStorageLocation(productSerialNumber, location, store))
            return "Failed";
        else
            return "Done successfully!";
    }

    public String updateInventoryMinimalQuantity(String productSerialNumber, int minimum, String store) {
        if (!dal.updateInventoryMinimalQuantity(productSerialNumber, store, minimum))
            return "Failed";
        else
            return "Done successfully!";
    }

    public String updateInventoryShelfDefQuantity(String productSerialNumber, int defQuantity, String store) {
        if (!dal.updateInventoryShelfDefQuantity(productSerialNumber, store, defQuantity))
            return "Failed";
        else
            return "Done successfully!";
    }

    public String updateInventoryStorageDefQuantity(String productSerialNumber, int defQuantity, String store) {
        if (!dal.updateInventoryStorageDefQuantity(productSerialNumber, store, defQuantity))
            return "Failed";
        else
            return "Done successfully!";
    }

    /**
     * @param productSerialNumber
     * @param loc                 1 for storage, 2 for shelf
     * @param quantity
     * @return
     */
    public String updateInventoryQuantity(String productSerialNumber, int loc, int quantity, String store) {
        String ans;
        if (dal.updateInventoryQuantity(productSerialNumber, store, loc, quantity)) {
            ans = "Update was done successfully!\n";
        } else {
            ans = "Update failed\n";
        }
        ans = ans + checkForShortage(dal.getStock(productSerialNumber, store));
        return ans;
    }

    public LinkedList<Supplier> getAllSuppliers() {
        return dal.getAllSuppliers();
    }

    public int getExecutedState(int orderNum) {
        return dal.getExecutedState(orderNum);
    }

    public boolean removeOrderThatHasNotBeenSentYet(int order_number) {
        if (rep.checkTransDocIDExist(order_number))  ///the transportation doc and his order has the same ID
            (rep).removeTransportationDoc(order_number);
        return dal.removeOrderThatHasNotBeenSentYet(order_number);
    }

    /**
     * Cancel order that was already sent to supplier.
     * Assumes all orders that was scheduled for deliver for today or before is already on state='3'. (because this check being made on start-up)
     *
     * @param order_number
     * @return
     */
    public String removeOrderThatHasBeenSent(int order_number) {
        if (getExecutedState(order_number) < 2) {
            return "Order number " + order_number + " wasn't sent yet!";
        }
        if (getExecutedState(order_number) > 2) {
            return "Order number " + order_number + " was already delivered!";
        }
        int transId = order_number;
        if (!rep.removeTransportationDoc(transId)) {
            return "Failed removing transportation doc!";
        }
        if (!dal.removeOrder(order_number)) {
            return "Failed removing order";
        }
        return "Order number " + order_number + " and the transportation was cancelled successfully!";
    }

    public boolean removeItemFromOrderThatHasNotBeenSentYet(int order_number, String serialNumber, String store) {
        return dal.removeItemFromOrderThatHasNotBeenSentYet(order_number, serialNumber, store);
    }

    public boolean updateItemQuantityInLacksOfInventory(int order_number, String serialNumber, int quantity, String store) {
        return dal.updateItemQuantityInLacksOfInventory(order_number, serialNumber, quantity, store);
    }

    public boolean saleExists(String saleId) {
        return dal.saleExists(saleId);
    }

    public boolean categoryExists(String categoryId) {
        return dal.categoryExists(categoryId);
    }

    public boolean orderExistsInList(int order_number, LinkedList<Order> orders) {
        boolean ans = false;
        for (Order order : orders) {
            if (order.getOrder_number() == order_number) {
                ans = true;
            }
        }
        return ans;
    }

    public boolean orderItemExistsInList(String serial_number, LinkedList<OrderItem> items) {
        boolean ans = false;
        for (OrderItem orderitem : items) {
            if (orderitem.getSerial_number().equals(serial_number)) {
                ans = true;
            }
        }
        return ans;
    }

    public Order getOrderFromList(int order_number, LinkedList<Order> orders) {
        for (Order order : orders) {
            if (order.getOrder_number() == order_number) {
                return order;
            }
        }
        return null;
    }

    public boolean approveOrder(int order_number, LinkedList<OrderItem> orderToApprove, HashMap<String, Pair<Integer, Integer>> orderItemsToUpdate, String store) {
        boolean ans = dal.approveOrder(order_number, store);
        for (OrderItem order : orderToApprove) {
            boolean contains = orderItemsToUpdate.containsKey(order.getSerial_number());
            if (orderItemsToUpdate.containsKey(order.getSerial_number())) {
                int defQuantity = orderItemsToUpdate.get(order.getSerial_number()).getValue();
                dal.updateInventoryStorageDefQuantity(order.getSerial_number(), store, defQuantity);
                int newQuantity = order.getQuantity() - orderItemsToUpdate.get(order.getSerial_number()).getKey() - orderItemsToUpdate.get(order.getSerial_number()).getValue();
                newQuantity += dal.getStock(order.getSerial_number(), store).getStorageQuantity();
                dal.updateInventoryStorageQuantity(order.getSerial_number(), store, newQuantity);
                Stock stock = dal.getStock(order.getSerial_number(), store);
                checkForShortage(stock);
            } else {
                int newQuantity = dal.getStock(order.getSerial_number(), store).getStorageQuantity() + order.getQuantity();
                dal.updateInventoryStorageQuantity(order.getSerial_number(), store, newQuantity);
            }
        }

        return ans;
    }

    public LinkedList<Order> getAllNotApprovedOrdersInStore(String store) {
        LinkedList<Order> ans = new LinkedList<>();
        LinkedList<Order> orders = dal.getOrdersWithExcecutedState("3");
        for (Order order : orders) {
            boolean added = false;
            LinkedList<OrderItem> items = new LinkedList<>();
            LinkedList<OrderItem> itemsOfOrder = order.getItems();
            for (OrderItem item : itemsOfOrder) {
                boolean equals = item.getStore().equals(store) && item.getStatus() == 0;
                if (equals) {
                    if (!added) {
                        ans.add(order);
                        order.setItems(items);
                        added = true;
                    }
                    items.add(item);
                }
            }
        }
        return ans;
    }

    public LinkedList<OrderItem> getOrderItemsInOrderByLocation(int order_number, String store) {
        return dal.getOrderItemsInOrderByLocation(order_number, store);
    }

    public LinkedList<Order> getOrdersThatHaveNotBeenSentYet() {
        return dal.getOrdersThatHaveNotBeenSentYet();
    }

    public int getStoreRegion(String store) {
        return dal.getRegionOfStore(store);
    }
}
