package LogicLayer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Mayu on 17/03/2017.
 */
public class Validator {

    public boolean isValidID(String id)
    {
        if(id.length() != 9 || !isNumeric(id))
            return false;
        if (id.charAt(0) == '-')
            return false;
        return true;
    }

    public boolean isValidName(String name)
    {
        for (int i = 0; i <name.length() ; i++) {
            if(!Character.isAlphabetic(name.charAt(i)))
                return false;
        }
        return true;
    }

    public boolean isValidSalary(String salary)
    {
        if(isNumeric(salary))
        {
            double salaryNum = Double.parseDouble(salary);
            if(salaryNum >= 0)
                return true;
        }
        return false;
    }

    public boolean isValidDate(String dateToValidate)
    {
        if(dateToValidate == null){
            return false;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        sdf.setLenient(false); //the validation is more strict

        try {

            //if not valid, it will throw ParseException
            Date date = sdf.parse(dateToValidate);

        } catch (ParseException e) {
            return false;
        }

        return true;
    }

    private boolean isNumeric(String str)
    {
        try
        {
            double num = Double.parseDouble(str);
        }
        catch(NumberFormatException exp)
        {
            return false;
        }
        return true;
    }

    public boolean isValidPhoneNo(String str)
    {
        if(str.length() != 10 || !isNumeric(str) || str.charAt(0) != '0')
            return false;
        return true;
    }

    public boolean isValidTime(String time)
    {
        if(time.length() != 5)
            return false;
        int hours = Integer.parseInt(time.substring(0, 1));
        int minutes = Integer.parseInt(time.substring(3, 4));
        if (hours < 0 || hours > 24 || minutes < 0 || minutes > 59)
            return false;
        return true;
    }

    public boolean isFuture(String checkIfFuture) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        sdf.setLenient(false); //the validation is more strict
        try {
            //if not valid, it will throw ParseException
            Date date = sdf.parse(checkIfFuture);
            return ((( date.getTime() - new java.util.Date().getTime()))/ (1000 * 60 * 60 * 24))>=0;
        } catch (ParseException e) {}
        return false;
    }

    public boolean isPast(String checkIfFuture) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        sdf.setLenient(false); //the validation is more strict
        try {
            //if not valid, it will throw ParseException
            Date date = sdf.parse(checkIfFuture);
            return ((( date.getTime() - new java.util.Date().getTime()))/ (1000 * 60 * 60 * 24))<=0;
        } catch (ParseException e) {}
        return false;
    }

    public boolean isAccount(String input) {
        return input.matches("[0-9]+");
    }

    public static boolean IsValidRole(List<String> validRoles, String input) {
        return validRoles.contains(input);
    }


}