import org.sqlite.SQLiteConfig;

import java.sql.*;
import java.util.*;


/**
 * Created by muszk on 5/26/2017.
 */
public class CreateDB {
    Connection con = null;
    Statement stmt;

    public static void main(String[] args) {
        CreateDB db = new CreateDB();
        db.dowork();
        return;
    }

    public void dowork() {
        create();
        createRoles();
        createNotifications();
        createStores();
        createtruckModels();
        createtruck();
        createshiftType();
        createemployee();
        createdrivers();
        createrolesInShifts();
        createshiftInDates();
        createemployeeConstraints();
        createemployeesInShifts();
        createstops();
        createcategories();
        createsales();
        createsuppliers();
        createcontacts();
        createcontactsOfSuppliers();
        createproducts();
        createorders();
        createinventory();
        createitemsOnSales();
        createitemsInContract();
        creatediscounts();
        createorderItems();
        createtransportaionDocs();
        createstoresOfItemsInContract();
    }

    public void create() {
        try {
            Class.forName("org.sqlite.JDBC");
            SQLiteConfig config = new SQLiteConfig();
            config.enforceForeignKeys(true);
            con = DriverManager.getConnection("jdbc:sqlite:DB/Shop.db", config.toProperties());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createRoles() {
        try {
            stmt = con.createStatement();
            String CreationSqlSt = "CREATE TABLE IF NOT EXISTS Roles (" +
                    "Role                   TEXT            PRIMARY KEY" +
                    ");";
            stmt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the Roles table");
            e.printStackTrace();
        }
    }

    private void createNotifications() {
        try {
            stmt = con.createStatement();
            String CreationSqlSt = "CREATE TABLE IF NOT EXISTS Notifications (" +
                    "Date TEXT," +
                    "Role TEXT," +
                    "OrderNo INTEGER," +
                    "FOREIGN KEY(Role) REFERENCES Roles," +
                    "FOREIGN KEY(OrderNo) REFERENCES Orders," +
                    "PRIMARY KEY(Date,Role,OrderNo)" +
                    ");";
            stmt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the Roles table");
            e.printStackTrace();
        }
    }

    private void createStores() {
        try {
            stmt = con.createStatement();
            String CreationSqlSt = "CREATE TABLE IF NOT EXISTS stores(" +
                    "Address                TEXT            PRIMARY KEY," +
                    "Region                 INTEGER         NOT NULL," +
                    "ContactName            TEXT            NOT NULL," +
                    "PhoneNum               TEXT            NOT NULL" +
                    ");";
            stmt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the stores table");
            e.printStackTrace();
        }
    }

    private void createtruckModels() {
        try {
            stmt = con.createStatement();
            String CreationSqlSt = "CREATE TABLE IF NOT EXISTS truckModels (" +
                    "Model                  INTEGER         PRIMARY KEY," +
                    "TruckWeight            INTEGER         NOT NULL        CHECK (TruckWeight>0)," +
                    "MaxWeight              INTEGER         NOT NULL        CHECK (MaxWeight>0) " +
                    ");";
            stmt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the truckModels table");
            e.printStackTrace();
        }
    }

    private void createtruck() {
        try {
            stmt = con.createStatement();
            String CreationSqlSt = "CREATE TABLE IF NOT EXISTS trucks (" +
                    "ID                     TEXT            PRIMARY KEY," +
                    "Model                  INTEGER         NOT NULL        CHECK (Model>=1 AND Model<=5)," +
                    "FOREIGN KEY (Model)    REFERENCES truckModels(Model)" +
                    ");";
            stmt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the trucks table");
            e.printStackTrace();
        }
    }

    private void createshiftType() {
        try {
            stmt = con.createStatement();
            String CreationSqlSt = "CREATE TABLE IF NOT EXISTS ShiftsTypes (" +
                    "ShiftType              INTEGER         PRIMARY KEY" +
                    ");";
            stmt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the ShiftsTypes table");
            e.printStackTrace();
        }
    }

    private void createemployee() {
        try {
            stmt = con.createStatement();
            String CreationSqlSt = "CREATE TABLE IF NOT EXISTS Employees (" +
                    "EmployeeID                     TEXT                PRIMARY KEY," +
                    "FirstName                      TEXT                NOT NULL," +
                    "LastName                       TEXT                NOT NULL," +
                    "Role                           TEXT                NOT NULL," +
                    "Salary                         INTEGER             CHECK(salary >= 0)," +
                    "DateOfTransaction              TEXT                NOT NULL," +
                    "EmploymentTerminationDate      TEXT                DEFAULT NULL," +
                    "TermsOfTheDeal                 TEXT                DEFAULT NULL," +
                    "StoreAddress                   TEXT                DEFAULT NULL," +
                    "BankNumber                     INTEGER             NOT NULL," +
                    "BranchNumber                   INTEGER             NOT NULL," +
                    "BankAccount                    INTEGER             NOT NULL," +
                    "FOREIGN KEY (StoreAddress) REFERENCES stores    (Address)," +
                    "FOREIGN KEY (Role) REFERENCES Roles    (Role)" +
                    ")";
            stmt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the Employees table");
            e.printStackTrace();
        }
    }

    private void createdrivers() {
        try {
            stmt = con.createStatement();
            String CreationSqlSt = "CREATE TABLE IF NOT EXISTS drivers(" +
                    "ID                     TEXT            PRIMARY KEY    NOT NULL," +
                    "License                INTEGER         NOT NULL," +
                    "Region                 INTEGER         NOT NULL ," +
                    "FOREIGN KEY (ID) REFERENCES Employees(EmployeeID)" +
                    ");";
            stmt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the drivers table");
            e.printStackTrace();
        }
    }

    private void createrolesInShifts() {
        try {
            stmt = con.createStatement();
            String CreationSqlSt = "CREATE TABLE IF NOT EXISTS RolesInShifts (" +
                    "Role                           TEXT                    NOT NULL," +
                    "ShiftType                      INTEGER                 NOT NULL," +
                    "Amount                         INTEGER                 DEFAULT 1," +
                    "StoreAddress                   TEXT                    NOT NULL, " +
                    "PRIMARY KEY (ShiftType, Role,StoreAddress)," +
                    "FOREIGN KEY(StoreAddress) REFERENCES stores(address)," +
                    "FOREIGN KEY(ShiftType) REFERENCES ShiftsTypes(ShiftType)," +
                    "FOREIGN KEY(Role) REFERENCES Roles(Role)" +
                    ");";
            stmt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the RolesInShifts table");
            e.printStackTrace();
        }
    }

    private void createshiftInDates() {
        try {
            stmt = con.createStatement();
            String CreationSqlSt = "CREATE TABLE IF NOT EXISTS ShiftsInDates (" +
                    "ShiftDate              TEXT            NOT NULL," +
                    "ShiftType              INTEGER         NOT NULL," +
                    "StoreAddress           TEXT            NOT NULL," +
                    "PRIMARY KEY (ShiftType, ShiftDate,StoreAddress)," +
                    "FOREIGN KEY(StoreAddress) REFERENCES stores(address)," +
                    "FOREIGN KEY (ShiftType) REFERENCES ShiftsTypes(ShiftType)" +
                    ");";
            stmt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the ShiftsInDates table");
            e.printStackTrace();
        }
    }

    private void createemployeeConstraints() {
        try {
            stmt = con.createStatement();
            String CreationSqlSt = "CREATE TABLE IF NOT EXISTS EmployeesConstraints(" +
                    "ID                         TEXT           ," +
                    "ShiftDate                  TEXT            NOT NULL," +
                    "ShiftType                  INTEGER         NOT NULL," +
                    "StoreAddress               TEXT            NOT NULL," +
                    "ConstraintDescription      TEXT            ," +
                    "PRIMARY KEY (ShiftDate,ShiftType, StoreAddress,ID)," +
                    "FOREIGN KEY(ShiftDate,ShiftType,StoreAddress) REFERENCES ShiftsInDates(ShiftDate,ShiftType,StoreAddress)," +
                    "FOREIGN KEY (ID) REFERENCES Employees(EmployeeID)" +
                    ");";
            stmt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the EmployeesConstraints table");
            e.printStackTrace();
        }
    }

    private void createemployeesInShifts() {
        try {
            stmt = con.createStatement();
            String CreationSqlSt = "CREATE TABLE IF NOT EXISTS EmployeesInShifts (" +
                    "ShiftDate              TEXT            NOT NULL," +
                    "ShiftType              INTEGER         NOT NULL," +
                    "StoreAddress           TEXT            NOT NULL," +
                    "ID                     TEXT            NOT NULL," +
                    "RoleInShift            TEXT            NOT NULL," +
                    "PRIMARY KEY (ShiftDate,ShiftType, StoreAddress,ID)," +
                    "FOREIGN KEY(ShiftDate,ShiftType,StoreAddress) REFERENCES ShiftsInDates(ShiftDate,ShiftType,StoreAddress)," +
                    "FOREIGN KEY (ID) REFERENCES Employees(EmployeeID)," +
                    "FOREIGN KEY (RoleInShift) REFERENCES Roles(Role)" +
                    ");";
            stmt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the EmployeesInShifts table");
            e.printStackTrace();
        }
    }

    private void createstops() {
        try {
            stmt = con.createStatement();
            String CreationSqlSt = "CREATE TABLE IF NOT EXISTS stops(" +
                    "TransportationDocNo    TEXT            NOT NULL," +
                    "destination            TEXT            NOT NULL," +
                    "PRIMARY KEY (TransportationDocNo, destination)," +
                    "FOREIGN KEY (destination)              REFERENCES stores(Address)," +
                    "FOREIGN KEY (TransportationDocNo)      REFERENCES transportationDocs(ID)" +
                    ");";
            stmt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the stops table");
            e.printStackTrace();
        }
    }

    private void createcategories() {
        try {
            stmt = con.createStatement();
            String CreationSqlSt = ("CREATE TABLE IF NOT EXISTS Categories (" +
                    "ID             INTEGER     NOT NULL    PRIMARY KEY     AUTOINCREMENT," +
                    "CategoryName   TEXT        NOT NULL," +
                    "FatherCategory INTEGER," +
                    "FOREIGN KEY (FatherCategory) REFERENCES Categories(ID)" +
                    ");");
            stmt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the stops table");
            e.printStackTrace();
        }
    }

    private void createsales() {
        try {
            stmt = con.createStatement();
            String CreationSqlSt = ("CREATE TABLE IF NOT EXISTS Sales (" +
                    "Id         INTEGER     NOT NULL    PRIMARY KEY     AUTOINCREMENT, " +
                    "StartDate  TEXT        NOT NULL, " +
                    "EndDate    TEXT        NOT NULL, " +
                    "PercentOff INTEGER NOT NULL" +
                    ")");
            stmt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the stops table");
            e.printStackTrace();
        }
    }

    private void createsuppliers() {
        try {
            stmt = con.createStatement();
            String CreationSqlSt = ("CREATE TABLE IF NOT EXISTS Suppliers (" +
                    "id             TEXT    NOT NULL UNIQUE," +
                    "name           TEXT    NOT NULL," +
                    "address        TEXT    NOT NULL," +
                    "bank_account   TEXT    NOT NULL," +
                    "payment        TEXT    NOT NULL," +
                    "supply_terms   TEXT    NOT NULL," +
                    "supply_days    TEXT," +
                    "region         INTEGER NOT NULL," +
                    "PRIMARY KEY(id)" +
                    ");");
            stmt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the stops table");
            e.printStackTrace();
        }
    }

    private void createcontacts() {
        try {
            stmt = con.createStatement();
            String CreationSqlSt = ("CREATE TABLE IF NOT EXISTS Contacts (" +
                    "name           TEXT    NOT NULL," +
                    "phone_number   TEXT    NOT NULL," +
                    "PRIMARY KEY(phone_number)" +
                    ");");
            stmt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the stops table");
            e.printStackTrace();
        }
    }

    private void createcontactsOfSuppliers() {
        try {
            stmt = con.createStatement();
            String CreationSqlSt = ("CREATE TABLE IF NOT EXISTS Contacts_Of_Suppliers (" +
                    "supplier_id        TEXT        NOT NULL," +
                    "phone_number       TEXT        NOT NULL," +
                    "PRIMARY KEY(supplier_id,phone_number)," +
                    "FOREIGN KEY(phone_number) REFERENCES Contacts(phone_number)," +
                    "FOREIGN KEY(supplier_id) REFERENCES Suppliers(id)" +
                    ");");
            stmt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the stops table");
            e.printStackTrace();
        }
    }

    private void createproducts() {
        try {
            stmt = con.createStatement();
            String CreationSqlSt = ("CREATE TABLE IF NOT EXISTS Products (" +
                    "SerialNumber   TEXT        NOT NULL," +
                    "ProductName    TEXT        NOT NULL," +
                    "Manufacturer   TEXT," +
                    "Category       TEXT," +
                    "Size           TEXT," +
                    "Cost           INTEGER," +
                    "SellingPrice   INTEGER," +
                    "PRIMARY KEY(SerialNumber)," +
                    "FOREIGN KEY(Category) REFERENCES Categories(ID)" +
                    ");");
            stmt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the stops table");
            e.printStackTrace();
        }
    }

    private void createorders() {
        try {
            stmt = con.createStatement();
            String CreationSqlSt = ("CREATE TABLE IF NOT EXISTS Orders (" +
                    "order_number   INTEGER     NOT NULL        PRIMARY KEY     AUTOINCREMENT," +
                    "supplier_id    TEXT        NOT NULL," +
                    "date           TEXT        NOT NULL," +
                    "executed       TEXT," +
                    "weight         INTEGER     NOT NULL," +
                    "FOREIGN KEY(supplier_id) REFERENCES Suppliers(id)" +
                    ");");
            stmt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the stops table");
            e.printStackTrace();
        }
    }

    private void createinventory() {
        try {
            stmt = con.createStatement();
            String CreationSqlSt = ("CREATE TABLE IF NOT EXISTS Inventory (" +
                    "SerialNumber           TEXT            NOT NULL," +
                    "MinimalQuantity        INTEGER         NOT NULL," +
                    "shelfLocation          TEXT            NOT NULL," +
                    "storageLocation        TEXT            NOT NULL," +
                    "shelfQuantity          INTEGER         NOT NULL," +
                    "storageQuantity        INTEGER         NOT NULL," +
                    "shelfDefectives        INTEGER         NOT NULL," +
                    "storageDefectives      INTEGER         NOT NULL," +
                    "store                  TEXT            NOT NULL," +
                    "PRIMARY KEY(SerialNumber,store)," +
                    "FOREIGN KEY(SerialNumber) REFERENCES Products(SerialNumber)," +
                    "FOREIGN KEY(store) REFERENCES stores(Address)" +
                    ");");
            stmt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the stops table");
            e.printStackTrace();
        }
    }

    private void createitemsOnSales() {
        try {
            stmt = con.createStatement();
            String CreationSqlSt = ("CREATE TABLE IF NOT EXISTS ItemsOnSale (" +
                    "SaleId         INTEGER         NOT NULL," +
                    "ProductId      TEXT            NOT NULL," +
                    "PRIMARY KEY(SaleId, ProductId)," +
                    "FOREIGN KEY(ProductId) REFERENCES  Products(SerialNumber)," +
                    "FOREIGN KEY(SaleId)    REFERENCES  Sales(Id)" +
                    ");");
            stmt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the stops table");
            e.printStackTrace();
        }
    }

    private void createitemsInContract() {
        try {
            stmt = con.createStatement();
            String CreationSqlSt = ("CREATE TABLE IF NOT EXISTS Items_In_Contract (\n" +
                    "supplier_id        TEXT        NOT NULL," +
                    "catalog_number     TEXT        NOT NULL," +
                    "serialNumber       TEXT        NOT NULL," +
                    "price              REAL        NOT NULL," +
                    "PRIMARY KEY(supplier_id,catalog_number)," +
                    "FOREIGN KEY(serialNumber) REFERENCES Products(SerialNumber)," +
                    "FOREIGN KEY(supplier_id) REFERENCES Suppliers(id)" +
                    ");");
            stmt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the stops table");
            e.printStackTrace();
        }
    }

    private void creatediscounts() {
        try {
            stmt = con.createStatement();
            String CreationSqlSt = ("CREATE TABLE IF NOT EXISTS Discounts (" +
                    "supplier_id    TEXT        NOT NULL," +
                    "catalog_number TEXT        NOT NULL," +
                    "quantity       INTEGER     NOT NULL," +
                    "discount       REAL        NOT NULL," +
                    "PRIMARY KEY(supplier_id,catalog_number,quantity)," +
                    "FOREIGN KEY(catalog_number,supplier_id) REFERENCES Items_In_Contract(catalog_number,supplier_id)" +
                    ");");
            stmt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the stops table");
            e.printStackTrace();
        }
    }

    private void createorderItems() {
        try {
            stmt = con.createStatement();
            String CreationSqlSt = ("CREATE TABLE IF NOT EXISTS Order_Items (" +
                    "order_number       INTEGER         NOT NULL," +
                    "serial_number      TEXT            NOT NULL," +
                    "catalog_number     TEXT            NOT NULL," +
                    "quantity           INTEGER         NOT NULL," +
                    "price              REAL            NOT NULL," +
                    "discount           REAL            NOT NULL," +
                    "store              INTEGER         NOT NULL," +
                    "status             INTEGER," +
                    "PRIMARY KEY(order_number,serial_number,store)," +
                    "FOREIGN KEY(order_number) REFERENCES Orders(order_number)," +
                    "FOREIGN KEY (serial_number, store) REFERENCES Inventory(serial_number, store)" +
                    ");");
            stmt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the stops table");
            e.printStackTrace();
        }
    }

    private void createtransportaionDocs() {
        try {
            stmt = con.createStatement();
            String CreationSqlSt = "CREATE TABLE IF NOT EXISTS transportationDocs(" +
                    "ID                     TEXT            PRIMARY KEY," +
                    "Date                   TEXT            NOT NULL," +
                    "Shift                  TEXT            NOT NULL," +
                    "TruckID                TEXT            NOT NULL," +
                    "DriverID               TEXT            NOT NULL," +
                    "Supplier               TEXT            NOT NULL," +
                    "WeightAtLeave          INTEGER         DEFAULT NULL," +
                    "Region                 INTEGER         NOT NULL," +
                    "Confirmed			INTEGER         DEFAULT 0,"+
                    "FOREIGN KEY (TruckID)  REFERENCES trucks(ID)," +
                    "FOREIGN KEY (DriverID) REFERENCES drivers(ID)," +
                    "FOREIGN KEY (supplier) REFERENCES suppliers(id)," +
                    "FOREIGN KEY (Shift)    REFERENCES ShiftsTypes(ShiftType)," +
                    "FOREIGN KEY (ID)       REFERENCES Orders(order_number)" +
                    ");";
            stmt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the transportationDocs table");
            e.printStackTrace();
        }
    }

    private void createstoresOfItemsInContract() {
        try {
            stmt = con.createStatement();
            String CreationSqlSt = "CREATE TABLE IF NOT EXISTS StoresOfItemsInContract (" +
                    "supplier_id        TEXT            NOT NULL," +
                    "catalog_number     TEXT            NOT NULL," +
                    "store              TEXT            NOT NULL," +
                    "quantity           INTEGER         NOT NULL, " +
                    "PRIMARY KEY(supplier_id,catalog_number,store)," +
                    "FOREIGN KEY (supplier_id, catalog_number)     REFERENCES Items_In_Contract(supplier_id, catalog_number)," +
                    "FOREIGN KEY (store)                           REFERENCES stores(Address)" +
                    ");";
            stmt.executeUpdate(CreationSqlSt);
        } catch (SQLException e) {
            System.out.println("there was a problem in creating the transportationDocs table");
            e.printStackTrace();
        }
    }

}
