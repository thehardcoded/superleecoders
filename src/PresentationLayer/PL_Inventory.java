package PresentationLayer;

import LogicLayer.LogicManager;
import SharedClasses.*;
import sun.rmi.runtime.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Scanner;

public class PL_Inventory {

    private LogicManager bl;
    private Scanner sc;
    private String store;
    private int role;

    public PL_Inventory(LogicManager bl, Scanner sc, String store, int role) {
        this.bl = bl;
        this.sc = sc;
        this.store = store;
        this.role = role;
    }

    public void run() {
        while (true) {
            boolean printOp = false;
            try {
                showMenu();
                String choice = sc.nextLine();
                if(role==1){
                    while (!printOp) {
                        switch (choice) {
                            case "1": {
                                System.out.println("Product Report:");
                                print(bl.getAllProducts());
                                printOp = true;
                                break;
                            }
                            case "2": {
                                String catName = "default";
                                ArrayList<String> categories = new ArrayList<String>();
                                System.out.println("Type the name of the categories. When done input 'd'");
                                while (!catName.equals("d")) {
                                    catName = sc.nextLine();
                                    if (!catName.equals("d"))
                                        categories.add(catName);
                                }
                                print(bl.getAllProducts(categories));
                                printOp = true;
                                break;
                            }
                            case "3": {
                                System.out.println("Defective Report:");
                                print(bl.getAllDeffectives(store));
                                printOp = true;
                                break;
                            }
                            case "4": {
                                System.out.println("Shortage Report:");
                                print(bl.getLowQuantity(store));
                                printOp = true;
                                break;
                            }
                            case "9": {
                                System.out.println("Select an option:");
                                System.out.println("1. Serial number");
                                System.out.println("2. Product's name");
                                choice = sc.nextLine();
                                boolean legal = false;
                                String data = "";
                                while (!legal) {
                                    switch (choice) {
                                        case "1":
                                            legal = true;
                                            System.out.println("Type product's serial number...");
                                            data = sc.nextLine();
                                            while (!bl.productExists(data)) {
                                                System.out.println("Product " + data + " does not exists. please try again or type back to go back.");
                                                data = sc.nextLine();
                                                if (data.equals("back")) {
                                                    return;
                                                }
                                            }
                                            break;
                                        case "2":
                                            legal = true;
                                            System.out.println("Type product's name...");
                                            data = sc.nextLine();
                                            break;
                                        default:
                                            System.out.println("invalid option. please try again");
                                            choice = sc.nextLine();
                                    }
                                }
                                System.out.println("Type category name...");
                                String category = sc.nextLine();
                                if(bl.setProductsCategory(choice, data, category)){
                                    System.out.println("Done successfully");
                                }
                                else{
                                    System.out.println("Update failed");
                                }
                                printOp = true;
                                break;
                            }
                            case "6": {
                                print(bl.getAllCategories());
                                printOp = true;
                                break;
                            }
                            case "10": {
                                System.out.println("Enter start date in format 'dd/mm/yyyy'");
                                String startDate = sc.nextLine();
                                SimpleDateFormat format1 = new SimpleDateFormat("dd/mm/yyyy");
                                format1.setLenient(false);
                                boolean isDate1 = false;
                                while (!isDate1) {
                                    try {
                                        format1.parse(startDate);
                                        if (startDate.length() != 10 || (startDate.length() == 10) && (startDate.charAt(2) != '/' || startDate.charAt(5) != '/')) {
                                            System.out.println("invalid date. Please enter date in format 'dd/mm/yyyy' or 'back' to return to Order Manager");
                                            startDate = sc.nextLine();
                                            if (startDate.equals("back")) {
                                                return;
                                            }
                                        } else {
                                            isDate1 = true;
                                        }
                                    } catch (ParseException e) {
                                        System.out.println("invalid date. Please enter date in format 'dd/mm/yyyy' or 'back' to return to Order Manager");
                                        startDate = sc.nextLine();
                                        if (startDate.equals("back")) {
                                            return;
                                        }
                                    }
                                }
                                System.out.println("Enter end date in format 'dd/mm/yyyy'");
                                String endDate = sc.nextLine();
                                SimpleDateFormat format2 = new SimpleDateFormat("dd/mm/yyyy");
                                format2.setLenient(false);
                                boolean isDate2 = false;
                                while (!isDate2) {
                                    try {
                                        format2.parse(endDate);
                                        if (endDate.length() != 10 || (endDate.length() == 10) && (endDate.charAt(2) != '/' || endDate.charAt(5) != '/')) {
                                            System.out.println("invalid date. Please enter date in format 'dd/mm/yyyy' or 'back' to return to Order Manager");
                                            endDate = sc.nextLine();
                                            if (endDate.equals("back")) {
                                                return;
                                            }
                                        } else {
                                            isDate2 = true;
                                        }
                                    } catch (ParseException e) {
                                        System.out.println("invalid date. Please enter date in format 'dd/mm/yyyy' or 'back' to return to Order Manager");
                                        endDate = sc.nextLine();
                                        if (endDate.equals("back")) {
                                            return;
                                        }
                                    }
                                }
                                System.out.println("Type percentage off...(eg. 20, 2...)");
                                String percentOff = sc.nextLine();
                                while (!bl.isNumber(percentOff) && Integer.parseInt(percentOff) < 0) {
                                    System.out.println("Percentage off must be a positive number, try again or enter 'back' to go back");
                                    System.out.println("try again or enter 'back' to go back");
                                    percentOff = sc.nextLine();
                                    if (percentOff.equals("back")) {
                                        return;
                                    }
                                }
                                Sale newSale = new Sale(startDate, endDate, Integer.parseInt(percentOff));
                                print(bl.newSale(newSale));
                                printOp = true;
                                break;
                            }
                            case "11": {
                                System.out.println("Type sale ID...");
                                boolean ret = false;
                                String saleId = sc.nextLine();
                                while (!ret) {
                                    if (!bl.isNumber(saleId)) {
                                        System.out.println("Id must be a positive number, please enter again or enter 'back' to go back");
                                        saleId = sc.nextLine();
                                        if (saleId.equals("back")) {
                                            return;
                                        }
                                    } else if (!bl.saleExists(saleId)) {
                                        System.out.println("Id is not in the system");
                                        System.out.println("try again or enter 'back' to return to go back");
                                        saleId = sc.nextLine();
                                        if (saleId.equals("back")) {
                                            return;
                                        }
                                    } else {
                                        ret = true;
                                    }
                                }
                                print(bl.deleteSale(Integer.parseInt(saleId)));
                                printOp = true;
                                break;
                            }
                            case "12": {
                                System.out.println("Type product's serial number...");
                                String serial = sc.nextLine();
                                while (bl.productExists(serial)) {
                                    System.out.println("Product " + serial + " already exists. please try again or type back to go back.");
                                    serial = sc.nextLine();
                                    if (serial.equals("back")) {
                                        return;
                                    }
                                }
                                System.out.println("Type product's name:");
                                String name = sc.nextLine();
                                System.out.println("Type manufacture's name:");
                                String manuf = sc.nextLine();
                                System.out.println("Type weight:");
                                int weight = sc.nextInt();
                                sc.nextLine();
                                int cost = 0;
                                System.out.println("Type selling price:");
                                int price = sc.nextInt();
                                sc.nextLine();
                                Product newProduct = new Product(serial, name, manuf, null, weight, cost, price);
                                print(bl.newProduct(newProduct));
                                printOp = true;
                                break;
                            }
                            case "13": {
                                System.out.println("Type product's serial number...");
                                String productSerialNumber = sc.nextLine();
                                while (!bl.productExists(productSerialNumber)) {
                                    System.out.println("Product " + productSerialNumber + " does not exists. please try again or type back to go back.");
                                    productSerialNumber = sc.nextLine();
                                    if (productSerialNumber.equals("back")) {
                                        return;
                                    }
                                }
                                print(bl.deleteProduct(productSerialNumber));
                                printOp = true;
                                break;
                            }
                            case "14": {
                                System.out.println("Type category's name...");
                                String name = sc.nextLine();
                                Category newCat = new Category(name);
                                print(bl.newCategory(newCat));
                                printOp = true;
                                break;
                            }
                            case "15": {
                                System.out.println("Type category's ID...");
                                boolean ret = false;
                                String id = sc.nextLine();
                                while (!ret) {
                                    if (!bl.isNumber(id)) {
                                        System.out.println("Id must be a positive number, please enter again or enter 'back' to go back");
                                        id = sc.nextLine();
                                        if (id.equals("back")) {
                                            return;
                                        }
                                    } else if (!bl.categoryExists(id)) {
                                        System.out.println("Id is not in the system");
                                        System.out.println("try again or enter 'back' to return to go back");
                                        id = sc.nextLine();
                                        if (id.equals("back")) {
                                            return;
                                        }
                                    } else {
                                        ret = true;
                                    }
                                }
                                print(bl.deleteCategory(Integer.parseInt(id)));
                                printOp = true;
                                break;
                            }
                            case "16": {
                                PL_InventoryUpdate update = new PL_InventoryUpdate(bl, sc, store);
                                update.run();
                                printOp= true;
                                break;
                            }
                            case "5": {
                                System.out.println("Inventory Report:");
                                print(bl.getAllInventory(store));
                                printOp = true;
                                break;
                            }
                            case "7": {
                                print(bl.getAllSales());
                                printOp = true;
                                break;
                            }
                            case "17": {
                                System.out.println("Type Sale ID...");
                                boolean ret = false;
                                String saleId = sc.nextLine();
                                while (!ret) {
                                    if (!bl.isNumber(saleId)) {
                                        System.out.println("Id must be a positive number, please enter again or enter 'back' to go back");
                                        saleId = sc.nextLine();
                                        if (saleId.equals("back")) {
                                            return;
                                        }
                                    } else if (!bl.saleExists(saleId)) {
                                        System.out.println("Id is not in the system");
                                        System.out.println("try again or enter 'back' to return to go back");
                                        saleId = sc.nextLine();
                                        if (saleId.equals("back")) {
                                            return;
                                        }
                                    } else {
                                        ret = true;
                                    }
                                }
                                System.out.println("Choose field to update from one of these:");
                                System.out.println("StartDate | EndDate | PercentOff");
                                String field = sc.nextLine();
                                System.out.println("Enter new value:");
                                String value = sc.nextLine();
                                if(bl.editSaleField(field, value, Integer.parseInt(saleId))){
                                    System.out.println("Sale updated successfully");
                                }
                                else{
                                    System.out.println("Problem updating sale");
                                }
                                printOp = true;
                                break;
                            }
                            case "18": {
                                System.out.println("Type product's serial number...");
                                String productSerialNumber = sc.nextLine();
                                while (!bl.productExists(productSerialNumber)) {
                                    System.out.println("Product " + productSerialNumber + " does not exists. please try again or type back to go back.");
                                    productSerialNumber = sc.nextLine();
                                    if (productSerialNumber.equals("back")) {
                                        return;
                                    }
                                }
                                System.out.println("Choose field to update from one of these:");
                                System.out.println("ProductName | Manufacturer | Weight | SellingPrice");
                                String field = sc.nextLine();
                                System.out.println("Enter new value:");
                                String value = sc.nextLine();
                                if(bl.editProductField(field, value, productSerialNumber)){
                                    System.out.println("SUCCESS!");
                                }
                                else {
                                    System.out.println("FAIL!");
                                }
                                printOp = true;
                                break;
                            }
                            case "19": {
                                System.out.println("Type category's ID...");
                                boolean ret = false;
                                String data = sc.nextLine();
                                while (!ret) {
                                    if (!bl.isNumber(data)) {
                                        System.out.println("Id must be a positive number, please enter again or enter 'back' to go back");
                                        data = sc.nextLine();
                                        if (data.equals("back")) {
                                            return;
                                        }
                                    } else if (!bl.categoryExists(data)) {
                                        System.out.println("Id is not in the system");
                                        System.out.println("try again or enter 'back' to return to go back");
                                        data = sc.nextLine();
                                        if (data.equals("back")) {
                                            return;
                                        }
                                    } else {
                                        ret = true;
                                    }
                                }
                                System.out.println("Choose field to update from one of these:");
                                System.out.println("CategoryName | FatherCategory");
                                String field = sc.nextLine();
                                System.out.println("Enter Category ID:");
                                String value = sc.nextLine();
                                if(bl.editCategoryField(field, value, Integer.parseInt(data))){
                                    System.out.println("Updated Successfully");
                                }
                                else{
                                    System.out.println("Problem updating category");
                                }

                                printOp = true;
                                break;
                            }
                            case "20": {
                                System.out.println("Add item to sale by:");
                                System.out.println("1. Serial number");
                                System.out.println("2. Category's ID");
                                choice = sc.nextLine();
                                boolean legal = false;
                                String data = "";
                                while (!legal) {
                                    switch (choice) {
                                        case "1":
                                            legal = true;
                                            System.out.println("Type product's serial number...");
                                            data = sc.nextLine();
                                            while (!bl.productExists(data)) {
                                                System.out.println("Product " + data + " does not exists. please try again or type back to go back.");
                                                data = sc.nextLine();
                                                if (data.equals("back")) {
                                                    return;
                                                }
                                            }
                                            break;
                                        case "2":
                                            legal = true;
                                            System.out.println("Type category's ID...");
                                            boolean ret = false;
                                            data = sc.nextLine();
                                            while (!ret) {
                                                if (!bl.isNumber(data)) {
                                                    System.out.println("Id must be a positive number, please enter again or enter 'back' to go back");
                                                    data = sc.nextLine();
                                                    if (data.equals("back")) {
                                                        return;
                                                    }
                                                } else if (!bl.categoryExists(data)) {
                                                    System.out.println("Category is not in the system");
                                                    System.out.println("try again or enter 'back' to return to go back");
                                                    data = sc.nextLine();
                                                    if (data.equals("back")) {
                                                        return;
                                                    }
                                                } else {
                                                    ret = true;
                                                }
                                            }
                                            break;
                                        default:
                                            System.out.println("invalid option. please try again");
                                            choice = sc.nextLine();
                                    }
                                }
                                System.out.println("Type Sale ID...");
                                boolean ret = false;
                                String saleId = sc.nextLine();
                                while (!ret) {
                                    if (!bl.isNumber(saleId)) {
                                        System.out.println("Id must be a positive number, please enter again or enter 'back' to go back");
                                        saleId = sc.nextLine();
                                        if (saleId.equals("back")) {
                                            return;
                                        }
                                    } else if (!bl.saleExists(saleId)) {
                                        System.out.println("Id is not in the system");
                                        System.out.println("try again or enter 'back' to return to go back");
                                        saleId = sc.nextLine();
                                        if (saleId.equals("back")) {
                                            return;
                                        }
                                    } else {
                                        ret = true;
                                    }
                                }
                                print(bl.addItemsToSale(choice, data, Integer.parseInt(saleId)));
                                printOp = true;
                                break;
                            }
                            case "8": {
                                System.out.println("Type Sale ID...");
                                boolean ret = false;
                                String saleId = sc.nextLine();
                                while (!ret) {
                                    if (!bl.isNumber(saleId)) {
                                        System.out.println("Id must be a positive number, please enter again or enter 'back' to go back");
                                        saleId = sc.nextLine();
                                        if (saleId.equals("back")) {
                                            return;
                                        }
                                    } else if (!bl.saleExists(saleId)) {
                                        System.out.println(" is not in the system");
                                        System.out.println("try again or enter 'back' to return to go back");
                                        saleId = sc.nextLine();
                                        if (saleId.equals("back")) {
                                            return;
                                        }
                                    } else {
                                        ret = true;
                                    }
                                }
                                System.out.println("Items on sale " + saleId +":");
                                print(bl.showAllItemsOnSale(Integer.parseInt(saleId)));
                                printOp = true;
                                break;
                            }
                            case "21": {
                                System.out.println("Type Sale ID...");
                                boolean ret = false;
                                String saleId = sc.nextLine();
                                while (!ret) {
                                    if (!bl.isNumber(saleId)) {
                                        System.out.println("Id must be a positive number, please enter again or enter 'back' to go back");
                                        saleId = sc.nextLine();
                                        if (saleId.equals("back")) {
                                            return;
                                        }
                                    } else if (!bl.saleExists(saleId)) {
                                        System.out.println("Id is not in the system");
                                        System.out.println("try again or enter 'back' to return to go back");
                                        saleId = sc.nextLine();
                                        if (saleId.equals("back")) {
                                            return;
                                        }
                                    } else {
                                        ret = true;
                                    }
                                }
                                System.out.println("Type product's serial number...");
                                String productSerialNumber = sc.nextLine();
                                while (!bl.productExists(productSerialNumber)) {
                                    System.out.println("Product " + productSerialNumber + " does not exists. please try again or type back to go back.");
                                    productSerialNumber = sc.nextLine();
                                    if (productSerialNumber.equals("back")) {
                                        return;
                                    }
                                }
                                print(bl.removeItemFromSale(Integer.parseInt(saleId), productSerialNumber));
                                printOp = true;
                                break;
                            }
                            case "22": {
                                System.out.println("Type product's serial number...");
                                String serial = sc.nextLine();
                                while (!bl.productExists(serial) || bl.productExistsInStore(serial,store)) {
                                    System.out.println("Product " + serial + " not exists OR already exists in Store please try again or type back to go back.");
                                    serial = sc.nextLine();
                                    if (serial.equals("back")) {
                                        return;
                                    }
                                }
                                System.out.println("Type minimal quantity:");
                                String minimal = sc.nextLine();
                                while (!bl.isNumber(minimal) && Integer.parseInt(minimal)<0) {
                                    System.out.println("Minimal quantity must be a positive number, try again or enter 'back' to go back");
                                    System.out.println("try again or enter 'back' to go back");
                                    minimal = sc.nextLine();
                                    if (minimal.equals("back")) {
                                        return;
                                    }
                                }
                                System.out.println("Type shelf location:");
                                String storagel = sc.nextLine();
                                System.out.println("Type storage location:");
                                String shelfl = sc.nextLine();
                                System.out.println("Type shelf quantity:");
                                int shelf = sc.nextInt();
                                sc.nextLine();
                                System.out.println("Type storage quantity:");
                                int storage = sc.nextInt();
                                Stock stock = new Stock(serial, Integer.parseInt(minimal), shelfl, storagel, shelf, storage, 0, 0, store);
                                print(bl.newInventory(stock));
                                printOp = true;
                                sc.nextLine();
                                break;
                            }
                            case "23": {
                                return;
                            }
                            default:
                                System.out.println("invalid option, please try again");
                                choice = sc.nextLine();
                        }
                    }
                }
                else{
                    while (!printOp) {
                        switch (choice) {
                            case "1": {
                                System.out.println("Product Report:");
                                print(bl.getAllProducts());
                                printOp = true;
                                break;
                            }
                            case "2": {
                                String catName = "default";
                                ArrayList<String> categories = new ArrayList<String>();
                                System.out.println("Type the name of the categories. When done input 'd'");
                                while (!catName.equals("d")) {
                                    catName = sc.nextLine();
                                    if (!catName.equals("d"))
                                        categories.add(catName);
                                }
                                print(bl.getAllProducts(categories));
                                printOp = true;
                                break;
                            }
                            case "3": {
                                System.out.println("Defective Report:");
                                print(bl.getAllDeffectives(store));
                                printOp = true;
                                break;
                            }
                            case "4": {
                                System.out.println("Shortage Report:");
                                print(bl.getLowQuantity(store));
                                printOp = true;
                                break;
                            }
                            case "6": {
                                System.out.println("Category Report:");
                                print(bl.getAllCategories());
                                printOp = true;
                                break;
                            }
                            case "5": {
                                System.out.println("Inventory Report:");
                                print(bl.getAllInventory(store));
                                printOp = true;
                                break;
                            }
                            case "7": {
                                System.out.println("Sales Report:");
                                print(bl.getAllSales());
                                printOp = true;
                                break;
                            }
                            case "8": {
                                System.out.println("Type Sale ID...");
                                boolean ret = false;
                                String saleId = sc.nextLine();
                                while (!ret) {
                                    if (!bl.isNumber(saleId)) {
                                        System.out.println("Id must be a positive number, please enter again or enter 'back' to go back");
                                        saleId = sc.nextLine();
                                        if (saleId.equals("back")) {
                                            return;
                                        }
                                    } else if (!bl.saleExists(saleId)) {
                                        System.out.println("Id is not in the system");
                                        System.out.println("try again or enter 'back' to return to go back");
                                        saleId = sc.nextLine();
                                        if (saleId.equals("back")) {
                                            return;
                                        }
                                    } else {
                                        ret = true;
                                    }
                                }
                                System.out.println("Items on sale " + saleId +":");
                                print(bl.showAllItemsOnSale(Integer.parseInt(saleId)));
                                printOp = true;
                                break;
                            }
                            case "9": {
                                return;
                            }
                            default:
                                System.out.println("invalid option, please try again");
                                choice = sc.nextLine();
                        }
                    }
                }
            } catch (RuntimeException e) {
                System.out.println("invalid input. please try again or exit");
            }
        }
    }

    /**
     * Prints the main menu
     */
    private void showMenu() {
        System.out.println("Welcome to Inventory Manager");
        System.out.print("What would you like to do?\n" +
                "1) Products Report\n" +                    //for all
                "2) Products Report (By Category)\n" +      //for all
                "3) Defectives Report\n" +                  //for specific store
                "4) Shortage Report\n" +                    //for specific store
                "5) Inventory Report\n" +                  //for specific store
                "6) Categories Report\n" +                  //for all
                "7) Sales Report\n" +              //for all
                "8) Items on a sale report\n");
        if(role==1){
            System.out.println("9) Set category for product\n" +           //for all
                    "10) Add new sale\n" +                       //for all
                    "11) Remove sale\n" +                        //for all
                    "12) Add new product\n" +                    //for all
                    "13) Remove product\n" +                    //for all
                    "14) Add new category\n" +                  //for all
                    "15) Remove category\n" +                   //for all
                    "16) Update inventory's amounts\n" +        //for specific store
                    "17) Edit sale details\n" +                 //for all
                    "18) Edit product details\n" +              //for all
                    "19) Edit category\n" +                     //for all
                    "20) Add items to a sale\n" +               //for all
                    "21) Remove item from a sale\n" +           //for all
                    "22) Add new item to store\n" +         //for specific store
                    "23) Return to main menu");                 //for all
        }
        else{
            System.out.println("9) Return to main menu");                 //for all
        }
    }

    /**
     * Prints a given String
     *
     * @param str
     */
    private void print(String str) {
        System.out.println(str);
    }

    /**
     * Prints the toString method of every element in the list
     *
     * @param list
     */
    private void print(ArrayList<? extends Object> list) {
        for (Object item : list) {
            System.out.println(item.toString());
        }
    }

}
