package PresentationLayer;

import LogicLayer.LogicManager;
import PresentationLayer.PL_Inventory;
import PresentationLayer.PL_Order;
import PresentationLayer.PL_Supplier;
import PresentationLayer.PresentationManager;
import SharedClasses.DTOs.EmployeeConstraintDTO;
import SharedClasses.DTOs.EmployeeDTO;
import SharedClasses.DTOs.TransportationDocDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by SHLOMI PAKADO on 5/23/2017.
 */
public class StoreManagerMenu {

    private LogicManager logM;
    private EmployeeDTO user;
    private String store;
    private Scanner sc = new Scanner(System.in);
    private PresentationManager presentationManager;
    private PL_Order OrederDetailsMenu;
    private PL_Supplier supplierDetailsMenu;
    private PL_Inventory invetoryInformationSource;

    public StoreManagerMenu (LogicManager logM, PresentationManager presentationManager)
    {
        this.logM = logM;
        this.presentationManager = presentationManager;
    }

    public void start (EmployeeDTO user)
    {
        this.user = user;
        this.store = user.getStore();
        this.OrederDetailsMenu=new PL_Order(logM , new Scanner(System.in),0,store);
        this.supplierDetailsMenu=new PL_Supplier(logM,new Scanner(System.in), 1, store);
        this.invetoryInformationSource=new PL_Inventory(logM,new Scanner(System.in), user.getStore(),0 );
        StoreManagerMainMenu();
    }

    public void StoreManagerMainMenu()
    {
        clearScreen();
        String option;
        System.out.println("Hello "+ user.getFirstName()+" what would you like to do? (Please choose an option number and press Enter.)");
        System.out.println("1.Watch scheduled shifts.");
        System.out.println("2.Get employee details.");
        System.out.println("3.Watch employee Constraints.");
        System.out.println("4.View transportation docs.");
        System.out.println("5.Watch order details.");
        System.out.println("6.Watch inventory details.");
        System.out.println("7.Manage supplier details.");
        System.out.println("0.Exit.");
        option=sc.nextLine();

        switch (option)
        {
            case "1" :
            {
                watchShiftByDate();
                break;
            }
            case "2" :
            {
                InsertEmployeeId();
                break;
            }
            case "3" :
            {
                getEmployeeConstrains();
                break;
            }
            case "4" :
            {
                System.out.println("The confirmed transportation documents for your store are:");
                List<Integer>  confirmedIDList = this.logM.getConfirmedTransDocsIDByStore(user.getStore());
                if(confirmedIDList.isEmpty()) {
                    System.out.println("There are no confirmed documents for your store.\nYou will be returned to the main menu.\n");
                    StoreManagerMainMenu();
                    return;
                }
                int i = 1;
                for(Integer id : confirmedIDList){
                    System.out.println(i + ") " + id);
                    i++;
                }
                System.out.println("\nChoose the document you would like to open and press enter.");
                while (true) {
                    try {
                        i = Integer.parseInt(sc.nextLine());
                        if (i > confirmedIDList.size())
                            System.out.println("Invalid Input,please enter a valid value and press enter.");
                        else
                            break;
                    } catch (NumberFormatException e) {
                        System.out.println("Invalid value!\n");
                    }
                }
                TransportationDocDTO transDoc = this.logM.getTransportDoc(confirmedIDList.get(i - 1));
                showDetails(transDoc);
                StoreManagerMainMenu();
                break;
            }
            case "5" :
            {
                this.OrederDetailsMenu.run();
                StoreManagerMainMenu();
                break;
            }
            case "6" :
            {
                this.invetoryInformationSource.run();
                StoreManagerMainMenu();
                break;
            }
            case "7" :
            {

                this.supplierDetailsMenu.run();
                StoreManagerMainMenu();
                break;
            }
            case "0" :
            {
                break;
            }
            default:
            {
                System.out.println("This option is not valid.\nPlease insert a valid option number.\n");
                StoreManagerMainMenu();
                break;
            }



        }}




    protected void watchShiftByDate()
    {
        String store=user.getStore();
        System.out.println("Welcome to the shift details menu.");
        System.out.println("Please insert the date of the shift you would like to watch.\n(the date format is DD-MM-YYYY .)");
        String from=sc.nextLine();
        if((!((LogicManager)this.logM).validator.isValidDate(from)))
        {
            DecideIfToReturn();
            return;
        }
        System.out.println("What shift would you like to watch?");
        System.out.println("1) morning shift.");
        System.out.println("2) Evening shift.");
        String option="";
        do
        {
            option=sc.nextLine();
            if((!option.equals("1"))&&(!option.equals("2")))
                System.out.println("This option is not valid.\nPlease insert a valid option.");
        }
        while((!option.equals("1"))&&(!option.equals("2")));
        ArrayList<String[]> ans=((LogicManager)this.logM).getEmployeesInShiftsBetweenDates(from,Integer.parseInt(option),store);
        int i=0;
        if(ans.size()==0)
        {
            System.out.println("The chosen shift is not set.");
            StoreManagerMainMenu();
            return;
        }
        for(String[] employee:ans)
        {
            System.out.println(i+") Role : "+employee[3]+" , ID : "+employee[0]+" , First Name : "+employee[1]+" , Last Name : "+employee[2]);
            i++;
        }
        sc.reset();
        System.out.println("Press Enter to return to the main menu.");
        String temp=sc.nextLine();
        StoreManagerMainMenu();
    }


    private void DecideIfToReturn()
    {
        String option;
        do
        {
            System.out.println("The given Date is not valid.\nPress 1 to try again or 0 to return to the main menu.");
            option=sc.nextLine();
            if (option.equals("1"))
            {
                watchShiftByDate();
            }
            else if(option.equals("0"))
            {
                presentationManager.start();
            }
            else
            {
                System.out.println("Please insert a valid input.");
            }
        }
        while((!option.equals("1"))&&(!option.equals("0")));
    }


    protected void InsertEmployeeId()
    {
        clearScreen();
        System.out.println("Selection menu:");
        System.out.println("Please Enter the Employee ID you would like to select:");
        String ID = sc.nextLine();
        if (((LogicManager)this.logM).validator.isValidID(ID))
        {
            EmployeeDTO selected = this.logM.getEmployee(ID);
            if (selected == null)
            {
                wrongId();
                return;
            }
            else
            {
                System.out.println("The employee details are:");
                showDetails(selected);
                StoreManagerMainMenu();
            }
        }
        else
        {
            System.out.println("The given ID is not valid.\n(A valid ID should contain at least 9 digits.)");
            System.out.println("Please insert a valid ID.");
            InsertEmployeeId();
        }



    }

    private void wrongId ()
    {
        String option;
        System.out.println("the given ID is not registered.");
        do{
            System.out.println("press 1 to try again or press 0 to return to the main menu.");
            option=sc.nextLine();
            if(option.equals("1"))
                InsertEmployeeId();
            else if(option.equals("0"))
            {
                StoreManagerMainMenu();
                return;
            }
            else
            {
                System.out.println("please Enter a valid option.");
            }
        }
        while((!option.equals("1"))&&(!option.equals("0")));
    }

    private void showDetails (EmployeeDTO selected)
    {
        System.out.println(selected.toString());
    }


    private void clearScreen()
    {
        System.out.println();
    }

    protected void getEmployeeConstrains() {
        System.out.println("please insert the employees id you would like to select.");
        String input=sc.nextLine();
        if(!((LogicManager)logM).validator.isValidID(input))
        {
            System.out.println("The given ID is not Valid.\n(A valid ID should contain exactly 9 digits.)");
            System.out.println("Please enter to try again or press 0 to return to the main menu.\n");
            System.out.println("Press Enter to return to the main menu.");
            String temp=sc.nextLine();
            if(temp.equals("0"))
                StoreManagerMainMenu();
            else
                getEmployeeConstrains();
            return;
        }
        else if(!this.logM.employeeIDExist(input))
        {
            System.out.println("the given ID is not registered.\nPlease try again.");
            getEmployeeConstrains();
            return;
        }
        else
        {
            EmployeeDTO selected=this.logM.getEmployee(input);
            List<EmployeeConstraintDTO> constraints=((LogicManager)logM).getEmployeeConstraint(selected);
            System.out.println("The employee constraints are:");
            for (int i = 0; i < constraints.size(); i++) {
                String shiftType="morning shift.";
                System.out.println("date : "+constraints.get(i).getDate()+"\n");
                if(constraints.get(i).getShiftType()==2)
                    shiftType="evening shift.";
                System.out.println("shift : "+shiftType+"\n");
                System.out.println("the constraint : "+constraints.get(i).getConstraint()+"\n");
            }
        }

        System.out.println("Press Enter to return to the main menu.");
        String temp=sc.nextLine();
        StoreManagerMainMenu();
    }


    public void showDetails(TransportationDocDTO selected)
    {
        System.out.println("The selected document is:");
        System.out.println("ID: " + selected.getID());
        System.out.println("Date: " + EmployeeDTO.shiftDateFormat(selected.getDate()));
        if(selected.getShift()==1)
            System.out.println("Shift: Morning");
        else
            System.out.println("Shift: Evening");
        System.out.println("Truck ID: " + selected.getTruckID());
        System.out.println("Driver ID: " + selected.getDriverID());
        System.out.println("Supplier: " + selected.getSupplier());
        if(selected.getWeightAtLeave()!=0)
            System.out.println("Weight at leave of the supplier: " + selected.getWeightAtLeave());
        List<String> currStopsList = logM.getStops(selected.getID());
        System.out.println("Stops:");
        if(currStopsList == null || currStopsList.size() == 0)
            System.out.println("none");
        int i = 1;
        for (String stop : currStopsList) {
            System.out.println(i + ") " + stop);
            i++;
        }
    }






}
