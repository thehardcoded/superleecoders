package PresentationLayer;
import LogicLayer.LogicManager;

import java.util.List;
import java.util.Scanner;

public class StorekeeperMainMenu {
    private LogicManager bl;
    private Scanner sc;
    private String store;

    public StorekeeperMainMenu(LogicManager bl) {
        this.bl = bl;
        sc = new Scanner(System.in);
        this.store = "";
    }

    public void start(String store) {
        this.store = store;
        while (true) {
            boolean printOp = false;
            System.out.println("Welcome to Storekeeper Menu");
            System.out.println("Select an option:");
            System.out.println("1. Supplier Manager");
            System.out.println("2. Inventory Manager");
            System.out.println("3. Order Manager");
            System.out.println("4. Exit");
            String option = sc.nextLine();
            while (!printOp) {
                switch (option) {
                    case "1":
                        PL_Supplier suppliersMenu = new PL_Supplier(bl, sc, 1,store);
                        suppliersMenu.run();
                        printOp = true;
                        break;
                    case "2":
                        PL_Inventory inventoryMenu = new PL_Inventory(bl, sc, store, 1);
                        inventoryMenu.run();
                        printOp = true;
                        break;
                    case "3":
                        PL_Order ordersMenu = new PL_Order(bl, sc, 1, store);
                        ordersMenu.run();
                        printOp = true;
                        break;
                    case "4":
                        return;
                    default:
                        System.out.println("invalid option, please try again");
                        option = sc.nextLine();
                }
            }
        }
    }

}
