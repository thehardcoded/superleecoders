package PresentationLayer;

import LogicLayer.LogicManager;
import SharedClasses.Order;
import SharedClasses.SupplyTerms;
import sun.rmi.runtime.Log;

import java.util.LinkedList;
import java.util.Scanner;

public class PL_Order {

    private LogicManager bl;
    private Scanner sc;
    private int role;
    private String store;
    private OrderArrivedMenu orderArrivedMenu;

    public PL_Order(LogicManager bl, Scanner sc, int role, String store) {
        this.bl = bl;
        this.sc = sc;
        this.role = role;
        this.store = store;
        orderArrivedMenu = new OrderArrivedMenu(bl, sc, store);
    }

    public void run() {
        while (true) {
            boolean printOp = false;
            System.out.println("Welcome to Order Manager");
            System.out.println("Select an option:");
            System.out.println("1. Show all orders");
            System.out.println("2. Show orders by order number");
            System.out.println("3. Show orders by supplier id");
            if (role == 1) {
                System.out.println("4. Close order");
                System.out.println("5. Approve arrival of orders");
                System.out.println("6. Update item's quantity in periodic order");
                System.out.println("7. Remove product from order that wasn't closed");
                System.out.println("8. Delete order that wasn't sent");
                System.out.println("9. Update quantity of product in lacks of inventory order that wasn't sent");
                System.out.println("10. Return to main menu");
            } else {
                System.out.println("4. Return to main menu");
            }
            String option = sc.nextLine();
            if(role == 0){
                while (!printOp) {
                    switch (option) {
                        case "1":
                            System.out.println("Orders Report:");
                            showAllOders();
                            printOp = true;
                            break;
                        case "2":
                            showOrdersByOrderNumber();
                            printOp = true;
                            break;
                        case "3":
                            showOrdersBySupplierID();
                            printOp = true;
                            break;
                        case "4":
                            return;
                        default:
                            System.out.println("invalid option, please try again");
                            option = sc.nextLine();
                    }
                }
            }
            else{
                while (!printOp) {
                    switch (option) {
                        case "1":
                            showAllOders();
                            printOp = true;
                            break;
                        case "2":
                            showOrdersByOrderNumber();
                            printOp = true;
                            break;
                        case "3":
                            showOrdersBySupplierID();
                            printOp = true;
                            break;
                        case "4":
                            setOrderSentToSupplier();
                            printOp = true;
                            break;
                        case "5":
                            orderArrivedMenu.start();
                            printOp = true;
                            break;
                        case "6":
                            UpdateQuantityItemInPereodicOrder();
                            printOp = true;
                            break;
                        case "7":
                            removeOrderItemFromOrderNotSent();
                            printOp = true;
                            break;
                        case "8":
                            removeOrderNotSent();
                            printOp = true;
                            break;
                        case "9":
                            updateItemQuantityInLacksOfInventory();
                            printOp = true;
                            break;
                        case "10":
                            return;
                        default:
                            System.out.println("invalid option, please try again");
                            option = sc.nextLine();
                    }
                }
            }
        }
    }

    private void updateItemQuantityInLacksOfInventory() {
        System.out.println("Enter order's order number:");
        String order_number = sc.nextLine();
        boolean ret = false;
        while (!ret) {
            if (!bl.isNumber(order_number)) {
                System.out.println("Order number must be a positive number, please enter again or enter 'back' to go back");
                order_number = sc.nextLine();
                if (order_number.equals("back")) {
                    return;
                }
            } else if (!bl.orderExists(Integer.parseInt(order_number))) {
                System.out.println("Order number is not in the system. Please try again or enter 'back' to return to go back");
                order_number = sc.nextLine();
                if (order_number.equals("back")) {
                    return;
                }
            } else if (bl.getExecutedState(Integer.parseInt(order_number)) != 1) {
                System.out.println("Cannot alter order that was already sent. Please try again or enter 'back' to return to go back");
                order_number = sc.nextLine();
                if (order_number.equals("back")) {
                    return;
                }
            }else if(bl.getOrderByOrderNumber(Integer.parseInt(order_number)).getFirst().getSupplier().getTerms() == SupplyTerms.SupplyOnSetDays ){
                System.out.println("Cannot alter orders from a periodic order supplier");
                order_number = sc.nextLine();
                if (order_number.equals("back")) {
                    return;
                }
            }
            else {
                ret = true;
            }
        }
        System.out.println("Enter product's serial number:");
        String serialNumber = sc.nextLine();
        while (!bl.productExists(serialNumber)) {
            System.out.println("Product doesn't exist in the system. Try again or enter back to go back");
            serialNumber = sc.nextLine();
            if (serialNumber.equals("back")) {
                return;
            }
        }
        System.out.println("Enter new quantity:");
        String quantity = sc.nextLine();
        while (!bl.isNumber(quantity) && Integer.parseInt(quantity) < 0) {
            System.out.println("Quantity must be a positive number, try again or enter 'back' to go back");
            System.out.println("try again or enter 'back' to go back");
            quantity = sc.nextLine();
            if (quantity.equals("back")) {
                return;
            }
        }
        if (bl.updateItemQuantityInLacksOfInventory(Integer.parseInt(order_number), serialNumber, Integer.parseInt(quantity), store)) {
            System.out.println("Quantity updated successfully");
        } else {
            System.out.println("Problem updating the quantity");
        }
    }

    private void removeOrderNotSent() {
        LinkedList<Order> orders = bl.getOrdersThatHaveNotBeenSentYet();
        System.out.println("Orders that can be removed:");
        for(Order order : orders){
            System.out.println("Order Number: " + order.getOrder_number());
        }

        System.out.println("\nEnter order's order number:");
        String order_number = sc.nextLine();
        boolean ret = false;
        while (!ret) {
            if (!bl.isNumber(order_number)) {
                System.out.println("Order number must be a positive number, please enter again or enter 'back' to go back");
                order_number = sc.nextLine();
                if (order_number.equals("back")) {
                    return;
                }
            } else if (!bl.orderExistsInList(Integer.parseInt(order_number), orders)) {
                System.out.println("Order number is not in the list. Please try again or enter 'back' to return to go back");
                order_number = sc.nextLine();
                if (order_number.equals("back")) {
                    return;
                }
            } else if (bl.getExecutedState(Integer.parseInt(order_number)) != 1) {
                System.out.println("Cannot delete order that was already sent. Please try again or enter 'back' to return to go back");
                order_number = sc.nextLine();
                if (order_number.equals("back")) {
                    return;
                }
            } else {
                ret = true;
            }
        }
        if (bl.removeOrderThatHasNotBeenSentYet(Integer.parseInt(order_number))) {
            System.out.println("Order was removed successfully");
        } else {
            System.out.println("Problem removing order");
        }

    }

    private void removeOrderItemFromOrderNotSent() {
        System.out.println("Enter order's order number:");
        String order_number = sc.nextLine();
        boolean ret = false;
        while (!ret) {
            if (!bl.isNumber(order_number)) {
                System.out.println("Order number must be a positive number, please enter again or enter 'back' to go back");
                order_number = sc.nextLine();
                if (order_number.equals("back")) {
                    return;
                }
            } else if (!bl.orderExists(Integer.parseInt(order_number))) {
                System.out.println("Order number is not in the system. Please try again or enter 'back' to return to go back");
                order_number = sc.nextLine();
                if (order_number.equals("back")) {
                    return;
                }
            } else if (bl.getExecutedState(Integer.parseInt(order_number)) != 1) {
                System.out.println("Cannot alter order that was already sent. Please try again or enter 'back' to return to go back");
                order_number = sc.nextLine();
                if (order_number.equals("back")) {
                    return;
                }
            } else {
                ret = true;
            }
        }
        System.out.println("Enter product's serial number:");
        String serialNumber = sc.nextLine();
        while (!bl.productExists(serialNumber)) {
            System.out.println("Product doesn't exist in the system. Try again or enter back to go back");
            serialNumber = sc.nextLine();
            if (serialNumber.equals("back")) {
                return;
            }
        }
        if (bl.removeItemFromOrderThatHasNotBeenSentYet(Integer.parseInt(order_number), serialNumber, store)) {
            System.out.println("Item in order was removed successfully");
        } else {
            System.out.println("Problem removingthe item in order");
        }

    }

    private void setArrival() {
        System.out.println("Enter order's order number:");
        boolean ret = false;
        String order_number = sc.nextLine();
        while (!ret) {
            if (!bl.isNumber(order_number)) {
                System.out.println("Order number must be a positive number, please enter again or enter 'back' to go back");
                order_number = sc.nextLine();
                if (order_number.equals("back")) {
                    return;
                }
            } else if (!bl.orderExists(Integer.parseInt(order_number))) {
                System.out.println("Order number is not in the system. Please try again or enter 'back' to return to go back");
                order_number = sc.nextLine();
                if (order_number.equals("back")) {
                    return;
                }
            } else {
                ret = true;
            }
        }
        if (bl.setArrival(Integer.parseInt(order_number), store)) {
            System.out.println("Arrival was set successfully");
        } else {
            System.out.println("Problem setting arrival");
        }
    }

    private void UpdateQuantityItemInPereodicOrder() {
        System.out.println("Enter supplier's id number:");
        boolean ret = false;
        String id = sc.nextLine();
        while (!ret) {
            if (!bl.isNumber(id)) {
                System.out.println("Id must be a positive number, please enter again or enter 'back' to go back");
                id = sc.nextLine();
                if (id.equals("back")) {
                    return;
                }
            } else if (!bl.supplierExists(id)) {
                System.out.println("Id is not in the system. try again or enter 'back' to return to go back");
                System.out.println("try again or enter 'back' to return to go back");
                id = sc.nextLine();
                if (id.equals("back")) {
                    return;
                }
            } else if (bl.getSupplier(id).getTerms() != SupplyTerms.SupplyOnSetDays) {
                System.out.println("This supplier doesn't supply on set days .try again or enter 'back' to return to go back");
                id = sc.nextLine();
                if (id.equals("back")) {
                    return;
                }
            } else {
                ret = true;
            }
        }
        System.out.println("Enter item catalog number number:");
        String catalogNum = sc.nextLine();
        while (!bl.ItemExists(id, catalogNum)) {
            System.out.println("Item doesn't exist in the system. Try again or enter back to go back");
            catalogNum = sc.nextLine();
            if (catalogNum.equals("back")) {
                return;
            }
        }
        System.out.println("Enter quantity to order on pereodic order:");
        String quantity1 = sc.nextLine();
        int quantity = 0;
        while (!bl.isNumber(quantity1)) {
            System.out.println("Enter quantity to order on pereodic order:");
            quantity1 = sc.nextLine();
            if (quantity1.equals("back")) {
                return;
            }
        }
        quantity = Integer.parseInt(quantity1);
        if (bl.UpdateQuantityItemInPereodicOrder(id, catalogNum, quantity, store)) {
            System.out.println("Updating quantity of item in pereodic order with supplier was successful\n");
        } else {
            System.out.println("Problem updating quantity\n");
        }
    }

    private void setOrderSentToSupplier() {
        LinkedList<Order> orders = bl.getOrdersThatHaveNotBeenSentYet();
        System.out.println("Orders that can be closed:");
        for(Order order : orders){
            System.out.println("Order Number: " + order.getOrder_number());
        }
        System.out.println("Enter order's order number:");
        boolean ret = false;
        String order_number = sc.nextLine();
        while (!ret) {
            if (!bl.isNumber(order_number)) {
                System.out.println("Order number must be a positive number, please enter again or enter 'back' to go back");
                order_number = sc.nextLine();
                if (order_number.equals("back")) {
                    return;
                }

            } else if (!bl.orderExistsInList(Integer.parseInt(order_number), orders)) {
                System.out.println("Order number is not in the list. Please try again or enter 'back' to return to go back");
                order_number = sc.nextLine();
                if (order_number.equals("back")) {
                    return;
                }
            } else {
                ret = true;
            }
        }
        int orderNo = Integer.parseInt(order_number);
        bl.getOrderFromList(orderNo, orders);
        if (bl.setOrderSentToSupplier(bl.getOrderFromList(orderNo, orders))) {
            System.out.println("Transportation was set successfully for the order");
        } else {
            System.out.println("Transportation couldn't be set for the order. due to missing conditions, notification was sent to the relevant roles.");
        }
    }

    private void showOrdersByOrderNumber() {
        System.out.println("Enter order's order number:");
        boolean ret = false;
        String order_number = sc.nextLine();
        while (!ret) {
            if (!bl.isNumber(order_number)) {
                System.out.println("Order number must be a positive number, please enter again or enter 'back' to go back");
                order_number = sc.nextLine();
                if (order_number.equals("back")) {
                    return;
                }
            } else if (!bl.orderExists(Integer.parseInt(order_number))) {
                System.out.println("Order number is not in the system. Please try again or enter 'back' to return to go back");
                order_number = sc.nextLine();
                if (order_number.equals("back")) {
                    return;
                }
            } else {
                ret = true;
            }
        }
        LinkedList<Order> orders = bl.getOrderByOrderNumber(Integer.parseInt(order_number));
        for (Order order : orders) {
            System.out.println(order.toString());
        }
    }

    private void showOrdersBySupplierID() {
        System.out.println("Enter supplier's id number:");
        boolean ret = false;
        String id = sc.nextLine();
        while (!ret) {
            if (!bl.isNumber(id)) {
                System.out.println("Id must be a positive number, please enter again or enter 'back' to go back");
                id = sc.nextLine();
                if (id.equals("back")) {
                    return;
                }
            } else if (!bl.supplierExists(id)) {
                System.out.println("Id is not in the system");
                System.out.println("try again or enter 'back' to return to go back");
                id = sc.nextLine();
                if (id.equals("back")) {
                    return;
                }
            } else {
                ret = true;
            }
        }
        LinkedList<Order> orders = bl.getOrderBySupplier(id);
        if(orders.size() > 0) {
            for (Order order : orders) {
                System.out.println(order.toString());
            }
        }
        else{
            System.out.println("No orders for the supplier\n");
        }
    }

    public void showAllOders() {
        LinkedList<Order> orders = bl.getAllOrders();
        for (Order order : orders) {
            System.out.println(order.toString());
        }
    }


}