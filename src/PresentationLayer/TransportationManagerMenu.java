package PresentationLayer;

import LogicLayer.LogicManager;
import SharedClasses.DTOs.EmployeeDTO;
import SharedClasses.DTOs.TransportationDocDTO;
import SharedClasses.DTOs.TruckDTO;
import SharedClasses.Order;

import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by AmirPC on 22/04/2017.
 */
public class TransportationManagerMenu {
    private LogicManager logicManager;
    private EmployeeDTO user;
    private Scanner sc;
    private notificationAlerter notif;

    public TransportationManagerMenu(LogicManager logicManager) {
        sc = new Scanner(System.in);
        this.logicManager = logicManager;
    }

    public void start(EmployeeDTO user) {
        clearScreen();
        this.user = user;
        System.out.println("Welcome to the Transportation System Main menu");
        this.notif = new notificationAlerter(logicManager, user);
        this.notif.start();
        firstMenu();
    }

    private void firstMenu() {
        clearScreen();
        String option;
        System.out.println("What would you like to do? (Please choose the option number and press Enter.)");
        System.out.println("1.Confirm/Reject transportation document requests.");
        System.out.println("2.View confirmed transportation docs.");
        System.out.println("3.Delete transportation doc.");
        System.out.println("4.Add truck.");
        System.out.println("5.Watch notifications.");
        System.out.println("6.Set notification as resolved.");
        System.out.println("7.Delete order that wasn't sent");
        System.out.println("0.Exit.");
        try {
            option = sc.nextLine();
        } catch (InputMismatchException e) {
            System.out.println("The input is not valid please try again.");
            firstMenu();
            return;
        }
        switch (option) {
            case "1": {
                unconfirmedStart();
                break;
            }
            case "2": {
                confirmedStart();
                break;
            }
            case "3": {
                deleteTransDocMenu();
                break;
            }
            case "4": {
                truckAddingMenu();
                break;
            }
            case "5": {
                this.notif.showNotifications();
                break;
            }
            case "6": {
                this.notif.resolveNotifications();
                break;
            }
            case "7": {
                removeOrderNotSent();
                break;
            }
            case "0": {
                return;
            }
            default: {
                System.out.println("This option does not exist.\nPlease insert a valid option number.");
                firstMenu();
                return;
            }
        }
        firstMenu();
        return;
    }



    private void removeOrderNotSent() {
        LinkedList<Order> orders = logicManager.getOrdersThatHaveNotBeenSentYet();
        System.out.println("Orders that can be removed:");
        for(Order order : orders){
            System.out.println("Order Number: " + order.getOrder_number());
        }

        System.out.println("Enter order's order number:");
        String order_number = sc.nextLine();
        boolean ret = false;
        while (!ret) {
            if (!logicManager.isNumber(order_number)) {
                System.out.println("Order number must be a positive number, please enter again or enter 'back' to go back");
                order_number = sc.nextLine();
                if (order_number.equals("back")) {
                    return;
                }
            } else if (!logicManager.orderExistsInList(Integer.parseInt(order_number), orders)) {
                System.out.println("Order number is not in the list. Please try again or enter 'back' to return to go back");
                order_number = sc.nextLine();
                if (order_number.equals("back")) {
                    return;
                }
            } else if (logicManager.getExecutedState(Integer.parseInt(order_number)) != 1) {
                System.out.println("Cannot alter order that was already sent. Please try again or enter 'back' to return to go back");
                order_number = sc.nextLine();
                if (order_number.equals("back")) {
                    return;
                }
            } else {
                ret = true;
            }
        }
        if (logicManager.removeOrderThatHasNotBeenSentYet(Integer.parseInt(order_number))) {
            System.out.println("Order was removed successfully");
        } else {
            System.out.println("Problem removing order");
        }

    }

    public void truckMenuStart(){
        clearScreen();
        String option;
        System.out.println("Truck add / remove menu:");
        System.out.println("What would you like to do? (Please choose the option number and press Enter.)");
        System.out.println("1. Add a truck.");
        System.out.println("2. Remove a truck.");
        System.out.println("3. Return.");
        try
        {
            option = sc.nextLine();
        }
        catch (InputMismatchException e)
        {
            System.out.println("The input is not valid please try again.");
            sc.reset();
            truckMenuStart();
            return;
        }
        switch (option){
            case "1" :
            {
                truckAddingMenu();
                break;
            }
            case "2" :
            {
                truckRemovingMenu();
                break;
            }
            case "3":
            {
                break;
            }
            default:
            {
                System.out.println("this option does not exist.\nPlease insert a valid option number.");
                truckMenuStart();
                break;
            }
        }
    }

    private void truckAddingMenu()
    {
        clearScreen();
        System.out.println("Welcome to the truck adding menu.");
        //System.out.println("at any point you can press 0 and the Enter key to return to the Database editing menu.");
        TruckDTO ToAdd = new TruckDTO();
        for(int i = 1; i <= 2; i++)
            truckAddField(ToAdd, i);
        if(logicManager.AddTruck(ToAdd))
            System.out.println("The truck was added successfully!");
        else
            System.out.println("There has been a problem with the database.\nPlease contact our company.");
        return;
    }

    private void truckAddField(TruckDTO toAdd, int fieldCode)
    {
        clearScreen();
        String field = getTruckField(fieldCode);
        System.out.println("Please insert the new Truck " + field + " :");

        if (fieldCode == 1)
        {
            String input = sc.nextLine();
            if(((LogicManager)logicManager).validator.isValidID(input)) {
                if (logicManager.truckIDExist(input)) {
                    System.out.println("The given truck ID already exists.\nPlease try again.");
                    truckAddField(toAdd, fieldCode);
                    return;
                }
                toAdd.setID(input);
            }
            else{
                System.out.println("the given ID is not valid.\nPlease try again.");
                truckAddField(toAdd, fieldCode);
                return;
            }
        }

        else if (fieldCode == 2){
            System.out.println("insert the truck model (a number between 1 to 5):");
            String input = sc.nextLine();
            if(input.equals("1") && input.equals("2") && input.equals("3") && input.equals("4") && input.equals("5")){
                System.out.println("the input is not compatible, please try again");
                truckAddField(toAdd, fieldCode);
                return;
            }
            toAdd.setModel(Integer.parseInt(input) - 1);
        }

        sc.reset();
    }

    private String getTruckField(int fieldCode)
    {
        String field="non";
        switch(fieldCode)
        {
            case 1:
                field="ID";
                break;
            case 2:
                field="model";
                break;
        }
        return field;
    }

    private void truckRemovingMenu() {

        clearScreen();
        List<String> truckList = logicManager.getRemovableTrucks();
        if(truckList != null && truckList.size() != 0){
            System.out.println("Please choose from the following the truck you would like to remove and press enter, or the number 0 to return." +
                    "\nNOTE: You can't remove a truck which is scheduled in a transportation doc.");
            int i = 1;
            for (String truck : truckList){
                System.out.println(i + ") " + truck);
                i++;
            }
            i = Integer.parseInt(sc.nextLine());
            if(i==0)
            {
                return;
            }
            else
            {
                while (true) {
                    try {
                        if (i > truckList.size())
                            System.out.println("Invalid Input,please enter a valid value and press enter.");
                        else
                            break;
                    } catch (NumberFormatException e) {
                        System.out.println("Invalid value!\n");
                    }
                    i = Integer.parseInt(sc.nextLine());
                }
                String toRemove = truckList.get(i - 1);
                if(logicManager.truckIDExist(toRemove) && logicManager.removeTruck(toRemove))
                    System.out.println("The truck was removed successfully.");
            }
        }
        else{
            System.out.println("There are no trucks in the database to remove.\nReturning to the main menu.");
            return;
        }

    }

    private void deleteTransDocMenu()
    {
        System.out.println("PLEASE NOTICE that you can delete only expired transportation docs. ");
        List<Integer> IDList = logicManager.getPastDocsList();
        if(IDList != null && IDList.size() != 0){
            System.out.println("Please choose from the following the document you would like to remove, or the number 0 to return");
            int i = 1;
            for (int ID : IDList){
                System.out.println(i + ") " + ID);
                i++;
            }
            i = Integer.parseInt(sc.nextLine());
            if(i==0)
            {
                return;
            }
            else
            {
                while (true) {
                    try {
                        if (i > IDList.size())
                            System.out.println("Invalid Input, please enter a valid value and press enter.");
                        else if(i==0)
                            return;
                        else
                            break;
                    } catch (NumberFormatException e) {
                        System.out.println("Invalid value!\n");
                    }
                    i = Integer.parseInt(sc.nextLine());
                }
                int transIDToRemove = IDList.get(i - 1);
                String DateOfTransDocToRemove = logicManager.getTransportDoc(transIDToRemove).getDate();
                String driverIDToRemove = logicManager.getTransportDoc(transIDToRemove).getDriverID();
                int shiftType = logicManager.getTransportDoc(transIDToRemove).getShift();
                if(logicManager.removeStopByTransportationDocNo(transIDToRemove) && logicManager.freeDriverShiftByDate(DateOfTransDocToRemove, driverIDToRemove, shiftType) && logicManager.removeTransportationDoc(transIDToRemove))
                    System.out.println("The transportation document was removed successfully.");
            }
        }
        else{
            System.out.println("There is no Transportation Documents in the database to remove.\nReturning to the transportation System Main Menu.");
            return;
        }
    }

    public void confirmedStart()
    {
        List<Integer> closedIDList = this.logicManager.getConfirmedID();
        if(closedIDList.isEmpty()) {
            System.out.println("There are no docs.\nYou will be returned to the main menu.");
            return;
        }
        int i = 1;
        System.out.println("Confirmed transportation documents:");
        for(int id : closedIDList){
            System.out.println(i + ") " + id);
            i++;
        }
        System.out.println("\nChoose the document you would like to open and press enter.\nInsert 0 to go back to the main menu.");
        while (true) {
            try {
                i = Integer.parseInt(sc.nextLine());
                if (i > closedIDList.size())
                    System.out.println("Invalid Input,please enter a valid value and press enter.");
                else if(i==0)
                    return;
                else
                    break;
            } catch (NumberFormatException e) {
                System.out.println("Invalid value!\n");
            }
        }

        TransportationDocDTO transDoc = this.logicManager.getTransportDoc(closedIDList.get(i - 1));
        showDetails(transDoc);
        confirmedStart();
        return;
    }

    public void unconfirmedStart()
    {

        List<Integer> unconfirmedIDList = this.logicManager.getUnconfirmedIDList();
        if(unconfirmedIDList.isEmpty()) {
            System.out.println("There are no unconfirmed documents.\nYou will be returned to the main menu.");
            return;
        }
        int i = 1;
        System.out.println("The unconfirmed documents are:");
        for(int id : unconfirmedIDList){
            System.out.println(i + ") " + id);
            i++;
        }
        System.out.println("\nChoose the document you would like to open and press enter.\nInsert 0 to go back to the main menu.");
        while (true) {
            try {
                i = Integer.parseInt(sc.nextLine());
                if (i > unconfirmedIDList.size())
                    System.out.println("Invalid Input,please enter a valid value and press enter.");
                else if(i==0)
                    return;
                else
                    break;
            } catch (NumberFormatException e) {
                System.out.println("Invalid value!\n");
            }
        }

        openDocMenu(unconfirmedIDList.get(i - 1));
        unconfirmedStart();
    }


    private void openDocMenu(int docNum)
    {
        clearScreen();
        TransportationDocDTO transDoc = this.logicManager.getTransportDoc(docNum);
        showDetails(transDoc);
        clearScreen();
        System.out.println("What would you like to do? (Please choose the option number and press Enter.)");
        System.out.println("1.Confirm the request.");
        System.out.println("2.Reject the request.");
        System.out.println("3.Return.");
        String option;
        option = sc.nextLine();
        switch (option)  {
            case "1" :
            {
                if(logicManager.confirmTransDoc(docNum))
                    System.out.println("The document was successfully confirmed");
                else
                    System.out.println("There has been a problem with the database.\nPlease contact our company.");
                return;
            }
            case "2" :
            {
                this.logicManager.deleteTransDocRequest(docNum);
                System.out.println("The request was rejected.\n");
                break;
            }
            case "3" :
            {
                return;
            }
            default:
            {
                System.out.println("this option does not exist.\nPlease insert a valid option number.");
                openDocMenu(docNum);
                break;
            }
        }
    }

    public void showDetails(TransportationDocDTO selected)
    {
        System.out.println("The selected document is:");
        System.out.println("ID: " + selected.getID());
        System.out.println("Date: " + EmployeeDTO.shiftDateFormat(selected.getDate()));
        if(selected.getShift()==1)
            System.out.println("Shift: Morning");
        else
            System.out.println("Shift: Evening");
        System.out.println("Truck ID: " + selected.getTruckID());
        System.out.println("Driver ID: " + selected.getDriverID());
        System.out.println("Supplier: " + selected.getSupplier());
        System.out.println("Weight: " + selected.getWeightAtLeave());
        List<String> currStopsList = logicManager.getStops(selected.getID());
        System.out.println("Stops:");
        if(currStopsList == null || currStopsList.size() == 0)
            System.out.println("none");
        int i = 1;
        for (String stop : currStopsList) {
            System.out.println(i + ") " + stop);
            i++;
        }
    }

    private void clearScreen(){
        System.out.println();
    }


}
