package PresentationLayer;
import LogicLayer.LogicManager;

import java.util.Scanner;

/**
 * Created by Shahar on 27/04/2017.
 */
public class PL_InventoryUpdate {

    private LogicManager bl;
    private Scanner sc;
	private String store;

    public PL_InventoryUpdate(LogicManager bl, Scanner sc, String store) {
        this.bl = bl;
        this.sc = sc;
        this.store = store;
    }

    public void run() {
        boolean printOp = false;
        while (true) {
        	try{
        		System.out.println("Type product's serial number...");
				String productSerialNumber = sc.nextLine();
				while (!bl.productExistsInStore(productSerialNumber, store)) {
					System.out.println("Product " + productSerialNumber + " does not exists. please try again or type back to go back.");
					productSerialNumber = sc.nextLine();
		            if (productSerialNumber.equals("back")) {
		                return;
		            }
				}
	            showMenu();
	            String choice = sc.nextLine();
	            while (!printOp) {
	                switch (choice) {
	                    case "1": {
	                        System.out.println("Type new shelf location...");
	                        String location = sc.nextLine();
	                        while (location.equals("") || location.charAt(0) == ' ') {
	                            System.out.println("Location cannot be empty or start with a space.");
	                            System.out.println("try again or enter 'back' to return to go back");
	                            location = sc.nextLine();
	                            if (location.equals("back")) {
	                                return;
	                            }
	                        }
	                        print(bl.updateInventoryShelfLocation(productSerialNumber, location, store));
	                        printOp = true;
	                        return;
	                    }
	                    case "2": {
	                        System.out.println("Type new storage location...");
	                        String location = sc.nextLine();
	                        while (location.equals("") || location.charAt(0) == ' ') {
	                            System.out.println("Location cannot be empty or start with a space.");
	                            System.out.println("try again or enter 'back' to return to go back");
	                            location = sc.nextLine();
	                            if (location.equals("back")) {
	                                return;
	                            }
	                        }
	                        print(bl.updateInventoryStorageLocation(productSerialNumber, location,store));
	                        printOp = true;
	                        return;
	                    }
	                    case "3": {
	                        System.out.println("Type new minimal quantity...");
	                        String quantity = sc.nextLine();
	                        while (!bl.isNumber(quantity) && Integer.parseInt(quantity)<0) {
	                            System.out.println("Quantity must be a positive number, try again or enter 'back' to go back");
	                            System.out.println("try again or enter 'back' to go back");
	                            quantity = sc.nextLine();
	                            if (quantity.equals("back")) {
	                                return;
	                            }
	                        }
	                        print(bl.updateInventoryMinimalQuantity(productSerialNumber, Integer.parseInt(quantity),store));
	                        printOp = true;
	                        return;
	                    }
	                    case "4": {
	                    	System.out.println("Select where to update quantity:");
		                    System.out.println("1. storage");
		                    System.out.println("2. shelf");
	                        int loc = sc.nextInt();
	                        sc.nextLine();
	                        System.out.println("Enter new quantity:");
	                        String quantity = sc.nextLine();
	                        while (!bl.isNumber(quantity) && Integer.parseInt(quantity)<0) {
	                            System.out.println("Quantity must be a positive number, try again or enter 'back' to go back");
	                            System.out.println("try again or enter 'back' to go back");
	                            quantity = sc.nextLine();
	                            if (quantity.equals("back")) {
	                                return;
	                            }
	                        }
	                        print(bl.updateInventoryQuantity(productSerialNumber, loc, Integer.parseInt(quantity),store));
	                        printOp = true;
	                        return;
	                    }
	                    case "5": {
	                        System.out.println("Type new shelf defectives quantity...");
	                        String quantity = sc.nextLine();
	                        while (!bl.isNumber(quantity) && Integer.parseInt(quantity)<0) {
	                            System.out.println("Quantity must be a positive number, try again or enter 'back' to go back");
	                            System.out.println("try again or enter 'back' to go back");
	                            quantity = sc.nextLine();
	                            if (quantity.equals("back")) {
	                                return;
	                            }
	                        }
	                        print(bl.updateInventoryShelfDefQuantity(productSerialNumber, Integer.parseInt(quantity),store));
	                        printOp = true;
	                        return;
	                    }
	                    case "6": {
	                        System.out.println("Type new storage defectives quantity...");
	                        String quantity = sc.nextLine();
	                        while (!bl.isNumber(quantity) && Integer.parseInt(quantity)<0) {
	                            System.out.println("Quantity must be a positive number, try again or enter 'back' to go back");
	                            System.out.println("try again or enter 'back' to go back");
	                            quantity = sc.nextLine();
	                            if (quantity.equals("back")) {
	                                return;
	                            }
	                        }
	                        print(bl.updateInventoryStorageDefQuantity(productSerialNumber, Integer.parseInt(quantity),store));
	                        printOp = true;
	                        return;
	                    }
	                    case "7": {
	                        return;
	                    }
	                    default:
	                        System.out.println("invalid option, please try again");
	                        choice = sc.nextLine();
	                }
	            }
        	}
        	catch (RuntimeException e){
        		System.out.println("invalid input. please try again or exit");
        	}
        }
    }

    private void showMenu() {
        System.out.println("What would you like to update?\n" +		//all is for specific store
                "1) Shelf Location\n" +
                "2) Storage Location\n" +
                "3) Minimal Quantity\n" +
                "4) Quantity\n" +
                "5) Shelf Defectives Quantity\n" +
                "6) Storage Defectives Quantity\n" +
                "7) Return to inventory manager");
    }

    /**
     * Prints a given String
     * @param str
     */
    private void print(String str) {
        System.out.println(str);
    }
}