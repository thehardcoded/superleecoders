package PresentationLayer;

import LogicLayer.LogicManager;
import SharedClasses.Order;
import SharedClasses.OrderItem;
import javafx.util.Pair;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Scanner;

public class OrderArrivedMenu {

    private LogicManager bl;
    private Scanner sc;
    private String store;
    private int role;

    public OrderArrivedMenu(LogicManager bl, Scanner sc, String store) {
        this.bl = bl;
        this.sc = sc;
        this.store = store;
    }

    public void start() {
        LinkedList<Order> orders = bl.getAllNotApprovedOrdersInStore(store);
        System.out.println("Orders available:");
        for(Order order : orders){
            System.out.println("Order Number: " + order.getOrder_number());
        }
        System.out.println("Enter order's order number:");
        boolean ret = false;
        String order_number = sc.nextLine();
        while (!ret) {
            if (!bl.isNumber(order_number)) {
                System.out.println("Order number must be a positive number, please enter again or enter 'back' to go back");
                order_number = sc.nextLine();
                if (order_number.equals("back")) {
                    return;
                }
            } else if (!bl.orderExistsInList(Integer.parseInt(order_number), orders)) {
                System.out.println("Order number is not in the list. Please try again or enter 'back' to return to go back");
                order_number = sc.nextLine();
                if (order_number.equals("back")) {
                    return;
                }
            } else {
                ret = true;
            }
        }
        LinkedList<OrderItem> orderItems = bl.getOrderItemsInOrderByLocation(Integer.parseInt(order_number), store);
        System.out.println("Are there any shortages or defectives in the order?");
        System.out.println("press 1 for 'yes', or 2 for 'no'");
        String op = sc.nextLine();
        boolean correctOp = false;
        HashMap<String, Pair<Integer, Integer>> orderItemsToUpdate = new HashMap<>();
        while (!correctOp) {
            switch (op) {
                case "1":
                    orderItemsToUpdate = getDefectivesAndShortages(orderItems);
                    correctOp = true;
                    break;
                case "2":
                    correctOp = true;
                    break;
                default:
                    System.out.println("invalid option, please try again");
                    op = sc.nextLine();
            }
        }
        if(bl.approveOrder(Integer.parseInt(order_number),orderItems, orderItemsToUpdate, store)){
            System.out.println("The Items in order were approved successfully!");
        }
        else{
            System.out.println("faild to aprrove the arrival of the items in order");
        }
    }

    private HashMap<String,Pair<Integer,Integer>> getDefectivesAndShortages(LinkedList<OrderItem> orderItems) {
        HashMap<String, Pair<Integer, Integer>> orderItemsToUpdate = new HashMap<>();
        System.out.println("Items that need approval:");
        for(OrderItem item : orderItems){
            System.out.println("Serial Number: "+ item.getSerial_number() + " Quantity ordered: " + item.getQuantity());
        }
        boolean toContinue = true;
        while(toContinue){
            System.out.println("Enter items's serial number:");
            String serialNumber = sc.nextLine();
            boolean ret = false;
            while (!ret) {
                if (!bl.isNumber(serialNumber)) {
                    System.out.println("serial number must be a positive number, please enter again or enter 'back' to go back");
                    serialNumber = sc.nextLine();
                    if (serialNumber.equals("back")) {
                        ret = true;
                    }
                }
                else if(orderItemsToUpdate.containsKey(serialNumber)){
                    System.out.println("shortages and defectives of the item with this serial number was already inserted");
                    System.out.println("do you want to update it's shortages and defectives?");
                    System.out.println("press 1 for 'yes', or 2 for 'no'");
                    boolean correctOp = false;
                    String op = sc.nextLine();
                    while (!correctOp) {
                        switch (op) {
                            case "1":
                                correctOp = true;
                                Pair<Integer, Integer> shortagesAndDefectives = getDefectivesAndShortagesInfo();
                                if(shortagesAndDefectives==null){
                                    ret = true;
                                }
                                orderItemsToUpdate.replace(serialNumber, shortagesAndDefectives);
                                correctOp = true;
                                break;
                            case "2":
                                correctOp = true;
                                break;
                            default:
                                System.out.println("invalid option, please try again");
                                op = sc.nextLine();
                        }
                    }
                }
                else if (!bl.orderItemExistsInList(serialNumber, orderItems)) {
                    System.out.println("serial number is not in the list");
                    System.out.println("try again or enter 'back' to return to go back");
                    serialNumber = sc.nextLine();
                    if (serialNumber.equals("back")) {
                        ret = true;
                    }
                } else {
                    Pair<Integer, Integer> shortagesAndDefectives = getDefectivesAndShortagesInfo();
                    if(shortagesAndDefectives!=null){
                        orderItemsToUpdate.put(serialNumber, shortagesAndDefectives);
                    }
                    ret = true;
                }
            }
            System.out.println("Are there any more shortages or defectives in the order?");
            System.out.println("press 1 for 'yes', or 2 for 'no'");
            String op = sc.nextLine();
            boolean correctOp = false;
            while (!correctOp) {
                switch (op) {
                    case "1":
                        correctOp = true;
                        break;
                    case "2":
                        correctOp = true;
                        toContinue = false;
                        break;
                    default:
                        System.out.println("invalid option, please try again");
                        op = sc.nextLine();
                        break;
                }
            }
        }
        return orderItemsToUpdate;
    }

    private Pair<Integer,Integer> getDefectivesAndShortagesInfo() {
        System.out.println("Type shortages quantity:");
        String shortage = sc.nextLine();
        while (!bl.isNumber(shortage) && Integer.parseInt(shortage)<0) {
            System.out.println("Shortages quantity must be a positive number, try again or enter 'back' to go back");
            System.out.println("try again or enter 'back' to go back");
            shortage = sc.nextLine();
            if (shortage.equals("back")) {
                return null;
            }
        }
        System.out.println("Type defectives quantity:");
        String defective = sc.nextLine();
        while (!bl.isNumber(defective) && Integer.parseInt(defective)<0) {
            System.out.println("Defectives quantity must be a positive number, try again or enter 'back' to go back");
            System.out.println("try again or enter 'back' to go back");
            defective = sc.nextLine();
            if (defective.equals("back")) {
                return null;
            }
        }
        Pair<Integer, Integer> shortagesAndDefectives = new Pair<>(Integer.parseInt(shortage), Integer.parseInt(defective));
        return shortagesAndDefectives;
    }
}
