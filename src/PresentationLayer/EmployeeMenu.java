package PresentationLayer;

import LogicLayer.LogicManager;
import PresentationLayer.PresentationManager;
import SharedClasses.DTOs.EmployeeConstraintDTO;
import SharedClasses.DTOs.EmployeeDTO;

import java.util.List;
import java.util.Scanner;

/**
 * Created by AmirPC on 22/04/2017.
 */
public class EmployeeMenu {

    private LogicManager logM;
    private EmployeeDTO user;
    private Scanner sc=new Scanner(System.in);
    private PresentationManager presentationManager;

    public EmployeeMenu(LogicManager logM, PresentationManager presentationManager)
    {
        this.logM = logM;
        this.presentationManager = presentationManager;
    }

    public void start (EmployeeDTO user)
    {
        this.user = user;
        employeeMenu();
    }

    private void employeeMenu()
    {
        System.out.println("Hello "+ user.getFirstName()+" what would you like to do? (Please choose the option number and press Enter.)");
        System.out.println("1.Insert a new constraint.");
        System.out.println("2.watch all future constraints.");
        System.out.println("3.Delete constraint.");
        System.out.println("4.Exit.");
        String option=sc.nextLine();
        switch (option)
        {
            case "1":
            {
                InsertNewConstraint();
                break;
            }
            case "2":
            {
                getEmployeeConstrains(user);
                return;
            }
            case "3":
            {
                DeleteConstraint();
                return;
            }
            case "4":
            {
                presentationManager.start();
                return;
            }
            default:
            {
                System.out.println("This option is not valid.\nPlease insert a valid option number.\n");
                employeeMenu();
                break;
            }
        }
    }


    private void getEmployeeConstrains(EmployeeDTO user) {
        List<EmployeeConstraintDTO> constraints=((LogicManager)logM).getEmployeeConstraint(user);
        if(constraints.size() == 0){
            System.out.println("You have no constraints!\n");
        }
        else {
            System.out.println("Dear " + user.getFirstName() + ", your constraints are:\n");
            for (int i = 0; i < constraints.size(); i++) {
                String shiftType = "morning shift.";
                System.out.println("date : " + constraints.get(i).getDate());
                if (constraints.get(i).getShiftType() == 2)
                    shiftType = "evening shift.";
                System.out.println("shift : " + shiftType);
                System.out.println("the constraint : " + constraints.get(i).getConstraint() + "\n");
            }
        }
        employeeMenu();
    }

    protected void DeleteConstraint() {

        System.out.println("Welcome to the shift DeleteConstraint menu.");
        System.out.println("Please insert the date of the constraint you would like to delete.\n(the date format is DD-MM-YYYY .)");
        String date=sc.nextLine();
        if((!((LogicManager)this.logM).validator.isValidDate(date)))
        {
            DecideIfToReturn();
            return;
        }
        System.out.println("What shift would you like to delete the constraint for?");
        System.out.println("1) morning shift.");
        System.out.println("2) Evening shift.");
        String option="";
        do
        {
            option=sc.nextLine();
            if((!option.equals("1"))&&(!option.equals("2")))
                System.out.println("This option is not valid.\nPlease insert a valid option.");
        }
        while((!option.equals("1"))&&(!option.equals("2")));
        if(this.logM.deleteConstraint(Integer.parseInt(option),date,user.getID()))
            System.out.println("The deletion was successful.");
        else
            System.out.println("There has been Error in the database.\nPlease contact our company");
        System.out.println("Press Enter to return to the main menu.");
        employeeMenu();
    }

    private void InsertNewConstraint()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println();
        String description;
        System.out.println("Please insert the date of the shift you won't be able to go to.\n(the date format is DD-MM-YYYY .)");
        String date = sc.nextLine();
        EmployeeConstraintDTO constraint = new EmployeeConstraintDTO();
        constraint.setID(user.getID());
        if(!logM.validator.isValidDate(date)||!logM.validator.isFuture(date))//TODO:check if there a shift in the given date
        {
            System.out.println("The given date is not valid.");
            String option;
            do
            {
                System.out.println("Press 1 to try again or 0 to return to the main menu.");
                option=sc.nextLine();
                if(option.equals("1"))
                {
                    InsertNewConstraint();
                }
                else if(option.equals("0"))
                {
                    employeeMenu();
                }
                else
                {
                    System.out.println("Please insert a valid input.");
                }
            }
            while((!option.equals("1"))&&(!option.equals("1")));
            return;
        }
        else
        {
            constraint.setShiftDate(date);
            System.out.println("Please choose the shift you won't be able to take a part in in this day.");
            int shift = chooseShift();
            if(shift==-1)
                return;
            constraint.setShiftType(shift);
            System.out.println("Please enter the constraint description.");
            description = sc.nextLine();
            constraint.setConstraint(description);
            constraint.setStore(this.user.getStore());
            if(this.logM.AddConstraint(constraint))
            {
                System.out.println("The constraint was added successfully.\n");
                employeeMenu();
            }
            else
            {
                System.out.println("There has been a problem with the data base please contact our company.");
            }
        }
    }

    private int chooseShift()
    {
        String shiftType;
        Scanner sc=new Scanner(System.in);
        System.out.println("1) Morning shift.");
        System.out.println("2) Evening shift.");
        System.out.println("0) Return to the main menu.");
        shiftType = sc.nextLine();
        if(shiftType.equals("1")||shiftType.equals("2"))
            return Integer.parseInt(shiftType);
        else if(shiftType.equals("0"))
        {
            employeeMenu();
            return -1;
        }
        else
        {
            System.out.println("please Enter a valid option.");
            return chooseShift();
        }
    }

    private void DecideIfToReturn()
    {
        String option;
        do
        {
            System.out.println("The given Date is not valid.\nPress 1 to try again or 0 to return to the main menu.");
            option=sc.nextLine();
            if (option.equals("1"))
            {
                DeleteConstraint();
            }
            else if(option.equals("0"))
            {
                presentationManager.start();
            }
            else
            {
                System.out.println("Please insert a valid input.");
            }
        }
        while((!option.equals("1"))&&(!option.equals("0")));
    }
}
