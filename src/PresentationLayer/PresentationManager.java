package PresentationLayer;

import LogicLayer.LogicManager;
import SharedClasses.DTOs.EmployeeDTO;

import java.util.Scanner;

/**
 * Created by SHLOMI PAKADO on 3/15/2017.
 * Updated by Amir Izhaky on 22/4/2017.
 */
public class PresentationManager {

    private LogicManager logM;
    private EmployeeDTO user;
    private Scanner sc = new Scanner(System.in);
    private EmployeeMenu employeeMenu;
    private ManManagerMenu manManagerMenu;
    private AdministrativeManagerMenu administrativeManagerMenu;
    private TransportationManagerMenu transportationSystemMainMenu;
    private StoreManagerMenu storeManagerMenu ;
    private StorekeeperMainMenu storekeeperMenu;

    public PresentationManager()
    {
        this.logM = new LogicManager();
        this.transportationSystemMainMenu = new TransportationManagerMenu(logM);
        this.employeeMenu = new EmployeeMenu(logM, this);
        this.manManagerMenu = new ManManagerMenu(logM, this);
        this.administrativeManagerMenu = new AdministrativeManagerMenu(logM, this);
        this.storeManagerMenu = new StoreManagerMenu(logM,this);
        this.storekeeperMenu = new StorekeeperMainMenu(logM);
        System.out.println("WELCOME TO SUPER LEE SYSTEMS");
    }

    public void start ()
    {
        //opening checks to order periodic and check if any orders arrived in stores
        logM.orderPeriodic();
        logM.checkForArrivedOrders();
        clearScreen();
        String ID;
        System.out.println("Please insert your ID for identification manners, or 0 to exit");
        ID = sc.nextLine();
        if(ID.equals("0"))
        {
            System.out.println("Good luck, and always remember to smile :)");
            closeDB();
            return;
        }
        else if(!(logM).validator.isValidID(ID))
        {
            System.out.println("The given ID is not Valid.\n(A valid ID should contain exactly 9 digits.)");
            System.out.println("Please try again.");
            start();
            return;
        }
        else if(!this.logM.employeeIDExist(ID))
        {
            System.out.println("the given ID is not registered.\nPlease try again.");
            start();
            return;
        }
        this.user = logM.getEmployee(ID);
        if(user.getRole().equals("ManPower Manager"))
        {
            manManagerMenu.start(user);
        }
        else if(user.getRole().equals("Logistic Manager"))
        {
            transportationSystemMainMenu.start(user);
        }
        else if(user.getRole().equals("Administrative Manager"))
        {
            administrativeManagerMenu.start();
        }
        else if(user.getRole().equals("Store Manager"))
        {
            storeManagerMenu.start(this.user);
        }
        else if(user.getRole().equals("Storekeeper")) {
            storekeeperMenu.start(user.getStore());
        }
        else
        {
            employeeMenu.start(user);
        }
        start();
    }

    private void clearScreen(){
        System.out.println();
    }

    private void closeDB()
    {
        logM.closeDB();
        sc.reset();
        sc.close();
        System.exit(0);
    }
}