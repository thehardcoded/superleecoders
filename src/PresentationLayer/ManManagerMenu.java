package PresentationLayer;

import LogicLayer.LogicManager;
import PresentationLayer.PresentationManager;
import PresentationLayer.notificationAlerter;
import SharedClasses.DTOs.*;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * Created by AmirPC on 22/04/2017.
 */
public class ManManagerMenu {

    private LogicManager logM;
    private EmployeeDTO user;
    private String store;
    private Scanner sc = new Scanner(System.in);
    private PresentationManager presentationManager;
    private notificationAlerter notif;

    public ManManagerMenu(LogicManager logM, PresentationManager presentationManager)
    {
        this.logM = logM;
        this.presentationManager = presentationManager;
    }

    public void start (EmployeeDTO user)
    {
        this.user = user;
        this.store = ChooseStore();
        this.notif=new notificationAlerter(logM,user);
        this.notif.start();
        managerMenu();
    }

    private void managerMenu()
    {
        clearScreen();
        String option;
        System.out.println("Hello "+ user.getFirstName()+" what would you like to do? (Please choose an option number and press Enter.)");
        System.out.println("1.Get employee details.");
        System.out.println("2.Add new employee.");
        System.out.println("3.Edit employee details.");
        System.out.println("4.Set new Shift.");
        System.out.println("5.Set shift-standard.");
        System.out.println("6.Delete scheduled Shift.");
        System.out.println("7.Watch scheduled shifts.");
        System.out.println("8.Add new position to the System.");
        System.out.println("9.Watch employee Constraints.");
        System.out.println("10.Watch notifications.");
        System.out.println("11.Set notification as resolved.");
        System.out.println("12.Delete order that wasn't sent");
        System.out.println("13.Switch store.");
        System.out.println("0.Exit.");
        try
        {
            option=sc.nextLine();
        }
        catch (InputMismatchException e)
        {
            System.out.println("The input is not valid please try again.");
            sc.reset();
            managerMenu();
            return;
        }
        switch (option)  {
            case "1" :
            case "3" :
            {
                int opt = Integer.parseInt(option);
                InsertEmployeeId(opt);
                break;
            }
            case "2":
            {
                employeeAddingMenu();
                break;
            }
            case "4":
            {
                SetNewShift();
                break;
            }
            case "5":
            {
                SetShiftStandardMenu();
                break;
            }
            case "6":
            {
                DeleteScheduledShift();
                break;
            }
            case "7":
            {
                watchShiftByDate();
                break;
            }
            case "8":
            {
                AddNewPositionScreen();
                break;
            }
            case "9":
            {
                getEmployeeConstrains();
                break;
            }
            case "10":
            {
                this.notif.showNotifications();
                managerMenu();
                break;

            }
            case "11":
            {
                this.notif.resolveNotifications();
                managerMenu();
                break;
            }
            case "12":
            {
                removeOrderNotSent();
                managerMenu();
                break;
            }
            case "13":
            {
                this.store = ChooseStore();
                managerMenu();
                break;
            }
            case "0":
            {
                presentationManager.start();
                return;
            }
            default:
            {
                System.out.println("this option does not exist.\nPlease insert a valid option number.");
                managerMenu();
                break;
            }
        }
    }

    protected void getEmployeeConstrains() {
        System.out.println("please insert the employees id you would like to select.");
        String input=sc.nextLine();
        if(!((LogicManager)logM).validator.isValidID(input))
        {
            System.out.println("The given ID is not Valid.\n(A valid ID should contain exactly 9 digits.)");
            System.out.println("Please enter to try again or press 0 to return to the main menu.\n");
            System.out.println("Press Enter to return to the main menu.");
            String temp=sc.nextLine();
            if(temp.equals("0"))
                managerMenu();
            else
                getEmployeeConstrains();
            return;
        }
        else if(!this.logM.employeeIDExist(input))
        {
            System.out.println("the given ID is not registered.\nPlease try again.");
            getEmployeeConstrains();
            return;
        }
        else
         {
             EmployeeDTO selected=this.logM.getEmployee(input);
             List<EmployeeConstraintDTO> constraints=((LogicManager)logM).getEmployeeConstraint(selected);
             System.out.println("The employee constraints are:");
             for (int i = 0; i < constraints.size(); i++) {
                 String shiftType="morning shift.";
                 System.out.println("date : "+constraints.get(i).getDate()+"\n");
                 if(constraints.get(i).getShiftType()==2)
                     shiftType="evening shift.";
                 System.out.println("shift : "+shiftType+"\n");
                 System.out.println("the constraint : "+constraints.get(i).getConstraint()+"\n");
             }
         }

        System.out.println("Press Enter to return to the main menu.");
        String temp=sc.nextLine();
        managerMenu();
    }




    protected void InsertEmployeeId(int opNum)
    {
        clearScreen();
        System.out.println("Selection menu:");
        switch (opNum){
            case 1:
            {
                System.out.println("Please enter the employee's ID you would like to select:");
                String ID = sc.nextLine();
                if (logM.validator.isValidID(ID))
                {
                    EmployeeDTO selected = this.logM.getEmployee(ID);
                    if (selected == null)
                    {
                        wrongId(opNum);
                        return;
                    }
                    else
                    {
                        System.out.println();
                        showDetails(selected);
                        managerMenu();
                    }
                }
                else
                {
                    System.out.println("The given ID is not valid.\n(A valid ID should contain at least 9 digits.)");
                    System.out.println("Please insert a valid ID.");
                    InsertEmployeeId(opNum);
                }
                break;
            }
            case 3: {
                System.out.println("Please enter the employee's ID you would like to edit:");
                String ID = sc.nextLine();
                if (this.logM.validator.isValidID(ID)) {
                    EmployeeDTO selected = this.logM.getEmployee(ID);
                    if (selected == null) {
                        wrongId ( opNum);
                    }
                    else
                    {
                        String id = selected.getID();
                        editMenu(id, selected, user,null);
                    }
                    break;
                }
                else
                {
                    System.out.println("The given ID is not valid.\n(A valid ID should contain at least 9 digits.)");
                    System.out.println("Please insert a valid ID.");
                    InsertEmployeeId(opNum);
                }
            }
        }
    }

    private void wrongId (int opNum)
    {
        String option;
        System.out.println("the given ID is not registered.");
        do{
            System.out.println("press 1 to try again or press 0 to return to the main menu.");
            option=sc.nextLine();
            if(option.equals("1"))
                InsertEmployeeId(opNum);
            else if(option.equals("0"))
            {
                managerMenu();
                return;
            }
            else
            {
                System.out.println("please Enter a valid option.");
            }
        }
        while((!option.equals("1"))&&(!option.equals("0")));
    }

    private void editMenu(String oldID, EmployeeDTO selected, EmployeeDTO user, DriverDTO driverProfile)
    {
        String option;
        System.out.println();
        showDetails(selected);//todo: make showDriverDetails
        System.out.println();
        String role = selected.getRole();
        if(role.equals("Driver") && driverProfile==null)
        {
            String id = selected.getID();
            driverProfile = logM.getDriver(id);
        }
        System.out.println("Employee editing menu:");
        System.out.println("What would you like to edit?");
        System.out.println("1.Save changes and return to the main menu.");
        System.out.println("2.Change employee ID.");
        System.out.println("3.Change employee first name.");
        System.out.println("4.Change employee last name.");
        System.out.println("5.Change employee Role.");
        System.out.println("6.Change employee salary.");
        System.out.println("7.Change employee Date Of Transaction.");
        System.out.println("8.Change employee employment termination date.");
        System.out.println("9.Change employee Terms Of The Deal.");
        System.out.println("10.Change employee BankNumber.");
        System.out.println("11.Change employee BranchNumber.");
        System.out.println("12.Change employee BankAccount.");

        if(selected.getRole().equals("Driver"))
        {
            System.out.println("13.Change Driver License level.");
            System.out.println("14.Change Driver Region.");
        }
        System.out.println("0.Exit to the main menu.");
        option=sc.nextLine();
        switch (option)
        {
            case "1":
            {
                String r = selected.getRole();
                if(r.equals("Driver"))
                {
                    this.logM.removeDriver(oldID);
                }
                if(logM.updateEmployee(oldID, selected))
                {
                    if(r.equals("Driver"))
                            this.logM.AddDriver(driverProfile);
                    System.out.println("The employee details has been saved successfully.");
                }
                managerMenu();
                break;
            }
            case "2":
            case "3":
            case "4":
            case "5":
            case "6":
            case "7":
            case "8":
            case "9":
            case "10":
            case "11":
            case "12":
            {
                int fieldCode = Integer.parseInt(option) - 1;
                editField(oldID, selected, fieldCode, user, driverProfile);
                break;
            }
            case "0":
            {
                managerMenu();
                break;
            }
            case "13":
            {
                if(selected.getRole().equals("Driver"))
                {
                    int fieldCode = Integer.parseInt(option) - 1;
                    editField(oldID, selected, fieldCode, user, driverProfile);
                    break;
                }
            }
            case "14":
            {
                if(selected.getRole().equals("Driver"))
                {
                    int fieldCode = Integer.parseInt(option) - 1;
                    editField(oldID, selected, fieldCode, user, driverProfile);
                    break;
                }
            }
            default:
            {
                System.out.println("this option does not exist.\nPlease insert a valid option number.");
                editMenu(oldID,selected,user,driverProfile);
                break;
            }

        }
    }

    private void showDetails (EmployeeDTO selected)
    {
        System.out.println(selected.toString());
    }

    private void editField(String oldID, EmployeeDTO selected, int fieldCode, EmployeeDTO user,DriverDTO driverProfile)
    {
        String option;
        String field = getField(fieldCode);
        String input="";
        System.out.println("please insert the new employee "+field+" :");
        if(fieldCode==12)
            System.out.println("Type the license level (a number between 1 to 5):");
        if(fieldCode==13)
        {
            System.out.println("Choose a number from the following:");
            System.out.println("1. Golan heights");
            System.out.println("2. Galil");
            System.out.println("3. Coastal plains");
            System.out.println("4. Valley");
            System.out.println("5. Negev");
        }

        if(fieldCode==6||fieldCode==7)
        {
            System.out.println("(The "+field+" format is Day/Month/Year.");
            if(fieldCode==7)
                System.out.println("In order to nullify this value insert none.)");
        }
            input=sc.nextLine();
        if(fieldCode==7&&input.equals("none"))
        {
            selected.setEmploymentTerminationDate(null);
            employeeChanged(field,oldID,selected,user,driverProfile);
        }
        else if (fieldCode==1&&((LogicManager)this.logM).validator.isValidID(input))
        {
            if(this.logM.employeeIDExist(input))
            {
                System.out.println("The given ID already exists.\nPlease try again.");
                editField(oldID,selected,fieldCode,user,driverProfile);
                return;
            }
            selected.setID(input);
            if(selected.getRole().equals("Driver"))
                driverProfile.setID(input);
            employeeChanged(field,oldID,selected,user,driverProfile);
        }
        else if ((fieldCode==2||fieldCode==3)&&((LogicManager)this.logM).validator.isValidName(input) )
        {
            if(fieldCode==2)
                selected.setFirstName(input);
            else
                selected.setLastName(input);
            employeeChanged(field,oldID,selected,user,driverProfile);
        }
        else if(fieldCode==4&&((LogicManager)this.logM).isValidRole(input,store))
        {
            selected.setRole(input);
            employeeChanged(field,oldID,selected,user,driverProfile);
        }
        else if ((fieldCode==5)&&((LogicManager)this.logM).validator.isValidSalary(input) )
        {
            selected.setSalary(Integer.parseInt(input));
            employeeChanged(field,oldID,selected,user,driverProfile);
        }
        else if (((fieldCode==6)&& ((LogicManager)this.logM).validator.isFuture(input))&&((LogicManager)this.logM).validator.isValidDate(input) )
        {
            if(fieldCode==6)
                selected.setDateOfTransaction(input);
            employeeChanged(field,oldID,selected,user,driverProfile);
        }
        else if ((fieldCode==7)&&((LogicManager)this.logM).validator.isValidDate(input) )
        {
            selected.setEmploymentTerminationDate(input);
            employeeChanged(field,oldID,selected,user,driverProfile);
        }
        else if ((fieldCode==8) )
        {
            selected.setTermsOfTheDeal(input);
            employeeChanged(field,oldID,selected,user,driverProfile);
        }
        else if ((fieldCode==9||fieldCode==10||fieldCode==11)&&((LogicManager)this.logM).validator.isAccount(input) )
        {
            if(fieldCode==9)
                selected.setBankNumber(Integer.parseInt(input));
            else if(fieldCode==10)
                selected.setBranchNumber(Integer.parseInt(input));
            else
                selected.setBankAccount(Integer.parseInt(input));
            employeeChanged(field,oldID,selected,user,driverProfile);
        }
        else if(fieldCode==12)
        {

            if((!input.equals("1")) && (!input.equals("2")) && (!input.equals("3")) && (!input.equals("4")) && (!input.equals("5"))){
                System.out.println("the input is not compatible, please try again");
                 editField(oldID,selected,fieldCode,user,driverProfile);
            }
            else
            {
                driverProfile.setLicense(Integer.parseInt(input) - 1);
                employeeChanged(field,oldID,selected,user,driverProfile);
            }

        }
        else if(fieldCode==13)
        {
            if(!input.equals("1") && !input.equals("2") && !input.equals("3") && !input.equals("4") && !input.equals("5")){
                System.out.println("The input is not compatible.\nPlease try again.");
                editField(oldID,selected,fieldCode,user,driverProfile);
            }
            else
            {
                driverProfile.setRegion(Integer.parseInt(input) - 1);
                employeeChanged(field,oldID,selected,user,driverProfile);
            }
        }
        else
        {
            System.out.println("the given "+field+" is not valid.");
            do
            {
                System.out.println("press 1 to try again or press 0 to return to the editing menu.");
                option=sc.nextLine();
                if(option.equals("1"))
                    editField(oldID,selected,fieldCode,user,driverProfile);
                else if(option.equals("0"))
                    editMenu(oldID,selected,user,driverProfile);
                else
                {
                    System.out.println("please Enter a valid option.");
                }
            }
            while((!option.equals("1"))&&(!option.equals("0")));
        }
        sc.reset();
    }

    private String getField(int fieldCode)
    {
        String field="non";
        switch(fieldCode)
        {
            case 1:
                field="ID";
                break;
            case 2:
                field="first name";
                break;
            case 3:
                field="last name";
                break;
            case 4:
                field="Role";
                break;
            case 5:
                field="salary";
                break;
            case 6:
                field="Date Of Transaction";
                break;
            case 7:
                field="Employment Termination Date";
                break;
            case 8:
                field="Terms Of The Deal";
                break;
            case 9:
                field="BankNumber";
                break;
            case 10:
                field="BranchNumber";
                break;
            case 11:
                field="BankAccount";
                break;
            case 12:
                field="License level";
                break;
            case 13:
                field="Region";
                break;

        }
        return field;
    }

    private void employeeChanged(String field, String oldID, EmployeeDTO selected, EmployeeDTO user,DriverDTO driverProfile)
    {
        System.out.println("the given "+field+" was updated.");
        editMenu(oldID,selected,user,driverProfile);
    }

    private void employeeAddingMenu()
    {
        boolean userAdding = true; //this boolean meant to prevent returning to this menu after the user got back from the loop to the main menu
        clearScreen();
        System.out.println("Welcome to the employee adding menu.");
        EmployeeDTO ToAdd = new EmployeeDTO();
        for (int i = 1; i <12 ; i++) {
            userAdding = userAdding && addField(ToAdd, i, user);
        }
        if(userAdding)
        {
            String role = ToAdd.getRole();
            if(role.equals("Driver"))
            {
                DriverDTO DetailsToAdd = new DriverDTO();
                DetailsToAdd.setID(ToAdd.getID());
                driverAddingMenu(DetailsToAdd, ToAdd);
            }
            else if(logM.AddEmployee(ToAdd))
            {
                System.out.println("The employee was added successfully!");
            }
            else
            {
                System.out.println("There has been a problem with the database.\nPlease contact our company.");
            }
            managerMenu();
        }
    }

    private void driverAddingMenu(DriverDTO driverToAdd,EmployeeDTO empToAdd)
    {
        clearScreen();
        System.out.println("The new Employees role is Driver.\nPlease insert the following details:");
        for(int i = 2; i <= 3; i++)
            driverAddField(driverToAdd, i);
        if(logM.AddEmployee(empToAdd) && logM.AddDriver(driverToAdd))
            System.out.println("The driver details has been saved successfully!");
        else
            System.out.println("There has been a problem with the database.\nPlease contact our company.");
        //start(this.user);
    }

    private boolean driverAddField(DriverDTO toAdd, int fieldCode)
    {
        clearScreen();
        String field = getDriverField(fieldCode);
        System.out.println("Please insert the new driver " + field + " :");
        if (fieldCode == 2){
            System.out.println("Type the license level (a number between 1 to 5):");
            String input = sc.nextLine();
            if((!input.equals("1")) && (!input.equals("2")) && (!input.equals("3")) && (!input.equals("4")) && (!input.equals("5"))){
                System.out.println("the input is not compatible, please try again");
                return driverAddField(toAdd, fieldCode);
            }
            toAdd.setLicense(Integer.parseInt(input) - 1);
        }
        if (fieldCode == 3){
            System.out.println("Choose a number from the following:");
            System.out.println("1. Golan heights");
            System.out.println("2. Galil");
            System.out.println("3. Coastal plains");
            System.out.println("4. Valley");
            System.out.println("5. Negev");
            String input = sc.nextLine();
            if(!input.equals("1") && !input.equals("2") && !input.equals("3") && !input.equals("4") && !input.equals("5")){
                System.out.println("The input is not compatible.\nPlease try again.");
                start(user);
                return false;
            }
            else
                toAdd.setRegion(Integer.parseInt(input) - 1);
        }
        sc.reset();
        return false;
    }

    private String getDriverField(int fieldCode)
    {
        String field="non";
        switch(fieldCode)
        {
            case 1:
                field="ID";
                break;
            case 2:
                field="license";
                break;
            case 3:
                field="region";
                break;
        }
        return field;
    }

    private boolean addField(EmployeeDTO toAdd, int fieldCode, EmployeeDTO user)
    {
        String option;
        String field=getField(fieldCode);
        toAdd.setStore(this.store);
        System.out.println("Please insert the new employee "+field+" :");
        if(fieldCode==7||fieldCode==6)
        {
            System.out.println("(The "+field+" format is Day-Month-Year .");
            if(fieldCode==7)
                System.out.println("In order to nullify this value insert none.)");
        }
        String input=sc.nextLine();
        if (fieldCode==1&&((LogicManager)this.logM).validator.isValidID(input))
        {
            if(((LogicManager)this.logM).employeeIDExist(input))
            {
                System.out.println("The given ID already exists.\nPlease try again.");
                return addField(toAdd,fieldCode,user);

            }
            toAdd.setID(input);
        }
        else if ((fieldCode==2||fieldCode==3)&&((LogicManager)this.logM).validator.isValidName(input) )
        {
            if(fieldCode==2)
                toAdd.setFirstName(input);
            else
                toAdd.setLastName(input);
        }
        else if (fieldCode==4 && ((LogicManager)this.logM).isValidRole(input,store) )
        {
            toAdd.setRole(input);
        }

        else if ((fieldCode==5)&&((LogicManager)this.logM).validator.isValidSalary(input) )
        {
            toAdd.setSalary(Integer.parseInt(input));
        }
        else if ((fieldCode==6 &&((LogicManager)this.logM).validator.isValidDate(input))&& ((LogicManager)this.logM).validator.isFuture(input) )
        {
            if(fieldCode==6)
                toAdd.setDateOfTransaction(input);
        }
        else if (fieldCode==7 && ((LogicManager)this.logM).validator.isFuture(input) )
        {
            toAdd.setEmploymentTerminationDate(input);
        }
        else if ((fieldCode==8))
        {
            toAdd.setTermsOfTheDeal(input);
        }
        else if ((fieldCode==9||fieldCode==10||fieldCode==11)&&((LogicManager)this.logM).validator.isAccount(input))
        {
            if(fieldCode==9)
                toAdd.setBankNumber(Integer.parseInt(input));
            if(fieldCode==10)
                toAdd.setBranchNumber(Integer.parseInt(input));
            else
                toAdd.setBankAccount(Integer.parseInt(input));
        }
        else
        {
            System.out.println("The given "+field+" is not valid.");
            do
            {
                sc.reset();
                System.out.println("press 1 to try again or press 0 to return to the main menu.");
                option=sc.nextLine();
                if(option.equals("1"))
                    return addField(toAdd,fieldCode,user);
                else if(option.equals("0"))
                {
                    managerMenu();
                    return false;
                }
                else
                {
                    System.out.println("please Enter a valid option.");
                }
            }
            while((!option.equals("1"))&&(!option.equals("0")));
        }
        sc.reset();
        return true;
    }

    private void SetNewShift()
    {
        ArrayList<String[]> Days14 = logM.getNext14DaysShiftsStatus(store);
        System.out.println("Welcome to setting new shift menu! (This is the next 14 days shifts : )" );
        for(String[] day:Days14){
            System.out.println(day[0] + " : "+day[1]+" : "+day[2]);
        }
        int shiftType;
        String ShiftDate;
        String input;
        while (true){
            System.out.println("Please insert the shift date that you would like to schedule.( Date format : dd-mm-yyyyy )");
            ShiftDate=this.sc.nextLine();
            if(logM.validator.isValidDate(ShiftDate)){
                break;
            }
            System.out.println("Wrong Date input,please try again");
        }
        while (true){
            System.out.println("Please choose the shift type that you would like to schedule.\n1) Morning shift.\n2) Evening shift.");
            input=this.sc.nextLine();
            if(input.equals("1")){
                shiftType=1;
                if(!(this.logM).ScheduledShiftExist(ShiftDate,shiftType,store))
                    break;
                else{
                    System.out.println("Shift already scheduled in this date please choose another date.");
                    this.SetNewShift();
                    return;
                }
            }
            else if(input.equals("2")){
                shiftType=2;
                if(!(this.logM).ScheduledShiftExist(ShiftDate,shiftType,store))
                    break;
                else{
                    System.out.println("Shift already scheduled in this date please choose another date.");
                    this.SetNewShift();
                    return;
                }
            }
            System.out.println("Wrong shift type input,please try again");
        }
        int ind=-1;
        ArrayList<RoleInShiftDTO> shiftStandard = logM.getShiftRoles(shiftType,store);
        ArrayList<String[]> EmployeesToSet = new ArrayList<>(); //list of employees to set in shift
        String temp2[];
        for(RoleInShiftDTO role:shiftStandard){
            String r = role.getRole();
            ArrayList<String[]> emplyeesByRole=this.logM.getEmployeesToAssignByRole(ShiftDate, shiftType, r, store);
            while (role.getAmount()!=0){
                System.out.print("Please Choose an employee to assign as " + role.getRole());
                StringBuilder sb = new StringBuilder();
                for (int i = 0;i <emplyeesByRole.size() ; i++) {
                    String temp3[]=emplyeesByRole.get(i);
                    sb.append("\n"+(i+1)+")"+temp3[0]+" "+temp3[1]+" "+temp3[2]);
                }
                if(emplyeesByRole.size()==0){
                    System.out.println("\nThis shift cannot be scheduled since there is not enough available employees.");
                    managerMenu();
                    return;
                }
                System.out.println(sb);
                try{
                    ind = Integer.parseInt(sc.nextLine());
                    if(ind <= emplyeesByRole.size() && ind >= 0){
                        String[] temp = new String[2];
                        EmployeesToSet.add(temp);
                        temp2=emplyeesByRole.remove(ind-1);
                        if(temp2!=null){
                            temp[0]=temp2[0];
                            temp[1]=role.getRole();
                            role.setAmount(role.getAmount()-1);
                        }
                        else{
                            System.out.println("you don't have enough "+role.getRole()+"s to complete setting shift.\n" +
                                    "please hire more employees and try again.");
                            managerMenu();
                            return;
                        }
                    }
                }catch(NumberFormatException e){
                    System.out.println("Invalid value!\n");
                }
            }
        }
        this.logM.setEmployeesInShift(ShiftDate,shiftType,EmployeesToSet,store);
        System.out.println("Shift scheduled successfully");
        managerMenu();
    }

    private void SetShiftStandardMenu() {

        System.out.println("Welcome to setting Shift-Standard menu,\n" +
                "Which shift would you like to edit?\n" +
                "1) Set morning shift\n" +
                "2) Set evening shift\n" +
                "3) Back to menu\n");
        String option=sc.nextLine();
        if(option.equals("1")){
            ArrayList<RoleInShiftDTO> roles=logM.getShiftRoles(1,store);
            ArrayList<String> toPresent=this.logM.getRelevantRoles(1,store); //get list of roles to add shift-standard
            SetShiftStandard(roles,toPresent,1,store);
        }
        else if(option.equals("2")){
            ArrayList<RoleInShiftDTO> roles=logM.getShiftRoles(2,store);
            ArrayList<String> toPresent=this.logM.getRelevantRoles(2,store); //get list of roles to add shift-standard
            SetShiftStandard(roles,toPresent,2,store);
        }
        else if(option.equals("3")){
            managerMenu();
            return;
        }
        else{
            System.out.println("this option does not exist.\nPlease insert a valid option number.");
            SetShiftStandardMenu();
            return;
        }
    }

    private String ChooseStore() {
        String ans=null;
        int input;
        List<String> stores =this.logM.getAllStores();
        if(stores!=null){
            for(;;){
                System.out.println("Please Choose Store from the list:");
                int i = 1;
                for(String store : stores) {
                    System.out.println(i + ") " + store);
                    i++;
                }
                try{
                    input=Integer.parseInt(sc.nextLine());
                    if(input<=stores.size()&&input>0){
                        return stores.get(input-1);
                    }
                    System.out.println("Invalid Input,please enter a valid value and press enter.");
                }catch(NumberFormatException e) {
                    System.out.println("Invalid value!\n");
                }
            }
        }
        return ans;
    }

    private void SetShiftStandard(ArrayList<RoleInShiftDTO> roles,ArrayList<String> toPresent,int shiftType,String store) {
        String toPrint="";
        for (RoleInShiftDTO role :roles) {
            toPrint+=role.toString()+'\n';
        }
        System.out.println("Current Shift-Standard is: (Format: Role : amount)\n"+toPrint);
        System.out.println("What would you like to do?\n" +
                "1) Add role\n" +
                "2) Edit role amount\n" +
                "3) Delete role\n" +
                "4) Save Changes\n" +
                "0) Back to menu");
        String option=sc.nextLine();
        if(option.equals("1")){
            StringBuilder sb = new StringBuilder();
            for (int i = 0;i <toPresent.size() ; i++) {
                sb.append("\n"+(i+1)+")"+toPresent.get(i));
            }
            if(toPresent.size()==0){
                System.out.println("\nAll roles are updated in the system already.");
                SetShiftStandard(roles,toPresent,shiftType,store);
            }
            System.out.println(sb.toString());
            int ind=-1;
            int amount;
            while(true){
                System.out.println("Please choose role to add from the list:\n (if you want to go back to Change Shift-Standard menu press 0)");
                try{
                    ind=Integer.parseInt(sc.nextLine());
                    if(ind<=toPresent.size()&&ind>=0){
                        if(ind==0){
                            SetShiftStandardMenu();
                            return;
                        }
                        break;
                    }
                    System.out.println("Invalid Input,please enter a valid value and press enter.");
                }catch(NumberFormatException e){
                    System.out.println("Invalid value!\n");
                }
            }
            System.out.println("Please write down how many "+toPresent.get(ind-1)+"s in shift:");
            while(true) {
                try {
                    amount = Integer.parseInt(sc.nextLine());
                    if(amount>=0){
                        break;
                    }
                } catch (NumberFormatException e) {
                    System.out.println("Invalid value!");
                    System.out.println("Please write down how many "+toPresent.get(ind-1)+"s in shift:");
                }
            }
            System.out.println("Role successfully added!\n");
            roles.add(new RoleInShiftDTO(toPresent.remove(ind-1),shiftType, amount,store));
        }
        
        else if(option.equals("2")){
            int ind=-1;
            PrintRoleDtoList(roles);
            while(true){
                System.out.println("Please Choose the role you want edit from the list :");
                try{
                    ind=Integer.parseInt(sc.nextLine());
                    if(ind>0&&ind<=roles.size()){
                        break;
                    }
                }catch(NumberFormatException e){
                    System.out.println("Invalid value!\n");
                    System.out.println("Please Choose the role you want edit from the list:");
                }
            }
            int amount=0;
            while(true) {
                System.out.println("Please write down how many "+roles.get(ind-1).getRole()+"s in shift :");
                try {
                    amount = Integer.parseInt(sc.nextLine());
                    if(amount>=0){
                        if(amount!=0||!(roles.get(ind-1).getRole().equals("Shift Manager"))){
                            break;
                        }
                        else
                            System.out.println("Its not posible to set shift-standard without shift manager");
                    }
                } catch (NumberFormatException e) {
                    System.out.println("Invalid value!\n");
                    System.out.println("Please write down how many "+roles.get(ind-1).getRole()+"s in shift :");
                }
            }
            roles.get( ind - 1).setAmount(amount);
        }
        else if(option.equals("3")){
            int ind=-1;
            PrintRoleDtoList(roles);
            while(true){
                try{
                    System.out.println("Please Choose the role you want Delete from the list: \n(insert 0 if you like to back menu.)");
                    ind=Integer.parseInt(sc.nextLine());
                    if(ind>0&&ind<=roles.size()){
                        if(!(roles.get(ind-1).getRole().equals("Shift Manager"))){
                            break;
                        }
                        else{
                            System.out.println("Its not possible to set shift-standard without 'Shift Manager'");
                        }
                    }
                    else if(ind==0)
                        SetShiftStandard(roles,toPresent,shiftType,store);
                }catch(NumberFormatException e){
                    System.out.println("Invalid value!");
                    System.out.println("Please Choose the role you want delete from the list :");
                }
            }
            toPresent.add(roles.remove(ind-1).getRole());
        }
        else if(option.equals("4")){
            if(this.logM.setShiftStandard(roles, shiftType)){
                System.out.println("Changes saved successfully");
                managerMenu();
            }
            else{
                System.out.println("Error,cannot save changes since database is disconnected please contact IT-Manager");
                managerMenu();
                return;
            }
        }
        else if(option.equals("0")){
            managerMenu();
            return;
        }
        else{
            System.out.println("this option does not exist.\nPlease insert a valid option number.");
            SetShiftStandardMenu();
            return;
        }
        SetShiftStandard(roles,toPresent,shiftType,store);
    }

    private void PrintRoleDtoList(ArrayList<RoleInShiftDTO> roles){
        StringBuilder sb = new StringBuilder();
        int ind=1;
        for (RoleInShiftDTO role :roles) {
            sb.append(ind);
            sb.append(") "+role.toString()+'\n');
            ind++;
        }
        System.out.println(sb.toString());
    }

    private void DeleteScheduledShift(){
        String ShiftDate;
        String temp;
        int type;
        while (true){
            System.out.println("Please enter the shift date that you like to delete (date format : dd-mm-yyyy).\npress 0 if you like to go back to main menu");
            ShiftDate = this.sc.nextLine();
            if (ShiftDate.equals("0")) {
                managerMenu();
                return;
            } else if (((LogicManager)this.logM).validator.isValidDate(ShiftDate)) {
                break;
            }
            System.out.println("Wrong Date input,please try again");
        }
        while (true) {
            System.out.println("Please choose the shift type that you would like to delete.\n1) Morning shift.\n2) Evening shift.");
            temp = this.sc.nextLine();
            if (temp.equals("1")||temp.equals("2")) {
                type = Integer.parseInt(temp);
                if (((LogicManager)this.logM).ScheduledShiftExist(ShiftDate, type,this.store)) {
                    if(this.logM.deleteScheduledShift(new ShiftInDateDTO(type,ShiftDate,store)))
                        break;
                    else{
                        System.out.println("The shift that you are trying to delete cannot be deleted,please check if TransportationDoc scheduled in this date.");
                        managerMenu();
                    }
                }
                else {
                    System.out.println("the shift is not scheduled in this date please choose another date.");
                    this.DeleteScheduledShift();
                    return;

                }
            }
            System.out.println("shift type is invalid!,please type again.");
        }
        System.out.println("Shift Successfully deleted.");
        managerMenu();
    }

    protected void watchShiftByDate()
    {
        String store=ChooseStore();
        System.out.println("Welcome to the shift details menu.");
        System.out.println("Please insert the date of the shift you would like to watch.\n(the date format is DD-MM-YYYY .)");
        String from=sc.nextLine();
        if((!((LogicManager)this.logM).validator.isValidDate(from)))
        {
            DecideIfToReturn();
            return;
        }
        System.out.println("What shift would you like to watch?");
        System.out.println("1) morning shift.");
        System.out.println("2) Evening shift.");
        String option="";
        do
        {
            option=sc.nextLine();
            if((!option.equals("1"))&&(!option.equals("2")))
                System.out.println("This option is not valid.\nPlease insert a valid option.");
        }
        while((!option.equals("1"))&&(!option.equals("2")));
        ArrayList<String[]> ans=((LogicManager)this.logM).getEmployeesInShiftsBetweenDates(from,Integer.parseInt(option),store);
        int i=0;
        if(ans.size()==0)
        {
            System.out.println("The chosen shift is not set.\nPress Enter To return to the main menu.");
            managerMenu();
            return;
        }
        for(String[] employee:ans)
        {
            System.out.println(i+") Role : "+employee[3]+" , ID : "+employee[0]+" , First Name : "+employee[1]+" , Last Name : "+employee[2]);
            i++;
        }
        sc.reset();
        System.out.println("Press Enter to return to the main menu.");
        String temp=sc.nextLine();
        managerMenu();
    }

    private void AddNewPositionScreen() {
        System.out.println("\nPlease enter a new role type ane press 'Enter':");
        String roleType=sc.nextLine();
        if(roleType.equals("")){
            System.out.println("Please enter valid role and press enter.\n");
            AddNewPositionScreen();
        }
        if(!((LogicManager)this.logM).isValidRole(roleType,this.store)){
            if(this.logM.insetNewRole(roleType))
                System.out.println("Role type successfully added!");
            else
                System.out.println("There has been data base error.\nPlease contact IT manager.");
            managerMenu();
        }
        else{
            System.out.println("role is already exist please try again!\n");
            AddNewPositionScreen();
        }
    }

    private void DecideIfToReturn()
    {
        String option;
        do
        {
            System.out.println("The given Date is not valid.\nPress 1 to try again or 0 to return to the main menu.");
            option=sc.nextLine();
            if (option.equals("1"))
            {
                watchShiftByDate();
            }
            else if(option.equals("0"))
            {
                presentationManager.start();
            }
            else
            {
                System.out.println("Please insert a valid input.");
            }
        }
        while((!option.equals("1"))&&(!option.equals("0")));
    }

    private void clearScreen()
    {
        System.out.println();
    }

    private void removeOrderNotSent() {
        System.out.println("Enter order's order number:");
        String order_number = sc.nextLine();
        boolean ret = false;
        while (!ret) {
            if (!logM.isNumber(order_number)) {
                System.out.println("Order number must be a positive number, please enter again or enter 'back' to go back");
                order_number = sc.nextLine();
                if (order_number.equals("back")) {
                    return;
                }
            } else if (!logM.orderExists(Integer.parseInt(order_number))) {
                System.out.println("Order number is not in the system. Please try again or enter 'back' to return to go back");
                order_number = sc.nextLine();
                if (order_number.equals("back")) {
                    return;
                }
            } else if (logM.getExecutedState(Integer.parseInt(order_number)) != 1) {
                System.out.println("Cannot alter order that was already sent. Please try again or enter 'back' to return to go back");
                order_number = sc.nextLine();
                if (order_number.equals("back")) {
                    return;
                }
            } else {
                ret = true;
            }
        }
        if (logM.removeOrderThatHasNotBeenSentYet(Integer.parseInt(order_number))) {
            System.out.println("Order was removed successfully");
        } else {
            System.out.println("Problem removing order");
        }

    }
}
