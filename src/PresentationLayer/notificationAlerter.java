package PresentationLayer;

import LogicLayer.LogicManager;
import SharedClasses.DTOs.EmployeeDTO;
import javafx.util.Pair;

import java.util.List;
import java.util.Scanner;

/**
 * Created by SHLOMI PAKADO on 5/26/2017.
 */
public class notificationAlerter {

    private LogicManager logM;
    private EmployeeDTO user;
    private Scanner sc=new Scanner(System.in);
    private List<Pair<String,String>> aletrList;        //<orderNo,Date>

    public notificationAlerter(LogicManager logM,EmployeeDTO user)
    {
        this.logM=logM;
        this.user=user;
        this.sc=new Scanner(System.in);
    }

    public void start ()
    {
        this.aletrList=this.logM.getNotificationsByRole(user.getRole());
        if (!this.aletrList.isEmpty())
        {
            System.out.println("Dear "+user.getFirstName()+", you have a new notification.\nPlease choose  \"watch notifications\" for details. ");
        }
    }

    public  void showNotifications ()
    {
        if(this.aletrList.isEmpty())
            System.out.println("you don't have any notifications.");
        else
        {
            for (int i = 0; i < this.aletrList.size(); i++) {
                System.out.println((i+1)+") Order Number :"+this.aletrList.get(i).getValue()+" from "+this.aletrList.get(i).getKey()+".");
            }
        }
    }

    public  void resolveNotifications ()
    {
        int input;
        showNotifications();
        if(this.aletrList.size()==0)
            return;
        System.out.println("Please choose the notification that you want to set as resolved:");
        System.out.println((this.aletrList.size()+1)+")Exit.");
        try{
            input=Integer.parseInt(sc.nextLine());
            if(input<=this.aletrList.size()&&input>0){
                 this.logM.deleteNotification(this.user.getRole(),this.aletrList.get(input-1).getValue());
                 this.aletrList.remove(this.aletrList.get(input-1));
                 System.out.println("the chosen notification was resolved.");
            }
            else if(input==this.aletrList.size()+1)
            {
                return;
            }

            else
            {
                System.out.println("Invalid Input,please enter a valid value and press enter.");
                resolveNotifications();
            }
        } catch(NumberFormatException e) {
        System.out.println("Invalid value!\nPlease try Again");}
    }





}
