package PresentationLayer;

import LogicLayer.LogicManager;
import PresentationLayer.PresentationManager;
import SharedClasses.DTOs.StoreDTO;
import java.util.List;
import java.util.Scanner;

/**
 * Created by AmirPC on 27/04/2017.
 */
public class AdministrativeManagerMenu {

    private LogicManager logM;
    private Scanner sc = new Scanner(System.in);
    private PresentationManager presentationManager;

    public AdministrativeManagerMenu(LogicManager logM, PresentationManager presentationManager)
    {
        this.logM = logM;
        this.presentationManager = presentationManager;
    }

    public void start ()
    {
        LogisticManagerMenu();
    }

    private void LogisticManagerMenu()
    {
        System.out.println("Welcome to the Administrative Manager screen");
        System.out.println("what would you like to do? (Please choose the option number and press Enter.)");
        System.out.println("1.Add a store.");
        System.out.println("2.Exit.");
        String option = sc.nextLine();
        switch (option)
        {
            case "1":
            {
                storeAddingMenu();
                break;
            }
            case "2":
            {
                presentationManager.start();
                return;
            }
            default:
            {
                System.out.println("This option is not valid.\nPlease insert a valid option number.\n");
                LogisticManagerMenu();
                break;
            }
        }
    }

    private void storeAddingMenu()
    {
        System.out.println("Welcome to the store adding menu");
        StoreDTO ToAdd = new StoreDTO();
        for(int i = 1; i <= 4; i++)
            storeAddField(ToAdd, i);
        if(logM.AddStore(ToAdd))
            System.out.println("The store was added successfully!");
        else
            System.out.println("There has been a problem with the database.\nPlease contact our company.");
        start();
    }

    private void storeAddField(StoreDTO toAdd, int fieldCode)
    {
        String field = getStoreField(fieldCode);
        System.out.println("Please insert the new store " + field + " :");

        if (fieldCode == 1)
        {
            String input = sc.nextLine();
            toAdd.setAddress(input);
        }

        else if (fieldCode == 2){
            System.out.println("Choose a number from the following:");
            System.out.println("1. Golan heights");
            System.out.println("2. Galil");
            System.out.println("3. Coastal plains");
            System.out.println("4. Valley");
            System.out.println("5. Negev");
            String input = sc.nextLine();
            if(!input.equals("1") && !input.equals("2") && !input.equals("3") && !input.equals("4") && !input.equals("5")){
                System.out.println("The input is not compatible.\nPlease try again.");
                storeAddField(toAdd, fieldCode);
                return;
            }
            toAdd.setRegion(Integer.parseInt(input) - 1);
        }

        else if(fieldCode == 3){
            String input = sc.nextLine();
            if(((LogicManager)logM).validator.isValidName(input))
                toAdd.setContactName(input);
            else{
                System.out.println("The inserted name contains numbers.\nPlease try again.");
                storeAddField(toAdd, fieldCode);
                return;
            }
        }

        else if(fieldCode == 4){
            System.out.println("Phone number must be 10 numbers long and start with a zero!");
            String input = sc.nextLine();
            if((logM).validator.isValidPhoneNo(input))
                toAdd.setPhoneNum(input);
            else{
                System.out.println("The typed number is not valid.\nPlease try again.");
                storeAddField(toAdd, fieldCode);
                return;
            }
        }
        sc.reset();
    }

    private String getStoreField(int fieldCode)
    {
        String field="non";
        switch(fieldCode)
        {
            case 1:
                field="address";
                break;
            case 2:
                field="region";
                break;
            case 3:
                field="contact name";
                break;
            case 4:
                field="phone number";
                break;
        }
        return field;
    }

    private void storeRemovingMenu()
    {
        clearScreen();
        List<String> addressesList = logM.getAllStores();
        if(addressesList != null && addressesList.size() != 0){
            System.out.println("Please choose from the following and type the store exact address, or the number 0 to return");
            int i = 1;
            for (String address : addressesList){
                System.out.println(i + ") " + address);
                i++;
            }
            String input = sc.nextLine();
            if(input.equals("0"))
            {
                LogisticManagerMenu();
                return;
            }
            else if(logM.storeExist(input) && logM.removeStore(input))
            {
                System.out.println("The store was successfully removed");
                LogisticManagerMenu();
            }
            else
            {
                System.out.println("the given address is not compatible, please try again");
                storeRemovingMenu();
            }
        }
        else{
            System.out.println("there is no stores in the database to remove.\n returning to the Main Menu.");
            LogisticManagerMenu();
            return;
        }
    }

    private void clearScreen(){
        System.out.println();
    }
}
