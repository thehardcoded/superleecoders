package PresentationLayer;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Scanner;

import LogicLayer.LogicManager;
import SharedClasses.Contact;
import SharedClasses.Discount;
import SharedClasses.Item;
import SharedClasses.Supplier;
import SharedClasses.SupplyTerms;

public class PL_Supplier {

    private Scanner sc;
    private int role;
    private LogicManager bl;
    private String store;

    public PL_Supplier(LogicManager bl, Scanner sc, int role, String store) {
        this.bl = bl;
        this.sc = sc;
        this.role = role;
        this.store = store;
    }

    public void run() {
        while (true) {
            boolean printOp = false;
            System.out.println("Welcome to Supplier Manager");
            System.out.println("Select an option:");
            System.out.println("1. Show all suppliers");
            System.out.println("2. Show all items in contract with supplier");
            System.out.println("3. Show supplier info");
            System.out.println("4. Show item in contract with supplier");
            if (role == 1) {
                System.out.println("5. Enter new supplier");
                System.out.println("6. Delete supplier");
                System.out.println("7. Add item to contract with supplier");
                System.out.println("8. Delete item from contact with supplier");
                System.out.println("9. Add contact to supplier");
                System.out.println("10. Delete contact of supplier");
                System.out.println("11. Add Discount of an item");
                System.out.println("12. Delete Discount of an item");
                System.out.println("13. Update price of supplier's item");
                System.out.println("14. Return to main menu");
            } else {
                System.out.println("5. Return to main menu");
            }
            String option = sc.nextLine();
            if (role == 1) {
                while (!printOp) {
                    switch (option) {
                        case "1":
                            showAllSuppliers();
                            printOp = true;
                            break;
                        case "2":
                            showItems();
                            printOp = true;
                            break;
                        case "3":
                            showSupplier();
                            printOp = true;
                            break;
                        case "4":
                            showItemOfSupplier();
                            printOp = true;
                            break;
                        case "5":
                            addSupplier();
                            printOp = true;
                            break;
                        case "6":
                            deleteSupplier();
                            printOp = true;
                            break;
                        case "7":
                            addItem();
                            printOp = true;
                            break;
                        case "8":
                            deleteItem();
                            printOp = true;
                            break;
                        case "9":
                            addContact();
                            printOp = true;
                            break;
                        case "10":
                            deleteContact();
                            printOp = true;
                            break;
                        case "11":
                            addDiscount();
                            printOp = true;
                            break;
                        case "12":
                            deleteDiscount();
                            printOp = true;
                            break;
                        case "13":
                            updateItemPrice();
                            printOp = true;
                            break;
                        case "14":
                            return;
                        default:
                            System.out.println("invalid option, please try again");
                            option = sc.nextLine();
                    }
                }
            } else {
                while (!printOp) {
                    switch (option) {
                        case "1":
                            showAllSuppliers();
                            printOp = true;
                            break;
                        case "2":
                            showItems();
                            printOp = true;
                            break;
                        case "3":
                            showSupplier();
                            printOp = true;
                            break;
                        case "4":
                            showItemOfSupplier();
                            printOp = true;
                            break;
                        case "5":
                            return;
                        default:
                            System.out.println("invalid option, please try again");
                            option = sc.nextLine();
                    }
                }
            }


        }
    }

    private void showAllSuppliers() {
        LinkedList<Supplier> suppliers = bl.getAllSuppliers();
        for (Supplier sup : suppliers) {
            System.out.println(sup.toString());
            System.out.println("");
        }
    }

    private void updateItemPrice() {
        String id = getSupplierId();
        if (id.equals("back")) {
            return;
        }
        System.out.println("Enter item catalog number");
        String catalogNum = sc.nextLine();
        while (!bl.ItemExists(id, catalogNum)) {
            System.out.println("Item doesn't exist in the system. Try again or enter back to go back");
            catalogNum = sc.nextLine();
            if (catalogNum.equals("back")) {
                return;
            }
        }
        System.out.println("Enter new price:");
        String sQuantity = "";
        double price = 0;
        sQuantity = getDouble("price");
        while ((price = Double.parseDouble(sQuantity)) < 0) {
            System.out.println("Price must be a positive number. Try again or enter back to go back");
            sQuantity = sc.nextLine();
            if (sQuantity.equals("back")) {
                return;
            }
        }
        if (bl.updateItemPrice(id, catalogNum, price)) {
            System.out.println("Update item's price was successful");
        } else {
            System.out.println("Error updating item's price");
        }
    }

    private void addSupplier() {
        System.out.println("Enter new supplier's Id number:");
        boolean ret = false;
        String id = sc.nextLine();
        while (!ret) {
            if (!bl.isNumber(id)) {
                System.out.println("Id must be a positive number, please enter again or enter 'back' to go back");
                id = sc.nextLine();
                if (id.equals("back")) {
                    return;
                }
            } else if (bl.supplierExists(id)) {
                System.out.println("Id is in the system");
                System.out.println("try again or enter 'back' to return to go back");
                id = sc.nextLine();
                if (id.equals("back")) {
                    return;
                }
            } else {
                ret = true;
            }
        }
        System.out.println("Enter supplier's name:");
        String name = sc.nextLine();
        while (name.equals("") || name.charAt(0) == ' ' || !bl.legalName(name)) {
            System.out.println("Name cannot be empty, start with a space or contain characters that aren't letters.");
            System.out.println("try again or enter 'back' to return to go back");
            name = sc.nextLine();
            if (name.equals("back")) {
                return;
            }
        }
        System.out.println("Enter address:");
        String address = sc.nextLine();
        while (address.equals("") || address.charAt(0) == ' ') {
            System.out.println("Address cannot be empty or start with a space");
            System.out.println("try again or enter 'back' to return to go back");
            address = sc.nextLine();
            if (address.equals("back")) {
                return;
            }
        }
        System.out.println("Choose a region from the following:");
        System.out.println("1. Golan heights");
        System.out.println("2. Galil");
        System.out.println("3. Coastal plains");
        System.out.println("4. Valley");
        System.out.println("5. Negev");
        String input = sc.nextLine();
        while (!input.equals("1") && !input.equals("2") && !input.equals("3") && !input.equals("4") && !input.equals("5")){
            System.out.println("The input is not valid.\nPlease try again.");
            input = sc.nextLine();
        }
        String region = input;

        System.out.println("Enter bank acocunt:");
        String bankAccount = getNumber("Bank Account");
        if (bankAccount.equals("back")) {
            return;
        }
        System.out.println("Enter payement method:");
        String payment = sc.nextLine();
        while (payment.equals("") || payment.charAt(0) == ' ' || !bl.legalName(payment)) {
            System.out.println("Payement method cannot be empty, start with a space or contain characters that aren't letters.");
            System.out.println("try again or enter 'back' to return to go back");
            payment = sc.nextLine();
            if (payment.equals("back")) {
                return;
            }
        }
        SupplyTerms supplyTerms = getSupplyTerms();
        String supply_days = "";
        if (supplyTerms == SupplyTerms.SupplyOnSetDays) {
            System.out.println("Enter the days of supply (Sunday, Monday, Tuesday, Wendsday, Thursday, Friday, Saturday) with ',' between:");
            supply_days = sc.nextLine();
            while (payment.equals("") || payment.charAt(0) == ' ') {
                System.out.println("Supply days cannot be empty or start with a space. Please try again.");
                supply_days = sc.nextLine();
            }
        }
        boolean addContact = true;
        LinkedList<Contact> contacts = new LinkedList<>();
        Contact contact = null;
        while (contact == null) {
            contact = getContact("");
        }
        contacts.add(contact);
        String op;
        boolean correctOp;
        while (addContact) {
            System.out.println("do you want to add a new contact to the supplier?");
            System.out.println("press 1 for 'yes', or press 2 for 'no'");
            op = sc.nextLine();
            correctOp = false;
            while (!correctOp) {
                switch (op) {
                    case "1":
                        contact = getContact("");
                        if (contact != null) {
                            contacts.add(contact);
                        }
                        correctOp = true;
                        break;
                    case "2":
                        correctOp = true;
                        addContact = false;
                        break;
                    default:
                        System.out.println("invalid option, please try again");
                        op = sc.nextLine();
                }
            }
        }
        boolean addItem = true;
        LinkedList<Item> items = new LinkedList<>();
        while (addItem) {
            System.out.println("do you want to add a new item to the contract with the supplier?");
            System.out.println("press 1 for 'yes', or press 2 for 'no'");
            op = sc.nextLine();
            correctOp = false;
            while (!correctOp) {
                switch (op) {
                    case "1":
                        Item item = getItem("", supplyTerms, Integer.parseInt(region));
                        if (item != null) {
                            items.add(item);
                        }
                        correctOp = true;
                        break;
                    case "2":
                        correctOp = true;
                        addItem = false;
                        break;
                    default:
                        System.out.println("invalid option, please try again");
                        op = sc.nextLine();
                }
            }
        }
        boolean success = bl.insetToSuppliers(new Supplier(id, name, address, bankAccount, payment, contacts, supplyTerms, supply_days, items, Integer.parseInt(region)));
        if (success) {
            System.out.println("The new supplier was added successfuly!");
        } else {
            System.out.println("Problem adding supplier");
        }
    }

    private void deleteSupplier() {
        String id = getSupplierId();
        if (id.equals("back")) {
            return;
        }
        boolean success = bl.deleteSupplier(id);
        if (success) {
            System.out.println("The supplier was deleted successfully!");
        } else {
            System.out.println("Problem deleting supplier");
        }
    }

    private void addItem() {
        int region = bl.getStoreRegion(store);
        String id = getSupplierId();
        if (id.equals("back")) {
            return;
        }
        Supplier sup = bl.getSupplier(id);
        while (region != sup.getRegion()){
            System.out.println("Supplier's region doesn't match " + store + "'s region");
            id = getSupplierId();
            if (id.equals("back")) {
                return;
            }
            sup = bl.getSupplier(id);
        }

        Item item = getItem(id, sup.getTerms(), sup.getRegion());
        if (item == null) {
            return;
        }
        if (bl.addItem(id, item)) {
            System.out.println("Item added successfully");
        } else {
            System.out.println("Error adding item");
        }
    }

    private void deleteItem() {
        String id = getSupplierId();
        if (id.equals("back")) {
            return;
        }
        System.out.println("Please enter item catalog number");
        String catalogNum = sc.nextLine();
        while (!bl.ItemExists(id, catalogNum)) {
            System.out.println("Item doesn't exist in the system");
            System.out.println("Try again or enter 'back' to go back");
            catalogNum = sc.nextLine();
            if (catalogNum.equals("back")) {
                return;
            }
        }
        if (bl.deleteItem(id, catalogNum,store)) {
            System.out.println("Item successfully deleted");
        } else {
            System.out.println("Problem deleting item");
        }
    }

    private void showItems() {
        String id = getSupplierId();
        if (id.equals("back")) {
            return;
        }
        LinkedList<Item> items = bl.getSupplierItems(id);
        if (items == null) {
            return;
        }
        for (Item item : items) {
            System.out.println(bl.getProduct(item.getSerialNumber()).toString());
            System.out.println(item.toString());
        }
        System.out.println("");
    }

    private void deleteContact() {
        String id = getSupplierId();
        System.out.println("Please enter contact's phone number:");
        String phoneNum = sc.nextLine();
        boolean ret = false;
        while (!ret) {
            if (!bl.isNumber(phoneNum)) {
                System.out.println("Phone number must be a positive number, please enter again or enter 'back' to go back");
                phoneNum = sc.nextLine();
                if (phoneNum.equals("back")) {
                    return;
                }
            }
            if (!bl.contactExistsWithSupp(id, phoneNum)) {
                System.out.println("Contact doesn't exist, please try again or enter 'back' to go back.");
                phoneNum = sc.nextLine();
                if (phoneNum.equals("back")) {
                    return;
                }
            } else {
                ret = true;
            }

        }
        if (bl.deleteContact(id, phoneNum)) {
            System.out.println("Contact was deleted successfully");
        } else {
            System.out.println("Error deleting contact");
        }
    }

    private String getSupplierId() {
        System.out.println("Enter supplier's id number:");
        boolean ret = false;
        String id = sc.nextLine();
        while (!ret) {
            if (!bl.isNumber(id)) {
                System.out.println("Id must be a positive number, please enter again or enter 'back' to go back");
                id = sc.nextLine();
                if (id.equals("back")) {
                    return "back";
                }
            } else if (!bl.supplierExists(id)) {
                System.out.println("Id is not in the system");
                System.out.println("try again or enter 'back' to return to go back");
                id = sc.nextLine();
                if (id.equals("back")) {
                    return "back";
                }
            } else {
                ret = true;
            }
        }
        return id;
    }

    private String getNumber(String s) {
        String get = sc.nextLine();
        while (!bl.isNumber(get)) {
            System.out.println(s + " must be a positive number, please try again or enter 'back' to go back");
            get = sc.nextLine();
            if (get.equals("back")) {
                return "back";
            }
        }
        return get;
    }
    private String getDouble(String s) {
        String get = sc.nextLine();
        while (!bl.isPositiveDouble(get)) {
            System.out.println(s + " must be a positive double, please try again or enter 'back' to go back");
            get = sc.nextLine();
            if (get.equals("back")) {
                return "back";
            }
        }
        return get;
    }


    private SupplyTerms getSupplyTerms() {
        System.out.println("Select an option for suppliying terms:");
        System.out.println("1. Transport on set days");
        System.out.println("2. On Order");
        String op = sc.nextLine();
        SupplyTerms supplyTerms = null;
        boolean correctOp = false;
        while (!correctOp) {
            switch (op) {
                case "1":
                    supplyTerms = SupplyTerms.SupplyOnSetDays;
                    correctOp = true;
                    break;
                case "2":
                    supplyTerms = SupplyTerms.SelfTransport;
                    correctOp = true;
                    break;
                default:
                    System.out.println("invalid option, please try again");
                    op = sc.nextLine();
            }
        }
        return supplyTerms;
    }

    private void addContact() {
        String id = getSupplierId();
        if (id.equals("back")) {
            return;
        }
        Contact contact = getContact(id);
        if (contact == null) {
            return;
        }
        boolean changeNameInContacts = bl.contactExistInContacts(contact.getPhone());
        if (changeNameInContacts) {
            System.out.println("Contact with the same phone already exists, his name will be changed");
        }
        if (bl.addContact(id, contact, changeNameInContacts)) {
            System.out.println("Contact was added successfully");
        } else {
            System.out.println("Error adding contact");
        }
    }

    private Contact getContact(String supplierID) {
        System.out.println("Enter contact name:");
        String name = "";
        String phoneNum = "";
        name = sc.nextLine();
        while (name.equals("") || name.charAt(0) == ' ' || !bl.legalName(name)) {
            System.out.println("name cannot be empty, start with a space or contain characters that aren't letters.");
            System.out.println("try again or enter 'back' to return to back");
            name = sc.nextLine();
            if (name.equals("main")) {
                return null;
            }
        }
        System.out.println("Please phone number");
        phoneNum = sc.nextLine();
        boolean ret = false;
        while (!ret) {
            if (!bl.isNumber(phoneNum)) {
                System.out.println("Phone number must be a positive number, please enter again or enter 'back' to go back");
                phoneNum = sc.nextLine();
                if (phoneNum.equals("back")) {
                    return null;
                }
            } else if (bl.contactExistsWithSupp(supplierID, phoneNum)) {
                System.out.println("Contact already exists, please try again or enter 'back' to go back.");
                phoneNum = sc.nextLine();
                if (phoneNum.equals("back")) {
                    return null;
                }
            } else {
                ret = true;
            }
        }
        return new Contact(name, phoneNum);
    }

    private Item getItem(String supplierID, SupplyTerms supplyTerms, int region) {
        String catalogNum = "";
        boolean good = true;
        System.out.println("Please enter item catalog number:");
        while (good) {
            catalogNum = getNumber("Catalog Number");
            if (catalogNum.equals("back")) {
                return null;
            } else if (!supplierID.equals("") && supplyTerms != SupplyTerms.SupplyOnSetDays) {
                good = bl.ItemExists(supplierID, catalogNum);
                if (good) {
                    System.out.println("Item already exists, please try again");
                }
            } else {
                good = false;
            }
        }
        System.out.println("Please enter the item's product serial number");
        String serialNumber = sc.nextLine();
        boolean ret = false;
        while (!ret) {
            if (!bl.isNumber(serialNumber)) {
                System.out.println("serial number must be a positive number, please enter again or enter 'back' to go back");
                serialNumber = sc.nextLine();
                if (serialNumber.equals("back")) {
                    return null;
                }
            } else if (!bl.productExists(serialNumber)) {
                System.out.println("serial number is not in the system");
                System.out.println("try again or enter 'back' to return to go back");
                serialNumber = sc.nextLine();
                if (serialNumber.equals("back")) {
                    return null;
                }
            } else {
                ret = true;
            }
        }
        System.out.println("Please enter item price:");
        double price = 0;
        String sPrice = sc.nextLine();
        while (!bl.isPositiveDouble(sPrice)) {
            System.out.println("Price must be a positive number. Try again or enter back to go back");
            sPrice = sc.nextLine();
            if (sPrice.equals("back")) {
                return null;
            }
        }
        price = Double.parseDouble(sPrice);
        int quantity = 0;
        if ((supplyTerms != null && supplyTerms == SupplyTerms.SupplyOnSetDays) || (!supplierID.equals("") && (bl.getSupplier(supplierID).getTerms() == SupplyTerms.SupplyOnSetDays))) {
            System.out.println("Enter quantity to order on pereodic order:");
            String quantity1 = sc.nextLine();
            while (!bl.isNumber(quantity1)) {
                System.out.println("Enter quantity to order on pereodic order:");
                quantity1 = sc.nextLine();
                if (quantity1.equals("back")) {
                    return null;
                }
            }
            quantity = Integer.parseInt(quantity1);
        }
        HashMap<String, Integer> storeQuan = new HashMap<>();
        storeQuan.put(store, quantity);
        return new Item(catalogNum, price, serialNumber, null, storeQuan);
    }

    private void addDiscount() {
        String id = getSupplierId();
        if (id.equals("back")) {
            return;
        }
        System.out.println("Enter item catalog number");
        String catalogNum = sc.nextLine();
        while (!bl.ItemExists(id, catalogNum)) {
            System.out.println("Item doesn't exist in the system. Try again or enter back to go back");
            catalogNum = sc.nextLine();
            if (catalogNum.equals("back")) {
                return;
            }
        }
        System.out.println("Enter quantity:");
        String sQuantity = "";
        int quantity = 0;
        sQuantity = getNumber("Quantity");
        while ((quantity = Integer.parseInt(sQuantity)) < 0) {
            System.out.println("Quantity must be a positive number. Try again or enter back to go back");
            sQuantity = sc.nextLine();
            if (sQuantity.equals("back")) {
                return;
            }
        }
        System.out.println("Enter discount(in percents):");
        double discount = 0;
        String sdiscount = sc.nextLine();
        while (!bl.isPositiveDouble(sdiscount)) {
            System.out.println("discount must be a positive number. Try again or enter back to go back");
            sdiscount = sc.nextLine();
            if (sdiscount.equals("back")) {
                return;
            }
        }
        discount = Double.parseDouble(sdiscount);
        if (bl.insertToDiscounts(id, catalogNum, new Discount(quantity, discount))) {
            System.out.println("Discount was added successfully");
        } else {
            System.out.println("Error adding discount. Discount already exists");
        }
    }

    private void deleteDiscount() {
        String id = getSupplierId();
        if (id.equals("back")) {
            return;
        }
        System.out.println("Enter item catalog number");
        String catalogNum = sc.nextLine();
        while (!bl.ItemExists(id, catalogNum)) {
            System.out.println("Item doesn't exist in the system. Try again or enter back to go back");
            catalogNum = sc.nextLine();
            if (catalogNum.equals("back")) {
                return;
            }
        }
        System.out.println("Enter quantity:");
        String sQuantity = "";
        int quantity = 0;
        sQuantity = getNumber("Quantity");
        while ((quantity = Integer.parseInt(sQuantity)) < 0) {
            System.out.println("quantity must be a positive number. Try again or enter back to go back");
            sQuantity = getNumber("Quantity");
            if (sQuantity.equals("back")) {
                return;
            }
        }
        if (bl.deleteFromDiscounts(id, catalogNum, quantity)) {
            System.out.println("Discount was deleted successfully");
        } else {
            System.out.println("Error deleting discount");
        }
    }

    private void showSupplier() {
        String id = getSupplierId();
        if (id.equals("back")) {
            return;
        }
        Supplier supplier = bl.getSupplier(id);
        System.out.println(supplier.toString());
    }

    private void showItemOfSupplier() {
        String id = getSupplierId();
        if (id.equals("back")) {
            return;
        }
        System.out.println("Please enter item catalog number");
        String catalogNum = sc.nextLine();
        while (!bl.ItemExists(id, catalogNum)) {
            System.out.println("Item doesn't exist in the system");
            System.out.println("Try again or enter 'back' to go back");
            catalogNum = sc.nextLine();
            if (catalogNum.equals("back")) {
                return;
            }
        }
        Item item = bl.getItem(id, catalogNum);
        System.out.println(bl.getProduct(item.getSerialNumber()).toString());
        System.out.println(item.toString());
    }

}
