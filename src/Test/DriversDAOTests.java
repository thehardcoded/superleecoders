package Test;

import DataProviders.DAOs.driversDAO;
import SharedClasses.DTOs.DriverDTO;
import SharedClasses.Enums;
import org.junit.Assert;
import org.junit.Test;
import org.sqlite.SQLiteConfig;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Barak on 24-May-17.
 */
public class DriversDAOTests {

    Connection con;
    driversDAO dao;

    public DriversDAOTests() {
    }

    @org.junit.Before
    public void setUp() throws Exception {
        this.con = null;
        try {
            Class.forName("org.sqlite.JDBC");
            SQLiteConfig config = new SQLiteConfig();
            //config.enforceForeignKeys(true);
            con = DriverManager.getConnection("jdbc:sqlite:DB/Shop.db",config.toProperties());
            dao =new driversDAO(con);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @org.junit.After
    public void tearDown() {
        dao.close();
    }

    @Test
    public void insertDeleteTest(){
        String id = "1";
        DriverDTO dto = new DriverDTO( id, Enums.License.values()[1],Enums.Region.values()[1] );
        dao.insert( dto );
        Assert.assertTrue( dao.exist( id ) );
        dao.delete( id );
        Assert.assertFalse( dao.exist( id ) );
    }
    @Test
    public void getLicenseTypeTest(){
        String id = "1";
        Enums.License license = Enums.License.lv1;
        DriverDTO dto = new DriverDTO( id, license, Enums.Region.values()[1] );
        dao.insert( dto );
        Assert.assertEquals( 1 ,dao.getLicenseType( id ) );
        Assert.assertNotEquals( 2 ,dao.getLicenseType( id ) );
        dao.delete(id);
    }

}
