package Test;

import DataProviders.DAOs.storesDAO;
import SharedClasses.DTOs.StoreDTO;
import SharedClasses.Enums;
import org.junit.Assert;
import org.junit.Test;
import org.sqlite.SQLiteConfig;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Barak on 25-May-17.
 */
public class storesDAOTests {

    Connection con;
    storesDAO dao;
    Enums.Region r = Enums.Region.galil;
    String address = "somewhere";
    String contactname = "contactName";
    String phoneNumber = "0544444444";
    StoreDTO dto = new StoreDTO( r,address,contactname,phoneNumber );

    @org.junit.Before
    public void setUp() throws Exception {
        this.con = null;
        try {
            Class.forName("org.sqlite.JDBC");
            SQLiteConfig config = new SQLiteConfig();
            config.enforceForeignKeys(true);
            con = DriverManager.getConnection("jdbc:sqlite:DB/Shop.db",config.toProperties());
            dao =new storesDAO(con);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @org.junit.After
    public void tearDown() {
        dao.close();
    }

    @Test
    public void insertDelete(){
        Assert.assertFalse( dao.exist( address ) );
        dao.insert( dto );
        Assert.assertTrue( dao.exist( address ) );
        dao.delete( address );
        Assert.assertFalse( dao.exist( address ) );
    }


    @Test
    public void existByRegion(){
        dao.insert( dto );

        Assert.assertFalse(dao.existByRegion( address,  1));
        Assert.assertTrue(dao.existByRegion( address,  2));

        dao.delete( dto.getAddress() );
    }

}
