package Test;

import DataProviders.DAOs.shiftsInDatesDAO;
import DataProviders.DAOs.shiftsTypesDAO;
import DataProviders.DAOs.storesDAO;
import SharedClasses.DTOs.StoreDTO;
import SharedClasses.Enums;
import org.junit.Assert;
import org.junit.Test;
import org.sqlite.SQLiteConfig;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Barak on 26-May-17.
 */
public class shiftsInDatesDAOTests {

    Connection con;
    shiftsInDatesDAO dao;
    shiftsTypesDAO sfDAO;
    storesDAO sDAO;


    public shiftsInDatesDAOTests() {

    }

    @org.junit.Before
    public void setUp() throws Exception {
        this.con = null;
        try {
            Class.forName("org.sqlite.JDBC");
            SQLiteConfig config = new SQLiteConfig();
            //config.enforceForeignKeys(true);
            con = DriverManager.getConnection("jdbc:sqlite:DB/Shop.db",config.toProperties());
            dao = new shiftsInDatesDAO(con);
            sfDAO = new shiftsTypesDAO( con );
            sDAO = new storesDAO( con );
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @org.junit.After
    public void tearDown() {
        dao.close();
    }

    @Test
    public void AddShiftDate(){
        String address = "-test1";
        String shiftDate = "01-01-2018";
        sDAO.insert(new StoreDTO(Enums.Region.negev, address, "a", "1" ));
        Assert.assertFalse(dao.checkIfShiftDated(shiftDate , 1, address));
        dao.AddShiftDate( shiftDate, 1,address);
        Assert.assertTrue(dao.checkIfShiftDated( "01-01-2018", 1, address));
        sDAO.delete( address );
        dao.deleteShiftDate( shiftDate, 1,address);

    }


}
