package Test;
import DataProviders.Repository;
import SharedClasses.DTOs.EmployeeConstraintDTO;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by SHLOMI PAKADO on 4/2/2017.
 */
public class RepositoryConTest {

    Repository rep;


    @org.junit.Before
    public void setUp() throws Exception {
        rep = new Repository();
    }


    @org.junit.After
    public void tearDown() {
        rep.close();
    }


    @Test
    public void addEmployeeConTest()
    {
        EmployeeConstraintDTO EmpCon=new EmployeeConstraintDTO("111111111",2,"2017-01-12","Shift Manager","Tel Aviv");
        rep.addEmployeeConstraint(EmpCon);
        Assert.assertFalse(rep.addEmployeeConstraint(EmpCon));
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
        }
    }





//    @Test
//    public void delEmployeeTest()
//    {
//        EmployeeConstraintDTO EmpCon=new EmployeeConstraintDTO("111111111",2,"0000/00/00","Shift Manager");
//        rep.addEmployeeConstraint(EmpCon);
//        Assert.assertTrue(rep.deleteConstraint(2,"0000/00/00","111111111"));
//    }

}
