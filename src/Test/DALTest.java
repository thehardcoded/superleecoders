package Test;

import DataProviders.DAL;
import DataProviders.DAOs.storesDAO;
import SharedClasses.*;
import SharedClasses.DTOs.StoreDTO;
import org.junit.Assert;
import org.junit.Test;
import org.sqlite.SQLiteConfig;

import java.sql.Connection;
import java.sql.DriverManager;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.LinkedList;

import static org.junit.Assert.*;

public class DALTest {
    private DAL dal;
    private Connection con;
    //new fields:
    Category category = new Category(-1, "blah", 0);
    int w1 = 11, w2 = 12, w3 = 13;
    Product p1 = new Product("11", "Blah1", "blah", category, w1, 12, 15);
    Product p2 = new Product("12", "Blah2", "blah", category, w2, 12, 15);
    Product p3 = new Product("13", "Blah3", "blah", category, w3, 12, 15);
    Supplier sup = new Supplier("-1", "dani", "Beer Sheva", "0", "Cash", new LinkedList<Contact>()
            , SupplyTerms.SupplyOnSetDays, "Sunday", new LinkedList<Item>(), 1);

    @org.junit.Before
    public void setUp() throws Exception {
        this.con = null;
        try {
            Class.forName("org.sqlite.JDBC");
            SQLiteConfig config = new SQLiteConfig();
            con = DriverManager.getConnection("jdbc:sqlite:DB/Shop.db", config.toProperties());
        } catch (Exception e) {
            e.printStackTrace();
        }

        dal = new DAL(con);
        dal.insertToSuppliers(sup);
    }


    @org.junit.After
    public void tearDown() {
        dal.deleteSupplier("-1");
        dal.close();
    }

    //--------------------------------------------------------------------------------------------------------

    //Supplier Tests
    @Test
    public void insertToSuppliers() throws Exception {
        dal.insertToSuppliers(new Supplier("-2", "dani", "Beer Sheva", "0", "Cash", new LinkedList<Contact>(), SupplyTerms.OnOrder, "Sunday", new LinkedList<Item>(), 1));
        assertTrue(dal.supplierExists("-2"));
        dal.deleteSupplier("-2");
    }

    @org.junit.Test
    public void insertToContacts() throws Exception {
        dal.insertToContacts("-1", new Contact("John Doe", "0"), false);
        assertTrue(dal.contactExistsWithSupp("-1", "0"));
    }

    @org.junit.Test
    public void insertToItemsInSupplier() throws Exception {
        assertTrue(dal.insertToItemsInSupplier("-1", new Item("0", 12.12, "-2", null, new HashMap<>())));
        assertTrue(dal.itemExists("-1", "0"));
        dal.deleteItemOfSupplier("-1", "0");
    }

    @org.junit.Test
    public void deleteItemOfSupplier() throws Exception {
        dal.insertToItemsInSupplier("-1", new Item("0", 12.12, "-2", null, new HashMap<>()));
        dal.deleteItemOfSupplier("-1", "0");
        assertFalse(dal.itemExists("-1", "0"));
    }

    @org.junit.Test
    public void deleteContact() throws Exception {
        dal.insertToContacts("-1", new Contact("John Doe", "0"), true);
        dal.deleteContact("-1", "0", true);
        assertFalse(dal.contactExistsInContacts("-1"));
    }

    @org.junit.Test
    public void deleteSupplier() throws Exception {

        Contact con = new Contact("Omry", "123");
        dal.insertToContacts("-1", con, false);

        Category category = new Category(1, "blah", 0);
        Product product = new Product("-1", "Blah", "blah", category, 1, 12, 15);
        dal.insertProduct(product);
//        HashMap h = new HashMap<String,Integer>();

        Item item = new Item("-1", 1, "-1", new LinkedList<Discount>(), new HashMap<>());
        dal.insertToItemsInSupplier("-1", item);

        int order = dal.addNewOrder("-1", LocalDate.now().toString(), "3");
        LinkedList<Item> items = new LinkedList<>();
        items.add(item);
        dal.addItemsToOrder(order, items);

        dal.deleteSupplier("-1");
        assertFalse(dal.supplierExists("-1"));
    }

    @org.junit.Test
    public void getSupplierItems() throws Exception {
        dal.insertToItemsInSupplier("-1", new Item("0", 12.12, "-2", null, new HashMap<>()));
        assertEquals(dal.getSupplierItems("-1").size(), 1);
        dal.deleteItemOfSupplier("-1", "0");
    }

    public void getItem() throws Exception {
        dal.insertToItemsInSupplier("-1", new Item("0", 12.12, "-2", null, new HashMap<>()));
        Item item = dal.getItem("0", "-1");
        assertTrue((item.getCatalogNumber()).equals("0"));
        assertTrue(item.getPrice() == 12.12);
        assertTrue((item.getSerialNumber()).equals("-2"));
        dal.deleteItemOfSupplier("-1", "0");
    }

    @org.junit.Test
    public void supplierExists() throws Exception {
        assertTrue(dal.supplierExists("-1"));
    }

    @org.junit.Test
    public void itemExists() throws Exception {
        dal.insertToItemsInSupplier("-1", new Item("0", 12.12, "-2", null, new HashMap<>()));
        assertTrue(dal.itemExists("-1", "0"));
        dal.deleteItemOfSupplier("-1", "0");
        assertFalse(dal.itemExists("-1", "0"));
    }

    @org.junit.Test
    public void insertToDiscounts() throws Exception {
        dal.insertToItemsInSupplier("-1", new Item("0", 12.12, "-2", null, new HashMap<>()));
        assertTrue(dal.insertToDiscounts("-1", "0", new Discount(1, 1)));
        dal.deleteDiscount("-1", "0", 1);

    }

    @org.junit.Test
    public void updateSupplierAddress() throws Exception {
        assertTrue(dal.updateSupplierAddress("-1", "Holon"));
        Supplier supplier = dal.getSupplier("-1");
        assertNotEquals(supplier.getAddress(), "Beer Sheva");
        assertEquals(supplier.getAddress(), "Holon");
    }

    @org.junit.Test
    public void updateItemPrice() throws Exception {
        dal.insertToItemsInSupplier("-1", new Item("0", 12.12, "-2", null, new HashMap<>()));
        assertTrue(dal.updateItemPrice("-1", "0", 10));
        Item item = dal.getItem("-1", "0");
        assertFalse(item.getPrice() == 12.12);
        assertTrue(item.getPrice() == 10);
    }

    //---------------------------------------------------------------------------------------------------------
    //Order Tests


    @org.junit.Test
    public void OrderFrom() throws Exception {
        dal.insertToSuppliers(new Supplier("-2", "Dana", "Beer Sheva", "1", "Cash", new LinkedList<Contact>(), SupplyTerms.OnOrder, "Monday", new LinkedList<Item>(), 1));
        dal.insertToItemsInSupplier("-1", new Item("0", 12.12, "-2", null, new HashMap<>()));
        LinkedList<Discount> discounts = new LinkedList<>();
        discounts.add(new Discount(200, 0.2));
        dal.insertToItemsInSupplier("-2", new Item("1", 12.12, "-2", discounts, new HashMap<>()));
        OrderItem order = dal.orderFrom("-2", 200, "Tavor");
        assertEquals(order.getCatalog_number(), "1");
        assertEquals(order.getSupplier_id(), "-2");
        dal.deleteSupplier("-2");
    }


    @org.junit.Test
    public void addNewOrder() throws Exception {
        int order_number = dal.addNewOrder("-1", "", "1");
        assertTrue(order_number >= 0);
        assertTrue(dal.orderExists(order_number));
        assertTrue(dal.removeOrderThatHasNotBeenSentYet(order_number));
    }

    @org.junit.Test
    public void removeOrderThatHasNotBeenSentYet() throws Exception {
        int order_number = dal.addNewOrder("-1", LocalDate.now().toString(), "2");
        assertFalse(dal.removeOrderThatHasNotBeenSentYet(order_number));
    }

    @org.junit.Test
    public void removeItemFromOrderThatHasNotBeenSentYet() throws Exception {
        String store = "tel aviv";
        int quantity = 10;
        HashMap<String, Integer> h = new HashMap<>();
        h.put(store, quantity);
        HashMap<String, Integer> h2 = new HashMap<>();
        h.put(store, quantity);
        dal.insertProduct(new Product("-2", "test2", "Osem", null, 5, 0, 22));
        dal.insertProduct(new Product("-1", "test2", "Osem", null, 5, 0, 22));

        int order_number = dal.addNewOrder("-1", LocalDate.now().toString(), "1");
        LinkedList<Item> items = new LinkedList<>();
        items.add(new Item("-1", 1, "-1", new LinkedList<Discount>(), h));
        items.add(new Item("-2", 1, "-2", new LinkedList<Discount>(), h2));
        dal.addItemsToOrder(order_number, items);
        dal.removeItemFromOrderThatHasNotBeenSentYet(order_number, "-1", store);
        dal.removeItemFromOrderThatHasNotBeenSentYet(order_number, "-2", store);
        assertFalse(dal.orderExists(order_number));
        dal.removeProduct("-1");
        dal.removeProduct("-2");
    }

    @Test
    public void orderWhenLackOfStock() throws Exception {
        String store = "tel aviv";
        int quantity = 10;

        int order_number = dal.addNewOrder("-1", LocalDate.now().toString(), "1");
        LinkedList<Item> items = new LinkedList<>();
        HashMap<String, Integer> h = new HashMap<>();
        h.put(store, quantity);

        items.add(new Item("-1", 1, "-1", new LinkedList<Discount>(), h));
        dal.addItemsToOrder(order_number, items);

        assertFalse(dal.orderWhenLackOfStock("-1", quantity, store));
        dal.removeAllItemsFromOrder(order_number);
        dal.removeOrder(order_number);
    }

    @org.junit.Test
    public void getExecutedState() throws Exception {
        int order_number1 = dal.addNewOrder("-1", "", "1");
        assertTrue(dal.getExecutedState(order_number1) == 1);
        dal.removeOrderThatHasNotBeenSentYet(order_number1);
    }

    @org.junit.Test
    public void getOrderItemsInOrderByLocation() throws Exception {
        String store = "tel aviv";
        //create products
        HashMap<String, Integer> h1 = new HashMap<>();
        HashMap<String, Integer> h2 = new HashMap<>();
        h1.put(store, 300);
        h2.put("haifa", 300);
        dal.insertProduct(new Product("-1", "test1", "Osem", null, 5, 0, 22));
        dal.insertProduct(new Product("-2", "test2", "Osem", null, 5, 0, 22));
        //create inventory
        //dal.insertInventory(new Stock("-1",200, "1a", "2b", 100, 300, 0, 0, store));
        //dal.insertInventory(new Stock("-2",200, "1c", "2d", 100, 300, 0, 0, "haifa"));
        //create order
        int order_number = dal.addNewOrder("-1", LocalDate.now().toString(), "3");
        //create items
        LinkedList<Item> items = new LinkedList<>();
        items.add(new Item("-1", 1, "-1", new LinkedList<Discount>(), h1));
        items.add(new Item("-2", 1, "-2", new LinkedList<Discount>(), h2));
        dal.addItemsToOrder(order_number, items);
        //test
        LinkedList<OrderItem> orderItems = dal.getOrderItemsInOrderByLocation(order_number, store);
        assertEquals(1, orderItems.size());
        assertEquals("-1", orderItems.removeFirst().getSerial_number());
        //clean
        dal.removeAllItemsFromOrder(order_number);
        dal.removeProduct("-1");
        dal.removeProduct("-2");
        dal.removeOrder(order_number);
    }


    //----------------------------------------------------------------------------------------------------------
    //Inventory Tests

    @Test
    public void totalWeightOfHashMap() {
        String serial = "-10";
        int weight = 10;
        Category category = new Category(1, "blah", 0);
        HashMap<String, Integer> h = new HashMap<>();
        Product p = new Product(serial, "Blah", "blah", category, weight, 12, 15);
        dal.insertProduct(p);
        h.put("tel aviv", 1000);
        h.put("beer sheva", 2000);
        Assert.assertEquals((1000 + 2000) * weight, dal.totalWeightOfHashMap(h, serial));

    }

    @Test
    public void addItemsToOrder() {

        dal.insertCategory(category);
        category = new Category(dal.getCategoryId(category.getName()),
                category.getName(), category.getFatherCategory(), category.getFatherCategoryName());

        LinkedList<Order> o;
        int q1 = 10, q2 = 30, q3 = 70;
        int w1 = 100, w2 = 200, w3 = 300;

        HashMap h1 = new HashMap<String, Integer>();
        HashMap h2 = new HashMap<String, Integer>();
        HashMap h3 = new HashMap<String, Integer>();

        Product p1 = new Product("111", "Blah1", "blah", category, w1, 12, 15);
        Product p2 = new Product("121", "Blah2", "blah", category, w2, 12, 15);
        Product p3 = new Product("131", "Blah3", "blah", category, w3, 12, 15);

        h1.put("tel aviv", q1);
        h2.put("tel aviv", q2);
        h3.put("tel aviv", q3);

        Item i1 = new Item("111", 12, p1.getSerialNumber(), new LinkedList<>(), h1);
        Item i2 = new Item("121", 12, p2.getSerialNumber(), new LinkedList<>(), h2);
        Item i3 = new Item("131", 12, p3.getSerialNumber(), new LinkedList<>(), h3);

        dal.insertProduct(p1);
        dal.insertProduct(p2);
        dal.insertProduct(p3);
        LinkedList<Item> l = new LinkedList<Item>();
        l.add(i1);
        l.add(i2);
        l.add(i3);
        int totalWeight = q1 * w1 + q2 * w2 + q3 * w3;
        //Order o = new Order(5, sup, "10-10-2020",new LinkedList<>(  ), "1", 0 );
        int id = dal.addNewOrder(sup.getId(), "10-10-2020", "1");
        o = dal.getOrderByOrderNumber(id);
        Assert.assertNotNull(o.getFirst());
        Assert.assertEquals(0, o.getFirst().getWeight());
        dal.addItemsToOrder(id, l);
        o = dal.getOrderByOrderNumber(id);
        Assert.assertEquals(totalWeight, o.getFirst().getWeight());

        dal.removeOrder(id);
        dal.removeProduct(p1.getSerialNumber());
        dal.removeProduct(p2.getSerialNumber());
        dal.removeProduct(p3.getSerialNumber());
    }


    @Test
    public void removeCategory() {
        int id;
        String categoryName = "someCategory";
        Assert.assertEquals(-1, dal.getCategoryId(categoryName));
        id = dal.addNewCategory(categoryName);
        Assert.assertNotEquals(-1, id);
        Assert.assertEquals(id, dal.getCategoryId(categoryName));

        dal.removeCategory(id);
        Assert.assertEquals(-1, dal.getCategoryId(categoryName));
    }


    @Test
    public void productExistsInStore() {
        String serial = p1.getSerialNumber();
        String store = "tel aviv";
        storesDAO sDAO = new storesDAO(con);
        StoreDTO sDTO = new StoreDTO(Enums.Region.negev, store, "a", "0");
        sDAO.insert(sDTO);
        Assert.assertFalse(dal.productExistsInStore(serial, store));
        Stock stock = new Stock(serial, 1, "1", "1", 10, 10, 0, 0, store);
        dal.insertInventory(stock);
        //if(! dal.updateInventoryStorageQuantity(serial, store, 1 )) return;
        boolean b = dal.productExistsInStore(serial, store);
        sDAO.delete(sDTO.getAddress());
        Assert.assertTrue(b);
        dal.removeProduct(serial);
    }

    @Test
    public void getRegionOfStore() {
        storesDAO sDAO = new storesDAO(con);
        StoreDTO sDTO = new StoreDTO(Enums.Region.negev, "beer sheva", "a", "0");
        sDAO.insert(sDTO);
        Assert.assertEquals(sDTO.getRegion(), dal.getRegionOfStore(sDTO.getAddress()));
        sDAO.delete(sDTO.getAddress());
    }


    @Test
    public void removeAllItemsFromOrder() {

        dal.insertCategory(category);
        category = new Category(dal.getCategoryId(category.getName()),
                category.getName(), category.getFatherCategory(), category.getFatherCategoryName());

        Product p1 = new Product("-111", "Blah1", "blah", category, w1, 12, 15);
        Product p2 = new Product("-121", "Blah2", "blah", category, w2, 12, 15);
        Product p3 = new Product("-131", "Blah3", "blah", category, w3, 12, 15);


        int order = dal.addNewOrder(sup.getId(), "01-03-2018", "1");
        HashMap<String, Integer> h = new HashMap<>();
        h.put("STORE!", 10);
        dal.insertProduct(p1);
        Item i1 = new Item("-111", 12, p1.getSerialNumber(), new LinkedList<>(), h);
        dal.insertProduct(p2);
        Item i2 = new Item("-121", 12, p2.getSerialNumber(), new LinkedList<>(), h);
        dal.insertProduct(p3);
        Item i3 = new Item("-131", 12, p3.getSerialNumber(), new LinkedList<>(), h);
        LinkedList<Item> l = new LinkedList<>();
        l.add(i1);
        l.add(i2);
        l.add(i3);
        dal.addItemsToOrder(order, l);
        LinkedList<Order> lOrder = dal.getOrderByOrderNumber(order);
        Assert.assertEquals(3, lOrder.getFirst().getItems().size());

        dal.removeAllItemsFromOrder(order);

        lOrder = dal.getOrderByOrderNumber(order);
        Assert.assertEquals(0, lOrder.getFirst().getItems().size());

        dal.removeProduct(p1.getSerialNumber());
        dal.removeProduct(p2.getSerialNumber());
        dal.removeProduct(p3.getSerialNumber());
        dal.removeCategory(category.getId());
    }


    @Test
    public void getAllSuppliers() {
        storesDAO sDAO = new storesDAO(con);
        StoreDTO sDTO1 = new StoreDTO(Enums.Region.golanHeights, "tel aviv1", "a", "0");
        StoreDTO sDTO2 = new StoreDTO(Enums.Region.golanHeights, "tel aviv2", "a", "0");
        StoreDTO sDTO3 = new StoreDTO(Enums.Region.general, "beer sheva1", "a", "0");
        sDAO.insert(sDTO1);
        sDAO.insert(sDTO2);
        sDAO.insert(sDTO3);

        int current = dal.getAllSuppliers("tel aviv1").size();

        Supplier sup1 = new Supplier("-2", "dani1", "tel aviv1", "0", "Cash", new LinkedList<Contact>()
                , SupplyTerms.SupplyOnSetDays, "Sunday", new LinkedList<Item>(), 1);
        Supplier sup2 = new Supplier("-3", "dani2", "tel aviv2", "0", "Cash", new LinkedList<Contact>()
                , SupplyTerms.SupplyOnSetDays, "Sunday", new LinkedList<Item>(), 1);
        Supplier sup3 = new Supplier("-4", "dani3", "beer sheva1", "0", "Cash", new LinkedList<Contact>()
                , SupplyTerms.SupplyOnSetDays, "Sunday", new LinkedList<Item>(), 5);

        dal.insertToSuppliers(sup1);
        dal.insertToSuppliers(sup2);
        dal.insertToSuppliers(sup3);

        Assert.assertEquals(2, dal.getAllSuppliers("tel aviv1").size() - current);


        dal.deleteSupplier(sup1.getId());
        dal.deleteSupplier(sup2.getId());
        dal.deleteSupplier(sup3.getId());
        sDAO.delete(sDTO1.getAddress());
        sDAO.delete(sDTO2.getAddress());
        sDAO.delete(sDTO3.getAddress());
    }
}