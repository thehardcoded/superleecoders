package Test;
import DataProviders.DAOs.employeesDAO;
import SharedClasses.DTOs.EmployeeDTO;
import org.junit.Assert;
import org.junit.Test;
import org.sqlite.SQLiteConfig;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by SHLOMI PAKADO on 4/2/2017.
 */

public class EmployeeDAOtest {

    Connection con;
    employeesDAO empDAO;



    @org.junit.Before
    public void setUp() throws Exception {
        this.con = null;
        try {
            Class.forName("org.sqlite.JDBC");
            SQLiteConfig config = new SQLiteConfig();
            //config.enforceForeignKeys(true);
            con = DriverManager.getConnection("jdbc:sqlite:DB/Shop.db",config.toProperties());
            empDAO =new employeesDAO(con);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @org.junit.After
    public void tearDown() {
        if(empDAO.getEmployeeById("1")!=null)
            empDAO.DeleteEmployee("1");
        empDAO.close();
    }



 //   @Test
//    public void addEmployeeTest()
//    {
//        try {
//            Class.forName("org.sqlite.JDBC");
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
//        SQLiteConfig config = new SQLiteConfig();
//        config.enforceForeignKeys(true);
//        empDAO =new employeesDAO(con);
//        EmployeeDTO ToAdd=new EmployeeDTO("1","Test","Testi","Shift Manager",100,"2011/02/02",null,"","General",12,12,12);
//        empDAO.AddEmployee(ToAdd);
//        Assert.assertTrue(empDAO.getEmployeeById("1")!=null);
//    }

    @Test
    public void delEmployeeTest()
    {
        EmployeeDTO ToAdd=new EmployeeDTO("1","Test","Testi","Shift Manager",100,"2011/02/02",null,"","General",12,12,12);
        empDAO.AddEmployee(ToAdd);
        empDAO.DeleteEmployee("1");
        Assert.assertTrue(empDAO.getEmployeeById("1")==null);
    }

    @Test
    public void updateEmployeeTest(){
        String oldId = "1";
        EmployeeDTO ToAdd=new EmployeeDTO(oldId,"Test","Testi","Shift Manager",100,"2011/02/02",null,"","General",12,12,12);
        EmployeeDTO ToUpdate=new EmployeeDTO("2","Test1","Testi1","Driver",200,"2012/02/02",null,"","General",11,11,11);
        empDAO.AddEmployee(ToAdd);
        empDAO.updateEmployee( oldId, ToUpdate );
        Assert.assertTrue(empDAO.getEmployeeById(oldId)==null);
        empDAO.DeleteEmployee("2");
    }



}
