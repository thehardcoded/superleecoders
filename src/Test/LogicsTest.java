package Test;

import DataProviders.DAL;
import DataProviders.Repository;
import LogicLayer.LogicManager;
import SharedClasses.*;
import SharedClasses.DTOs.TransportationDocDTO;

import java.util.HashMap;
import java.util.LinkedList;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Shahar on 25/05/2017.
 */
public class LogicsTest {

    private LogicManager logM;
    private DAL dal;
    private Repository rep;

    @org.junit.Before
    public void setUp() throws Exception {
        logM = new LogicManager();
        dal = logM.getDal();
        rep = logM.getRep();
        dal.insertToSuppliers(new Supplier("-1", "dani","Beer Sheva","0", "Cash", new LinkedList<Contact>(), SupplyTerms.SupplyOnSetDays,"Sunday", new LinkedList<Item>(),1));
    }

    @org.junit.After
    public void tearDown(){
        dal.deleteSupplier("-1");
        dal.close();
        rep.close();
    }

    @org.junit.Test
    public void removeOrderThatHasBeenSentTest() throws Exception {
        String result;
        String store = "tel aviv";
        int quantity = 10;
        LinkedList<Item> items = new LinkedList<>();
        HashMap<String, Integer> h = new HashMap<>(  );
        h.put( store, quantity );

        int order_number = dal.addNewOrder("-1","2017-05-25","1");
        items.add(new Item("-1", 1, "-1", new LinkedList<Discount>(), h));
        dal.addItemsToOrder(order_number, items);

        rep.AddTransportationDocs(new TransportationDocDTO(order_number, "2017-05-29", 1, "111111111", "004444444", "-1", 0, 3,0));

        result = logM.removeOrderThatHasBeenSent(order_number);
        assertTrue(result.equals("Order number " + order_number + " wasn't sent yet!"));

        dal.updateExecutedInOrders(order_number, "2");

        result = logM.removeOrderThatHasBeenSent(order_number);
        assertTrue(result.equals("Order number " + order_number + " and the transportation was cancelled successfully!"));
        assertFalse(dal.orderExists(-1));
        assertFalse(rep.checkTransDocIDExist(order_number));

        rep.removeTransportationDoc(-1);
    }

}
