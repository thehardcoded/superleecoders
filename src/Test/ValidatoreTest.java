package Test;
import SharedClasses.DTOs.EmployeeDTO;
import org.junit.*;
import LogicLayer.Validator;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
 * Created by SHLOMI PAKADO on 4/2/2017.
 */
public class ValidatoreTest {

    Validator val;

    public ValidatoreTest()
    {
        this.val=new Validator();

    }

    @Test
    public void isValidDateTest()
    {
        Assert.assertTrue(this.val.isValidDate("12-12-2017"));
        Assert.assertTrue(this.val.isValidDate("1-1-0001"));
        Assert.assertTrue(this.val.isValidDate("3-2-2122"));
        Assert.assertFalse(this.val.isValidDate("sad dasad sa "));
        Assert.assertTrue(this.val.isValidDate("3-3-3"));
    }

    @Test
    public void isValidSalaryTest()
    {
        Assert.assertFalse(this.val.isValidSalary("12/12/2017"));
        Assert.assertFalse(this.val.isValidSalary("1/1/0001"));
        Assert.assertFalse(this.val.isValidSalary("3/sadas2122"));
        Assert.assertTrue(this.val.isValidSalary("123"));
        Assert.assertTrue(this.val.isValidSalary("1"));
    }

    @Test
    public void isValidNameTest()
    {
        Assert.assertTrue(this.val.isValidName("moshe"));
        Assert.assertTrue(this.val.isValidName("haims"));
        Assert.assertFalse(this.val.isValidName("asd aasd asd "));
        Assert.assertFalse(this.val.isValidName("-12321241"));
        Assert.assertFalse(this.val.isValidName("asd+asdas"));
    }

    @Test
    public void isValidPastTest()
    {
        LocalDate next = LocalDate.now();
        next= next.minus(1, ChronoUnit.DAYS);
        Assert.assertTrue(val.isPast((EmployeeDTO.shiftDateFormat(next.toString()))));
        next= next.minus(13, ChronoUnit.DAYS);
        Assert.assertTrue(val.isPast((EmployeeDTO.shiftDateFormat(next.toString()))));
        next= next.plus(100, ChronoUnit.DAYS);
        Assert.assertFalse(val.isPast((EmployeeDTO.shiftDateFormat(next.toString()))));
        next = LocalDate.now();
        Assert.assertTrue(val.isPast((EmployeeDTO.shiftDateFormat(next.toString()))));
    }

    @Test
    public void isValidFutureTest()
    {
        LocalDate next = LocalDate.now();
        next= next.minus(1, ChronoUnit.DAYS);
        Assert.assertFalse(val.isFuture((EmployeeDTO.shiftDateFormat(next.toString().replace("-","-")))));
        next= next.minus(13, ChronoUnit.DAYS);
        Assert.assertFalse(val.isFuture((EmployeeDTO.shiftDateFormat(next.toString().replace("-","-")))));
        next= next.plus(100, ChronoUnit.DAYS);
        Assert.assertTrue(val.isFuture((EmployeeDTO.shiftDateFormat(next.toString().replace("-","-")))));
        next = LocalDate.now();
        Assert.assertTrue(val.isFuture((EmployeeDTO.shiftDateFormat(next.toString().replace("-","-")))));
    }

    @Test
    public void isValidAccountTest()
    {
        Assert.assertFalse(this.val.isAccount("moshe"));
        Assert.assertFalse(this.val.isAccount("haims"));
        Assert.assertFalse(this.val.isAccount("asd aasd asd "));
        Assert.assertFalse(this.val.isAccount("-12321241"));
        Assert.assertTrue(this.val.isAccount("1243"));
    }

    @Test
    public void isValidEmpIdTest()
    {
        Assert.assertFalse(this.val.isValidID("moshe"));
        Assert.assertFalse(this.val.isValidID("1231-=-12"));
        Assert.assertFalse(this.val.isValidID("asd aasd asd "));
        Assert.assertFalse(this.val.isValidID("-12321241"));
        Assert.assertFalse(this.val.isValidID("1243567821"));
        Assert.assertFalse(this.val.isValidID("124356782113274893273428748032742307"));
        Assert.assertTrue(this.val.isValidID("124356782"));
    }

}
