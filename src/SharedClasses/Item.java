package SharedClasses;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class Item {

	private String catalog_number;
	private double price;
	private String serialNumber;
	private LinkedList<Discount> discounts;
	private HashMap<String, Integer> storeQuantity;

	public Item(String catalogNumber, double price, String serialNumber, LinkedList<Discount> discounts, HashMap<String, Integer> storeQuantity){
		this.catalog_number = catalogNumber;
		this.serialNumber = serialNumber;
		this.price = price;
		this.discounts = discounts;
		this.storeQuantity = storeQuantity;
	}

	public HashMap<String, Integer> getStoreQuantity() {
		return storeQuantity;
	}

	public void setStoreQuantity(HashMap<String, Integer> storeQuantity) {
		this.storeQuantity = storeQuantity;
	}

	public String getCatalogNumber() {
		return catalog_number;
	}

	public void setCatalogNumber(String catalogNumber) {
		this.catalog_number = catalogNumber;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	//todo check printing
	public String toString(){
		String s = "Catalog number: " + catalog_number + " , Price: " + price + "\n";
		if(storeQuantity!=null && storeQuantity.size() != 0){
			s += "Stores and Quantity:\n";
			for(Map.Entry entry : storeQuantity.entrySet()){
			    s += "store: " + entry.getKey() + ", quantity: " + entry.getValue() + "\n";
            }
		}
		s = s + "Discounts:";
		for(Discount discount: discounts){
			s = s + "\n" + discount.toString();
		}
		return s;
	}

	public LinkedList<Discount> getDiscounts() {
		return discounts;
	}

	public void setDiscounts(LinkedList<Discount> discounts) {
		this.discounts = discounts;
	}
}
