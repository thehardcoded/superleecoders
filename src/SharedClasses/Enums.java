package SharedClasses;

/**
 * Created by AmirPC on 29/03/2017.
 */
public class Enums {

    // ---------------------------------- special -----------------------------------

    public enum License {lv1, lv2, lv3, lv4, lv5}
    public enum TruckModel {t1, t2, t3, t4, t5}
    public enum Region {golanHeights, galil, coastalPlain, valley, negev, general}

}
