package SharedClasses;

public class Product {

	private String serialNumber;
    private String name;
    private String manufacturer;
    private Category category;
    private int weight;
    private int cost;
    private int sellingPrice;

    public Product(String serialNumber, String name, String manufacturer, Category category, int size, int cost, int sellingPrice) {
        this.serialNumber = serialNumber;
        this.name = name;
        this.manufacturer = manufacturer;
        this.category = category;
        this.weight = size;
        this.cost = cost;
        this.sellingPrice = sellingPrice;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public String getName() {
        return name;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public Category getCategory() {
        return category;
    }

    public int getWeight() {
        return weight;
    }

    public int getCost() {
        return cost;
    }

    public int getSellingPrice() {
        return sellingPrice;
    }

    @Override
    public String toString() {
        String result = "Product{" +
                "Serial Number ='" + serialNumber + '\'' +
                ", name ='" + name + '\'' +
                ", manufacturer ='" + manufacturer + '\'';
        if (category == null)
            result += ", category ={null}";
        else
            result += ", category =" + category.getName();
        result += ", weight =" + weight +
                ", selling Price =" + sellingPrice +
                "}\n";
        return result;
    }
	
}
