package SharedClasses;

public class Contact {

	private String name;
	private String phone_number;
	
	public Contact(String name, String phone){
		this.name = name;
		this.phone_number = phone;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone_number;
	}

	public void setPhone(String phone) {
		this.phone_number = phone;
	}
	
	public String toString(){
		return ("Name: " + name + ", Phone Number: " + phone_number);
	}
	
}
