package SharedClasses;

public class Stock {

    private String serialNumber;
    private String productName;
    private int shelfQuantity;
    private int storageQuantity;
    private int shelfDefectives;
    private int storageDefectives;
    private int minimalQuantity;
    private String shelfLocation;
    private String storageLocation;
    private String store;

    public Stock(String serialNumber, int minimalQuantity, String shelfLocation, String storageLocation, int shelfQuantity, int storageQuantity, int shelfDefectives, int storageDefectives, String store) {
        this.serialNumber = serialNumber;
        this.productName = "";
        this.minimalQuantity = minimalQuantity;
        this.shelfDefectives = shelfDefectives;
        this.storageDefectives = storageDefectives;
        this.shelfLocation = shelfLocation;
        this.storageLocation = storageLocation;
        this.shelfQuantity = shelfQuantity;
        this.storageDefectives = storageDefectives;
        this.storageQuantity = storageQuantity;
        this.store = store;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public String getProductName() {
        return productName;
    }

    public String getStore() {
        return store;
    }

    public void setProductName(String name) {
        productName = name;
    }

    public int getMinimalQuantity() {
        return minimalQuantity;
    }

    public int getShelfQuantity() {
        return shelfQuantity;
    }

    public int getStorageQuantity() {
        return storageQuantity;
    }

    public int getShelfDefectives() {
        return shelfDefectives;
    }

    public int getStorageDefectives() {
        return storageDefectives;
    }

    public String getShelfLocation() {
        return shelfLocation;
    }

    public String getStorageLocation() {
        return storageLocation;
    }

    public void setStorageQuantity(int storageQuantity) {
        this.storageQuantity = storageQuantity;
    }

    public void setStorageDefectives(int storageDefectives) {
        this.storageDefectives = storageDefectives;
    }

    @Override
    public String toString() {
        String ans = "";
        ans += "Stock in store: " +
                store + " \n{ " +
                "serialNumber=" + serialNumber;
        if (!productName.equals(""))
            ans += ", name='" + productName + '\'';
        ans += "\nlocation in shelf='" + shelfLocation + '\'' +
                ", location in storage='" + storageLocation + '\'' +
                "\nquantity in shelf=" + shelfQuantity +
                ", quantity in storage=" + storageQuantity +
                "\ndefectives in shelf=" + shelfDefectives +
                ", defectives in storage=" + storageDefectives +
                "\nminimal quantity=" + minimalQuantity +
                "}\n";
        return ans;
    }
}
