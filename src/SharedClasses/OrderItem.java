package SharedClasses;

public class OrderItem {

	private String supplier_id;
	private String serial_number;
	private String catalog_number;
	private int quantity;
	private double price;
	private double discount;
	private String name;
	private String store;
	private int status;

	public OrderItem(String supplier_id, String serial_number, String catalog_number, int quantity, double price, double discount, String name, String store, int status){
		this.supplier_id = supplier_id;
		this.serial_number = serial_number;
		this.catalog_number = catalog_number;
		this.quantity = quantity;
		this.price = price;
		this.discount = discount;
		this.name = name;
		this.store = store;
		this.status = status;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}

	public String getCatalog_number() {
		return catalog_number;
	}

	public void setCatalog_number(String catalog_number) {
		this.catalog_number = catalog_number;
	}

	public String getSerial_number() {
		return serial_number;
	}

	public void setSerial_number(String serial_number) {
		this.serial_number = serial_number;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public String getSupplier_id() {
		return supplier_id;
	}

	public void setSupplier_id(String supplier_id) {
		this.supplier_id = supplier_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString(){
		String s = "Store: " + store + ", Catalog Number: " + catalog_number +", Serial Number: " + serial_number +", Product Name: " + name + ", Quantity: " + quantity + "\n";
		s = s + "Price: " + price + ", Discount: " + discount*100+"%" + ", Final Price: " + ((price - (price*discount))*quantity) + "\n";
		return s;
	}
}
