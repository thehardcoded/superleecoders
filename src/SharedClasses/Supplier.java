package SharedClasses;

import java.util.LinkedList;

public class Supplier {

	private String ID;
	private String name;
	private String address;
	private String bank_Account;
	private String payment;
	private LinkedList<Contact> contacts;
	private SupplyTerms supply_terms;
	private String supply_days;
	private LinkedList<Item> items;
	private int region;

	public Supplier(String id, String name, String address, String bankAccount, String termsOfPayment, LinkedList<Contact> contacts, SupplyTerms terms, String supply_days, LinkedList<Item> items, int region){
		this.ID = id;
		this.name = name;
		this.address = address;
		this.bank_Account = bankAccount;
		this.payment = termsOfPayment;
		this.contacts = contacts;
		this.supply_terms = terms;
		this.supply_days = supply_days;
		this.items = items;
		this.region = region;
	}

    public int getRegion() {
        return region;
    }

    public void setRegion(int region) {
        this.region = region;
    }

    public String getBankAccount() {
		return bank_Account;
	}

	public void setBankAccount(String bankAccount) {
		this.bank_Account = bankAccount;
	}

	public String getId() {
		return ID;
	}

	public void setId(String id) {
		this.ID = id;
	}

	public String getTermsOfPayment() {
		return payment;
	}

	public void setTermsOfPayment(String termsOfPayment) {
		this.payment = termsOfPayment;
	}

	public LinkedList<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(LinkedList<Contact> contacts) {
		this.contacts = contacts;
	}

	public SupplyTerms getTerms() {
		return supply_terms;
	}

	public void setTerms(SupplyTerms terms) {
		this.supply_terms = terms;
	}

	public LinkedList<Item> getItems() {
		return items;
	}

	public void setItems(LinkedList<Item> items) {
		this.items = items;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSupply_days() {
		return supply_days;
	}


	public void setSupply_days(String supply_days) {
		this.supply_days = supply_days;
	}

	public String toString(){
		String s = "ID: "+ getId() + " Name: " + name + ", Address: " + address + ", Bank Account: " + bank_Account;
		s = s + "\nPayment Method: " + payment + ", Supply terms: " + supply_terms;
		if(supply_terms==SupplyTerms.SupplyOnSetDays){
			s = s + ", Supply Days: " + supply_days;
		}
		s = s + "\nContacts:";
		for (Contact c : contacts){
			s = s + "\n" + c.toString();
		}
		return s;
	}
}
