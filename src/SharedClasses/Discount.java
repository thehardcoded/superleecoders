package SharedClasses;

public class Discount {
	
	private int quantity;
	private double discount;
	
	public Discount(int quantity, double discount){
		this.quantity = quantity;
		this.discount = discount;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}
	
	public String toString(){
		return "Quantity: " + quantity + ", Discount: " + discount;
	}
	
}
