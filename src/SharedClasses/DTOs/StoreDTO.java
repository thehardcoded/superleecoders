package SharedClasses.DTOs;

import SharedClasses.Enums;

/**
 * Created by AmirPC on 27/04/2017.
 */
public class StoreDTO extends LocationDTO {

    public StoreDTO()
    {
        super();
    }

    public StoreDTO(Enums.Region region, String address, String contactName, String phoneNum){
        super(region, address, contactName, phoneNum);
    }
}
