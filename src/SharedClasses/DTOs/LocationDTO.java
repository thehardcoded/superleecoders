package SharedClasses.DTOs;

import SharedClasses.Enums;

/**
 * Created by AmirPC on 28/03/2017.
 */
public class LocationDTO {

    private String Address;
    private Enums.Region Region;
    private String ContactName;
    private String PhoneNum;

    public LocationDTO(){

    }

    public LocationDTO(Enums.Region Region, String Address, String ContactName, String PhoneNum){

        this.Address = Address;
        this.Region = Region;
        this.ContactName = ContactName;
        this.PhoneNum = PhoneNum;
    }

    // ------------------------------------------- getters -------------------------------------------------------------

    public int getRegion(){
        return Region.ordinal() + 1;
    }

    public String getAddress(){
        return Address;
    }

    public String getContactName(){
        return ContactName;
    }

    public String getPhoneNum(){
        return this.PhoneNum;
    }

    // -------------------------------------------- setters ------------------------------------------------------------

    public void setRegion(int regionNum){
        this.Region = Enums.Region.values()[regionNum];
    }

    public void setAddress(String Address){
        this.Address = Address;
    }

    public void setContactName(String ContactName){
        this.ContactName = ContactName;
    }

    public void setPhoneNum(String PhoneNum){
        this.PhoneNum = PhoneNum;
    }

}
