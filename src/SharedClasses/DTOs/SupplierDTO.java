package SharedClasses.DTOs;

import SharedClasses.Enums;

/**
 * Created by AmirPC on 27/04/2017.
 */
public class SupplierDTO extends LocationDTO {

    public SupplierDTO()
    {
        super();
    }

    public SupplierDTO(Enums.Region region, String address, String contactName, String phoneNum){
        super(region, address, contactName, phoneNum);
    }

}
