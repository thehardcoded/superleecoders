package SharedClasses.DTOs;

import SharedClasses.Enums;

/**
 * Created by AmirPC on 28/03/2017.
 */
public class TruckDTO {

    private String ID;
    private Enums.TruckModel Model;

    public TruckDTO(){
    }

    public TruckDTO(String ID, Enums.TruckModel Model){
        this.ID = ID;
        this.Model = Model;
    }

    // ---------------------------------------- getters -----------------------------------------------

    public String getID(){
        return ID;
    }

    public int getModel(){
        return Model.ordinal() + 1;
    }

    // ---------------------------------------- setters -----------------------------------------------

    public void setID(String ID) {
        this.ID = ID;
    }

    public void setModel(int modelNum) {
        this.Model = Enums.TruckModel.values()[modelNum];
    }

	public static int getTruckMaxWeightByModel(int model){
		switch (model){
			case 1:
				return 4000000;
			case 2:
				return 5000000;
			case 3:
				return 6000000;
			case 4:
				return 7000000;
			case 5:
				return 8000000;
			default:
				return -1;
		}
	}
}
