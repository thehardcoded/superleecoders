package SharedClasses.DTOs;

import SharedClasses.Enums;

import java.sql.Time;
import java.util.Date;

/**
 * Created by AmirPC on 28/03/2017.
 */
public class TransportationDocDTO {

    private int ID;
    private String Date;
    private int Shift;
    private String TruckID;
    private String DriverID;
    private String Supplier;
    private int WeightAtLeave;
    private Enums.Region Region;
    private int confirm;

    public TransportationDocDTO(){
    }

    public TransportationDocDTO(int ID, String Date, int Shift, String TruckID, String DriverID, String Supplier, int weightAtLeave, int Region,int conf){
        this.ID = ID;
        this.Date = Date;
        this.Shift = Shift;
        this.TruckID = TruckID;
        this.DriverID = DriverID;
        this.Supplier = Supplier;
        this.WeightAtLeave = weightAtLeave;
        this.Region = Enums.Region.values()[Region];
        this.confirm=conf;
    }

    // ------------------------------------------- getters ----------------------------------------

    public int getConfirm() {
        return confirm;
    }

    public int getID(){
        return ID;
    }

    public String getDate(){
        return Date;
    }

    public int getShift(){
        return Shift;
    }

    public String getTruckID(){
        return TruckID;
    }

    public String getDriverID(){
        return DriverID;
    }

    public String getSupplier(){
        return Supplier;
    }


    public int getWeightAtLeave(){
        return WeightAtLeave;
    }

    public int getRegion() {
        return Region.ordinal() + 1;
    }

    //-------------------------------------------- setters ----------------------------------------

    public void setConfirm(int confirm) {
        this.confirm = confirm;
    }
    public void setID(int ID) {
        this.ID = ID;
    }


    public void setDate(String date) {
        this.Date =EmployeeDTO.shiftDateFormat(date) ;}


    public void setShift(int Shift) {
        this.Shift = Shift;
    }

    public void setTruckID(String TruckID) {
        this.TruckID = TruckID;
    }

    public void setDriverID(String DriverID) {
        this.DriverID = DriverID;
    }

    public void setSupplier(String Supplier) {
        this.Supplier = Supplier;
    }

    public void setWeightAtLeave(int WeightAtLeave) {
        this.WeightAtLeave = WeightAtLeave;
    }

    public void setRegion(int regionNum) {
        this.Region = Enums.Region.values()[regionNum];
    }
}
