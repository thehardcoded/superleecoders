package SharedClasses.DTOs;

/**
 * Created by Dvir Alkobi on 3/29/2017.
 */
public class RoleInShiftDTO {

    private String Role;
    private int shiftType;
    private int amount;
    private String StoreAddress;
    /**
     * Constructor
     */
    public RoleInShiftDTO(String role, int shiftType, int amount, String storeAddress) {
        this.Role = role;
        this.shiftType = shiftType;
        this.amount = amount;
        StoreAddress = storeAddress;
    }

    /**
     * Getters && Setters
     */
    //todo:think if we want to encapsulate some of the field by permissions

    public String getStoreAddress() {
        return StoreAddress;
    }public void setStoreAddress(String storeAddress) {
        StoreAddress = storeAddress;
    }public String getRole() {
        return Role;
    }

    public int getShiftType() {
        return shiftType;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Role+" : "+amount);
        return sb.toString();
    }
}
