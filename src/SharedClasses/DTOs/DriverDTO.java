package SharedClasses.DTOs;

import SharedClasses.Enums;

/**
 * Created by AmirPC on 28/03/2017.
 */
public class DriverDTO {

    private String ID;
    private Enums.License License;
    private Enums.Region Region;

    public DriverDTO(){

    }

    public DriverDTO(String ID, Enums.License License,Enums.Region Region){
        this.ID = ID;
        this.License = License;
        this.Region=Region;
    }

    //-------------------------------------- getters -----------------------------------

    public String getID(){
        return ID;
    }

    public int getLicense(){
        return License.ordinal() + 1;
    }


	public int getRegion() { return Region.ordinal() + 1;
	}


    //--------------------------------------- setters ----------------------------------

    public void setID(String ID) {
        this.ID = ID;
    }

    public void setLicense(int licenseNum) {
        this.License = Enums.License.values()[licenseNum];
    }


    public void setRegion(int Region) {
        this.Region = Enums.Region.values()[Region];

    }


}
