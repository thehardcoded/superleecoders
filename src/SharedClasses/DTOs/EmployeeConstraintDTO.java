package SharedClasses.DTOs;

/**
 * Created by Dvir Alkobi on 3/29/2017.
 */
public class EmployeeConstraintDTO {
    private String ID;
    private int shiftType;
    private String shiftDate;
    private String ConstraintDescription;
    private String store;

    /**
     * Constructor
     */

    public EmployeeConstraintDTO(String ID, int shiftType, String shiftDate, String constraint, String store) {
        this.ID = ID;
        this.shiftType = shiftType;
        this.shiftDate = shiftDate;
        ConstraintDescription = constraint;
        this.store = store;
    }


    public EmployeeConstraintDTO() {

    }

    /**
     * Getters && Setters
     */

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public String getID() {
        return ID;
    }

    public int getShiftType() {
        return shiftType;
    }

    public String getDate() {
        return EmployeeDTO.shiftDateFormat(shiftDate);
    }

    public String getConstraint() {
        return ConstraintDescription;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public void setShiftType(int shiftType) {
        this.shiftType = shiftType;
    }

    public void setShiftDate(String shiftDate) {
        this.shiftDate = EmployeeDTO.shiftDateFormat(shiftDate);
    }

    public void setConstraint(String constraint) {
        ConstraintDescription = constraint;
    }


}
