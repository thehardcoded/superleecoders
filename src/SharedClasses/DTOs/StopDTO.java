package SharedClasses.DTOs;

/**
 * Created by AmirPC on 29/03/2017.
 */
public class StopDTO {

    private int TransportationDocNo;
    private String Destination;

    public StopDTO(){
    }

    public StopDTO(int TransportationDocNo, String Destination)
    {
        this.TransportationDocNo = TransportationDocNo;
        this.Destination = Destination;
    }

    //--------------------------------------------- getters --------------------------------------------


    public int getTransportationDocNo(){
        return TransportationDocNo;
    }

    public String getDestination(){
        return Destination;
    }

    //---------------------------------------------- setters -------------------------------------------


    public void setTransportationDocNo(int TransportationDocNo) {
        this.TransportationDocNo = TransportationDocNo;
    }

    public void setDestination(String Destination) {
        this.Destination = Destination;
    }
}
