package SharedClasses.DTOs;

import java.sql.Date;

/**
 * Created by SHLOMI PAKADO on 3/15/2017.
 */
public class EmployeeDTO {



    private String EmployeeID;
    private String FirstName;
    private String LastName;
    private String Role;
    private int Salary;
    private String DateOfTransaction;
    private String EmploymentTerminationDate;
    private String TermsOfTheDeal;
    private String StoreAddress;
    private int BankNumber;
    private int BranchNumber;
    private int BankAccount;



    public EmployeeDTO(String ID, String FirstName, String LastName, String Role, int Salary, String DateOfTransaction, String EmploymentTerminationDate,
                       String TermsOfTheDeal,String store, int BankNumber, int BranchNumber, int BankAccount  )
    {
       this.EmployeeID=ID;
       this.FirstName=FirstName;
       this.LastName=LastName;
       this.Role=Role;
       this.Salary=Salary;
       this.DateOfTransaction=DateOfTransaction;
       this.EmploymentTerminationDate=EmploymentTerminationDate;
       this.TermsOfTheDeal=TermsOfTheDeal;
       this.StoreAddress=store;
       this.BankNumber=BankNumber;
       this.BranchNumber=BranchNumber;
       this.BankAccount=BankAccount;
    }

    public EmployeeDTO()
    {}


    //getters and setters
    //todo:think if we want to encapsulate some of the field by permissions
    public void setRole(String role) {
        Role = role;
    }

    public void setDateOfTransaction(String dateOfTransaction) {
        DateOfTransaction = shiftDateFormat(dateOfTransaction);
    }

    public String getStore() {
        return StoreAddress;
    }

    public void setStore(String store) {
        this.StoreAddress = store;
    }

    public void setTermsOfTheDeal(String termsOfTheDeal) {
        TermsOfTheDeal = termsOfTheDeal;
    }

    public void setBankNumber(int bankNumber) {
        BankNumber = bankNumber;
    }

    public void setBranchNumber(int branchNumber) {
        BranchNumber = branchNumber;
    }

    public void setBankAccount(int bankAccount) {
        BankAccount = bankAccount;
    }

    public String getRole() {
        return Role;
    }

    public String getDateOfTransaction() {
        return DateOfTransaction;
    }

    public String getTermsOfTheDeal() {
        return TermsOfTheDeal;
    }

    public int getBankNumber() {
        return BankNumber;
    }

    public int getBranchNumber() {
        return BranchNumber;
    }

    public int getBankAccount() {
        return BankAccount;
    }

    public String getID() {
        return this.EmployeeID;
    }

    public String getFirstName() {
        return FirstName;
    }

    public String getLastName() {
        return LastName;
    }

    public int getSalary() {
        return Salary;
    }

    public String getEmploymentTerminationDate() {
        return EmploymentTerminationDate;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public void setSalary(int salary) {
        Salary = salary;
    }

    public void setEmploymentTerminationDate(String employmentTerminationDate) {
        EmploymentTerminationDate = shiftDateFormat(employmentTerminationDate);
    }

    public void setID(String ID){this.EmployeeID=ID;}

    public static String shiftDateFormat(String Date)
    {
        String[] beforeArr=Date.split("-");
        return (beforeArr[2]+"-"+beforeArr[1]+"-"+beforeArr[0]);
    }

    public String toString(){
        String date="non";
        if(EmploymentTerminationDate!=null)
        {
            date=EmploymentTerminationDate;
        }
        return "ID: "+EmployeeID+"\n" +
                "First name: "+FirstName+"\n" +
                "Last name: "+LastName+"\n" +
                "Role: "+Role+"\n" +
                "Salary: "+Salary+"\n"+
                "Date Of Transaction: "+DateOfTransaction+"\n"+
                "Employment Termination Date: "+date+"\n"+
                "Terms Of The Deal: "+TermsOfTheDeal+"\n" +
                "Store: "+StoreAddress+"\n"+
                "BankNumber: "+BankNumber+"\n"+
                "BranchNumber: "+BranchNumber+"\n"+
                "BankAccount: "+BankAccount+"\n";
    }
}
