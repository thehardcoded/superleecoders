package SharedClasses.DTOs;

/**
 * Created by Dvir Alkobi on 4/28/2017.
 */
public class ShiftInDateDTO {
    private int shiftType;
    private String shiftDate;
    private String store;

    public ShiftInDateDTO(int shiftType, String shiftDate, String store) {
        this.shiftType = shiftType;
        this.shiftDate = shiftDate;
        this.store = store;
    }

    public int getShiftType() {
        return shiftType;
    }

    public void setShiftType(int shiftType) {
        this.shiftType = shiftType;
    }

    public String getShiftDate() {
        return shiftDate;
    }

    public void setShiftDate(String shiftDate) {
        this.shiftDate = shiftDate;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }
}
