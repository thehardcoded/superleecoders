package SharedClasses;

import java.util.LinkedList;

public class Order {

	private int order_number;
	private Supplier supplier;
	private String date;
	private String executed;
	private LinkedList<OrderItem> items;
	private int weight;

	public Order(int order_number, Supplier supplier, String date, LinkedList<OrderItem> items, String executed, int weight){
		this.order_number = order_number;
		this.supplier = supplier;
		this.date = date;
		this.items = items;
		this.executed = executed;
		this.weight = weight;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public int getOrder_number() {
		return order_number;
	}

	public void setOrder_number(int order_number) {
		this.order_number = order_number;
	}

	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public LinkedList<OrderItem> getItems() {
		return items;
	}

	public void setItems(LinkedList<OrderItem> items) {
		this.items = items;
	}

	public LinkedList<String> getStoresFromOrder(){
		LinkedList<String> stores=new LinkedList<>();
		for(OrderItem item : items){
			if(!stores.contains(item.getStore())){
				stores.add(item.getStore());
			}
		}
		return stores;
	}

	public String toString(){
		//contatcs and executed
		String s = "Order details:\n";
		s = s + "Supplier Name: " +supplier.getName() + ", Address: " + supplier.getAddress() + ", Order Number: " + order_number + "\n";
		s = s + "Supplier ID: " + supplier.getId() + ", Order Date: " + date + ", Executed State: " + executed;
		s = s + "\nSupplier Contacts:";
		for(Contact contact : supplier.getContacts()){
			s = s + "\n" + contact.toString();
		}
		s = s + "\nOrder Items:";
		if(items.size() == 0){
			s += "\nNo Items to show";
		}
		else {
			for (OrderItem item : items) {
				s = s + "\n" + item.toString();
			}
		}
		return s + "\n";
	}
}
