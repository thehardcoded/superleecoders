import PresentationLayer.PresentationManager;

/**
 * Created by SHLOMI PAKADO on 3/17/2017.
 */
public class Main
{
    public static void main(String [ ] args)
    {
        PresentationManager start = new PresentationManager();
        start.start();
    }
}
