import SharedClasses.SupplyTerms;
import org.sqlite.SQLiteConfig;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by muszk on 5/27/2017.
 */
public class PopDB {
    private Connection con = null;
    private Statement stmt;

    public static void main(String[] args){
        CreateDB db = new CreateDB();
        db.dowork();
        PopDB popdb = new PopDB();
        popdb.dowork();
    }

    public void dowork(){
        connectToDB();
        populateRoles();
        populateStores();
        populateTruckModels();
        populateTrucks();
        populateShiftTypes();
        populateEmployee();
        populateDrivers();
        populateRolesInShifts();
        populateShiftInDates();
        populateEmployeeConstraints();
        populateEmployeeInShift();
        populateSales();
        populateSuppliers();
        populateCategories();
        populateContacts();
        populateProducts();
        populateContactsOfSuppliers();
        populateItemsOnSale();
        populateInventory();
        populateItemsInContract();
        populateStoresOfItemsInContract();
        populateDiscount();
    }

    public void connectToDB() {
        try {
            Class.forName("org.sqlite.JDBC");
            SQLiteConfig config = new SQLiteConfig();
            config.enforceForeignKeys(true);
            con = DriverManager.getConnection("jdbc:sqlite:DB/Shop.db", config.toProperties());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void populateRoles() {
        String[] roles = {"ManPower Manager",
                "Shift Manager",
                "Logistic Manager",
                "Driver", "Cashier",
                "Storekeeper",
                "Administrative Manager",
                "Store Manager"

        };
        stmt = null;
        for (String role : roles) {
            try {
                stmt = con.createStatement();
                String CreationSqlSt = "INSERT INTO  Roles " +
                        "SELECT '" + role + "' " +
                        "WHERE NOT EXISTS (SELECT * FROM Roles WHERE Role = '" + role + "');";
                stmt.executeUpdate(CreationSqlSt);
            } catch (SQLException e) {
                System.out.println(role);
            }
        }
    }

    public void populateStores() {
        String[] stores = {"INSERT INTO stores VALUES('General',6,'ADMIN','0500000000')",
                "INSERT INTO stores VALUES('Tavor',1,'Shlomo','0508846799')",
                "INSERT INTO stores VALUES('Kiryat Shmona',1,'Haim','0547623622')",
                "INSERT INTO stores VALUES('Nahariyya',2,'Dan','0539876642')",
                "INSERT INTO stores VALUES('Haifa',2,'Mayu','0525462007')",
                "INSERT INTO stores VALUES('Herzliya',3,'Ron','0509987654')",
                "INSERT INTO stores VALUES('Tel Aviv',3,'Omri','0504567855')",
                "INSERT INTO stores VALUES('Jerusalem',4,'Rabbi Nahum','0505555555')",
                "INSERT INTO stores VALUES('Maale Adumim',4,'Rami','0587868554')",
                "INSERT INTO stores VALUES('Beer Sheva',5,'Amir','0544564838')",
                "INSERT INTO stores VALUES('Eshkolot',5,'Dvir','0505558850')",
        };
        for (String store : stores) {
            try {
                stmt = con.createStatement();
                stmt.executeUpdate(store);
            } catch (SQLException e) {
                System.out.println(store);

            }
        }
    }

    public void populateTruckModels() {
        String[] trucks = {"INSERT INTO truckModels SELECT 1,'3000000','7000000' WHERE NOT EXISTS (SELECT * FROM truckModels WHERE Model = 1);",
                "INSERT INTO truckModels SELECT 2,'5000000','10000000' WHERE NOT EXISTS (SELECT * FROM truckModels WHERE Model = 2);",
                "INSERT INTO truckModels SELECT 3,'7000000','13000000' WHERE NOT EXISTS (SELECT * FROM truckModels WHERE Model = 3);",
                "INSERT INTO truckModels SELECT 4,'8000000','15000000' WHERE NOT EXISTS (SELECT * FROM truckModels WHERE Model = 4);",
                "INSERT INTO truckModels SELECT 5,'10000000','18000000' WHERE NOT EXISTS (SELECT * FROM truckModels WHERE Model = 5);"
        };
        for (String truck : trucks) {
            try {
                stmt = con.createStatement();
                stmt.executeUpdate(truck);
            } catch (SQLException e) {
                System.out.println(truck);
                System.out.println(e.getMessage());


            }
        }
    }

    public void populateTrucks() {
        String[] trucks = {
                "INSERT INTO trucks VALUES ('111111110', 1)",
                "INSERT INTO trucks VALUES ('111111111', 1)",
                "INSERT INTO trucks VALUES ('222222220', 2)",
                "INSERT INTO trucks VALUES ('222222221', 2)",
                "INSERT INTO trucks VALUES ('333333330', 3)",
                "INSERT INTO trucks VALUES ('333333331', 3)",
                "INSERT INTO trucks VALUES ('444444440', 4)",
                "INSERT INTO trucks VALUES ('444444441', 4)",
                "INSERT INTO trucks VALUES ('555555550', 5)",
                "INSERT INTO trucks VALUES ('555555551', 5)"

        };
        for (String truck : trucks) {
            try {
                stmt = con.createStatement();
                stmt.executeUpdate(truck);
            } catch (SQLException e) {
                System.out.println(truck);
                System.out.println(e.getMessage());


            }
        }
    }

    public void populateShiftTypes() {
        try {
            stmt = con.createStatement();
            String CreationSqlSt = "INSERT INTO  ShiftsTypes SELECT 1 WHERE NOT EXISTS (SELECT * FROM ShiftsTypes WHERE ShiftType = 1);" +
                    "INSERT INTO  ShiftsTypes SELECT 2 WHERE NOT EXISTS (SELECT * FROM ShiftsTypes WHERE ShiftType = 2);";
            stmt.executeUpdate(CreationSqlSt);
        } catch (SQLException ignored) {
            System.out.println(ignored.getMessage());


        }
    }

    public void populateEmployee() {
        String url = "'http://www.workrights.co.il/%D7%AA%D7%A0%D7%90%D7%99_%D7%94%D7%A2%D7%A1%D7%A7%D7%94'";
        String[] employees = {"INSERT INTO  Employees SELECT '111111111','Mr.','Mime'        ,'ManPower Manager'      ,15000  ,'2017-01-01'   ,NULL ," + url + "  ,'General'      ,1,1,1   WHERE NOT EXISTS (SELECT * FROM Employees WHERE EmployeeID = '111111111');",
                "INSERT INTO  Employees SELECT '222222222','Pika','Chu'        ,'Logistic Manager'      ,17000  ,'2017-02-01'   ,NULL ," + url + "  ,'General'      ,1,2,2   WHERE NOT EXISTS (SELECT * FROM Employees WHERE EmployeeID = '222222222');",
                "INSERT INTO  Employees SELECT '333333333','Char','Mander'     ,'Administrative Manager',20000  ,'2017-02-07'   ,NULL ," + url + "  ,'General'      ,1,3,3   WHERE NOT EXISTS (SELECT * FROM Employees WHERE EmployeeID = '333333333');",
                "INSERT INTO  Employees SELECT '444444441','Tomb','Raider'     ,'Store Manager'         ,10000  ,'2017-03-14'   ,NULL ," + url + "  ,'Herzliya'     ,1,3,3   WHERE NOT EXISTS (SELECT * FROM Employees WHERE EmployeeID = '444444441');",
                "INSERT INTO  Employees SELECT '444444442','Lara','Croft'      ,'Store Manager'         ,10000  ,'2017-02-03'   ,NULL ," + url + "  ,'Tel Aviv'     ,1,3,3   WHERE NOT EXISTS (SELECT * FROM Employees WHERE EmployeeID = '444444442');",
                "INSERT INTO  Employees SELECT '444444443','Sandra','Bullock'  ,'Store Manager'         ,10000  ,'2017-02-23'   ,NULL ," + url + "  ,'Jerusalem'    ,1,3,3   WHERE NOT EXISTS (SELECT * FROM Employees WHERE EmployeeID = '444444443');",
                "INSERT INTO  Employees SELECT '555555551','Psy','Duck'        ,'Storekeeper'           ,7500   ,'2017-04-23'   ,NULL ," + url + "  ,'Herzliya'     ,1,3,3   WHERE NOT EXISTS (SELECT * FROM Employees WHERE EmployeeID = '555555551');",
                "INSERT INTO  Employees SELECT '555555552','Ay','Machsen'      ,'Storekeeper'           ,7500   ,'2017-04-21'   ,NULL ," + url + "  ,'Tel Aviv'     ,1,3,3   WHERE NOT EXISTS (SELECT * FROM Employees WHERE EmployeeID = '555555552');",
                "INSERT INTO  Employees SELECT '555555553','Milka','Oreo'      ,'Storekeeper'           ,7500   ,'2017-03-19'   ,NULL ," + url + "  ,'Jerusalem'    ,1,3,3   WHERE NOT EXISTS (SELECT * FROM Employees WHERE EmployeeID = '555555553');",
                "INSERT INTO  Employees SELECT '666666661','MoMo','Segal'      ,'Shift Manager'         ,7500   ,'2017-03-19'   ,NULL ," + url + "  ,'Herzliya'    ,1,3,3   WHERE NOT EXISTS (SELECT * FROM Employees WHERE EmployeeID = '666666661');",
                "INSERT INTO  Employees SELECT '666666662','Yehudale','Noy'    ,'Shift Manager'         ,7500   ,'2017-03-19'   ,NULL ," + url + "  ,'Tel Aviv'    ,1,3,3   WHERE NOT EXISTS (SELECT * FROM Employees WHERE EmployeeID = '666666662');",
                "INSERT INTO  Employees SELECT '666666663','Bentzi','Shmuelo'   ,'Shift Manager'         ,7500   ,'2017-03-19'   ,NULL ," + url + "  ,'Jerusalem'    ,1,3,3   WHERE NOT EXISTS (SELECT * FROM Employees WHERE EmployeeID = '666666663');",
                "INSERT INTO  Employees SELECT '777777771','Jason','Red'       ,'Driver'                ,7500   ,'2017-03-19'   ,NULL ," + url + "  ,'Herzliya'    ,1,3,3   WHERE NOT EXISTS (SELECT * FROM Employees WHERE EmployeeID = '777777771');",
                "INSERT INTO  Employees SELECT '777777711','Billy','Blue'      ,'Driver'                ,7500   ,'2017-03-19'   ,NULL ," + url + "  ,'Herzliya'    ,1,3,3   WHERE NOT EXISTS (SELECT * FROM Employees WHERE EmployeeID = '777777711');",
                "INSERT INTO  Employees SELECT '777777772','Tommy','White'     ,'Driver'                ,7500   ,'2017-03-19'   ,NULL ," + url + "  ,'Tel Aviv'    ,1,3,3   WHERE NOT EXISTS (SELECT * FROM Employees WHERE EmployeeID = '777777772');",
                "INSERT INTO  Employees SELECT '777777722','Zack','Black'      ,'Driver'                ,7500   ,'2017-03-19'   ,NULL ," + url + "  ,'Tel Aviv'    ,1,3,3   WHERE NOT EXISTS (SELECT * FROM Employees WHERE EmployeeID = '777777722');",
                "INSERT INTO  Employees SELECT '777777773','Kiberly','Pink'    ,'Driver'                ,7500   ,'2017-03-19'   ,NULL ," + url + "  ,'Jerusalem'    ,1,3,3   WHERE NOT EXISTS (SELECT * FROM Employees WHERE EmployeeID = '777777773');",
                "INSERT INTO  Employees SELECT '777777733','Trini','Yellow'    ,'Driver'                ,7500   ,'2017-03-19'   ,NULL ," + url + "  ,'Jerusalem'    ,1,3,3   WHERE NOT EXISTS (SELECT * FROM Employees WHERE EmployeeID = '777777733');",
                "INSERT INTO  Employees SELECT '888888881','Lily','Auldrain'   ,'Cashier'               ,7500   ,'2017-03-19'   ,NULL ," + url + "  ,'Herzliya'    ,1,3,3   WHERE NOT EXISTS (SELECT * FROM Employees WHERE EmployeeID = '888888881');",
                "INSERT INTO  Employees SELECT '888888882','Marshal','Ericson' ,'Cashier'               ,7500   ,'2017-03-19'   ,NULL ," + url + "  ,'Tel Aviv'    ,1,3,3   WHERE NOT EXISTS (SELECT * FROM Employees WHERE EmployeeID = '888888882');",
                "INSERT INTO  Employees SELECT '888888883','Ted','Moasby'      ,'Cashier'               ,7500   ,'2017-03-19'   ,NULL ," + url + "  ,'Jerusalem'    ,1,3,3   WHERE NOT EXISTS (SELECT * FROM Employees WHERE EmployeeID = '888888883');",

        };
        for (String employee : employees) {

            try {
                stmt = con.createStatement();
                stmt.executeUpdate(employee);
            } catch (SQLException ignored) {
                System.out.println(employee);
                System.out.println(ignored.getMessage());

            }
        }
    }

    public void populateDrivers() {
        String[] Drivers = {
                "INSERT INTO  drivers VALUES ('777777771'  ,5, 3);",
                "INSERT INTO  drivers VALUES ('777777711'   ,4, 3);",
                "INSERT INTO  drivers VALUES ('777777772'   ,5, 3);",
                "INSERT INTO  drivers VALUES ('777777722'   ,4, 3);",
                "INSERT INTO  drivers VALUES ('777777773'   ,5, 4);",
                "INSERT INTO  drivers VALUES ('777777733'   ,4, 4);"
        };
        for (String Driver : Drivers) {
            try {
                stmt = con.createStatement();
                stmt.executeUpdate(Driver);
            } catch (SQLException ignored) {
                System.out.println(Driver);
                System.out.println(ignored.getMessage());

            }
        }
    }


    public void populateSales() {
        String[] sales = {
                "INSERT INTO Sales(StartDate, EndDate, PercentOff) VALUES ('2017-06-01', '2017-12-12', '50');",
                "INSERT INTO Sales(StartDate, EndDate, PercentOff) VALUES ('2017-05-30', '2017-12-29', '10');",
        };
        for (String sale : sales) {
            try {
                stmt = con.createStatement();
                stmt.executeUpdate(sale);
            } catch (SQLException ignored) {
                System.out.println(sale);
                System.out.println(ignored.getMessage());


            }
        }
    }

    public void populateSuppliers() {
        String[] suppliers = {
                "INSERT INTO Suppliers VALUES ('1', 'Region 3 Days(1)',        'Herzel 30 Tel Aviv',        '9985542',          'Cash',     '" + SupplyTerms.SupplyOnSetDays + "', 'Sunday,Monday,Tuesday,Wendsday,Thusday,Friday,Saturday',      '3');",
                "INSERT INTO Suppliers VALUES ('2', 'Region 3 Order(1)',       'Habarzel 11 Tel Aviv',      '1234888',          'Check',    '" + SupplyTerms.OnOrder + "', NULL,                                                                  '3');",
                "INSERT INTO Suppliers VALUES ('3', 'Region 3 Order(2)',       'Pinkas 30 Herzeliya',       '556884',           'Credit',   '" + SupplyTerms.OnOrder + "', NULL,                                                                  '3');",
                "INSERT INTO Suppliers VALUES ('4', 'Region 4 Order(1)',       'Ha Kotel 12 Jerusalem',     '994888321',        'Cash',     '" + SupplyTerms.OnOrder + "', NULL,                                                                  '4');",
                "INSERT INTO Suppliers VALUES ('5', 'Region 4 Order(2)',       'Gates 100 Jerusalem',       '1237786658',       'Credit',   '" + SupplyTerms.OnOrder + "', NULL,                                                                  '4');"
        };
        for (String supplier : suppliers) {
            try {
                stmt = con.createStatement();
                stmt.executeUpdate(supplier);
            } catch (SQLException ignored) {
                System.out.println(supplier);
                System.out.println(ignored.getMessage());


            }
        }
    }

    public void populateCategories() {
        String[] categories = {
                "INSERT INTO Categories(CategoryName, FatherCategory) VALUES('Milk Products', NULL ); ",
                "INSERT INTO Categories(CategoryName, FatherCategory) VALUES('Yogurt', 1); ",
                "INSERT INTO Categories(CategoryName, FatherCategory) VALUES('Bread', NULL ); ",
                "INSERT INTO Categories(CategoryName, FatherCategory) VALUES('Drinks', NULL ); ",
                "INSERT INTO Categories(CategoryName, FatherCategory) VALUES('General', NULL ); ",
                "INSERT INTO Categories(CategoryName, FatherCategory) VALUES('Furniture', 5 ); "
        };
        for (String category : categories) {
            try {
                stmt = con.createStatement();
                stmt.executeUpdate(category);
            } catch (SQLException ignored) {
                System.out.println(category);
                System.out.println(ignored.getMessage());


            }
        }
    }

    public void populateContacts() {
        String[] Contacts = {
                "INSERT INTO Contacts VALUES ('Judy Williams', '0579956621');",
                "INSERT INTO Contacts VALUES ('Fred Jones', '0521238854');",
                "INSERT INTO Contacts VALUES ('Walter Hernandez', '0560112623');",
                "INSERT INTO Contacts VALUES ('Andrew Wright', '0542269555');",
                "INSERT INTO Contacts VALUES ('Chris Foster', '0536695542');",
                "INSERT INTO Contacts VALUES ('Norma Jenkins', '0525419735');",
                "INSERT INTO Contacts VALUES ('Lori Ramirez', '0529985842');"
        };
        for (String contact : Contacts) {
            try {
                stmt = con.createStatement();
                stmt.executeUpdate(contact);
            } catch (SQLException ignored) {
                System.out.println(contact);
                System.out.println(ignored.getMessage());


            }
        }
    }

    public void populateProducts() {
        String[] products = {
                "INSERT INTO Products VALUES ('1',  'Milk 1 Liter',      'Tnuva',           1,          '1000',         0, 20);",
                "INSERT INTO Products VALUES ('2',  'Milk 1 Liter',      'Terra',           1,          '1000',         0, 21);",
                "INSERT INTO Products VALUES ('3',  'Yogurt',            'Yoplet',          2,          '200',          0, 9);",
                "INSERT INTO Products VALUES ('4',  'Bagel',             'Ovens',           3,          '100',          0, 5);",
                "INSERT INTO Products VALUES ('5',  'Toast',             'Ovens',           3,          '150',          0, 7);",
                "INSERT INTO Products VALUES ('6',  'Coke 1 Liter',      'Coca Cola',       4,          '1000',         0, 15);",
                "INSERT INTO Products VALUES ('7',  'Coke 2 Liter',      'Coca Cola',       4,          '2000',         0, 22);",
                "INSERT INTO Products VALUES ('8',  'Water 2 Liter',     'Ein Gedi',        4,          '2000',         0, 15);",
                "INSERT INTO Products VALUES ('9',  'Study Chair',       'WoodStuff',       6,          '5000',         0, 200);",
                "INSERT INTO Products VALUES ('10', 'LivingRoom Chair',  'SteelStuff',      6,          '7000',         0, 250);",
        };
        for (String prodcut : products) {
            try {
                stmt = con.createStatement();
                stmt.executeUpdate(prodcut);
            } catch (SQLException ignored) {
                System.out.println(prodcut);
                System.out.println(ignored.getMessage());

            }
        }
    }

    public void populateContactsOfSuppliers() {
        String[] contacts = {
                "INSERT INTO Contacts_Of_Suppliers VALUES ('1', '0579956621')",
                "INSERT INTO Contacts_Of_Suppliers VALUES ('1', '0521238854')",
                "INSERT INTO Contacts_Of_Suppliers VALUES ('2', '0560112623')",
                "INSERT INTO Contacts_Of_Suppliers VALUES ('3', '0542269555')",
                "INSERT INTO Contacts_Of_Suppliers VALUES ('4', '0536695542')",
                "INSERT INTO Contacts_Of_Suppliers VALUES ('5', '0525419735')",
                "INSERT INTO Contacts_Of_Suppliers VALUES ('5', '0529985842')"
        };
        for (String contact : contacts) {
            try {
                stmt = con.createStatement();
                stmt.executeUpdate(contact);
            } catch (SQLException ignored) {
                System.out.println(contact);
                System.out.println(ignored.getMessage());

            }
        }
    }

    public void populateItemsOnSale() {
        String[] items = {
                "INSERT INTO ItemsOnSale VALUES (1, '1')",
                "INSERT INTO ItemsOnSale VALUES (1, '2')",
                "INSERT INTO ItemsOnSale VALUES (2, '9')",
                "INSERT INTO ItemsOnSale VALUES (2, '10')"
        };
        for (String item : items) {
            try {
                stmt = con.createStatement();
                stmt.executeUpdate(item);
            } catch (SQLException ignored) {
                System.out.println(item);
                System.out.println(ignored.getMessage());

            }
        }
    }


    public void populateRolesInShifts() {
        String[] Roles = {
                "INSERT INTO  RolesInShifts  VALUES ('Shift Manager',1,1,'Tavor')",
                "INSERT INTO  RolesInShifts  VALUES ('Shift Manager',2,1,'Tavor')",
                "INSERT INTO  RolesInShifts  VALUES ('Shift Manager',1,1,'Kiryat Shmona')",
                "INSERT INTO  RolesInShifts  VALUES ('Shift Manager',2,1,'Kiryat Shmona')",
                "INSERT INTO  RolesInShifts  VALUES ('Shift Manager',1,1,'Nahariyya')",
                "INSERT INTO  RolesInShifts  VALUES ('Shift Manager',2,1,'Nahariyya')",
                "INSERT INTO  RolesInShifts  VALUES ('Shift Manager',1,1,'Haifa')",
                "INSERT INTO  RolesInShifts  VALUES ('Shift Manager',2,1,'Haifa')",
                "INSERT INTO  RolesInShifts  VALUES ('Shift Manager',1,1,'Herzliya')",
                "INSERT INTO  RolesInShifts  VALUES ('Shift Manager',2,1,'Herzliya')",
                "INSERT INTO  RolesInShifts  VALUES ('Shift Manager',1,1,'Tel Aviv')",
                "INSERT INTO  RolesInShifts  VALUES ('Shift Manager',2,1,'Tel Aviv')",
                "INSERT INTO  RolesInShifts  VALUES ('Shift Manager',1,1,'Jerusalem')",
                "INSERT INTO  RolesInShifts  VALUES ('Shift Manager',2,1,'Jerusalem')",
                "INSERT INTO  RolesInShifts  VALUES ('Shift Manager',1,1,'Maale Adumim')",
                "INSERT INTO  RolesInShifts  VALUES ('Shift Manager',2,1,'Maale Adumim')",
                "INSERT INTO  RolesInShifts  VALUES ('Shift Manager',1,1,'Beer Sheva')",
                "INSERT INTO  RolesInShifts  VALUES ('Shift Manager',2,1,'Beer Sheva')",
                "INSERT INTO  RolesInShifts  VALUES ('Shift Manager',1,1,'Eshkolot')",
                "INSERT INTO  RolesInShifts  VALUES ('Shift Manager',2,1,'Eshkolot')",
                "INSERT INTO  RolesInShifts  VALUES ('Storekeeper',1,1,'Tel Aviv')",
                "INSERT INTO  RolesInShifts  VALUES ('Storekeeper',1,1,'Herzliya')",
                "INSERT INTO  RolesInShifts  VALUES ('Storekeeper',1,1,'Jerusalem')"
        };
        for(String Role : Roles) {
            try {
                stmt = con.createStatement();
                stmt.executeUpdate(Role);
            } catch (SQLException ignored) {
                System.out.println(Roles);
                System.out.println(ignored.getMessage());

            }
        }
    }


    public void populateInventory() {
        String[] items = {
                "INSERT INTO Inventory VALUES ('1',     500,  'R1','B1', 300,   700,    10,0,   'Tel Aviv');",
                "INSERT INTO Inventory VALUES ('2',     500,  'R1','B1', 300,   700,    0,0,    'Tel Aviv');",
                "INSERT INTO Inventory VALUES ('3',     240,  'R1','B1', 120,   300,    1,0,    'Tel Aviv');",
                "INSERT INTO Inventory VALUES ('4',     200,  'R2','B2', 200,   0,      0,0,    'Tel Aviv');",
                "INSERT INTO Inventory VALUES ('5',     200,  'R2','B2', 200,   0,      3,0,    'Tel Aviv');",
                "INSERT INTO Inventory VALUES ('6',     300,  'R3','B3', 300,   300,    0,9,    'Tel Aviv');",
                "INSERT INTO Inventory VALUES ('7',     350,  'R3','B3', 300,   350,    0,0,    'Tel Aviv');",
                "INSERT INTO Inventory VALUES ('8',     1000, 'R3','B3', 700,   700,    0,7,    'Tel Aviv');",
                "INSERT INTO Inventory VALUES ('9',     50,   'R4','B4', 3,     90,     1,0,    'Tel Aviv');",
                "INSERT INTO Inventory VALUES ('10',    25,   'R4','B4', 6,     30,     0,0,    'Tel Aviv');",
                "INSERT INTO Inventory VALUES ('1',     250,  'R1','B1', 300,   650,    0,0,    'Herzliya');",
                "INSERT INTO Inventory VALUES ('2',     250,  'R1','B1', 320,   200,    0,1,    'Herzliya');",
                "INSERT INTO Inventory VALUES ('3',     200,  'R1','B1', 120,   100,    2,0,    'Herzliya');",
                "INSERT INTO Inventory VALUES ('4',     300,  'R2','B2', 200,   150,    0,4,    'Herzliya');",
                "INSERT INTO Inventory VALUES ('5',     320,  'R2','B2', 200,   200,    1,0,    'Herzliya');",
                "INSERT INTO Inventory VALUES ('6',     400,  'R3','B3', 300,   450,    0,9,    'Herzliya');",
                "INSERT INTO Inventory VALUES ('7',     400,  'R3','B3', 300,   350,    7,0,    'Herzliya');",
                "INSERT INTO Inventory VALUES ('8',     400,  'R3','B3', 700,   50,     0,5,    'Herzliya');",
                "INSERT INTO Inventory VALUES ('9',     10,   'R4','B4', 3,     90,     0,0,    'Herzliya');",
                "INSERT INTO Inventory VALUES ('10',    10,   'R4','B4', 6,     30,     0,0,    'Herzliya');",
                "INSERT INTO Inventory VALUES ('1',     350,  'R1','B1', 200,   300,    0,0,    'Jerusalem');",
                "INSERT INTO Inventory VALUES ('2',     400,  'R1','B1', 200,   250,    0,1,    'Jerusalem');",
                "INSERT INTO Inventory VALUES ('3',     230,  'R1','B1', 120,   100,    2,0,    'Jerusalem');",
                "INSERT INTO Inventory VALUES ('4',     250,  'R2','B2', 200,   150,    0,4,    'Jerusalem');",
                "INSERT INTO Inventory VALUES ('5',     270,  'R2','B2', 300,   50,     1,0,    'Jerusalem');",
                "INSERT INTO Inventory VALUES ('6',     350,  'R3','B3', 300,   200,    0,9,    'Jerusalem');",
                "INSERT INTO Inventory VALUES ('7',     375,  'R3','B3', 150,   200,    7,0,    'Jerusalem');",
                "INSERT INTO Inventory VALUES ('8',     550,  'R3','B3', 500,   150,    0,5,    'Jerusalem');",
                "INSERT INTO Inventory VALUES ('9',     10,   'R4','B4', 5,     25,     0,0,    'Jerusalem');",
                "INSERT INTO Inventory VALUES ('10',    10,   'R4','B4', 5,     30,     0,0,    'Jerusalem');"
        };
        for (String item : items) {
            try {
                stmt = con.createStatement();
                stmt.executeUpdate(item);
            } catch (SQLException ignored) {
                System.out.println(item);
                System.out.println(ignored.getMessage());}}}

    public void populateShiftInDates() {
        String[] shiftInDates = {
                "INSERT INTO  ShiftsInDates  VALUES ('2017-11-17',1,'Tel Aviv');",                  //shifts for watch shift
                "INSERT INTO  ShiftsInDates  VALUES ('2017-11-17',2,'Tel Aviv');",
                "INSERT INTO  ShiftsInDates  VALUES ('2017-11-17',1,'Herzliya');",
                "INSERT INTO  ShiftsInDates  VALUES ('2017-11-17',2,'Herzliya');",
                "INSERT INTO  ShiftsInDates  VALUES ('2017-11-17',1,'Jerusalem');",
                "INSERT INTO  ShiftsInDates  VALUES ('2017-11-17',2,'Jerusalem');",

                "INSERT INTO  ShiftsInDates  VALUES ('2017-12-12',1,'Jerusalem');",                //shifts for constraints
                "INSERT INTO  ShiftsInDates  VALUES ('2017-12-12',2,'Jerusalem');",

                "INSERT INTO  ShiftsInDates  VALUES ('2017-06-10',1,'Herzliya');",      //shifts for
                "INSERT INTO  ShiftsInDates  VALUES ('2017-06-10',1,'Tel Aviv');",
                "INSERT INTO  ShiftsInDates  VALUES ('2017-06-10',1,'Jerusalem');",
        };
        for(String shift : shiftInDates ) {
            try {
                stmt = con.createStatement();
                stmt.executeUpdate(shift);
            } catch (SQLException ignored) {
                ignored.printStackTrace();

            }
        }
    }


    public void populateItemsInContract() {
        String[] items = {
                "INSERT INTO Items_In_Contract VALUES ('1','111', '1', 10);", //region 3 periodic
                "INSERT INTO Items_In_Contract VALUES ('1','112', '2', 11);",


                "INSERT INTO Items_In_Contract VALUES ('2','211', '1', 9);", //region 3 on order
                "INSERT INTO Items_In_Contract VALUES ('2','212', '2', 9);",
                "INSERT INTO Items_In_Contract VALUES ('2','213', '3', 5);",
                "INSERT INTO Items_In_Contract VALUES ('2','214', '4', 2);",
                "INSERT INTO Items_In_Contract VALUES ('2','217', '7', 7.5);",
                "INSERT INTO Items_In_Contract VALUES ('2','218', '8', 2.5);",
                "INSERT INTO Items_In_Contract VALUES ('2','219', '9', 87.5);",
                "INSERT INTO Items_In_Contract VALUES ('2','220', '10', 100.5);",


                "INSERT INTO Items_In_Contract VALUES ('3','311', '1', 9.5);",//region 3 on order
                "INSERT INTO Items_In_Contract VALUES ('3','312', '2', 10);",
                "INSERT INTO Items_In_Contract VALUES ('3','314', '4', 2);",
                "INSERT INTO Items_In_Contract VALUES ('3','315', '5', 3.3);",
                "INSERT INTO Items_In_Contract VALUES ('3','316', '6', 5.75);",
                "INSERT INTO Items_In_Contract VALUES ('3','317', '7', 7.5);",
                "INSERT INTO Items_In_Contract VALUES ('3','318', '8', 2);",
                "INSERT INTO Items_In_Contract VALUES ('3','319', '9', 95.5);",


                "INSERT INTO Items_In_Contract VALUES ('4','411', '1', 10);",//region 4 on order
                "INSERT INTO Items_In_Contract VALUES ('4','412', '2', 11);",
                "INSERT INTO Items_In_Contract VALUES ('4','413', '3', 3.5);",
                "INSERT INTO Items_In_Contract VALUES ('4','414', '4', 1);",
                "INSERT INTO Items_In_Contract VALUES ('4','415', '5', 2.5);",
                "INSERT INTO Items_In_Contract VALUES ('4','416', '6', 5.5);",
                "INSERT INTO Items_In_Contract VALUES ('4','417', '7', 7);",
                "INSERT INTO Items_In_Contract VALUES ('4','418', '8', 2.5);",
                "INSERT INTO Items_In_Contract VALUES ('4','419', '9', 95.5);",
                "INSERT INTO Items_In_Contract VALUES ('4','420', '10', 105.5);",


                "INSERT INTO Items_In_Contract VALUES ('5','511', '1', 10);",//region 4 on order
                "INSERT INTO Items_In_Contract VALUES ('5','512', '2', 11);",
                "INSERT INTO Items_In_Contract VALUES ('5','513', '3', 4);",
                "INSERT INTO Items_In_Contract VALUES ('5','514', '4', 1.5);",
                "INSERT INTO Items_In_Contract VALUES ('5','515', '5', 3.5);",
                "INSERT INTO Items_In_Contract VALUES ('5','516', '6', 4.75);",
                "INSERT INTO Items_In_Contract VALUES ('5','517', '7', 5);",
                "INSERT INTO Items_In_Contract VALUES ('5','518', '8', 2.75);",
                "INSERT INTO Items_In_Contract VALUES ('5','519', '9', 93.5);",
                "INSERT INTO Items_In_Contract VALUES ('5','520', '10', 105.5);"
        };
        for (String item : items) {
            try {
                stmt = con.createStatement();
                stmt.executeUpdate(item);
            } catch (SQLException ignored) {
                System.out.println(item);
                System.out.println(ignored.getMessage());}}}


    public void populateEmployeeConstraints() {
        String[] shiftInDates = {
                "INSERT INTO  EmployeesConstraints VALUES ('888888883','2017-12-12',1,'Jerusalem','I have a wedding')",
                "INSERT INTO  EmployeesConstraints VALUES ('444444443','2017-12-12',2,'Jerusalem','I have a Date with shlomi')",
                "INSERT INTO  EmployeesConstraints VALUES ('777777773','2017-12-12',1,'Jerusalem','I need to fight rita')",

        };
        for(String shift : shiftInDates ) {
            try {
                stmt = con.createStatement();
                stmt.executeUpdate(shift);
            } catch (SQLException ignored) {
                ignored.printStackTrace();
            }
        }
    }


    public void populateStoresOfItemsInContract() {
        String[] stores = {
                "INSERT INTO StoresOfItemsInContract VALUES ('1', '111', 'Herzliya', 30);",
                "INSERT INTO StoresOfItemsInContract VALUES ('1', '112', 'Herzliya', 15);",
                "INSERT INTO StoresOfItemsInContract VALUES ('1', '111', 'Tel Aviv', 55);",
                "INSERT INTO StoresOfItemsInContract VALUES ('1', '112', 'Tel Aviv', 9);"
        };
        for (String store : stores) {
            try {
                stmt = con.createStatement();
                stmt.executeUpdate(store);
            } catch (SQLException ignored) {
                ignored.printStackTrace();

            }}}


    public void populateEmployeeInShift() {
        String[] shiftInDates = {
                "INSERT INTO  EmployeesInShifts VALUES ('2017-06-10',1,'Herzliya','666666661','Shift Manager')",
                "INSERT INTO  EmployeesInShifts VALUES ('2017-06-10',1,'Herzliya','555555551','Storekeeper')",
                "INSERT INTO  EmployeesInShifts VALUES ('2017-06-10',1,'Tel Aviv','666666662','Shift Manager')",
                "INSERT INTO  EmployeesInShifts VALUES ('2017-06-10',1,'Tel Aviv','555555552','Storekeeper')",
                "INSERT INTO  EmployeesInShifts VALUES ('2017-06-10',1,'Jerusalem','666666663','Shift Manager')",
                "INSERT INTO  EmployeesInShifts VALUES ('2017-06-10',1,'Jerusalem','555555553','Storekeeper')"
        };
        for(String shift : shiftInDates ) {
            try {
                stmt = con.createStatement();
                stmt.executeUpdate(shift);
            } catch (SQLException ignored) {
                ignored.printStackTrace();
            }
        }
    }


    public void populateDiscount() {
        String[] discounts = {
                "INSERT INTO Discounts VALUES ('2', '214',      123,    0.1);",
                "INSERT INTO Discounts VALUES ('2', '217',      250,    0.15);",
                "INSERT INTO Discounts VALUES ('3', '311',      80,     0.2);",
                "INSERT INTO Discounts VALUES ('3', '312',      200,    0.18);",
                "INSERT INTO Discounts VALUES ('4', '413',      130,    0.12);",
                "INSERT INTO Discounts VALUES ('4', '419',      100,    0.09);",
                "INSERT INTO Discounts VALUES ('5', '518',      150,    0.15);",
                "INSERT INTO Discounts VALUES ('5', '514',      300,    0.25);",
        };
        for (String discount : discounts) {
            try {
                stmt = con.createStatement();
                stmt.executeUpdate(discount);
            } catch (SQLException ignored) {
                System.out.println(discount);
                System.out.println(ignored.getMessage());

            }
        }
    }


}
